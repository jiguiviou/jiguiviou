﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMINTCONVERTER_H
#define GENICAMINTCONVERTER_H

#include "genicamformula.h"
#include "internal.h"

#include <variant>

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Genicam2 {

class Inode;

class IntConverter final : public IInteger, public IInvalidate {
    const tinyxml2::XMLElement* const _xmlElement;
    Formula _formulaTo;
    Formula _formulaFrom;
    pValue _pValue;
    std::vector<std::function<void(void)>> _invalideFunctions;

public:
    explicit IntConverter(const tinyxml2::XMLElement* element);
    ~IntConverter() = default;
    IntConverter(const IntConverter&) = delete;
    IntConverter(IntConverter&&) = delete;
    auto operator=(const IntConverter&) -> IntConverter& = delete;
    auto operator=(IntConverter &&) -> IntConverter& = delete;

    [[nodiscard]] auto mapInterfaces(const InodeGetter& inodeGetter) noexcept -> Inode*;
    void addInvalidateFunction(std::function<void(void)>&& function) noexcept override;

    auto value() noexcept -> int64_t override;
    void setValue(int64_t value) noexcept override;
    auto min() noexcept -> int64_t override;
    auto max() noexcept -> int64_t override;
    auto inc() noexcept -> int64_t override;
};

} // namespace Jgv::Genicam2

#endif // GENICAMINTCONVERTER_H
