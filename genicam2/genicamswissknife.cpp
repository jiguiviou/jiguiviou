/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamswissknife.h"

#include "genicaminode.h"
#include "internal.h"

#include <iostream>
#include <limits>
#include <tinyxml2.h>

namespace Jgv::Genicam2 {

SwissKnife::SwissKnife(const tinyxml2::XMLElement* element)
    : _formula(element)
{
}

void SwissKnife::mapInterfaces(const InodeGetter& inodeGetter) noexcept
{
    _formula.mapInterfaces(inodeGetter, XmlNode::Formula);
}

auto SwissKnife::value() -> double
{
    const auto res { _formula.evaluate() };
    if (const auto pVal { std::get_if<double>(&res) }) {
        return *pVal;
    }
    return static_cast<double>(std::get<int64_t>(res));
}

void SwissKnife::setValue(double /*value*/)
{
    std::cerr << "SwissKnife::setValue makes no sense !" << std::endl;
}

auto SwissKnife::min() -> double
{
    return std::numeric_limits<double>::lowest();
}

auto SwissKnife::max() -> double
{
    return std::numeric_limits<double>::max();
}

auto SwissKnife::inc() -> double
{
    return 0.;
}

} // namespace Jgv::Genicam2
