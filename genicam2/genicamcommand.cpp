﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamcommand.h"

#include "genicaminode.h"
#include "internal.h"

#include <iostream>
#include <tinyxml2.h>

namespace Jgv::Genicam2 {

Command::Command(const tinyxml2::XMLElement* element)
    : _xmlElement { element }
{
    if (const auto child { element->FirstChildElement(XmlNode::CommandValue.data()) }; child != nullptr) {
        _commandValue.initValue(toValue<int64_t>(child->GetText()));
    }
}

auto Command::mapInterfaces(const InodeGetter& inodeGetter) noexcept -> Inode*
{
    if (auto child { _xmlElement->FirstChildElement(XmlNode::pCommandValue.data()) }; child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _commandValue.setInterface(inode->IntegerInterface());
        }
    }

    if (const auto child { _xmlElement->FirstChildElement(XmlNode::pValue.data()) }; child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _pValue = inode->IntegerInterface();
            return inode;
        }
    }
    return nullptr;
}

void Command::addInvalidateFunction(std::function<void(void)>&& function) noexcept
{
    _invalideFunctions.emplace_back(std::move(function));
}

void Command::execute() noexcept
{
    if (_pValue != nullptr) {
        _pValue->setValue(_commandValue.value());
        std::for_each(_invalideFunctions.cbegin(), _invalideFunctions.cend(), [](const auto& invalidate) {
            invalidate();
        });
    }
}

} // namespace Jgv::Genicam2
