﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamfloat.h"

#include "genicaminode.h"
#include "internal.h"

#include <iostream>
#include <tinyxml2.h>

namespace Jgv::Genicam2 {

Float::Float(const tinyxml2::XMLElement* element)
    : _xmlElement { element }
{
    if (const auto *const child { _xmlElement->FirstChildElement(XmlNode::Value.data()) }; child != nullptr) {
        _value.initValue(toValue<double>(child->GetText()));
    }

    if (const auto *const child { _xmlElement->FirstChildElement(XmlNode::Min.data()) }; child != nullptr) {
        _min.initValue(toValue<double>(child->GetText()));
    }

    if (const auto *const child { _xmlElement->FirstChildElement(XmlNode::Max.data()) }; child != nullptr) {
        _max.initValue(toValue<double>(child->GetText()));
    }

    if (const auto *const child { _xmlElement->FirstChildElement(XmlNode::Inc.data()) }; child != nullptr) {
        _inc.initValue(toValue<double>(child->GetText()));
    }
}

auto Float::mapInterfaces(const InodeGetter& inodeGetter) noexcept -> Inode*
{

    if (const auto *const child { _xmlElement->FirstChildElement(XmlNode::pMin.data()) }; child != nullptr) {
        if (auto *const inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _min.setInterface(inode->FloatInterface());
        }
    }

    if (const auto *const child { _xmlElement->FirstChildElement(XmlNode::pMax.data()) }; child != nullptr) {
        if (auto *const inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _max.setInterface(inode->FloatInterface());
        }
    }

    if (const auto *const child { _xmlElement->FirstChildElement(XmlNode::pInc.data()) }; child != nullptr) {
        if (auto *const inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _inc.setInterface(inode->FloatInterface());
        }
    }

    if (const auto *const child { _xmlElement->FirstChildElement(XmlNode::pValue.data()) }; child != nullptr) {
        if (auto *const inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _value.setInterface(inode->FloatInterface());
            return inode;
        }
    }

    return nullptr;
}

void Float::addInvalidateFunction(std::function<void(void)>&& function) noexcept
{
    _invalideFunctions.emplace_back(std::move(function));
}

auto Float::value() -> double
{
    return _value.value();
}

void Float::setValue(double value)
{
    _value.setValue(value);
    std::for_each(_invalideFunctions.cbegin(), _invalideFunctions.cend(), [](const auto& invalidate) {
        invalidate();
    });
}

auto Float::min() -> double
{
    return _min.value();
}

auto Float::max() -> double
{
    return _max.value();
}

auto Float::inc() -> double
{
    return _inc.value();
}

} // namespace Jgv::Genicam2
