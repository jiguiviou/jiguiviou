/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAM_H
#define GENICAM_H

#include <string>

namespace Jgv::Genicam {

namespace Dictionary::Feature {
    constexpr std::string_view AcquisitionStart { "AcquisitionStart" };
    constexpr std::string_view AcquisitionStop { "AcquisitionStop" };
    constexpr std::string_view AcquisitionFrameRate { "AcquisitionFrameRate" };
    constexpr std::string_view AcquisitionFrameRateAbs { "AcquisitionFrameRateAbs" };
    constexpr std::string_view PayloadSize { "PayloadSize" };
    constexpr std::string_view PixelFormat { "PixelFormat" };
    constexpr std::string_view Width { "Width" };
    constexpr std::string_view Height { "Height" };
    constexpr std::string_view OffsetX { "OffsetX" };
    constexpr std::string_view OffsetY { "OffsetY" };
    constexpr std::string_view ExposureAuto { "ExposureAuto" };
    constexpr std::string_view ExposureTimeAbs { "ExposureTimeAbs" };
    constexpr std::string_view ExposureAutoMax { "ExposureAutoMax" };
    constexpr std::string_view GainAuto { "GainAuto" };
    constexpr std::string_view Gain { "Gain" };
    constexpr std::string_view TriggerSource { "TriggerSource" };
    constexpr std::string_view EventSelector { "EventSelector" };
    constexpr std::string_view EventNotification { "EventNotification" };
}

namespace Dictionary::ExposureAuto {
    constexpr std::string_view Off { "Off" };
    constexpr std::string_view Once { "Once" };
    constexpr std::string_view Continuous { "Continuous" };
}

namespace Dictionary::GainAuto {
    constexpr std::string_view Off { Dictionary::ExposureAuto::Off };
    constexpr std::string_view Once { Dictionary::ExposureAuto::Once };
    constexpr std::string_view Continuous { Dictionary::ExposureAuto::Continuous };
}

namespace Dictionary::EventNotification {
    constexpr std::string_view On { "On" };
    constexpr std::string_view Off { Dictionary::ExposureAuto::Off };
}

} // namespace Jgv::GenICam

#endif // GENICAM_H
