/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamtokensstack.h"

#include "genicamtoken.h"

#include <cmath>
#include <iostream>
#include <span>

namespace {

constexpr std::size_t FIRST { 0U };
constexpr std::size_t SECOND { 1U };
constexpr std::size_t THIRD { 2U };

constexpr auto addition(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) + (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) + static_cast<double>(*first);
        }
    }
    if (auto first = std::get_if<double>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return static_cast<double>(*second) + (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) + (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: addition failure." << std::endl;
    return INT64_C(0);
}

constexpr auto substraction(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) - (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) - static_cast<double>(*first);
        }
    }
    if (auto first = std::get_if<double>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return static_cast<double>(*second) - (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) - (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: substraction failure." << std::endl;
    return INT64_C(0);
}

constexpr auto multiplication(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) * (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) * static_cast<double>(*first);
        }
    }
    if (auto first = std::get_if<double>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return static_cast<double>(*second) * (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) * (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: multiplication failure." << std::endl;
    return INT64_C(0);
}

auto division(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (*first != 0) {
            if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
                // division euclidienne, si il y a un reste, on passe en division vraie
                auto [quot, rem] { std::div(*second, *first) };
                if (rem == 0) {
                    return quot;
                }
                return static_cast<double>(*second) / static_cast<double>(*first);
            }
            if (auto second = std::get_if<double>(&operands[SECOND])) {
                return (*second) / static_cast<double>(*first);
            }
        }
    } else if (auto first = std::get_if<double>(&operands[FIRST])) {
        if (*first != 0.) {
            if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
                return static_cast<double>(*second) / (*first);
            }
            if (auto second = std::get_if<double>(&operands[SECOND])) {
                return (*second) / (*first);
            }
        }
    }
    std::cerr << "Genicam2::TokenStack: division failure." << std::endl;
    return INT64_C(0);
}

constexpr auto remainder(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (*first != 0) {
            if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
                return (*second) % (*first);
            }
        }
    }
    std::cerr << "Genicam2::TokenStack: remainder failure." << std::endl;
    return INT64_C(0);
}

constexpr auto power(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return std::pow(*second, *first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return std::pow(*second, *first);
        }

    } else if (auto first = std::get_if<double>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return std::pow(*second, *first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return std::pow(*second, *first);
        }
    }
    std::cerr << "Genicam2::TokenStack: power failure." << std::endl;
    return INT64_C(0);
}

constexpr auto bitwiseAnd(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) & (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: bitwise And failure." << std::endl;
    return INT64_C(0);
}

constexpr auto bitwiseOr(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) | (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: bitwise Or failure." << std::endl;
    return INT64_C(0);
}

constexpr auto bitwiseXor(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) ^ (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: bitwise Xor failure." << std::endl;
    return INT64_C(0);
}

constexpr auto bitwiseNot(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        return ~(*first);
    }
    std::cerr << "Genicam2::TokenStack: bitwise Not failure." << std::endl;
    return INT64_C(0);
}

constexpr auto logicalNotEqual(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) != (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) != static_cast<double>(*first);
        }
    }
    if (auto first = std::get_if<double>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return static_cast<double>(*second) != (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) != (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: logical Not equal failure." << std::endl;
    return false;
}

constexpr auto logicalEqual(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) == (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) == static_cast<double>(*first);
        }
    }
    if (auto first = std::get_if<double>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return static_cast<double>(*second) == (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) == (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: logical Equal failure." << std::endl;
    return false;
}

constexpr auto logicalGreater(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) > (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) > static_cast<double>(*first);
        }
    }
    if (auto first = std::get_if<double>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return static_cast<double>(*second) > (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) > (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: logical Greater failure." << std::endl;
    return false;
}

constexpr auto logicalLess(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) < (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) < static_cast<double>(*first);
        }
    }
    if (auto first = std::get_if<double>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return static_cast<double>(*second) < (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) < (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: logical Greater failure." << std::endl;
    return false;
}

constexpr auto logicalGreaterOrEqual(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) >= (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) >= static_cast<double>(*first);
        }
    }
    if (auto first = std::get_if<double>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return static_cast<double>(*second) >= (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) >= (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: logical Greater or Equal failure." << std::endl;
    return false;
}

constexpr auto logicalLessOrEqual(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) <= (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) <= static_cast<double>(*first);
        }
    }
    if (auto first = std::get_if<double>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return static_cast<double>(*second) <= (*first);
        }
        if (auto second = std::get_if<double>(&operands[SECOND])) {
            return (*second) <= (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: logical Less or Equal failure." << std::endl;
    return false;
}

constexpr auto logicalAnd(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<bool>(&operands[FIRST])) {
        if (auto second = std::get_if<bool>(&operands[SECOND])) {
            return (*second) && (*first);
        }
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second != 0) && (*first);
        }
    }

    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<bool>(&operands[SECOND])) {
            return (*second) && (*first != 0);
        }
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second != 0) && (*first != 0);
        }
    }
    std::cerr << "Genicam2::TokenStack: logical And failure." << std::endl;
    return false;
}

constexpr auto logicalOr(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<bool>(&operands[FIRST])) {
        if (auto second = std::get_if<bool>(&operands[SECOND])) {
            return (*second) || (*first);
        }
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second != 0) || (*first);
        }
    }

    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<bool>(&operands[SECOND])) {
            return (*second) || (*first != 0);
        }
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second != 0) || (*first != 0);
        }
    }

    std::cerr << "Genicam2::TokenStack: logical Or failure." << std::endl;
    return false;
}

constexpr auto shiftLeft(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) << (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: shift Left failure." << std::endl;
    return false;
}

constexpr auto shiftRight(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto first = std::get_if<int64_t>(&operands[FIRST])) {
        if (auto second = std::get_if<int64_t>(&operands[SECOND])) {
            return (*second) >> (*first);
        }
    }
    std::cerr << "Genicam2::TokenStack: shift Right failure." << std::endl;
    return false;
}

constexpr auto ternaryQuestionMark(std::span<Jgv::Genicam2::Operand> operands) -> Jgv::Genicam2::Operand
{
    if (auto third = std::get_if<bool>(&operands[THIRD])) {
        return (*third) ? operands[SECOND] : operands[FIRST];
    }
    std::cerr << "Genicam2::TokenStack: ternary question mark failure." << std::endl;
    return false;
}

} // namespace

namespace Jgv::Genicam2 {

void TokensStack::push(int64_t value) noexcept
{
    _stack.push(value);
}

void TokensStack::push(double value) noexcept
{
    _stack.push(value);
}

void TokensStack::push(const Token& token) noexcept
{
    const auto args { token.operandsCount() };
    if (_stack.size() < args) {
        std::cerr << "GenICam2::TokensStack::push: failed, stack size error !" << std::endl;
        return;
    }

    Operand shortEval;

    if (args >= 1) {
        operands[FIRST] = _stack.top();
        _stack.pop();
    }
    if (args >= 2) {
        operands[SECOND] = _stack.top();
        _stack.pop();
    }
    if (args == 3) {
        operands[THIRD] = _stack.top();
        _stack.pop();
    }

    switch (token.type()) {
    case TokenType::ADDITION:
        shortEval = addition(operands);
        break;
    case TokenType::SUBSTRACTION:
        shortEval = substraction(operands);
        break;
    case TokenType::MULTIPLICATION:
        shortEval = multiplication(operands);
        break;
    case TokenType::DIVISION:
        shortEval = division(operands);
        break;
    case TokenType::REMAINDER:
        shortEval = remainder(operands);
        break;
    case TokenType::POWER:
        shortEval = power(operands);
        break;
    case TokenType::BITWISE_AND:
        shortEval = bitwiseAnd(operands);
        break;
    case TokenType::BITWISE_OR:
        shortEval = bitwiseOr(operands);
        break;
    case TokenType::BITWISE_XOR:
        shortEval = bitwiseXor(operands);
        break;
    case TokenType::BITWISE_NOT:
        shortEval = bitwiseNot(operands);
        break;
    case TokenType::LOGICAL_NOT_EQUAL:
        shortEval = logicalNotEqual(operands);
        break;
    case TokenType::LOGICAL_EQUAL:
        shortEval = logicalEqual(operands);
        break;
    case TokenType::LOGICAL_GREATER:
        shortEval = logicalGreater(operands);
        break;
    case TokenType::LOGICAL_LESS:
        shortEval = logicalLess(operands);
        break;
    case TokenType::LOGICAL_GREATER_OR_EQUAL:
        shortEval = logicalGreaterOrEqual(operands);
        break;
    case TokenType::LOGICAL_LESS_OR_EQUAL:
        shortEval = logicalLessOrEqual(operands);
        break;
    case TokenType::LOGICAL_AND:
        shortEval = logicalAnd(operands);
        break;
    case TokenType::LOGICAL_OR:
        shortEval = logicalOr(operands);
        break;
    case TokenType::SHIFT_LEFT:
        shortEval = shiftLeft(operands);
        break;
    case TokenType::SHIFT_RIGHT:
        shortEval = shiftRight(operands);
        break;
    case TokenType::TERNARY_QUESTION_MARK:
        shortEval = ternaryQuestionMark(operands);
        break;
    case TokenType::TERNARY_COLON:
        shortEval = operands[FIRST];
        break;
    default:
        std::cerr << "GenICam2::TokensStackInteger::push: token not evaluated " << toString(token.type()) << std::endl;
    }
    _stack.push(shortEval);
}

auto TokensStack::value() noexcept -> std::variant<int64_t, double>
{
    if (_stack.size() == 1) {
        auto value { _stack.top() };
        if (auto pVal = std::get_if<int64_t>(&value)) {
            return *pVal;
        }
        if (auto pVal = std::get_if<double>(&value)) {
            return *pVal;
        }
        if (auto pVal = std::get_if<bool>(&value)) {
            return (*pVal) ? INT64_C(1) : INT64_C(0);
        }
        std::cerr << "GenICam2::TokensStack result type error." << std::endl;
    }

    std::cerr << "GenICam2::TokensStack::value result stack size != 1 : " << _stack.size() << std::endl;
    return INT64_C(0);
}

} // namespace Jgv::Genicam2
