/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GENICAMFORMULA_H
#define GENICAMFORMULA_H

#include "genicamtoken.h"
#include "internal.h"

#include <list>
#include <variant>

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Genicam2 {

class FormulaPrivate;
class Formula final {
    const tinyxml2::XMLElement* const _xmlElement;
    struct ExternVar {
    };
    using IntegerGetter = std::function<int64_t(void)>;
    using FloatGetter = std::function<double(void)>;
    std::list<std::variant<Token, IntegerGetter, FloatGetter, ExternVar, int64_t, double>> _out;

public:
    explicit Formula(const tinyxml2::XMLElement* element);
    void mapInterfaces(const InodeGetter& inodeGetter, std::string_view xmlNode) noexcept;

    [[nodiscard]] auto evaluate() const noexcept -> std::variant<int64_t, double>;
    [[nodiscard]] auto evaluate(std::variant<int64_t, double> externVar) const noexcept -> std::variant<int64_t, double>;

}; // class Formula

} // namespace Jgv

#endif // GENICAMFORMULA_H
