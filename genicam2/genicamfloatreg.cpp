/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamfloatreg.h"

#include "genicaminode.h"
#include "internal.h"

#include <gvcpdevices/gvcpclient.h>
#include <iostream>
#include <tinyxml2.h>
#include <utility>

namespace {

template <typename T>
void toUnaligned(const T src, void* dest) noexcept
{
    __builtin_memcpy(dest, &src, sizeof(T));
}

template <typename T>
void fromUnaligned(const void* src, void* dest) noexcept
{
    __builtin_memcpy(dest, src, sizeof(T));
}

void littleEndian32ToCache(float value, std::span<uint8_t> cache) noexcept
{
    uint32_t temp { 0U };
    toUnaligned<float>(value, &temp);
    toUnaligned<uint32_t>(__builtin_bswap32(temp), cache.data());
}

void littleEndian64ToCache(double value, std::span<uint8_t> cache) noexcept
{
    uint64_t temp { 0U };
    toUnaligned<double>(value, &temp);
    toUnaligned<uint64_t>(__builtin_bswap64(temp), cache.data());
}

auto fromBigEndian32Cache(std::span<uint8_t> cache) noexcept -> float
{
    uint32_t temp { 0U };
    fromUnaligned<uint32_t>(cache.data(), &temp);
    temp = __builtin_bswap32(temp);
    float out;
    toUnaligned<uint32_t>(temp, &out);
    return out;
}

auto fromBigEndian64Cache(std::span<uint8_t> cache) noexcept -> double
{
    uint64_t temp { 0U };
    fromUnaligned<uint64_t>(cache.data(), &temp);
    temp = __builtin_bswap64(temp);
    double out;
    toUnaligned<uint64_t>(temp, &out);
    return out;
}

} // namespace

namespace Jgv::Genicam2 {

FloatReg::FloatReg(const tinyxml2::XMLElement* element, std::shared_ptr<Gvcp::Client> client)
    : Register { element, std::move(client) }
{
    _cachable = Cachable::WriteAround;
}

//void FloatReg::mapInterfaces(const InodeGetter& inodeGetter) noexcept
//{
//    Register::mapInterfaces(inodeGetter);
//}

void FloatReg::initCache() noexcept
{
    _cache.resize(_length.value());
}

auto FloatReg::access() const noexcept -> std::tuple<bool, bool>
{
    return { _canRead, _canWrite };
}

auto FloatReg::value() -> double
{
    if (!_canRead) {
        std::cerr << "Genicam::FloatReg::Value " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " is write only !\n";
        return 0;
    }

    if (_length.value() == sizeof(float)) {
        if (_cachable == Cachable::NoCache) {
            if (auto buffer { _client->readMemory(_address.value(), sizeof(float)) }; !buffer.empty()) {
                return fromBigEndian32Cache(buffer);
            }
            std::cerr << "Genicam2::FloatReg::value() " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " buffer from device is empty !" << std::endl;
            return 0.;
        }
        if (!_cacheIsValid) {
            updateCache();
        }
        return fromBigEndian32Cache(_cache);
    }

    if (_length.value() == sizeof(double)) {
        if (_cachable == Cachable::NoCache) {
            if (auto buffer { _client->readMemory(_address.value(), sizeof(double)) }; !buffer.empty()) {
                return fromBigEndian64Cache(buffer);
            }
            std::cerr << "Genicam2::FloatReg::value() " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " buffer from device is empty !" << std::endl;
            return 0.;
        }
        if (!_cacheIsValid) {
            updateCache();
        }
        return fromBigEndian64Cache(_cache);
    }

    std::cerr << "Genicam::FloatReg::Value " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " bad length " << _length.value() << std::endl;
    return 0;
}

void FloatReg::setValue(double value)
{
    if (!_canWrite) {
        std::cerr << "Genicam::FloatReg::setValue " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " is read only !\n";
        return;
    }
    if (_length.value() == sizeof(float)) {
        if (_cachable != Cachable::WriteThrough) {
            std::array<uint8_t, sizeof(float)> buffer {};
            littleEndian32ToCache(static_cast<float>(value), buffer);
            _client->writeMemory(_address.value(), buffer);
            _cacheIsValid.reset();
        } else {
            littleEndian32ToCache(static_cast<float>(value), _cache);
            writeCache();
        }

    } else if (_length.value() == sizeof(double)) {
        if (_cachable != Cachable::WriteThrough) {
            std::array<uint8_t, sizeof(double)> buffer {};
            littleEndian64ToCache(value, buffer);
            _client->writeMemory(_address.value(), buffer);
            _cacheIsValid.reset();
        } else {
            littleEndian64ToCache(value, _cache);
            writeCache();
        }
    }
    std::for_each(_invalideFunctions.cbegin(), _invalideFunctions.cend(), [](const auto& invalidate) {
        invalidate();
    });
}
auto FloatReg::min() -> double
{
    return std::numeric_limits<double>::lowest();
}
auto FloatReg::max() -> double
{
    return std::numeric_limits<double>::max();
}
auto FloatReg::inc() -> double
{
    return 0.;
}

} // namespace Jgv::Genicam2
