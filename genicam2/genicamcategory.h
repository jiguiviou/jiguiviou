﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMCATEGORY_H
#define GENICAMCATEGORY_H

#include "internal.h"

#include <vector>

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Genicam2 {

class Inode;
class Category final : public ICategory {
    const tinyxml2::XMLElement* _xmlElement;
    std::vector<Inode*> _childs;
    std::size_t _row { 0 };

public:
    explicit Category(const tinyxml2::XMLElement* element);
    ~Category() = default;
    Category(const Category&) = delete;
    Category(Category&&) = delete;
    auto operator=(const Category&) -> Category& = delete;
    auto operator=(Category &&) -> Category& = delete;

    void mapInterfaces(const InodeGetter& inodeGetter);
    void mapFeatures() noexcept;
    void setHierarchy(std::size_t row, Inode* me) noexcept;
    [[nodiscard]] auto row() const noexcept -> std::size_t;
    [[nodiscard]] auto isImplemented() const noexcept -> bool;

    auto featuresCount() const noexcept -> std::size_t;
    auto feature(std::size_t row) const noexcept -> IInode*;
    // Interface ICategory
    [[nodiscard]] auto features() const noexcept -> std::vector<IInode*> override;
};

} // namespace Jgv::Genicam2

#endif // GENICAMCATEGORY_H
