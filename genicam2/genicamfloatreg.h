﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMFLOATREG_H
#define GENICAMFLOATREG_H

#include "genicamregister.h"
#include "internal.h"

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Gvcp {
class Client;
} // namespace Jgv::Gvcp

namespace Jgv::Genicam2 {

class Inode;

class FloatReg final : public IFloat, public Register {

public:
    explicit FloatReg(const tinyxml2::XMLElement* element, std::shared_ptr<Gvcp::Client> client);
    ~FloatReg() override = default;
    FloatReg(const FloatReg&) = delete;
    FloatReg(FloatReg&&) = delete;
    auto operator=(const FloatReg&) -> FloatReg& = delete;
    auto operator=(FloatReg &&) -> FloatReg& = delete;

    //void mapInterfaces(const InodeGetter& inodeGetter) noexcept;

    void initCache() noexcept;
    [[nodiscard]] auto access() const noexcept -> std::tuple<bool, bool>;

    auto value() -> double override;
    void setValue(double value) override;
    auto min() -> double override;
    auto max() -> double override;
    auto inc() -> double override;
};

} // namespace Jgv::Genicam2

#endif // GENICAMFLOATREG_H
