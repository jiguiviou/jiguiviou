﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMINTEGER_H
#define GENICAMINTEGER_H

#include "internal.h"

#include <limits>

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Genicam2 {

class Inode;

class Integer final : public IInteger, public IInvalidate {
    const tinyxml2::XMLElement* _xmlElement;
    ValueInteger _value { 0 };
    ValueInteger _min { std::numeric_limits<int64_t>::lowest() };
    ValueInteger _max { std::numeric_limits<int64_t>::max() };
    ValueInteger _inc { 1 };
    std::vector<std::function<void(void)>> _invalideFunctions;

public:
    explicit Integer(const tinyxml2::XMLElement* element);
    ~Integer() = default;
    Integer(const Integer&) = delete;
    Integer(Integer&&) = delete;
    auto operator=(const Integer&) -> Integer& = delete;
    auto operator=(Integer &&) -> Integer& = delete;

    [[nodiscard]] auto mapInterfaces(const InodeGetter& inodeGetter) noexcept -> Inode*;
    void addInvalidateFunction(std::function<void(void)>&& function) noexcept override;

    auto value() -> int64_t override;
    void setValue(int64_t value) override;
    auto min() -> int64_t override;
    auto max() -> int64_t override;
    auto inc() -> int64_t override;
};

} // namespace Jgv::Genicam2

#endif // GENICAMINTEGER_H
