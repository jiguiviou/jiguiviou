/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMADDRESS_H
#define GENICAMADDRESS_H

#include "internal.h"

#include <memory>
#include <vector>

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Genicam2 {

class Inode;
class IntSwissKnife;

class pIndex {
    int64_t _offset { 0 };
    IInteger* _inode { nullptr };

public:
    explicit pIndex(const tinyxml2::XMLElement* child, const InodeGetter& inodeGetter);
    auto value() noexcept -> int64_t;
};

class Address final {
    const tinyxml2::XMLElement* _xmlElement;
    uint32_t _address { 0U };
    std::vector<IInteger*> _addresses;
    std::vector<std::unique_ptr<IntSwissKnife>> _intSwissKnife;
    std::vector<std::unique_ptr<pIndex>> _indexes;

public:
    explicit Address(const tinyxml2::XMLElement* element);
    ~Address();
    Address(const Address&) = delete;
    Address(Address&&) = delete;
    auto operator=(const Address&) -> Address& = delete;
    auto operator=(Address &&) -> Address& = delete;

    void mapInterfaces(const InodeGetter& inodeGetter) noexcept;

    auto value() noexcept -> uint32_t;
};

} // namespace Jgv::Genicam2

#endif // GENICAMADDRESS_H
