﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMCOMMAND_H
#define GENICAMCOMMAND_H

#include "internal.h"

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Genicam2 {

class Inode;

class Command final : public ICommand, public IInvalidate {
    const tinyxml2::XMLElement* _xmlElement;
    ValueInteger _commandValue { 0 };
    IInteger* _pValue {};
    std::vector<std::function<void(void)>> _invalideFunctions;

public:
    explicit Command(const tinyxml2::XMLElement* element);
    ~Command() = default;
    Command(const Command&) = delete;
    Command(Command&&) = delete;
    auto operator=(const Command&) -> Command& = delete;
    auto operator=(Command &&) -> Command& = delete;

    [[nodiscard]] auto mapInterfaces(const InodeGetter& inodeGetter) noexcept -> Inode*;
    void addInvalidateFunction(std::function<void(void)>&& function) noexcept override;

    void execute() noexcept override;
};

} // namespace Jgv::Genicam2

#endif // GENICAMCOMMAND_H
