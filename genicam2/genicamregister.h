/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMREGISTER_H
#define GENICAMREGISTER_H

#include "genicamaddress.h"
#include "internal.h"

#include <chrono>
#include <memory>

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Gvcp {
class Client;
} // namespace Jgv::Gvcp

namespace Jgv::Genicam2 {

class Length {
    int64_t _value { 0 };
    IInteger* _pValue { nullptr };

public:
    explicit Length(const tinyxml2::XMLElement* element);
    void mapInterfaces(const tinyxml2::XMLElement* element, const InodeGetter& inodeGetter) noexcept;
    auto value() noexcept -> int64_t;
};

enum class Cachable {
    NoCache,
    WriteThrough,
    WriteAround
};

class CacheValidity {
    std::chrono::steady_clock::time_point _last;
    int _interval { 0 };

public:
    void setInterval(int millisecondes)
    {
        _interval = millisecondes;
    }

    operator bool() const
    {
        return (_interval > 0)
            ? std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - _last).count() < _interval
            : _interval < 0;
    }

    void set()
    {
        // gestion avec timeout
        if (_interval > 0) {
            _last = std::chrono::steady_clock::now();
        } else {
            // interval négatif == cache valid
            _interval = -1;
        }
    }

    void reset()
    {
        // gestion avec timeout
        if (_interval > 0) {
            _last = std::chrono::steady_clock::time_point {};
        } else {
            // interval null == cache invalid
            _interval = 0;
        }
    }
};

class Register : public IRegister, public IInvalidate {
    const tinyxml2::XMLElement* _xmlElement;
    const std::shared_ptr<Gvcp::Client> _client;
    std::vector<uint8_t> _cache {};
    Address _address;
    Length _length;
    Cachable _cachable { Cachable::WriteThrough };
    CacheValidity _cacheIsValid;
    std::vector<std::function<void(void)>> _invalideFunctions;
    bool _canWrite { true };
    bool _canRead { true };

public:
    explicit Register(const tinyxml2::XMLElement* element, std::shared_ptr<Gvcp::Client> client);
    virtual ~Register() = default;
    Register(const Register&) = delete;
    Register(Register&&) = delete;
    auto operator=(const Register&) -> Register& = delete;
    auto operator=(Register &&) -> Register& = delete;

    void mapInterfaces(const InodeGetter& inodeGetter) noexcept;
    void addInvalidateFunction(std::function<void(void)>&& function) noexcept override;

    [[nodiscard]] auto canWrite() const noexcept -> bool;

    [[nodiscard]] auto get() -> std::span<std::uint8_t> override;
    void set(std::span<uint8_t> span) override;
    [[nodiscard]] auto address() -> int64_t override;
    [[nodiscard]] auto length() -> int64_t override;

private:
    friend class IntReg;
    friend class FloatReg;
    friend class StringReg;
    friend class MaskedIntReg;
    void updateCache() noexcept;
    void writeCache() noexcept;
};

} // namespace Jgv::Genicam2

#endif // GENICAMREGISTER_H
