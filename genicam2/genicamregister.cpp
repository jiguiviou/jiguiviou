﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamregister.h"

#include "internal.h"

#include <genicaminode.h>
#include <gvcpdevices/gvcpclient.h>
#include <iostream>
#include <tinyxml2.h>

namespace Jgv::Genicam2 {

Length::Length(const tinyxml2::XMLElement* element)
{
    if (auto child { element->FirstChildElement(XmlNode::Length.data()) }; child != nullptr) {
        _value = toValue<int64_t>(child->GetText());
    }
}

void Length::mapInterfaces(const tinyxml2::XMLElement* element, const InodeGetter& inodeGetter) noexcept
{
    if (const auto child { element->FirstChildElement(XmlNode::pLength.data()) }; child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _pValue = inode->IntegerInterface();
        }
    }
}

auto Length::value() noexcept -> int64_t
{
    if (_pValue != nullptr) {
        return _pValue->value();
    }
    return _value;
}

Register::Register(const tinyxml2::XMLElement* element, std::shared_ptr<Gvcp::Client> client)
    : _xmlElement { element }
    , _client { std::move(client) }
    , _address { _xmlElement }
    , _length { _xmlElement }
{
    if (const auto child { _xmlElement->FirstChildElement(XmlNode::AccessMode.data()) }; child != nullptr) {
        _canRead = (child->GetText() != XmlValue::WO);
        _canWrite = (child->GetText() != XmlValue::RO);
    }
    if (const auto child { _xmlElement->FirstChildElement(XmlNode::Cachable.data()) }; child != nullptr) {
        _cachable = (child->GetText() == XmlValue::NoCache) ? Cachable::NoCache : (child->GetText() == XmlValue::WriteAround) ? Cachable::WriteAround : Cachable::WriteThrough;
    }
    // la valeur par défaut d'un FloatReg est WriteAround
    else if (element->Name() == XmlNode::FloatReg) {
        _cachable = Cachable::WriteAround;
    }
    if (const auto child { _xmlElement->FirstChildElement(XmlNode::PollingTime.data()) }; child != nullptr) {
        _cacheIsValid.setInterval(toValue<uint64_t>(child->GetText()));
    }
}

void Register::mapInterfaces(const InodeGetter& inodeGetter) noexcept
{
    _address.mapInterfaces(inodeGetter);
    _length.mapInterfaces(_xmlElement, inodeGetter);

    auto child { _xmlElement->FirstChildElement(XmlNode::pInvalidator.data()) };
    while (child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            inode->addInvalidateFunction(std::bind(&CacheValidity::reset, &_cacheIsValid));
        }
        child = child->NextSiblingElement(XmlNode::pInvalidator.data());
    }
}

void Register::addInvalidateFunction(std::function<void()>&& function) noexcept
{
    _invalideFunctions.emplace_back(std::move(function));
}

auto Register::canWrite() const noexcept -> bool
{
    return _canWrite;
}

auto Register::get() -> std::span<uint8_t>
{
    if (auto buffer { _client->readMemory(_address.value(), _length.value()) }; !buffer.empty()) {
        _cache = std::move(buffer);
    }
    std::cerr << "Genicam2::Register::get() " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " buffer from device is empty !" << std::endl;
    return _cache;
}

void Register::set(std::span<uint8_t> span)
{
    const auto length { _length.value() };
    if (span.size() == length) {
        _cache.clear();
        std::copy(span.begin(), span.end(), std::back_inserter(_cache));
        _client->writeMemory(_address.value(), _cache);
    }
}

auto Register::address() -> int64_t
{
    return _address.value();
}

auto Register::length() -> int64_t
{
    return _length.value();
}

void Register::updateCache() noexcept
{
    if (const auto memory { _client->readMemory(_address.value(), _cache.size()) }; memory.size() == _cache.size()) {
        std::copy(memory.cbegin(), memory.cend(), _cache.begin());
        _cacheIsValid.set();
    } else {
        _cacheIsValid.reset();
        std::cerr << "Genicam2::Register::updateCache() " << _xmlElement->Attribute(XmlAttribute::Name.data())
                  << " wrong buffer or cache size !"
                  << " length: " << _length.value()
                  << " cache size: " << _cache.size()
                  << " buffer size: " << memory.size() << std::endl;
    }
}

void Register::writeCache() noexcept
{
    if (_client->writeMemory(_address.value(), { _cache.data(), _length.value() })) {
        _cacheIsValid.set();
    } else {
        _cacheIsValid.reset();
    }
}

} // namespace Jgv::Genicam2
