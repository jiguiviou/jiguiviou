﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMINTERFACE_H
#define GENICAMINTERFACE_H

#include <iomanip>
#include <memory>
#include <span>
#include <sstream>
#include <variant>
#include <vector>

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Genicam2 {

class IInode;
class ICategory {
public:
    [[nodiscard]] virtual auto features() const noexcept -> std::vector<IInode*> = 0;
};

class IInteger {
public:
    virtual auto value() -> int64_t = 0;
    virtual void setValue(int64_t value) = 0;
    virtual auto min() -> int64_t = 0;
    virtual auto max() -> int64_t = 0;
    virtual auto inc() -> int64_t = 0;
};

class IFloat {
public:
    virtual auto value() -> double = 0;
    virtual void setValue(double value) = 0;
    virtual auto min() -> double = 0;
    virtual auto max() -> double = 0;
    virtual auto inc() -> double = 0;
};

class IEnumEntry {
public:
    virtual auto index() const noexcept -> int64_t = 0;
    virtual auto name() const noexcept -> std::string_view = 0;
};

class IEnumeration {
public:
    virtual auto value() -> int64_t = 0;
    virtual void setValue(int64_t value) = 0;
    virtual auto entries() const noexcept -> std::vector<IEnumEntry*> = 0;
    virtual auto symbolics() const noexcept -> std::vector<std::string_view> = 0;
    virtual auto entryByName(std::string_view name) const noexcept -> IEnumEntry* = 0;
    virtual auto entry(int64_t index) const noexcept -> IEnumEntry* = 0;
    virtual auto currentEntry() const noexcept -> IEnumEntry* = 0;
};

class ICommand {
public:
    virtual void execute() noexcept = 0;
};

class IString {
public:
    [[nodiscard]] virtual auto value() noexcept -> std::string = 0;
    virtual void setValue(std::string_view value) noexcept = 0;
    [[nodiscard]] virtual auto maxLength() noexcept -> int64_t = 0;
};

class IBoolean {
public:
    [[nodiscard]] virtual auto value() noexcept -> bool = 0;
    virtual void setValue(bool value) noexcept = 0;
};

class IRegister {
public:
    [[nodiscard]] virtual auto get() -> std::span<std::uint8_t> = 0;
    virtual void set(std::span<uint8_t> span) = 0;
    [[nodiscard]] virtual auto address() -> int64_t = 0;
    [[nodiscard]] virtual auto length() -> int64_t = 0;
};

class ITree {
public:
    [[nodiscard]] virtual auto child(std::size_t row) const noexcept -> IInode* = 0;
    [[nodiscard]] virtual auto parent() const noexcept -> IInode* = 0;
    [[nodiscard]] virtual auto parentRow() const noexcept -> std::size_t = 0;
    [[nodiscard]] virtual auto rowCount() const noexcept -> std::size_t = 0;
};

enum class Representation {
    ByInterface = 0,
    Linear,
    Logarithmic,
    Boolean,
    PureNumber,
    HexNumber,
    IPV4Address,
    MACAddress
};

class IInode : public ITree {
public:
    [[nodiscard]] virtual auto IntegerInterface() const noexcept -> IInteger* = 0;
    [[nodiscard]] virtual auto FloatInterface() const noexcept -> IFloat* = 0;
    [[nodiscard]] virtual auto EnumerationInterface() const noexcept -> IEnumeration* = 0;
    [[nodiscard]] virtual auto CategoryInterface() const noexcept -> ICategory* = 0;
    [[nodiscard]] virtual auto StringInterface() const noexcept -> IString* = 0;
    [[nodiscard]] virtual auto CommandInterface() const noexcept -> ICommand* = 0;
    [[nodiscard]] virtual auto BooleanInterface() const noexcept -> IBoolean* = 0;

    [[nodiscard]] virtual auto tooltip() const noexcept -> std::string_view = 0;
    [[nodiscard]] virtual auto type() const noexcept -> std::string_view = 0;
    [[nodiscard]] virtual auto featureName() const noexcept -> std::string_view = 0;
    [[nodiscard]] virtual auto displayName() const noexcept -> std::string_view = 0;
    [[nodiscard]] virtual auto description() const noexcept -> std::string_view = 0;
    [[nodiscard]] virtual auto visibility() const noexcept -> std::string_view = 0;
    [[nodiscard]] virtual auto isReadable() const noexcept -> bool = 0;
    [[nodiscard]] virtual auto isWritable() const noexcept -> bool = 0;
    [[nodiscard]] virtual auto isLocked() const noexcept -> bool = 0;
    [[nodiscard]] virtual auto isAvailable() const noexcept -> bool = 0;
    [[nodiscard]] virtual auto representation() const noexcept -> Representation = 0;
};

enum class VariantError {
};

using EnumerationEntry = std::tuple<int64_t, std::string_view>;
using GenICamVariant = std::variant<VariantError, int64_t, double, bool, std::string_view, EnumerationEntry>;
using IntegerLimits = std::tuple<int64_t, int64_t, int64_t>; // min, max, inc
using FloatLimits = std::tuple<double, double, double>; // min, max, inc
using GenicamLimits = std::variant<VariantError, IntegerLimits, FloatLimits>;

struct GenICamVariantToString {
    std::string operator()(VariantError) const noexcept;
    std::string operator()(int64_t value) const noexcept;
    std::string operator()(double value) const noexcept;
    std::string operator()(bool value) const noexcept;
    std::string operator()(std::string_view value) const noexcept;
    std::string operator()(EnumerationEntry&& value) const noexcept;
};

struct InterfacePrivate;
class Interface final {

public:
    Interface(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~Interface();
    Interface(const Interface&) = delete;
    Interface(Interface&&) = delete;
    auto operator=(const Interface&) -> Interface& = delete;
    auto operator=(Interface &&) -> Interface& = delete;

    void setXMLContent(std::string_view content);
    [[nodiscard]] auto getValue(std::string_view name) noexcept -> GenICamVariant;
    void setValue(std::string_view name, GenICamVariant value) noexcept;
    [[nodiscard]] auto limits(std::string_view name) noexcept -> GenicamLimits;

    auto inode(std::string_view name) -> IInode*;

private:
    const std::unique_ptr<InterfacePrivate> _impl;

}; // class GenICamXMLFile

} // namespace Jgv::Genicam2

#endif // GENICAMXMLFILE_H
