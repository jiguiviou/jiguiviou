/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamconverter.h"

#include "genicaminode.h"
#include "internal.h"

#include <iostream>
#include <tinyxml2.h>

namespace Jgv::Genicam2 {

Converter::Converter(const tinyxml2::XMLElement* element)
    : _xmlElement(element)
    , _formulaTo(element)
    , _formulaFrom(element)
{
    if (const auto child { _xmlElement->FirstChildElement(Jgv::Genicam2::XmlNode::Slope.data()) }; child != nullptr) {
        _decreasing = child->GetText() == XmlValue::Decreasing;
    }
}

auto Converter::mapInterfaces(const InodeGetter& inodeGetter) noexcept -> Inode*
{
    _formulaTo.mapInterfaces(inodeGetter, XmlNode::FormulaTo);
    _formulaFrom.mapInterfaces(inodeGetter, XmlNode::FormulaFrom);
    if (const auto child { _xmlElement->FirstChildElement(Jgv::Genicam2::XmlNode::pValue.data()) }; child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            if (auto interface { inode->IntegerInterface() }; interface != nullptr) {
                _pValue.set(interface);
                return inode;
            }
            if (auto interface { inode->FloatInterface() }; interface != nullptr) {
                _pValue.set(interface);
                return inode;
            }
        }
    }
    return nullptr;
}

void Converter::addInvalidateFunction(std::function<void(void)>&& function) noexcept
{
    _invalideFunctions.emplace_back(std::move(function));
}

auto Converter::value() noexcept -> double
{
    if (auto interface { _pValue.get<IInteger>() }; interface != nullptr) {
        const auto res { _formulaFrom.evaluate(interface->value()) };
        if (auto pVal { std::get_if<double>(&res) }) {
            return *pVal;
        }
        if (auto pVal { std::get_if<int64_t>(&res) }) {
            return static_cast<double>(*pVal);
        }
    }

    if (auto interface { _pValue.get<IFloat>() }; interface != nullptr) {
        const auto res { _formulaFrom.evaluate(interface->value()) };
        if (auto pVal { std::get_if<double>(&res) }) {
            return *pVal;
        }
        if (auto pVal { std::get_if<int64_t>(&res) }) {
            return static_cast<double>(*pVal);
        }
    }

    return -1.;
}

void Converter::setValue(double value) noexcept
{
    const auto res { _formulaTo.evaluate(value) };
    if (auto interface { _pValue.get<IInteger>() }; interface != nullptr) {
        if (auto pVal { std::get_if<int64_t>(&res) }) {
            interface->setValue(*pVal);

        } else if (auto pVal { std::get_if<double>(&res) }) {
            interface->setValue(static_cast<int64_t>(*pVal));
        }
    } else if (auto interface { _pValue.get<IFloat>() }; interface != nullptr) {
        if (auto pVal { std::get_if<double>(&res) }) {
            interface->setValue(*pVal);

        } else if (auto pVal { std::get_if<int64_t>(&res) }) {
            interface->setValue(static_cast<double>(*pVal));
        }
    }
    std::for_each(_invalideFunctions.cbegin(), _invalideFunctions.cend(), [](const auto& invalidate) {
        invalidate();
    });
}

auto Converter::min() noexcept -> double
{
    if (auto interface { _pValue.get<IInteger>() }; interface != nullptr) {
        const auto value { _decreasing ? interface->max() : interface->min() };
        const auto res { _formulaFrom.evaluate(value) };
        if (auto pVal { std::get_if<double>(&res) }) {
            return *pVal;
        }
        if (auto pVal { std::get_if<int64_t>(&res) }) {
            return static_cast<double>(*pVal);
        }
    }

    if (auto interface { _pValue.get<IFloat>() }; interface != nullptr) {
        const auto value { _decreasing ? interface->max() : interface->min() };
        const auto res { _formulaFrom.evaluate(value) };
        if (auto pVal { std::get_if<double>(&res) }) {
            return *pVal;
        }
        if (auto pVal { std::get_if<int64_t>(&res) }) {
            return static_cast<double>(*pVal);
        }
    }

    return std::numeric_limits<double>::lowest();
}

auto Converter::max() noexcept -> double
{
    if (auto interface { _pValue.get<IInteger>() }; interface != nullptr) {
        const auto value { _decreasing ? interface->min() : interface->max() };
        const auto res { _formulaFrom.evaluate(value) };
        if (auto pVal { std::get_if<double>(&res) }) {
            return *pVal;
        }
        if (auto pVal { std::get_if<int64_t>(&res) }) {
            return static_cast<double>(*pVal);
        }
    }

    if (auto interface { _pValue.get<IFloat>() }; interface != nullptr) {
        const auto value { _decreasing ? interface->min() : interface->max() };
        const auto res { _formulaFrom.evaluate(value) };
        if (auto pVal { std::get_if<double>(&res) }) {
            return *pVal;
        }
        if (auto pVal { std::get_if<int64_t>(&res) }) {
            return static_cast<double>(*pVal);
        }
    }

    return std::numeric_limits<double>::max();
}

auto Converter::inc() noexcept -> double
{
    if (auto interface { _pValue.get<IInteger>() }; interface != nullptr) {
        const auto res { _formulaFrom.evaluate(interface->inc()) };
        if (auto pVal { std::get_if<double>(&res) }) {
            return *pVal;
        }
        if (auto pVal { std::get_if<int64_t>(&res) }) {
            return static_cast<double>(*pVal);
        }
    }

    if (auto interface { _pValue.get<IFloat>() }; interface != nullptr) {
        const auto res { _formulaFrom.evaluate(interface->inc()) };
        if (auto pVal { std::get_if<double>(&res) }) {
            return *pVal;
        }
        if (auto pVal { std::get_if<int64_t>(&res) }) {
            return static_cast<double>(*pVal);
        }
    }

    return 0.;
}

} // namespace Jgv::Genicam2
