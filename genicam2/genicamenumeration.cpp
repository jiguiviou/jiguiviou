﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamenumeration.h"

#include "genicaminode.h"
#include "internal.h"

#include <iostream>
#include <tinyxml2.h>

namespace Jgv::Genicam2 {

Enumeration::Enumeration(const tinyxml2::XMLElement* element)
    : _xmlElement { element }
{
    if (const auto child { element->FirstChildElement(XmlNode::Value.data()) }; child != nullptr) {
        _index.initValue(toValue<int64_t>(child->GetText()));
    }

    auto child { element->FirstChildElement(XmlNode::EnumEntry.data()) };
    while (child != nullptr) {
        _entries.emplace_back(child);
        child = child->NextSiblingElement(XmlNode::EnumEntry.data());
    }
}

auto Enumeration::mapInterfaces(const InodeGetter& inodeGetter) noexcept -> Inode*
{
    for (auto& entry : _entries) {
        entry.mapInterfaces(inodeGetter);
    }

    if (const auto child { _xmlElement->FirstChildElement(XmlNode::pValue.data()) }; child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _index.setInterface(inode->IntegerInterface());
            return inode;
        }
    }
    return nullptr;
}

void Enumeration::mapEntries() noexcept
{
    for (auto& entry : _entries) {
        if (entry.isImplemented()) {
            _implemented.emplace_back(&entry);
        }
    }
}

void Enumeration::addInvalidateFunction(std::function<void(void)>&& function) noexcept
{
    _invalideFunctions.emplace_back(std::move(function));
}

auto Enumeration::value() -> int64_t
{
    return _index.value();
}

void Enumeration::setValue(int64_t value)
{
    _index.setValue(value);
    std::for_each(_invalideFunctions.cbegin(), _invalideFunctions.cend(), [](const auto& invalidate) {
        invalidate();
    });
}

auto Enumeration::entries() const noexcept -> std::vector<IEnumEntry*>
{
    return _implemented;
}

auto Enumeration::symbolics() const noexcept -> std::vector<std::string_view>
{
    std::vector<std::string_view> implemented;
    std::transform(_implemented.cbegin(), _implemented.cend(), std::back_inserter(implemented), [](auto entry) {
        return entry->name();
    });
    return implemented;
}

auto Enumeration::entryByName(std::string_view name) const noexcept -> IEnumEntry*
{
    if (const auto entry { std::find_if(_implemented.cbegin(), _implemented.cend(), [name](const auto entry) { return entry->name() == name; }) }; entry != _implemented.cend()) {
        return *entry;
    }
    return nullptr;
}

auto Enumeration::entry(int64_t index) const noexcept -> IEnumEntry*
{
    if (const auto entry { std::find_if(_implemented.cbegin(), _implemented.cend(), [index](const auto entry) { return entry->index() == index; }) }; entry != _implemented.cend()) {
        return *entry;
    }
    return nullptr;
}

auto Enumeration::currentEntry() const noexcept -> IEnumEntry*
{
    return entry(_index.value());
}

} // namespace Jgv::Genicam2
