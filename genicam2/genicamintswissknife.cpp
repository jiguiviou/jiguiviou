/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamintswissknife.h"

#include "genicaminode.h"
#include "internal.h"

#include <iostream>
#include <limits>
#include <tinyxml2.h>

namespace Jgv::Genicam2 {

IntSwissKnife::IntSwissKnife(const tinyxml2::XMLElement* element)
    : _formula(element)
{
}

void IntSwissKnife::mapInterfaces(const InodeGetter& inodeGetter) noexcept
{
    _formula.mapInterfaces(inodeGetter, XmlNode::Formula);
}

auto IntSwissKnife::value() -> int64_t
{
    const auto res { _formula.evaluate() };
    if (const auto pVal { std::get_if<int64_t>(&res) }) {
        return *pVal;
    }
    return static_cast<int64_t>(std::get<double>(res));
}

void IntSwissKnife::setValue(int64_t /*value*/)
{
    std::cerr << "IntSwissKnife::setValue makes no sense !" << std::endl;
}

auto IntSwissKnife::min() -> int64_t
{
    return std::numeric_limits<int64_t>::lowest();
}

auto IntSwissKnife::max() -> int64_t
{
    return std::numeric_limits<int64_t>::max();
}

auto IntSwissKnife::inc() -> int64_t
{
    return 1;
}

} // namespace Jgv::Genicam2
