/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "genicamformula.h"

#include "genicaminode.h"
#include "genicamtoken.h"
#include "genicamtokensstack.h"

#include <cmath>
#include <iostream>
#include <map>
#include <stack>
#include <tinyxml2.h>
#include <tuple>

namespace {

// On classe les variables par longueur, de la plus grande à la plus petite.
// Evite ainsi les faux positifs sur les variables qui commencent par le même nom.
struct VariableCmp {
    auto operator()(const std::string_view& lhs, const std::string_view& rhs) const -> bool
    {
        return (lhs.size() == rhs.size()) ? lhs > rhs : lhs.size() > rhs.size();
    }
};

using FormulaIterator = std::string_view::const_iterator;
using VariablesMap = std::map<std::string_view, std::string_view, VariableCmp>;
using VariablesIterator = VariablesMap::const_iterator;

constexpr std::string_view TO { "TO" };
constexpr std::string_view FROM { "FROM" };

constexpr auto stringView(std::string_view::const_iterator begin, std::string_view::const_iterator end) -> std::string_view
{
    return { begin, static_cast<std::string_view::size_type>(std::distance(begin, end)) };
}

auto isEqual(const std::string_view::const_iterator begin, std::string_view other) -> std::string_view::const_iterator
{
    auto it { begin };
    for (const auto c : other) {
        if (c != (*it)) {
            return begin;
        }
        ++it;
    }
    return it;
}

auto isValue(const std::string_view::const_iterator begin, std::string_view::const_iterator end) -> std::tuple<std::variant<int64_t, double>, std::string_view::const_iterator>
{
    bool containPoint { false };
    auto it { begin };

    // traite les préfixes - et 0x
    if ((std::distance(begin, end) >= 2) && (*begin == '-')) {
        std::advance(it, 1);
        if (std::isdigit(*it) == 0) {
            return { INT64_C(0), begin };
        }
    } else if ((std::distance(begin, end) > 2) && (stringView(begin, begin + 2) == "0x")) {
        std::advance(it, 2);
        if (std::isxdigit(*it) == 0) {
            return { INT64_C(0), begin };
        }
    }

    while (it != end) {
        if (*it == '.') {
            containPoint = true;
            std::advance(it, 1);
            continue;
        }
        if (std::isxdigit(*it) != 0) {
            std::advance(it, 1);
            continue;
        }
        break;
    }

    // on n'a rien convertit
    if (it == begin) {
        return { INT64_C(0), begin };
    }

    if (containPoint) {
        return { Jgv::Genicam2::toValue<double>(stringView(begin, it)), it };
    }

    return { Jgv::Genicam2::toValue<int64_t>(stringView(begin, it)), it };
}

auto isToken(std::string_view::const_iterator begin, std::string_view::const_iterator end) -> std::tuple<Jgv::Genicam2::Token, std::string_view::const_iterator>
{
    auto isDoubleToken = [end](std::string_view::const_iterator& it, char c) -> bool {
        if (it != end && *it == c) {
            std::advance(it, 1);
            return true;
        }
        return false;
    };

    if (*begin == '(') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::LEFT_BRACKET }, begin };
    }

    if (*begin == ')') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::RIGHT_BRACKET }, begin };
    }

    if (*begin == '+') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::ADDITION }, begin };
    }

    if (*begin == '-') {
        std::advance(begin, 1);
        // pour que le token soit une soustraction,
        // il faut qu'il soit suivi d'un blanc
        return { Jgv::Genicam2::Token { (isDoubleToken(begin, ' ') ? Jgv::Genicam2::TokenType::SUBSTRACTION : Jgv::Genicam2::TokenType::UNKNOW) }, begin };
    }

    if (*begin == '*') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { (isDoubleToken(begin, '*') ? Jgv::Genicam2::TokenType::POWER : Jgv::Genicam2::TokenType::MULTIPLICATION) }, begin };
    }

    if (*begin == '/') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::DIVISION }, begin };
    }

    if (*begin == '%') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::REMAINDER }, begin };
    }

    if (*begin == '&') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { (isDoubleToken(begin, '&') ? Jgv::Genicam2::TokenType::LOGICAL_AND : Jgv::Genicam2::TokenType::BITWISE_AND) }, begin };
    }

    if (*begin == '|') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { (isDoubleToken(begin, '|') ? Jgv::Genicam2::TokenType::LOGICAL_OR : Jgv::Genicam2::TokenType::BITWISE_OR) }, begin };
    }

    if (*begin == '^') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::BITWISE_XOR }, begin };
    }

    if (*begin == '~') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::BITWISE_NOT }, begin };
    }

    if (*begin == '<') {
        std::advance(begin, 1);
        if (isDoubleToken(begin, '>')) {
            return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::LOGICAL_NOT_EQUAL }, begin };
        }
        if (isDoubleToken(begin, '=')) {
            return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::LOGICAL_LESS_OR_EQUAL }, begin };
        }
        if (isDoubleToken(begin, '<')) {
            return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::SHIFT_LEFT }, begin };
        }
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::LOGICAL_LESS }, begin };
    }

    if (*begin == '>') {
        std::advance(begin, 1);
        if (isDoubleToken(begin, '=')) {
            return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::LOGICAL_GREATER_OR_EQUAL }, begin };
        }
        if (isDoubleToken(begin, '>')) {
            return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::SHIFT_RIGHT }, begin };
        }
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::LOGICAL_GREATER }, begin };
    }

    if (*begin == '=') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::LOGICAL_EQUAL }, begin };
    }

    if (*begin == '?') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::TERNARY_QUESTION_MARK }, begin };
    }

    if (*begin == ':') {
        std::advance(begin, 1);
        return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::TERNARY_COLON }, begin };
    }

    return { Jgv::Genicam2::Token { Jgv::Genicam2::TokenType::UNKNOW }, begin };

    //    // le token est un chiffre
    //    // on est dans le cas d'une valeur décimale ou hexa
    //    else if (std::isdigit(begin) != 0) {
    //        auto [type, var] { getNumber(text) };
    //        token.setType(type, var);
    //    }
    //    // le dernier cas possible est d'avoir une variable nommée
    //    else {
    //        //       token = getVariable(it);
    //    }

    //    return token;
}

auto predecendenceIsEqual(const Jgv::Genicam2::Token& left, const Jgv::Genicam2::Token& right) -> bool
{
    return (left.precedence() == right.precedence());
}

auto predecendenceIsLess(const Jgv::Genicam2::Token& left, const Jgv::Genicam2::Token& right) -> bool
{
    return (left.precedence() < right.precedence());
}

auto parseFormula(const tinyxml2::XMLElement* element, std::string_view xmlNode) -> std::string_view
{
    auto child { element->FirstChildElement(xmlNode.data()) };
    if (child != nullptr) {
        return child->GetText();
    }
    std::cerr << "GenICam2::Formula: failed to parse " << xmlNode << std::endl;
    return {};
}

auto parseVariables(const tinyxml2::XMLElement* element) -> VariablesMap
{
    VariablesMap out;
    auto child { element->FirstChildElement(Jgv::Genicam2::XmlNode::pVariable.data()) };
    while (child != nullptr) {
        out.emplace(child->Attribute(Jgv::Genicam2::XmlAttribute::Name.data()), child->GetText());
        child = child->NextSiblingElement(Jgv::Genicam2::XmlNode::pVariable.data());
    }
    return out;
}

auto isVariable(FormulaIterator formBegin, FormulaIterator formEnd, VariablesIterator varBegin, VariablesIterator varEnd) noexcept -> std::tuple<FormulaIterator, VariablesIterator>
{
    auto next { formBegin };
    const auto it = std::find_if(varBegin, varEnd, [formBegin, formEnd, &next](const auto variable) {
        if (std::distance(formBegin, formEnd) < variable.first.size()) {
            return false;
        }

        next = isEqual(formBegin, variable.first);
        if (next == formBegin) {
            return false;
        }

        // Si la variable n'est pas finie (elle est suivie par une majuscule),
        // on se trouve dans le cas où plusieurs variables commance par la même chaîne.
        return std::isupper(*next) == 0;
    });

    //    if (it != varEnd) {
    //        std::advance(formBegin, it->first.size());
    //    }
    return { next, it };
}

auto isExternalVariable(std::string_view::const_iterator begin, std::string_view::const_iterator end, std::string_view external) noexcept -> std::string_view::const_iterator
{
    if (static_cast<std::size_t>(std::distance(begin, end)) < external.size()) {
        return begin;
    }

    if (auto next { isEqual(begin, external) }; next != begin) {
        if (std::isupper(*next) == 0) {
            return next;
        }
    }

    return begin;
}

auto inodeInt64Getter(VariablesIterator variable, const Jgv::Genicam2::InodeGetter& inodeGetter) -> std::function<int64_t(void)>
{
    auto [variableName, inodeName] { *variable };
    std::string_view getter {};
    // le nom de variable peut avoir une extension, pour accéder à la bonne interface
    if (auto point { variableName.find_first_of('.') }; point != std::string_view::npos) {
        getter = variableName.substr(point + 1);
    }

    if (const auto inode { inodeGetter(inodeName) }; inode != nullptr) {
        if (auto interface { inode->IntegerInterface() }; interface != nullptr) {
            if (getter.empty() || (getter == Jgv::Genicam2::XmlNode::Value)) {
                return std::bind(&Jgv::Genicam2::IInteger::value, interface);
            }
            if (getter == Jgv::Genicam2::XmlNode::Min) {
                return std::bind(&Jgv::Genicam2::IInteger::min, interface);
            }
            if (getter == Jgv::Genicam2::XmlNode::Max) {
                return std::bind(&Jgv::Genicam2::IInteger::max, interface);
            }
            if (getter == Jgv::Genicam2::XmlNode::Inc) {
                return std::bind(&Jgv::Genicam2::IInteger::inc, interface);
            }
        }
        std::cerr << "GenICam2::Formula: failed to bind interface " << variableName << std::endl;
    }

    return {};
}

} // namespace

namespace Jgv::Genicam2 {

Formula::Formula(const tinyxml2::XMLElement* element)
    : _xmlElement { element }
{
}

void Formula::mapInterfaces(const InodeGetter& inodeGetter, std::string_view xmlNode) noexcept
{
    const auto formula { parseFormula(_xmlElement, xmlNode) };
    const auto variables { parseVariables(_xmlElement) };
    std::stack<Token> tokensStack;

    auto characterIterator { formula.cbegin() };
    while (characterIterator != formula.cend()) {
        // drop les espaces
        if (std::isspace(*characterIterator) != 0) {
            std::advance(characterIterator, 1);
            continue;
        }

        // Si le charactère est une lettre majuscule, on teste si on a une variable
        if (std::isupper(*characterIterator) != 0) {
            if (const auto [next, variable] { isVariable(characterIterator, formula.cend(), variables.cbegin(), variables.cend()) }; next != characterIterator) {
                // On a une variable, on construit l'interface
                auto getter { inodeInt64Getter(variable, inodeGetter) };
                if (!getter) {
                    std::cerr << "GenICam2::Formula::mapInterfaces: failed, null interface ! variable: " << variable->first << " Inode: " << variable->second << std::endl;
                    return;
                }
                _out.emplace_back(getter);
                characterIterator = next;
                continue;
            }

            if (const auto next { isExternalVariable(characterIterator, formula.cend(), FROM) }; next != characterIterator) {
                _out.emplace_back(ExternVar {});
                characterIterator = next;
                continue;
            }

            if (const auto next { isExternalVariable(characterIterator, formula.cend(), TO) }; next != characterIterator) {
                _out.emplace_back(ExternVar {});
                characterIterator = next;
                continue;
            }
        }

        if (auto [variant, end] { isValue(characterIterator, formula.cend()) }; end != characterIterator) {
            if (auto pVal { std::get_if<double>(&variant) }) {
                _out.emplace_back(*pVal);
            } else if (auto pVal { std::get_if<int64_t>(&variant) }) {
                _out.emplace_back(*pVal);
            }

            characterIterator = end;
            continue;
        }

        if (auto [token, end] { isToken(characterIterator, formula.cend()) }; end != characterIterator) {
            if (token.isOperator()) {
                // traitement des autres token GenICam
                while (!tokensStack.empty() // la pile n'est pas vide
                    && tokensStack.top().isOperator() // le haut de la pile contient un opérateur
                    && (
                        // associativité gauche droite et priorité égale
                        (token.isLeftAssociative() && predecendenceIsEqual(token, tokensStack.top()))
                        // priorité plus faible
                        || predecendenceIsLess(token, tokensStack.top()))) {
                    // on dépile la pile vers la sortie
                    _out.emplace_back(tokensStack.top());
                    tokensStack.pop();
                }

                // le traitement de la pile est fini, on empile le token
                tokensStack.push(token);
            }
            // cas parenthèse gauche
            else if (token.isLeftParenthesis()) {
                tokensStack.push(token); // parenthèse gauche toujours sur la pile
            }

            else if (token.isRightParenthesis()) {
                bool leftParenthesisFounded = false;

                // on traite la pile
                while (!tokensStack.empty()) {
                    // tant qu'on ne trouve pas une parenthèse gauche, on dépile
                    if (!tokensStack.top().isLeftParenthesis()) {
                        // on empile les opérateurs
                        _out.emplace_back(tokensStack.top());
                        tokensStack.pop();
                    } else {
                        // on supprime juste la parenthèse gauche
                        tokensStack.pop();
                        leftParenthesisFounded = true;
                        break;
                    }
                }
                if (!leftParenthesisFounded) {
                    std::cerr << "GenICam2::Formula::mapInterfaces: failed, left parenthesis not found on the stack !\n";
                    //stack.clear();
                    break;
                }
            }

            characterIterator = end;
            continue;
        }

        std::advance(characterIterator, 1);
    }

    // tous les tokens on été lu, on pousse la pile vers la sortie
    while (!tokensStack.empty()) {
        // si il reste des parenthèses sur la pile, on a une erreur
        if (tokensStack.top().isLeftParenthesis() || tokensStack.top().isRightParenthesis()) {
            std::cerr << "GenICam2::Formula::mapInterfaces: failed, parenthesis left on the stack !\n";
            break;
        }
        _out.emplace_back(tokensStack.top());
        tokensStack.pop();
    }

    //    std::cout << "DEBUG FORMULA " << _formula << std::endl;
    //    for (const auto& variant : _out) {
    //        if (auto pVal = std::get_if<IInteger*>(&variant)) {
    //            std::cout << "IInteger " << (*pVal)->value() << std::endl;
    //        } else if (auto pVal = std::get_if<Token>(&variant)) {
    //            std::cout << toString((*pVal).type()) << std::endl;
    //        } else if (auto pVal = std::get_if<int64_t>(&variant)) {
    //            std::cout << *pVal << std::endl;
    //        } else if (auto pVal = std::get_if<double>(&variant)) {
    //            std::cout << *pVal << std::endl;
    //        } else {
    //            std::cout << "ERROR" << std::endl;
    //        }
    //    }
    //    std::cout << "---------------------------------------" << std::endl;
}

auto Formula::evaluate() const noexcept -> std::variant<int64_t, double>
{
    return evaluate(INT64_C(0));
}

auto Formula::evaluate(std::variant<int64_t, double> externVar) const noexcept -> std::variant<int64_t, double>
{
    TokensStack stack;
    for (auto& variant : _out) {
        // pVariable
        if (auto integerGetter { std::get_if<IntegerGetter>(&variant) }) {
            if (*integerGetter) {
                stack.push((*integerGetter)());
            }
        } else if (auto floatGetter { std::get_if<FloatGetter>(&variant) }) {
            if (*floatGetter) {
                stack.push(static_cast<int64_t>((*floatGetter)()));
            }
        }
        // value
        else if (auto pVal { std::get_if<int64_t>(&variant) }) {
            stack.push(*pVal);
        } else if (auto pVal { std::get_if<double>(&variant) }) {
            stack.push(*pVal);
        }

        // External Value
        else if (auto pVal { std::get_if<ExternVar>(&variant) }) {
            if (auto pVar { std::get_if<int64_t>(&externVar) }) {
                stack.push(*pVar);
            } else {
                stack.push(std::get<double>(externVar));
            }
        }

        // token
        else if (auto pVal { std::get_if<Token>(&variant) }) {
            stack.push(*pVal);
        }
    }

    return stack.value();
}

} // namespace Jgv::Genicam2
