﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamboolean.h"

#include "genicaminode.h"

#include <tinyxml2.h>

#include <iostream>

namespace Jgv::Genicam2 {

Boolean::Boolean(const tinyxml2::XMLElement* element)
    : _xmlElement { element }
{
    if (const auto child { _xmlElement->FirstChildElement(XmlNode::OnValue.data()) }; child != nullptr) {
        _onValue = toValue<int64_t>(child->GetText());
    }

    if (const auto child { _xmlElement->FirstChildElement(XmlNode::OffValue.data()) }; child != nullptr) {
        _offValue = toValue<int64_t>(child->GetText());
    }

    if (const auto child { _xmlElement->FirstChildElement(XmlNode::Value.data()) }; child != nullptr) {
        _value.initValue(toValue<int64_t>(child->GetText()));
    }
}

auto Boolean::mapInterfaces(const InodeGetter& inodeGetter) noexcept -> Inode*
{
    if (const auto child { _xmlElement->FirstChildElement(XmlNode::pValue.data()) }; child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _value.setInterface(inode->IntegerInterface());
            return inode;
        }
    }
    return nullptr;
}

void Boolean::addInvalidateFunction(std::function<void(void)>&& function) noexcept
{
    _invalideFunctions.emplace_back(std::move(function));
}

auto Boolean::value() noexcept -> bool
{
    return _value.value() == _onValue;
}

void Boolean::setValue(bool value) noexcept
{
    _value.setValue((value ? _onValue : _offValue));
    std::for_each(_invalideFunctions.cbegin(), _invalideFunctions.cend(), [](const auto& invalidate) {
        invalidate();
    });
}

} // namespace Jgv::Genicam2
