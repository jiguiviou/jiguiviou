﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMTOKENSSTACK_H
#define GENICAMTOKENSSTACK_H

#include <array>
#include <stack>
#include <variant>

namespace Jgv::Genicam2 {

class Token;
using Operand = std::variant<int64_t, double, bool>;

class TokensStack final {
    std::stack<Operand> _stack;
    std::array<Operand, 3> operands;

public:
    void push(int64_t value) noexcept;
    void push(double value) noexcept;
    void push(const Token& token) noexcept;
    auto value() noexcept -> std::variant<int64_t, double>;
};

} // namespace Jgv::Genicam2

#endif // GENICAMTOKENSSTACK_H
