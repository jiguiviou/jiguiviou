﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMENUMENTRY_H
#define GENICAMENUMENTRY_H

#include "internal.h"

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Genicam2 {

class EnumEntry final : public IEnumEntry {
    const tinyxml2::XMLElement* _xmlElement;
    std::string_view _name {};
    int64_t _value { 0 };
    IInteger* _isImplemented { nullptr };

public:
    explicit EnumEntry(const tinyxml2::XMLElement* element);
    void mapInterfaces(const InodeGetter& inodeGetter) noexcept;
    [[nodiscard]] auto isImplemented() -> bool;

    // IEnumEntry interface
    [[nodiscard]] auto index() const noexcept -> int64_t final;
    [[nodiscard]] auto name() const noexcept -> std::string_view final;
};

} // namespace Jgv::Genicam2

#endif // GENICAMENUMENTRY_H
