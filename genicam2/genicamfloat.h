﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMFLOAT_H
#define GENICAMFLOAT_H

#include "internal.h"

#include <limits>

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Genicam2 {

class Inode;

class Float final : public IFloat, public IInvalidate {
    const tinyxml2::XMLElement* _xmlElement;
    ValueFloat _value { 0. };
    ValueFloat _min { std::numeric_limits<double>::lowest() };
    ValueFloat _max { std::numeric_limits<double>::max() };
    ValueFloat _inc { 0. };
    std::vector<std::function<void(void)>> _invalideFunctions;

public:
    explicit Float(const tinyxml2::XMLElement* element);
    ~Float() = default;
    Float(const Float&) = delete;
    Float(Float&&) = delete;
    auto operator=(const Float&) -> Float& = delete;
    auto operator=(Float &&) -> Float& = delete;

    [[nodiscard]] auto mapInterfaces(const InodeGetter& inodeGetter) noexcept -> Inode*;
    void addInvalidateFunction(std::function<void(void)>&& function) noexcept override;

    auto value() -> double override;
    void setValue(double value) override;
    auto min() -> double override;
    auto max() -> double override;
    auto inc() -> double override;
};

} // namespace Jgv::Genicam2

#endif // GENICAMINTEGER_H
