﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef INTERNAL_H
#define INTERNAL_H

#include "genicaminterface.h"

#include <functional>
#include <iomanip>
#include <sstream>
#include <string_view>

namespace Jgv::Genicam2 {

namespace XmlName {
    constexpr std::string_view Root { "Root" };
}

namespace XmlAttribute {
    constexpr std::string_view Name { "Name" };
    constexpr std::string_view Offset { "Offset" };
} // namespace XmlAttribute

namespace XmlNode {
    constexpr std::string_view Category { "Category" };
    constexpr std::string_view Command { "Command" };
    constexpr std::string_view StringReg { "StringReg" };
    constexpr std::string_view Integer { "Integer" };
    constexpr std::string_view IntReg { "IntReg" };
    constexpr std::string_view IntConverter { "IntConverter" };
    constexpr std::string_view IntSwissKnife { "IntSwissKnife" };
    constexpr std::string_view Enumeration { "Enumeration" };
    constexpr std::string_view EnumEntry { "EnumEntry" };
    constexpr std::string_view MaskedIntReg { "MaskedIntReg" };
    constexpr std::string_view Float { "Float" };
    constexpr std::string_view FloatReg { "FloatReg" };
    constexpr std::string_view Converter { "Converter" };
    constexpr std::string_view SwissKnife { "SwissKnife" };
    constexpr std::string_view Boolean { "Boolean" };
    constexpr std::string_view Register { "Register" };
    constexpr std::string_view StructEntry { "StructEntry" };

    constexpr std::string_view pFeature { "pFeature" };
    constexpr std::string_view Value { "Value" };
    constexpr std::string_view Min { "Min" };
    constexpr std::string_view Max { "Max" };
    constexpr std::string_view Inc { "Inc" };
    constexpr std::string_view pValue { "pValue" };
    constexpr std::string_view pMin { "pMin" };
    constexpr std::string_view pMax { "pMax" };
    constexpr std::string_view pInc { "pInc" };
    constexpr std::string_view CommandValue { "CommandValue" };
    constexpr std::string_view pCommandValue { "pCommandValue" };
    constexpr std::string_view Address { "Address" };
    constexpr std::string_view pAddress { "pAddress" };
    constexpr std::string_view pIndex { "pIndex" };
    constexpr std::string_view AccessMode { "AccessMode" };
    constexpr std::string_view Length { "Length" };
    constexpr std::string_view pLength { "pLength" };
    constexpr std::string_view Sign { "Sign" };
    constexpr std::string_view pVariable { "pVariable" };
    constexpr std::string_view Formula { "Formula" };
    constexpr std::string_view FormulaTo { "FormulaTo" };
    constexpr std::string_view FormulaFrom { "FormulaFrom" };
    constexpr std::string_view Expression { "Expression" };
    constexpr std::string_view Constant { "Constant" };
    constexpr std::string_view LSB { "LSB" };
    constexpr std::string_view MSB { "MSB" };
    constexpr std::string_view Bit { "Bit" };
    constexpr std::string_view OnValue { "OnValue" };
    constexpr std::string_view OffValue { "OffValue" };
    constexpr std::string_view ToolTip { "ToolTip" };
    constexpr std::string_view Description { "Description" };
    constexpr std::string_view DisplayName { "DisplayName" };
    constexpr std::string_view Visibility { "Visibility" };
    constexpr std::string_view ImposedAccessMode { "ImposedAccessMode" };
    constexpr std::string_view Cachable { "Cachable" };
    constexpr std::string_view pIsImplemented { "pIsImplemented" };
    constexpr std::string_view pIsLocked { "pIsLocked" };
    constexpr std::string_view pIsAvailable { "pIsAvailable" };
    constexpr std::string_view Slope { "Slope" };
    constexpr std::string_view PollingTime { "PollingTime" };
    constexpr std::string_view pInvalidator { "pInvalidator" };
    constexpr std::string_view Representation { "Representation" };

} // namespace XmlNode

namespace XmlValue {
    constexpr std::string_view Signed { "Signed" };
    constexpr std::string_view Unsigned { "Unsigned" };
    constexpr std::string_view RO { "RO" };
    constexpr std::string_view RW { "RW" };
    constexpr std::string_view WO { "WO" };
    constexpr std::string_view NoCache { "NoCache" };
    constexpr std::string_view WriteThrough { "WriteThrough" };
    constexpr std::string_view WriteAround { "WriteAround" };
    constexpr std::string_view Beginner { "Beginner" };
    constexpr std::string_view Expert { "Expert" };
    constexpr std::string_view Guru { "Guru" };
    constexpr std::string_view Invisible { "Invisible" };
    constexpr std::string_view Increasing { "Increasing" };
    constexpr std::string_view Decreasing { "Decreasing" };
    constexpr std::string_view Linear { "Linear" };
    constexpr std::string_view Logarithmic { "Logarithmic" };
    constexpr std::string_view Boolean { "Boolean" };
    constexpr std::string_view PureNumber { "PureNumber" };
    constexpr std::string_view HexNumber { "HexNumber" };
    constexpr std::string_view IPV4Address { "IPV4Address" };
    constexpr std::string_view MACAddress { "MACAddress" };

} // namespace XmlValue

enum class InodeType {
    Null,
    Category,
    Command,
    StringReg,
    Integer,
    IntReg,
    IntConverter,
    IntSwissKnife,
    Enumeration,
    EnumEntry,
    MaskedIntReg,
    Float,
    FloatReg,
    Converter,
    SwissKnife,
    Boolean,
    Register,
    StructEntry
};

static auto toString(InodeType type) -> std::string_view
{
    switch (type) {
    case InodeType::Category:
        return XmlNode::Category;
    case InodeType::Command:
        return XmlNode::Command;
    case InodeType::StringReg:
        return XmlNode::StringReg;
    case InodeType::Integer:
        return XmlNode::Integer;
    case InodeType::IntReg:
        return XmlNode::IntReg;
    case InodeType::IntConverter:
        return XmlNode::IntConverter;
    case InodeType::IntSwissKnife:
        return XmlNode::IntSwissKnife;
    case InodeType::Enumeration:
        return XmlNode::Enumeration;
    case InodeType::EnumEntry:
        return XmlNode::EnumEntry;
    case InodeType::MaskedIntReg:
        return XmlNode::MaskedIntReg;
    case InodeType::Float:
        return XmlNode::Float;
    case InodeType::FloatReg:
        return XmlNode::FloatReg;
    case InodeType::Converter:
        return XmlNode::Converter;
    case InodeType::SwissKnife:
        return XmlNode::SwissKnife;
    case InodeType::Boolean:
        return XmlNode::Boolean;
    case InodeType::Register:
        return XmlNode::Register;
    case InodeType::StructEntry:
        return XmlNode::StructEntry;
    default:
        return "Null";
    }
}

static auto toRepresentation(std::string_view text) noexcept -> Jgv::Genicam2::Representation
{
    if (text == Jgv::Genicam2::XmlValue::Linear)
        return Jgv::Genicam2::Representation::Linear;
    if (text == Jgv::Genicam2::XmlValue::Logarithmic)
        return Jgv::Genicam2::Representation::Logarithmic;
    if (text == Jgv::Genicam2::XmlValue::Boolean)
        return Jgv::Genicam2::Representation::Boolean;
    if (text == Jgv::Genicam2::XmlValue::PureNumber)
        return Jgv::Genicam2::Representation::PureNumber;
    if (text == Jgv::Genicam2::XmlValue::HexNumber)
        return Jgv::Genicam2::Representation::HexNumber;
    if (text == Jgv::Genicam2::XmlValue::IPV4Address)
        return Jgv::Genicam2::Representation::IPV4Address;
    if (text == Jgv::Genicam2::XmlValue::MACAddress)
        return Jgv::Genicam2::Representation::MACAddress;
    return Jgv::Genicam2::Representation::ByInterface;
}

class IInvalidate {
public:
    virtual void addInvalidateFunction(std::function<void(void)>&& function) noexcept = 0;
};

template <typename T>
static auto toValue(std::string_view text) noexcept -> T
{
    T out { 0 };
    std::stringstream ss;
    ss << text;
    ss >> std::setbase(0) >> out;
    return out;
}

class pValue {
    IInteger* _iinteger { nullptr };
    IFloat* _ifloat { nullptr };

public:
    void set(IInteger* p) { _iinteger = p; }
    void set(IFloat* p) { _ifloat = p; }

    template <class T>
    std::enable_if_t<std::is_same<T, IInteger>::value, T*> get() const noexcept
    {
        return _iinteger;
    }

    template <class T>
    std::enable_if_t<std::is_same<T, IFloat>::value, T*> get() const noexcept
    {
        return _ifloat;
    }
};

class ValueInteger {
    int64_t _value;
    IInteger* _pValue;

public:
    explicit ValueInteger(int64_t defaultValue)
        : _value { defaultValue }
        , _pValue { nullptr }
    {
    }
    void initValue(int64_t value) noexcept { _value = value; }
    void setInterface(IInteger* interface) noexcept
    {
        _pValue = interface;
    }
    [[nodiscard]] auto value() const noexcept -> int64_t { return _pValue == nullptr ? _value : _pValue->value(); }
    void setValue(int64_t value)
    {
        if (_pValue == nullptr) {
            _value = value;
        } else {
            _pValue->setValue(value);
        }
    }
};

class ValueFloat {
    double _value;
    IFloat* _pValue;

public:
    explicit ValueFloat(double defaultValue)
        : _value { defaultValue }
        , _pValue { nullptr }
    {
    }
    void initValue(double value) noexcept { _value = value; }
    void setInterface(IFloat* interface) noexcept
    {
        _pValue = interface;
    }
    [[nodiscard]] auto value() const noexcept -> double { return (_pValue == nullptr) ? _value : _pValue->value(); }
    void setValue(double value)
    {
        if (_pValue == nullptr) {
            _value = value;
        } else {
            _pValue->setValue(value);
        }
    }
};

class Inode;
using InodeGetter = std::function<Inode*(std::string_view)>;

} // namespace Jgv::Genicam2

#endif // INTERNAL_H
