/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamstringreg.h"

#include "genicaminode.h"
#include "internal.h"

#include <gvcpdevices/gvcpclient.h>
#include <iostream>
#include <tinyxml2.h>

namespace Jgv::Genicam2 {

StringReg::StringReg(const tinyxml2::XMLElement* element, std::shared_ptr<Gvcp::Client> client)
    : Register { element, std::move(client) }
{
    _cache.reserve(STRING_MAX_SIZE);
}

//void StringReg::mapInterfaces(const InodeGetter& inodeGetter) noexcept
//{
//    Register::mapInterfaces(inodeGetter);
//}

void StringReg::initCache() noexcept
{
    _cache.resize(_length.value());
}

auto StringReg::access() const noexcept -> std::tuple<bool, bool>
{
    return { _canRead, _canWrite };
}

auto StringReg::value() noexcept -> std::string
{
    if (_cachable == Cachable::NoCache) {
        if (auto buffer { _client->readMemory(_address.value(), _length.value()) }; !buffer.empty()) {
            return { buffer.cbegin(), buffer.cend() };
        }
        std::cerr << "Genicam2::StringReg::value() " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " buffer from device is empty !" << std::endl;
        return "";
    }
    if (!_cacheIsValid) {
        updateCache();
    }

    auto end { _cache.begin() };
    std::advance(end, _length.value());
    return { _cache.begin(), end };
}

void StringReg::setValue(std::string_view value) noexcept
{
    if (const auto length { _length.value() }; value.size() <= length) {
        if (_cachable != Cachable::WriteThrough) {
            std::vector<uint8_t> buffer(length, 0);
            std::copy(value.cbegin(), value.cend(), buffer.begin());
            _client->writeMemory(_address.value(), buffer);
        } else {
            auto begin { _cache.begin() };
            auto end { begin };
            std::advance(end, length);
            begin = std::copy(value.cbegin(), value.cend(), begin);
            std::fill(begin, end, 0);
            writeCache();
        }
    }
    std::for_each(_invalideFunctions.cbegin(), _invalideFunctions.cend(), [](const auto& invalidate) {
        invalidate();
    });
}

auto StringReg::maxLength() noexcept -> int64_t
{
    return _length.value();
}

} // namespace Jgv::Genicam2
