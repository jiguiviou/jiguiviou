/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMENUMERATION_H
#define GENICAMENUMERATION_H

#include "genicamenumentry.h"
#include "internal.h"

#include <vector>

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Genicam2 {

class Inode;

class Enumeration final : public IEnumeration, public IInvalidate {
    const tinyxml2::XMLElement* _xmlElement;
    std::vector<EnumEntry> _entries;
    std::vector<IEnumEntry*> _implemented;
    ValueInteger _index { 0 };
    std::vector<std::function<void(void)>> _invalideFunctions;

public:
    explicit Enumeration(const tinyxml2::XMLElement* element);
    ~Enumeration() = default;
    Enumeration(const Enumeration&) = delete;
    Enumeration(Enumeration&&) = delete;
    auto operator=(const Enumeration&) -> Enumeration& = delete;
    auto operator=(Enumeration &&) -> Enumeration& = delete;

    [[nodiscard]] auto mapInterfaces(const InodeGetter& inodeGetter) noexcept -> Inode*;
    void mapEntries() noexcept;
    void addInvalidateFunction(std::function<void(void)>&& function) noexcept override;

    auto value() -> int64_t final;
    void setValue(int64_t value) final;
    auto entries() const noexcept -> std::vector<IEnumEntry*> final;
    auto symbolics() const noexcept -> std::vector<std::string_view> final;
    auto entryByName(std::string_view name) const noexcept -> IEnumEntry* final;
    auto entry(int64_t index) const noexcept -> IEnumEntry* final;
    auto currentEntry() const noexcept -> IEnumEntry* final;
};

} // namespace Jgv::Genicam2

#endif // GENICAMENUMERATION_H
