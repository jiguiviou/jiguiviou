/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamenumentry.h"

#include "genicaminode.h"
#include "internal.h"

#include <tinyxml2.h>

namespace Jgv::Genicam2 {

EnumEntry::EnumEntry(const tinyxml2::XMLElement* element)
    : _xmlElement { element }
{
    _name = _xmlElement->Attribute(XmlAttribute::Name.data());

    auto child { _xmlElement->FirstChildElement(XmlNode::Value.data()) };
    if (child != nullptr) {
        _value = toValue<int64_t>(child->GetText());
    }
}

void EnumEntry::mapInterfaces(const InodeGetter& inodeGetter) noexcept
{
    if (const auto child { _xmlElement->FirstChildElement(XmlNode::pIsImplemented.data()) }; child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _isImplemented = inode->IntegerInterface();
        }
    }
}

auto EnumEntry::isImplemented() -> bool
{
    if (_isImplemented != nullptr) {
        return _isImplemented->value() != 0;
    }
    return true;
}

auto EnumEntry::name() const noexcept -> std::string_view
{
    return _name;
}

auto EnumEntry::index() const noexcept -> int64_t
{
    return _value;
}

} // namespace Jgv::Genicam2
