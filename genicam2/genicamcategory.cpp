﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamcategory.h"

#include "genicaminode.h"
#include "internal.h"

#include <tinyxml2.h>

#include <iostream>

namespace Jgv::Genicam2 {

Category::Category(const tinyxml2::XMLElement* element)
    : _xmlElement { element }
{
}

void Category::mapInterfaces(const InodeGetter& inodeGetter)
{
    auto child { _xmlElement->FirstChildElement(XmlNode::pFeature.data()) };
    while (child != nullptr) {
        _childs.emplace_back(inodeGetter(child->GetText()));
        child = child->NextSiblingElement(XmlNode::pFeature.data());
    }
}

void Category::mapFeatures() noexcept
{
    std::vector<Inode*> implemented;
    auto fillImplemented = [&implemented](Inode* inode) {
        inode->mapTreeInterface();
        if (inode->isImplemented()) {
            implemented.emplace_back(inode);
        }
    };

    std::for_each(_childs.begin(), _childs.end(), fillImplemented);

    _childs = std::move(implemented);
}

void Category::setHierarchy(std::size_t row, Inode* me) noexcept
{
    std::size_t childRow { 0U };
    for (auto implemented : _childs) {
        implemented->setHierarchy(childRow++, me);
    }
    _row = row;
}

auto Category::row() const noexcept -> std::size_t
{
    return _row;
}

auto Category::isImplemented() const noexcept -> bool
{
    return !_childs.empty();
}

auto Category::featuresCount() const noexcept -> std::size_t
{
    return _childs.size();
}

auto Category::feature(std::size_t row) const noexcept -> IInode*
{
    return _childs[row];
}

auto Category::features() const noexcept -> std::vector<IInode*>
{
    std::vector<IInode*> features;
    std::copy(_childs.cbegin(), _childs.cend(), std::back_inserter(features));
    return features;
}

} // namespace Jgv::Genicam2
