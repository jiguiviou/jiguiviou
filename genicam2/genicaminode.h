﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMINODE_H
#define GENICAMINODE_H

#include "internal.h"

#include <memory>

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Gvcp {
class Client;
} // namespace Jgv::Gvcp

namespace Jgv::Genicam2 {

enum class InodeType;

struct InodePrivate;
class Inode : public IInode {
public:
    explicit Inode();
    virtual ~Inode();
    Inode(const Inode&) = delete;
    Inode(Inode&&) = delete;
    auto operator=(const Inode&) -> Inode& = delete;
    auto operator=(Inode &&) -> Inode& = delete;

    void initializeType(InodeType type, const tinyxml2::XMLElement* element, const std::shared_ptr<Gvcp::Client>& gvcpClient) noexcept;
    [[nodiscard]] auto getValue() noexcept -> GenICamVariant;
    void setValue(GenICamVariant value) noexcept;
    [[nodiscard]] auto limits() noexcept -> GenicamLimits;

    [[nodiscard]] auto access() const noexcept -> std::tuple<bool, bool>;

    void mapInterfaces(const InodeGetter& inodeGetter) noexcept;
    void initStaticProperties() noexcept;

    void mapTreeInterface() noexcept;
    void setHierarchy(std::size_t row, Inode* parent);
    void addInvalidateFunction(std::function<void(void)> function) noexcept;

    [[nodiscard]] auto row() const noexcept -> std::size_t;
    [[nodiscard]] auto isCategoryNode() const noexcept -> bool;
    [[nodiscard]] auto isImplemented() const noexcept -> bool;

    // ITree interface
    [[nodiscard]] auto rowCount() const noexcept -> std::size_t override;
    [[nodiscard]] auto child(std::size_t parentRow) const noexcept -> IInode* override;
    [[nodiscard]] auto parent() const noexcept -> IInode* override;
    [[nodiscard]] auto parentRow() const noexcept -> std::size_t override;

    // IInode interface
    [[nodiscard]] auto IntegerInterface() const noexcept -> IInteger* override;
    [[nodiscard]] auto FloatInterface() const noexcept -> IFloat* override;
    [[nodiscard]] auto EnumerationInterface() const noexcept -> IEnumeration* override;
    [[nodiscard]] auto CategoryInterface() const noexcept -> ICategory* override;
    [[nodiscard]] auto StringInterface() const noexcept -> IString* override;
    [[nodiscard]] auto CommandInterface() const noexcept -> ICommand* override;
    [[nodiscard]] auto BooleanInterface() const noexcept -> IBoolean* override;

    [[nodiscard]] auto tooltip() const noexcept -> std::string_view override;
    [[nodiscard]] auto type() const noexcept -> std::string_view override;
    [[nodiscard]] auto featureName() const noexcept -> std::string_view override;
    [[nodiscard]] auto displayName() const noexcept -> std::string_view override;
    [[nodiscard]] auto description() const noexcept -> std::string_view override;
    [[nodiscard]] auto visibility() const noexcept -> std::string_view override;
    [[nodiscard]] auto isReadable() const noexcept -> bool override;
    [[nodiscard]] auto isWritable() const noexcept -> bool override;
    [[nodiscard]] auto isLocked() const noexcept -> bool override;
    [[nodiscard]] auto isAvailable() const noexcept -> bool override;
    [[nodiscard]] auto representation() const noexcept -> Representation override;

private:
    const std::unique_ptr<InodePrivate> _impl;

}; // class GenICamXMLFile

} // namespace Jgv::Genicam2

#endif // GENICAMINODE_H
