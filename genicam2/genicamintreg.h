﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMINTREG_H
#define GENICAMINTREG_H

#include "genicamregister.h"

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Gvcp {
class Client;
} // namespace Jgv::Gvcp

namespace Jgv::Genicam2 {

class IntReg final : public IInteger, public Register {
    bool _isUnsigned { false };

public:
    explicit IntReg(const tinyxml2::XMLElement* element, std::shared_ptr<Gvcp::Client> client);
    ~IntReg() override = default;
    IntReg(const IntReg&) = delete;
    IntReg(IntReg&&) = delete;
    auto operator=(const IntReg&) -> IntReg& = delete;
    auto operator=(IntReg &&) -> IntReg& = delete;

    //void mapInterfaces(const InodeGetter& inodeGetter) noexcept;
    void initCache() noexcept;
    [[nodiscard]] auto access() const noexcept -> std::tuple<bool, bool>;

    auto value() -> int64_t override;
    void setValue(int64_t value) override;
    auto min() -> int64_t override;
    auto max() -> int64_t override;
    auto inc() -> int64_t override;
};

} // namespace Jgv::Genicam2

#endif // GENICAMINTREG_H
