/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicaminterface.h"
#include "genicaminterface_p.h"

#include <globalipc/globalipc.h>
#include <iostream>

namespace Jgv::Genicam2 {

std::string GenICamVariantToString::operator()(VariantError) const noexcept { return "VariantError"; }

std::string GenICamVariantToString::operator()(int64_t value) const noexcept { return std::to_string(value); }

std::string GenICamVariantToString::operator()(double value) const noexcept { return Jgv::toString(value); }

std::string GenICamVariantToString::operator()(bool value) const noexcept { return value ? "on" : "off"; }

std::string GenICamVariantToString::operator()(std::string_view value) const noexcept { return std::string { value }; }

std::string GenICamVariantToString::operator()(EnumerationEntry&& value) const noexcept { return std::string { std::get<1>(value) }; }

InterfacePrivate::InterfacePrivate(Channel::Type c, std::shared_ptr<GlobalIPC::Object>&& ipc)
    : channel { c }
    , globalIpc { std::move(ipc) }
{
    inodeGetter = [this](std::string_view name) -> Inode* {
        if (const auto it { inodes.find(name) }; it != inodes.cend()) {
            return &it->second;
        }
        return nullptr;
    };
    ;
}

void InterfacePrivate::initializeInodes(std::string_view content)
{
    const auto err { document.Parse(content.data(), content.size()) };
    if (err != tinyxml2::XMLError::XML_SUCCESS) {
        std::cerr << "GenICamXMLFile::setContent: failed " << err << std::endl;
        return;
    }

    const auto rootElement { document.RootElement() };
    if (rootElement != nullptr) {
        auto child { rootElement->FirstChildElement(XmlNode::Category.data()) };
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::Category, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::Category.data());
        }

        child = rootElement->FirstChildElement(XmlNode::Command.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::Command, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::Command.data());
        }

        child = rootElement->FirstChildElement(XmlNode::Integer.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::Integer, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::Integer.data());
        }

        child = rootElement->FirstChildElement(XmlNode::IntSwissKnife.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::IntSwissKnife, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::IntSwissKnife.data());
        }

        child = rootElement->FirstChildElement(XmlNode::IntReg.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::IntReg, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::IntReg.data());
        }

        child = rootElement->FirstChildElement(XmlNode::Enumeration.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::Enumeration, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::Enumeration.data());
        }

        child = rootElement->FirstChildElement(XmlNode::MaskedIntReg.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::MaskedIntReg, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::MaskedIntReg.data());
        }

        child = rootElement->FirstChildElement(XmlNode::Float.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::Float, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::Float.data());
        }

        child = rootElement->FirstChildElement(XmlNode::Converter.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::Converter, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::Converter.data());
        }

        child = rootElement->FirstChildElement(XmlNode::FloatReg.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::FloatReg, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::FloatReg.data());
        }

        child = rootElement->FirstChildElement(XmlNode::StringReg.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::StringReg, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::StringReg.data());
        }

        child = rootElement->FirstChildElement(XmlNode::Boolean.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::Boolean, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::Boolean.data());
        }

        child = rootElement->FirstChildElement(XmlNode::SwissKnife.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::SwissKnife, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::SwissKnife.data());
        }

        child = rootElement->FirstChildElement(XmlNode::IntConverter.data());
        while (child != nullptr) {
            inodes[child->Attribute(XmlAttribute::Name.data())].initializeType(InodeType::IntConverter, child, globalIpc->gvcpClient(channel));
            child = child->NextSiblingElement(XmlNode::IntConverter.data());
        }
    }
}

void InterfacePrivate::mapInterfaces() noexcept
{
    for (auto& inode : inodes) {
        inode.second.mapInterfaces(inodeGetter);
    }
    for (auto& inode : inodes) {
        inode.second.initStaticProperties();
    }
}

void InterfacePrivate::mapTreeInterface() noexcept
{
    if (auto root { inodes.find(XmlName::Root) }; root != inodes.cend()) {
        root->second.mapTreeInterface();
        if (root->second.isImplemented()) {
            root->second.setHierarchy(0, nullptr);
        }
    }
}

Interface::Interface(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl { std::make_unique<InterfacePrivate>(channel, std::move(globalIpc)) }
{
}

Interface::~Interface() = default;

void Interface::setXMLContent(std::string_view content)
{
    _impl->initializeInodes(content);
    _impl->mapInterfaces();
    _impl->mapTreeInterface();
}

auto Interface::getValue(std::string_view name) noexcept -> GenICamVariant
{
    return _impl->inodes[name].getValue();
}

void Interface::setValue(std::string_view name, GenICamVariant value) noexcept
{
    _impl->inodes[name].setValue(std::move(value));
}

auto Interface::limits(std::string_view name) noexcept -> GenicamLimits
{
    return _impl->inodes[name].limits();
}

auto Interface::inode(std::string_view name) -> IInode*
{
    if (auto inode { _impl->inodes.find(name) }; inode != _impl->inodes.cend()) {
        return &inode->second;
    }
    return nullptr;
}

} // namespace Jgv::Genicam2
