/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMTOKEN_H
#define GENICAMTOKEN_H

#include <string_view>

namespace Jgv::Genicam2 {

enum class TokenType {
    UNKNOW,
    LEFT_BRACKET,
    RIGHT_BRACKET,
    ADDITION,
    SUBSTRACTION,
    MULTIPLICATION,
    DIVISION,
    REMAINDER,
    POWER,
    BITWISE_AND,
    BITWISE_OR,
    BITWISE_XOR,
    BITWISE_NOT,
    LOGICAL_NOT_EQUAL,
    LOGICAL_EQUAL,
    LOGICAL_GREATER,
    LOGICAL_LESS,
    LOGICAL_LESS_OR_EQUAL,
    LOGICAL_GREATER_OR_EQUAL,
    LOGICAL_AND,
    LOGICAL_OR,
    SHIFT_LEFT,
    SHIFT_RIGHT,
    TERNARY_QUESTION_MARK,
    TERNARY_COLON,
    SGN,
    NEG,
    ATAN,
    COS,
    SIN,
    TAN,
    ABS,
    EXP,
    LN,
    SQRT,
    TRUNC,
    FLOOR,
    CEIL,
    ROUND,
    ASIN,
    ACOS,
    E,
    PI
};

constexpr auto toString(TokenType type) -> std::string_view
{
    switch (type) {
    case TokenType::UNKNOW:
        return "UNKNOW";
    case TokenType::LEFT_BRACKET:
        return "(";
    case TokenType::RIGHT_BRACKET:
        return ")";
    case TokenType::ADDITION:
        return "+";
    case TokenType::SUBSTRACTION:
        return "-";
    case TokenType::MULTIPLICATION:
        return "*";
    case TokenType::DIVISION:
        return "/";
    case TokenType::REMAINDER:
        return "%";
    case TokenType::POWER:
        return "**";
    case TokenType::BITWISE_AND:
        return "&";
    case TokenType::BITWISE_OR:
        return "|";
    case TokenType::BITWISE_XOR:
        return "^";
    case TokenType::BITWISE_NOT:
        return "~";
    case TokenType::LOGICAL_NOT_EQUAL:
        return "<>";
    case TokenType::LOGICAL_EQUAL:
        return "=";
    case TokenType::LOGICAL_GREATER:
        return ">";
    case TokenType::LOGICAL_LESS:
        return "<";
    case TokenType::LOGICAL_LESS_OR_EQUAL:
        return "<=";
    case TokenType::LOGICAL_GREATER_OR_EQUAL:
        return ">=";
    case TokenType::LOGICAL_AND:
        return "&&";
    case TokenType::LOGICAL_OR:
        return "||";
    case TokenType::SHIFT_LEFT:
        return "<<";
    case TokenType::SHIFT_RIGHT:
        return ">>";
    case TokenType::TERNARY_QUESTION_MARK:
        return "?";
    case TokenType::TERNARY_COLON:
        return ":";
    case TokenType::SGN:
        return "SGN";
    case TokenType::NEG:
        return "NEG";
    case TokenType::ATAN:
        return "ATAN";
    case TokenType::COS:
        return "COS";
    case TokenType::SIN:
        return "SIN";
    case TokenType::TAN:
        return "TAN";
    case TokenType::ABS:
        return "ABS";
    case TokenType::EXP:
        return "EXP";
    case TokenType::LN:
        return "LN";
    case TokenType::SQRT:
        return "SQRT";
    case TokenType::TRUNC:
        return "TRUNC";
    case TokenType::FLOOR:
        return "FLOOR";
    case TokenType::CEIL:
        return "CEIL";
    case TokenType::ROUND:
        return "ROUND";
    case TokenType::ASIN:
        return "ASIN";
    case TokenType::ACOS:
        return "ACOS";
    case TokenType::E:
        return "E";
    case TokenType::PI:
        return "PI";
    }
    return "UNKNOW";
}

enum class Associativity {
    LeftToRight,
    RightToLeft
};

struct TokenInfo {
    int precedence;
    Associativity associativity;
    unsigned operandsCount;
};

class Token final {
    TokenType _type { TokenType::UNKNOW };
    TokenInfo _info {};

public:
    explicit Token(TokenType type) noexcept;
    [[nodiscard]] auto type() const noexcept -> TokenType;
    [[nodiscard]] auto isNumber() const noexcept -> bool;
    [[nodiscard]] auto isOperator() const noexcept -> bool;
    [[nodiscard]] auto isLeftParenthesis() const noexcept -> bool;
    [[nodiscard]] auto isRightParenthesis() const noexcept -> bool;
    [[nodiscard]] auto isLeftAssociative() const noexcept -> bool;
    [[nodiscard]] auto operandsCount() const noexcept -> unsigned;
    [[nodiscard]] auto precedence() const noexcept -> int;
};

} // namespace Jgv::Genicam2

#endif // GENICAMTOKEN_H
