/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMINODE_P_H
#define GENICAMINODE_P_H

#include "internal.h"

#include <memory>
#include <vector>

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Genicam2 {
class Inode;
class Category;
class Command;
class Integer;
class IntSwissKnife;
class IntReg;
class MaskedIntReg;
class Enumeration;
class Float;
class FloatReg;
class Converter;
class StringReg;
class Boolean;
class SwissKnife;
class IntConverter;

struct InodePrivate {
    InodeType type;
    const tinyxml2::XMLElement* xmlElement { nullptr };

    std::unique_ptr<Category> categoryNode;
    std::unique_ptr<Command> commandNode;
    std::unique_ptr<Integer> integerNode;
    std::unique_ptr<IntSwissKnife> intswissknifeNode;
    std::unique_ptr<IntReg> intregNode;
    std::unique_ptr<MaskedIntReg> maskedintregNode;
    std::unique_ptr<Enumeration> enumerationNode;
    std::unique_ptr<Float> floatNode;
    std::unique_ptr<FloatReg> floatregNode;
    std::unique_ptr<Converter> converterNode;
    std::unique_ptr<StringReg> stringRegNode;
    std::unique_ptr<Boolean> booleanNode;
    std::unique_ptr<SwissKnife> swissknifeNode;
    std::unique_ptr<IntConverter> intconverterNode;

    Inode* parent { nullptr };
    Inode* pValue { nullptr };

    IInteger* iinteger { nullptr };
    IFloat* ifloat { nullptr };
    IInteger* isImplemented { nullptr };
    IInteger* isLocked { nullptr };
    IInteger* isAvailable { nullptr };
    IInvalidate* invalidate { nullptr };

    Representation representation {};
    bool canRead { true };
    bool canWrite { true };
};

} // namespace Jgv::Genicam2

#endif // GENICAMINODE_P_H
