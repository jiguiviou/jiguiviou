/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamtoken.h"

#include <array>

namespace {

constexpr std::array<Jgv::Genicam2::TokenInfo, 43> Infos {
    Jgv::Genicam2::TokenInfo { 0, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // UNKNOW
    Jgv::Genicam2::TokenInfo { -1, Jgv::Genicam2::Associativity::LeftToRight, 0 }, // LEFT_BRACKET
    Jgv::Genicam2::TokenInfo { 990, Jgv::Genicam2::Associativity::LeftToRight, 0 }, // RIGHT_BRACKET
    Jgv::Genicam2::TokenInfo { 100, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // ADDITION
    Jgv::Genicam2::TokenInfo { 100, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // SUBSTRACTION
    Jgv::Genicam2::TokenInfo { 110, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // MULTIPLICATION
    Jgv::Genicam2::TokenInfo { 110, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // DIVISION
    Jgv::Genicam2::TokenInfo { 110, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // REMAINDER
    Jgv::Genicam2::TokenInfo { 120, Jgv::Genicam2::Associativity::RightToLeft, 2 }, // POWER
    Jgv::Genicam2::TokenInfo { 60, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // BITWISE_AND
    Jgv::Genicam2::TokenInfo { 40, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // BITWISE_OR
    Jgv::Genicam2::TokenInfo { 50, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // BITWISE_XOR
    Jgv::Genicam2::TokenInfo { 30, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // BITWISE_NOT
    Jgv::Genicam2::TokenInfo { 70, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // LOGICAL_NOT_EQUAL
    Jgv::Genicam2::TokenInfo { 70, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // LOGICAL_EQUAL
    Jgv::Genicam2::TokenInfo { 80, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // LOGICAL_GREATER
    Jgv::Genicam2::TokenInfo { 80, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // LOGICAL_LESS
    Jgv::Genicam2::TokenInfo { 80, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // LOGICAL_LESS_OR_EQUAL
    Jgv::Genicam2::TokenInfo { 80, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // LOGICAL_GREATER_OR_EQUAL
    Jgv::Genicam2::TokenInfo { 20, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // LOGICAL_AND
    Jgv::Genicam2::TokenInfo { 10, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // LOGICAL_OR
    Jgv::Genicam2::TokenInfo { 90, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // SHIFT_LEFT
    Jgv::Genicam2::TokenInfo { 90, Jgv::Genicam2::Associativity::LeftToRight, 2 }, // SHIFT_RIGHT
    Jgv::Genicam2::TokenInfo { 5, Jgv::Genicam2::Associativity::RightToLeft, 3 }, // TERNARY_QUESTION_MARK
    Jgv::Genicam2::TokenInfo { 5, Jgv::Genicam2::Associativity::RightToLeft, 1 }, // TERNARY_COLON
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // SGN
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // NEG
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // ATAN
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // COS
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // SIN
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // TAN
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // ABS
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // EXP
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // LN
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // SQRT
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // TRUNC
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // FLOOR
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // CEIL
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // ROUND
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // ASIN
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 1 }, // ACOS
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 0 }, // E
    Jgv::Genicam2::TokenInfo { 200, Jgv::Genicam2::Associativity::LeftToRight, 0 } // PI

};

} // namespace

namespace Jgv::Genicam2 {

Token::Token(TokenType type) noexcept
    : _type(type)
    , _info(Infos[static_cast<std::size_t>(type)]) // NOLINT(cppcoreguidelines-pro-bounds-constant-array-index)
{
}

auto Token::type() const noexcept -> TokenType
{
    return _type;
}

auto Token::isNumber() const noexcept -> bool
{
    return (_type >= TokenType::E);
}

auto Token::isOperator() const noexcept -> bool
{
    return (_type >= TokenType::ADDITION) && (_type <= TokenType::ACOS);
}

auto Token::isLeftParenthesis() const noexcept -> bool
{
    return (_type == TokenType::LEFT_BRACKET);
}

auto Token::isRightParenthesis() const noexcept -> bool
{
    return (_type == TokenType::RIGHT_BRACKET);
}

auto Token::isLeftAssociative() const noexcept -> bool
{
    return (_info.associativity == Associativity::LeftToRight);
}

auto Token::operandsCount() const noexcept -> unsigned
{
    return _info.operandsCount;
}

auto Token::precedence() const noexcept -> int
{
    return _info.precedence;
}

} // namespace Jgv::Genicam2
