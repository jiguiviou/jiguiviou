﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamaddress.h"

#include "genicaminode.h"
#include "genicamintswissknife.h"

#include <tinyxml2.h>

namespace Jgv::Genicam2 {

pIndex::pIndex(const tinyxml2::XMLElement* child, const InodeGetter& inodeGetter)
{
    if (const auto* inode { inodeGetter(child->GetText()) }; inode != nullptr) {
        if (auto *interface { inode->IntegerInterface() }; interface != nullptr) {
            _offset = toValue<int64_t>(child->Attribute(XmlAttribute::Offset.data()));
            _inode = interface;
        }
    }
}

auto pIndex::value() noexcept -> int64_t
{
    if (_inode != nullptr) {
        return _offset * _inode->value();
    }
    return 0;
}

Address::Address(const tinyxml2::XMLElement* element)
    : _xmlElement { element }
{
    const auto *child { element->FirstChildElement(XmlNode::Address.data()) };
    while (child != nullptr) {
        _address += toValue<uint32_t>(child->GetText());
        child = _xmlElement->NextSiblingElement(XmlNode::Address.data());
    }

    child = element->FirstChildElement(XmlNode::IntSwissKnife.data());
    while (child != nullptr) {
        _intSwissKnife.emplace_back(std::make_unique<IntSwissKnife>(child));
        child = _xmlElement->NextSiblingElement(XmlNode::IntSwissKnife.data());
    }
}

Address::~Address() = default;

void Address::mapInterfaces(const InodeGetter& inodeGetter) noexcept
{
    const auto *child { _xmlElement->FirstChildElement(XmlNode::pAddress.data()) };
    while (child != nullptr) {
        if (auto *const inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            if (auto *const interface { inode->IntegerInterface() }; interface != nullptr) {
                _addresses.emplace_back(interface);
            }
            child = _xmlElement->NextSiblingElement(XmlNode::pAddress.data());
        }
    }

    child = _xmlElement->FirstChildElement(XmlNode::pIndex.data());
    while (child != nullptr) {
        _indexes.emplace_back(std::make_unique<pIndex>(child, inodeGetter));
        child = _xmlElement->NextSiblingElement(XmlNode::pIndex.data());
    }

    for (const auto& intSwissKnife : _intSwissKnife) {
        intSwissKnife->mapInterfaces(inodeGetter);
    }
}

auto Address::value() noexcept -> uint32_t
{
    auto address { _address };

    auto addValue = [&address](const auto& p) {
        address += p->value();
    };

    std::for_each(_addresses.cbegin(), _addresses.cend(), addValue);
    std::for_each(_intSwissKnife.cbegin(), _intSwissKnife.cend(), addValue);
    std::for_each(_indexes.begin(), _indexes.end(), addValue);

    return address;
}

} // namespace Jgv::Genicam2
