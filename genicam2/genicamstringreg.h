/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMSTRINGREG_H
#define GENICAMSTRINGREG_H

#include "genicamregister.h"
#include "internal.h"

namespace tinyxml2 {
class XMLElement;
} // namespace tinyxml2

namespace Jgv::Gvcp {
class Client;
} // namespace Jgv::Gvcp

namespace Jgv::Genicam2 {

class Inode;
constexpr std::size_t STRING_MAX_SIZE { 128 };
class StringReg final : public IString, public Register {

public:
    explicit StringReg(const tinyxml2::XMLElement* element, std::shared_ptr<Gvcp::Client> client);
    ~StringReg() override = default;
    StringReg(const StringReg&) = delete;
    StringReg(StringReg&&) = delete;
    auto operator=(const StringReg&) -> StringReg& = delete;
    auto operator=(StringReg &&) -> StringReg& = delete;

    //void mapInterfaces(const InodeGetter& inodeGetter) noexcept;
    void initCache() noexcept;
    [[nodiscard]] auto access() const noexcept -> std::tuple<bool, bool>;

    [[nodiscard]] auto value() noexcept -> std::string override;
    void setValue(std::string_view value) noexcept override;
    [[nodiscard]] auto maxLength() noexcept -> int64_t override;
};

} // namespace Jgv::Genicam2

#endif // GENICAMSTRINGREG_H
