/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamintreg.h"

#include "genicaminode.h"
#include "internal.h"

#include <gvcpdevices/gvcpclient.h>
#include <iostream>
#include <limits>
#include <tinyxml2.h>

namespace {

template <typename T>
void toUnaligned(const T src, void* dest) noexcept
{
    __builtin_memcpy(dest, &src, sizeof(T));
}

template <typename T>
void fromUnaligned(const void* src, void* dest) noexcept
{
    __builtin_memcpy(dest, src, sizeof(T));
}

void littleEndian32ToCache(uint32_t value, std::span<uint8_t> cache) noexcept
{
    toUnaligned<uint32_t>(__builtin_bswap32(value), cache.data());
}

void littleEndian64ToCache(int64_t value, std::span<uint8_t> cache) noexcept
{
    toUnaligned<int64_t>(__builtin_bswap64(value), cache.data());
}

auto fromBigEndian64Cache(std::span<uint8_t> cache) noexcept -> int64_t
{
    int64_t out { 0 };
    fromUnaligned<int64_t>(cache.data(), &out);
    return __builtin_bswap64(out);
}

auto fromBigEndian32Cache(std::span<uint8_t> cache) noexcept -> uint32_t
{
    uint32_t out { 0 };
    fromUnaligned<uint32_t>(cache.data(), &out);
    return __builtin_bswap32(out);
}

} // namespace

namespace Jgv::Genicam2 {

IntReg::IntReg(const tinyxml2::XMLElement* element, std::shared_ptr<Gvcp::Client> client)
    : Register { element, std::move(client) }
{
    if (const auto child { element->FirstChildElement(XmlNode::Sign.data()) }; child != nullptr) {
        _isUnsigned = (child->GetText() == XmlValue::Unsigned);
    }
}

//void IntReg::mapInterfaces(const InodeGetter& inodeGetter) noexcept
//{
//    Register::mapInterfaces(inodeGetter);
//}

void IntReg::initCache() noexcept
{
    _cache.resize(_length.value());
}

auto IntReg::access() const noexcept -> std::tuple<bool, bool>
{
    return { _canRead, _canWrite };
}

auto IntReg::value() -> int64_t
{
    if (!_canRead) {
        std::cerr << "Genicam::IntReg::Value " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " is write only !\n";
        return 0;
    }

    if (_length.value() == sizeof(uint32_t)) {
        if (_cachable == Cachable::NoCache) {
            if (auto buffer { _client->readMemory(_address.value(), sizeof(uint32_t)) }; !buffer.empty()) {
                return _isUnsigned ? fromBigEndian32Cache(buffer) : static_cast<int32_t>(fromBigEndian32Cache(buffer));
            }
            std::cerr << "Genicam2::IntReg::value() " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " buffer from device is empty !" << std::endl;
            return 0;
        }
        if (!_cacheIsValid) {
            updateCache();
        }
        return _isUnsigned ? fromBigEndian32Cache(_cache) : static_cast<int32_t>(fromBigEndian32Cache(_cache));
    }

    if (_length.value() == sizeof(uint64_t)) {
        if (_cachable == Cachable::NoCache) {
            if (auto buffer { _client->readMemory(_address.value(), sizeof(uint64_t)) }; !buffer.empty()) {
                return fromBigEndian64Cache(buffer);
            }
            std::cerr << "Genicam2::IntReg::value() " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " buffer from device is empty !" << std::endl;
            return 0;
        }
        if (!_cacheIsValid) {
            updateCache();
        }
        return fromBigEndian64Cache(_cache);
    }

    //    std::cerr << "Genicam::IntReg::Value " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " bad length " << _length.value() << std::endl;
    return 0;
}

void IntReg::setValue(int64_t value)
{
    if (!_canWrite) {
        std::cerr << "Genicam::IntReg::setValue " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " is read only !\n";
        return;
    }
    if (_length.value() == sizeof(uint32_t)) {
        if (_cachable != Cachable::WriteThrough) {
            std::array<uint8_t, sizeof(uint32_t)> buffer {};
            if (_isUnsigned) {
                littleEndian32ToCache(static_cast<uint32_t>(value), buffer);
            } else {
                littleEndian32ToCache(static_cast<int32_t>(value), buffer);
            }
            _client->writeMemory(_address.value(), buffer);
            _cacheIsValid.reset();
        } else {
            if (_isUnsigned) {
                littleEndian32ToCache(static_cast<uint32_t>(value), _cache);
            } else {
                littleEndian32ToCache(static_cast<int32_t>(value), _cache);
            }
            writeCache();
        }
    } else if (_length.value() == sizeof(uint64_t)) {
        if (_cachable != Cachable::WriteThrough) {
            std::array<uint8_t, sizeof(uint64_t)> buffer {};
            littleEndian64ToCache(static_cast<uint32_t>(value), buffer);
            _client->writeMemory(_address.value(), buffer);
            _cacheIsValid.reset();
        } else {
            littleEndian64ToCache(value, _cache);
            writeCache();
        }
    }
    std::for_each(_invalideFunctions.cbegin(), _invalideFunctions.cend(), [](const auto& invalidate) {
        invalidate();
    });
}

auto IntReg::min() -> int64_t
{
    return std::numeric_limits<int64_t>::lowest();
}
auto IntReg::max() -> int64_t
{
    return std::numeric_limits<int64_t>::max();
}
auto IntReg::inc() -> int64_t
{
    return 1;
}

} // namespace Jgv::Genicam2
