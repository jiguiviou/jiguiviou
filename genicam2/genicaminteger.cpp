﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicaminteger.h"

#include "genicaminode.h"
#include "internal.h"

#include <iostream>
#include <tinyxml2.h>

namespace Jgv::Genicam2 {

Integer::Integer(const tinyxml2::XMLElement* element)
    : _xmlElement(element)
{
    if (const auto child { _xmlElement->FirstChildElement(XmlNode::Value.data()) }; child != nullptr) {
        _value.initValue(toValue<int64_t>(child->GetText()));
    }

    if (const auto child { _xmlElement->FirstChildElement(XmlNode::Min.data()) }; child != nullptr) {
        _min.initValue(toValue<int64_t>(child->GetText()));
    }

    if (const auto child { _xmlElement->FirstChildElement(XmlNode::Max.data()) }; child != nullptr) {
        _max.initValue(toValue<int64_t>(child->GetText()));
    }

    if (const auto child { _xmlElement->FirstChildElement(XmlNode::Inc.data()) }; child != nullptr) {
        _inc.initValue(toValue<int64_t>(child->GetText()));
    }
}

auto Integer::mapInterfaces(const InodeGetter& inodeGetter) noexcept -> Inode*
{
    if (const auto child { _xmlElement->FirstChildElement(XmlNode::pMin.data()) }; child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _min.setInterface(inode->IntegerInterface());
        }
    }

    if (const auto child { _xmlElement->FirstChildElement(XmlNode::pMax.data()) }; child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _max.setInterface(inode->IntegerInterface());
        }
    }

    if (const auto child { _xmlElement->FirstChildElement(XmlNode::pInc.data()) }; child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _inc.setInterface(inode->IntegerInterface());
        }
    }

    if (const auto child { _xmlElement->FirstChildElement(XmlNode::pValue.data()) }; child != nullptr) {
        if (const auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _value.setInterface(inode->IntegerInterface());
            return inode;
        }
    }

    return nullptr;
}

void Integer::addInvalidateFunction(std::function<void(void)>&& function) noexcept
{
    _invalideFunctions.emplace_back(std::move(function));
}

auto Integer::value() -> int64_t
{
    return _value.value();
}

void Integer::setValue(int64_t value)
{
    _value.setValue(value);
    std::for_each(_invalideFunctions.cbegin(), _invalideFunctions.cend(), [](const auto& invalidate) {
        invalidate();
    });
}
auto Integer::min() -> int64_t
{
    return _min.value();
}
auto Integer::max() -> int64_t
{
    return _max.value();
}
auto Integer::inc() -> int64_t
{
    return _inc.value();
}

} // namespace Jgv::Genicam2
