﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicaminode.h"
#include "genicaminode_p.h"

#include "genicamboolean.h"
#include "genicamcategory.h"
#include "genicamcommand.h"
#include "genicamconverter.h"
#include "genicamenumeration.h"
#include "genicamfloat.h"
#include "genicamfloatreg.h"
#include "genicamintconverter.h"
#include "genicaminteger.h"
#include "genicamintreg.h"
#include "genicamintswissknife.h"
#include "genicammaskedintreg.h"
#include "genicamstringreg.h"
#include "genicamswissknife.h"

#include <iostream>
#include <tinyxml2.h>

namespace Jgv::Genicam2 {

Inode::Inode()
    : _impl { std::make_unique<InodePrivate>() }
{
}

Inode::~Inode() = default;

void Inode::initializeType(InodeType type, const tinyxml2::XMLElement* element, const std::shared_ptr<Gvcp::Client>& gvcpClient) noexcept
{
    if (const auto child { element->FirstChildElement(XmlNode::Representation.data()) }; child != nullptr) {
        _impl->representation = toRepresentation(child->GetText());
    }

    _impl->type = type;
    _impl->xmlElement = element;
    switch (_impl->type) {
    case InodeType::Category:
        _impl->categoryNode = std::make_unique<Category>(element);
        break;
    case InodeType::Command:
        _impl->commandNode = std::make_unique<Command>(element);
        _impl->invalidate = _impl->commandNode.get();
        break;
    case InodeType::Integer:
        _impl->integerNode = std::make_unique<Integer>(element);
        _impl->iinteger = _impl->integerNode.get();
        _impl->invalidate = _impl->integerNode.get();
        break;
    case InodeType::IntSwissKnife:
        _impl->intswissknifeNode = std::make_unique<IntSwissKnife>(element);
        _impl->iinteger = _impl->intswissknifeNode.get();
        break;
    case InodeType::IntReg:
        _impl->intregNode = std::make_unique<IntReg>(element, gvcpClient);
        _impl->iinteger = _impl->intregNode.get();
        _impl->invalidate = _impl->intregNode.get();
        break;
    case InodeType::MaskedIntReg:
        _impl->maskedintregNode = std::make_unique<MaskedIntReg>(element, gvcpClient);
        _impl->iinteger = _impl->maskedintregNode.get();
        _impl->invalidate = _impl->maskedintregNode.get();
        break;
    case InodeType::Enumeration:
        _impl->enumerationNode = std::make_unique<Enumeration>(element);
        _impl->invalidate = _impl->enumerationNode.get();
        break;
    case InodeType::Float:
        _impl->floatNode = std::make_unique<Float>(element);
        _impl->ifloat = _impl->floatNode.get();
        _impl->invalidate = _impl->floatNode.get();
        break;
    case InodeType::Converter:
        _impl->converterNode = std::make_unique<Converter>(element);
        _impl->ifloat = _impl->converterNode.get();
        _impl->invalidate = _impl->converterNode.get();
        break;
    case InodeType::FloatReg:
        _impl->floatregNode = std::make_unique<FloatReg>(element, gvcpClient);
        _impl->ifloat = _impl->floatregNode.get();
        _impl->invalidate = _impl->floatregNode.get();
        break;
    case InodeType::StringReg:
        _impl->stringRegNode = std::make_unique<StringReg>(element, gvcpClient);
        _impl->invalidate = _impl->stringRegNode.get();
        break;
    case InodeType::Boolean:
        _impl->booleanNode = std::make_unique<Boolean>(element);
        _impl->invalidate = _impl->booleanNode.get();
        break;
    case InodeType::SwissKnife:
        _impl->swissknifeNode = std::make_unique<SwissKnife>(element);
        _impl->ifloat = _impl->swissknifeNode.get();
        break;
    case InodeType::IntConverter:
        _impl->intconverterNode = std::make_unique<IntConverter>(element);
        _impl->iinteger = _impl->intconverterNode.get();
        _impl->invalidate = _impl->intconverterNode.get();
        break;
    }
}

auto Inode::getValue() noexcept -> GenICamVariant
{
    switch (_impl->type) {
    case InodeType::Integer:
        return _impl->integerNode->value();
    case InodeType::IntSwissKnife:
        return _impl->intswissknifeNode->value();
    case InodeType::Enumeration: {
        if (const auto current { _impl->enumerationNode->currentEntry() }; current != nullptr) {
            return std::make_tuple(current->index(), current->name());
        }
        return std::make_tuple(0, "");
    }
    case InodeType::Float:
        return _impl->floatNode->value();
    case InodeType::Converter:
        return _impl->converterNode->value();
    case InodeType::FloatReg:
        return _impl->floatregNode->value();
    case InodeType::StringReg:
        return _impl->stringRegNode->value();
    case InodeType::Boolean:
        return _impl->booleanNode->value();
    case InodeType::SwissKnife:
        return _impl->swissknifeNode->value();
    case InodeType::IntConverter:
        return _impl->intconverterNode->value();
    default:
        std::cerr << "GenICam2::Inode::getValue: failed on InodeType " << toString(_impl->type) << std::endl;
        return {};
    }
}

void Inode::setValue(GenICamVariant value) noexcept
{
    switch (_impl->type) {
    case InodeType::Command:
        _impl->commandNode->execute();
        break;
    case InodeType::Integer:
        if (auto pVal = std::get_if<int64_t>(&value)) {
            _impl->integerNode->setValue(*pVal);
        }
        break;
    case InodeType::IntSwissKnife:
        if (auto pVal = std::get_if<int64_t>(&value)) {
            _impl->intswissknifeNode->setValue(*pVal);
        }
        break;
    case InodeType::Enumeration:
        if (auto pVal = std::get_if<int64_t>(&value)) {
            _impl->enumerationNode->setValue(*pVal);
        } else if (auto pVal = std::get_if<std::string_view>(&value)) {
            if (const auto entry { _impl->enumerationNode->entryByName(*pVal) }; entry != nullptr) {
                _impl->enumerationNode->setValue(entry->index());
            }
        }
        break;
    case InodeType::Float:
        if (auto pVal = std::get_if<double>(&value)) {
            _impl->floatNode->setValue(*pVal);
        }
        break;
    case InodeType::Converter:
        if (auto pVal = std::get_if<double>(&value)) {
            _impl->converterNode->setValue(*pVal);
        }
        break;
    case InodeType::FloatReg:
        if (auto pVal = std::get_if<double>(&value)) {
            _impl->floatregNode->setValue(*pVal);
        }
        break;
    case InodeType::StringReg:
        if (auto pVal = std::get_if<std::string_view>(&value)) {
            _impl->stringRegNode->setValue(*pVal);
        }
        break;
    case InodeType::Boolean:
        if (auto pVal = std::get_if<bool>(&value)) {
            _impl->booleanNode->setValue(*pVal);
        }
        break;
    case InodeType::SwissKnife:
        if (auto pVal = std::get_if<double>(&value)) {
            _impl->swissknifeNode->setValue(*pVal);
        }
        break;
    case InodeType::IntConverter:
        if (auto pVal = std::get_if<int64_t>(&value)) {
            _impl->intconverterNode->setValue(*pVal);
        }
        break;

    default:
        std::cerr << "Inode::setValue() " << toString(_impl->type) << std::endl;
    }
}

auto Inode::limits() noexcept -> GenicamLimits
{
    switch (_impl->type) {
    case InodeType::Integer:
        return std::make_tuple(_impl->integerNode->min(), _impl->integerNode->max(), _impl->integerNode->inc());
    case InodeType::Float:
        return std::make_tuple(_impl->floatNode->min(), _impl->floatNode->max(), _impl->floatNode->inc());

    default:
        std::cerr << "GenICam2::Inode::getLimits: failed on InodeType " << toString(_impl->type) << std::endl;
        return {};
    }
}

auto Inode::access() const noexcept -> std::tuple<bool, bool>
{
    if (const auto child { _impl->xmlElement->FirstChildElement(XmlNode::ImposedAccessMode.data()) }; child != nullptr) {
        const auto access { child->GetText() };
        return std::make_tuple(access != XmlValue::WO, access != XmlValue::RO);
    }

    switch (_impl->type) {

    case InodeType::Category:
    case InodeType::IntSwissKnife:
    case InodeType::SwissKnife:
        return std::make_tuple(true, false);

    case InodeType::IntReg:
        return _impl->intregNode->access();
    case InodeType::MaskedIntReg:
        return _impl->maskedintregNode->access();
    case InodeType::FloatReg:
        return _impl->floatregNode->access();
    case InodeType::StringReg:
        return _impl->stringRegNode->access();

    case InodeType::Command:
    case InodeType::Enumeration:
    case InodeType::Integer:
    case InodeType::IntConverter:
    case InodeType::Float:
    case InodeType::Converter:
    case InodeType::Boolean:
        if (_impl->pValue != nullptr) {
            return _impl->pValue->access();
        }
        return std::make_tuple(true, true);

    default:
        std::cerr << "Inode::mapInterfaces() " << toString(_impl->type) << std::endl;
        return std::make_tuple(false, false);
    }
}

void Inode::mapInterfaces(const InodeGetter& inodeGetter) noexcept
{
    switch (_impl->type) {
    case InodeType::Category:
        _impl->categoryNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::Command:
        _impl->pValue = _impl->commandNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::Integer:
        _impl->pValue = _impl->integerNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::IntSwissKnife:
        _impl->intswissknifeNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::IntReg:
        _impl->intregNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::MaskedIntReg:
        _impl->maskedintregNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::Enumeration:
        _impl->pValue = _impl->enumerationNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::Float:
        _impl->pValue = _impl->floatNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::Converter:
        _impl->pValue = _impl->converterNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::FloatReg:
        _impl->floatregNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::StringReg:
        _impl->stringRegNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::Boolean:
        _impl->pValue = _impl->booleanNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::SwissKnife:
        _impl->swissknifeNode->mapInterfaces(inodeGetter);
        break;
    case InodeType::IntConverter:
        _impl->pValue = _impl->intconverterNode->mapInterfaces(inodeGetter);
        break;
    default:
        std::cerr << "Inode::mapInterfaces() " << toString(_impl->type) << std::endl;
    }

    if (const auto child { _impl->xmlElement->FirstChildElement(XmlNode::pIsImplemented.data()) }; child != nullptr) {
        if (auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _impl->isImplemented = inode->IntegerInterface();
        }
    }

    if (const auto child { _impl->xmlElement->FirstChildElement(XmlNode::pIsLocked.data()) }; child != nullptr) {
        if (auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _impl->isLocked = inode->IntegerInterface();
        }
    }

    if (const auto child { _impl->xmlElement->FirstChildElement(XmlNode::pIsAvailable.data()) }; child != nullptr) {
        if (auto inode { inodeGetter(child->GetText()) }; inode != nullptr) {
            _impl->isAvailable = inode->IntegerInterface();
        }
    }
}

void Inode::initStaticProperties() noexcept
{

    if (_impl->intregNode) {
        _impl->intregNode->initCache();
    }
    if (_impl->maskedintregNode) {
        _impl->maskedintregNode->initCache();
    }
    if (_impl->floatregNode) {
        _impl->floatregNode->initCache();
    }
    if (_impl->stringRegNode) {
        _impl->stringRegNode->initCache();
    }

    const auto [readable, writable] { access() };
    _impl->canRead = readable;
    _impl->canWrite = writable;
}

void Inode::mapTreeInterface() noexcept
{
    if (_impl->categoryNode) {
        _impl->categoryNode->mapFeatures();
    } else if (_impl->enumerationNode) {
        _impl->enumerationNode->mapEntries();
    }
}

void Inode::setHierarchy(std::size_t row, Inode* parent)
{
    if (isCategoryNode()) {
        _impl->categoryNode->setHierarchy(row, this);
    }
    _impl->parent = parent;
}

void Inode::addInvalidateFunction(std::function<void(void)> function) noexcept
{
    if (_impl->invalidate != nullptr) {
        _impl->invalidate->addInvalidateFunction(std::move(function));
    } else {
        std::cerr << "Genicam::Inode::addInvalidateFunction: failed inode is not IInvalidate " << displayName() << std::endl;
    }
}

auto Inode::row() const noexcept -> std::size_t
{
    if (isCategoryNode()) {
        return _impl->categoryNode->row();
    }
    return 0U;
}

auto Inode::isCategoryNode() const noexcept -> bool
{
    return _impl->type == InodeType::Category;
}

auto Inode::isImplemented() const noexcept -> bool
{
    if (isCategoryNode()) {
        return _impl->categoryNode->isImplemented();
    }
    return (_impl->isImplemented != nullptr) ? (_impl->isImplemented->value() != 0) : true;
}

auto Inode::rowCount() const noexcept -> std::size_t
{
    if (_impl->categoryNode) {
        return _impl->categoryNode->featuresCount();
    }
    return 0;
}

auto Inode::child(std::size_t row) const noexcept -> IInode*
{
    if (_impl->categoryNode) {
        return _impl->categoryNode->feature(row);
    }
    return nullptr;
}

auto Inode::parent() const noexcept -> IInode*
{
    return _impl->parent;
}

auto Inode::parentRow() const noexcept -> std::size_t
{
    if (_impl->parent != nullptr) {
        return _impl->parent->row();
    }
    return 0;
}

auto Inode::IntegerInterface() const noexcept -> IInteger*
{
    return _impl->iinteger;
}

auto Inode::FloatInterface() const noexcept -> IFloat*
{
    return _impl->ifloat;
}

auto Inode::EnumerationInterface() const noexcept -> IEnumeration*
{
    return _impl->enumerationNode.get();
}

auto Inode::CategoryInterface() const noexcept -> ICategory*
{
    return _impl->categoryNode.get();
}

auto Inode::StringInterface() const noexcept -> IString*
{
    return _impl->stringRegNode.get();
}

auto Inode::CommandInterface() const noexcept -> ICommand*
{
    return _impl->commandNode.get();
}

auto Inode::BooleanInterface() const noexcept -> IBoolean*
{
    return _impl->booleanNode.get();
}

auto Inode::tooltip() const noexcept -> std::string_view
{
    if (const auto child { _impl->xmlElement->FirstChildElement(XmlNode::ToolTip.data()) }; child != nullptr) {
        return child->GetText();
    }
    return {};
}

auto Inode::type() const noexcept -> std::string_view
{
    return _impl->xmlElement->Name();
}

std::string_view Inode::featureName() const noexcept
{
    return _impl->xmlElement->Attribute(XmlAttribute::Name.data());
}

auto Inode::displayName() const noexcept -> std::string_view
{
    if (const auto child { _impl->xmlElement->FirstChildElement(XmlNode::DisplayName.data()) }; child != nullptr) {
        return child->GetText();
    }
    return featureName();
}

auto Inode::description() const noexcept -> std::string_view
{
    if (const auto child { _impl->xmlElement->FirstChildElement(XmlNode::Description.data()) }; child != nullptr) {
        return child->GetText();
    }
    return {};
}

auto Inode::visibility() const noexcept -> std::string_view
{
    if (const auto child { _impl->xmlElement->FirstChildElement(XmlNode::Visibility.data()) }; child != nullptr) {
        return child->GetText();
    }
    return XmlValue::Beginner;
}

auto Inode::isReadable() const noexcept -> bool
{
    return _impl->canRead;
}

auto Inode::isWritable() const noexcept -> bool
{
    return _impl->canWrite;
}

auto Inode::isLocked() const noexcept -> bool
{
    if (_impl->isLocked != nullptr) {
        return _impl->isLocked->value() != 0;
    }
    return false;
}

auto Inode::isAvailable() const noexcept -> bool
{
    if (_impl->isAvailable != nullptr) {
        return _impl->isAvailable->value() != 0;
    }
    return true;
}

Representation Inode::representation() const noexcept
{
    return _impl->representation;
}

} // namespace Jgv::Genicam2
