/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicammaskedintreg.h"

#include "genicaminode.h"
#include "internal.h"

#include <gvcpdevices/gvcpclient.h>
#include <iostream>
#include <limits>
#include <tinyxml2.h>

namespace {
constexpr uint UINT32_LSB { 31U };
constexpr auto MASK(uint size) -> uint64_t
{
    return ~(std::numeric_limits<uint64_t>::max() << size);
}

template <typename T>
void fromUnaligned(const void* src, void* dest) noexcept
{
    __builtin_memcpy(dest, src, sizeof(T));
}

template <typename T>
void toUnaligned(const T src, void* dest) noexcept
{
    __builtin_memcpy(dest, &src, sizeof(T));
}

auto fromBigEndian32Cache(std::span<uint8_t> cache) noexcept -> uint32_t
{
    uint32_t out { 0 };
    fromUnaligned<uint32_t>(cache.data(), &out);
    return __builtin_bswap32(out);
}

void littleEndian32ToCache(uint32_t value, std::span<uint8_t> cache) noexcept
{
    toUnaligned<uint32_t>(__builtin_bswap32(value), cache.data());
}

} // namespace

namespace Jgv::Genicam2 {

MaskedIntReg::MaskedIntReg(const tinyxml2::XMLElement* element, std::shared_ptr<Gvcp::Client> client)
    : Register { element, std::move(client) }
{
    if (_length.value() == 4) {
        const auto lsbXmlNode { element->FirstChildElement(XmlNode::LSB.data()) };
        const auto msbXmlNode { element->FirstChildElement(XmlNode::MSB.data()) };
        if ((lsbXmlNode != nullptr) && (msbXmlNode != nullptr)) {
            const auto lsb { toValue<int64_t>(lsbXmlNode->GetText()) };
            const auto msb { toValue<int64_t>(msbXmlNode->GetText()) };
            _dec = UINT32_LSB - lsb;
            _mask = MASK(1 + lsb - msb);
        } else {
            if (const auto child { element->FirstChildElement(XmlNode::Bit.data()) }; child != nullptr) {
                const auto bit { toValue<int64_t>(child->GetText()) };
                _dec = UINT32_LSB - bit;
                _mask = UINT64_C(1);
            }
        }
    }
}

//void MaskedIntReg::mapInterfaces(const InodeGetter& inodeGetter) noexcept
//{
//    Register::mapInterfaces(inodeGetter);
//}

void MaskedIntReg::initCache() noexcept
{
    _cache.resize(_length.value());
}

auto MaskedIntReg::access() const noexcept -> std::tuple<bool, bool>
{
    return { _canRead, _canWrite };
}

auto MaskedIntReg::value() -> int64_t
{
    if (!_canRead) {
        std::cerr << "Genicam::MaskedIntReg::Value " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " is write only !\n";
        return 0;
    }

    if (_length.value() == sizeof(uint32_t)) {
        if (_cachable == Cachable::NoCache) {
            if (auto buffer { _client->readMemory(_address.value(), sizeof(uint32_t)) }; !buffer.empty()) {
                auto reg { fromBigEndian32Cache(buffer) };
                reg >>= _dec;
                return reg & _mask;
            }
            std::cerr << "Genicam2::MaskedIntReg::value() " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " buffer from device is empty !" << std::endl;
            return 0;
        }

        if (!_cacheIsValid) {
            updateCache();
        }

        auto reg { fromBigEndian32Cache(_cache) };
        reg >>= _dec;
        return reg & _mask;
    }

    return 0;
}

void MaskedIntReg::setValue(int64_t value)
{
    if (_length.value() == sizeof(uint32_t)) {
        if (_cachable == Cachable::NoCache) {
            if (auto buffer { _client->readMemory(_address.value(), sizeof(uint32_t)) }; !buffer.empty()) {
                // on met à 0 les bits à changer
                auto reg { static_cast<uint64_t>(fromBigEndian32Cache(_cache)) & (~(_mask << _dec)) };
                // un OU
                reg |= (value << _dec);
                _client->writeRegister(_address.value(), static_cast<uint32_t>(reg));

            } else {
                std::cerr << "Genicam2::MaskedIntReg::setValue() " << _xmlElement->Attribute(XmlAttribute::Name.data()) << " buffer from device is empty !" << std::endl;
            }

        } else {
            if (!_cacheIsValid) {
                updateCache();
            }
            // on met à 0 les bits à changer
            auto reg { static_cast<uint64_t>(fromBigEndian32Cache(_cache)) & (~(_mask << _dec)) };
            // un OU
            reg |= (value << _dec);
            // écriture vers le cache
            littleEndian32ToCache(static_cast<uint32_t>(reg), _cache);
            // écriture vers le device
            writeCache();
        }
    }
    std::for_each(_invalideFunctions.cbegin(), _invalideFunctions.cend(), [](const auto& invalidate) {
        invalidate();
    });
}

auto MaskedIntReg::min() -> int64_t
{
    return std::numeric_limits<int64_t>::lowest();
}

auto MaskedIntReg::max() -> int64_t
{
    return std::numeric_limits<int64_t>::max();
}

auto MaskedIntReg::inc() -> int64_t
{
    return 1;
}

} // namespace Jgv::Genicam2
