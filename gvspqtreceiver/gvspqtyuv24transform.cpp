/***************************************************************************
 *   Copyright (C) 2014-2018 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtyuv24transform.h"
#include "gvspqtyuv24transform_p.h"

#include "gvspqtcrosshair.h"
#include "gvspqtjpegrecorder.h"
#include "gvspqttimecodepainter.h"

#include <gvspdevices/gvsp.h>
#include <iostream>
#include <openglutils/openglprogram.h>

namespace Jgv::Gvsp::QtBackend {

Yuv24TransformPrivate::Yuv24TransformPrivate(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : PixelTransformPrivate(width, height, pixelFormat, channel, std::move(globalIpc))
{
    jpegFormat = JpegRecorderFormat::Yuv;
}

void Yuv24TransformPrivate::initializeTransform()
{
    const std::string vSource =
#include "shader/texelfetch.vert"
        ;
    const std::string fSource =
#include "shader/yuv444packed.frag"
        ;

    program = OpenGLUtils::Program::create(vSource, fSource);

    glGenTextures(1, &uploadTexture);
    glBindTexture(GL_TEXTURE_2D, uploadTexture);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, width, height);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);
}

auto Yuv24TransformPrivate::transform(GLuint pbo) -> GLuint
{
    glBindTexture(GL_TEXTURE_2D, uploadTexture);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    glUseProgram(program);
    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, OpenGLUtils::QUAD_ELEMENTS_COUNT, GL_UNSIGNED_BYTE, nullptr);
    glBindVertexArray(0);
    glUseProgram(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    return 0;
}

void Yuv24TransformPrivate::destroyTransform()
{
    glDeleteTextures(1, &uploadTexture);
    glDeleteProgram(program);
}

Yuv24Transform::Yuv24Transform(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : PixelTransform(std::make_unique<Yuv24TransformPrivate>(width, height, pixelFormat, channel, std::move(globalIpc)))
{
}

} // namespace Jgv::Gvsp::QtBackend
