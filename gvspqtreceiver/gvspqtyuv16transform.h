/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTYUV16TRANSFORM_H
#define GVSPQTYUV16TRANSFORM_H

#include "gvspqtpixeltransform.h"

namespace Jgv::Gvsp::QtBackend {

class Yuv16Transform final : public PixelTransform {
public:
    Yuv16Transform(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~Yuv16Transform() override = default;
    Yuv16Transform(const Yuv16Transform&) = delete;
    Yuv16Transform(Yuv16Transform&&) = delete;
    auto operator=(const Yuv16Transform&) -> Yuv16Transform& = delete;
    auto operator=(Yuv16Transform &&) -> Yuv16Transform& = delete;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTYUV16TRANSFORM_H
