/***************************************************************************
 *   Copyright (C) 2014-2018 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTHISTOGRAMGETTER_H
#define GVSPQTHISTOGRAMGETTER_H

#include <epoxy/gl.h>

#include <functional>
#include <memory>
#include <vector>

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Gvsp::QtBackend {

template <typename T>
class AtomicVector;
class HistogramGetterPrivate;

class HistogramGetter {

protected:
    explicit HistogramGetter(std::unique_ptr<HistogramGetterPrivate>&& impl);

public:
    virtual ~HistogramGetter();
    HistogramGetter(const HistogramGetter&) = delete;
    HistogramGetter(HistogramGetter&&) = delete;
    auto operator=(const HistogramGetter&) -> HistogramGetter& = delete;
    auto operator=(HistogramGetter &&) -> HistogramGetter& = delete;

    void setBalance(float balance);

    void setFboSize(uint32_t width, uint32_t height);
    void render();
    void destroy();

private:
    const std::unique_ptr<HistogramGetterPrivate> _impl;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTHISTOGRAMGETTER_H
