/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTPBORING_H
#define GVSPQTPBORING_H

#include "moodycamel/readerwriterqueue.h"

#include <epoxy/gl.h>
#include <span>

namespace Jgv::Gvsp::QtBackend {

constexpr std::size_t PBO_COUNT { 3 };

class PboRing final {
    std::array<GLuint, PBO_COUNT> _pbos {};
    moodycamel::ReaderWriterQueue<std::tuple<GLuint, GLsync>> _queue;
    std::size_t _size { 0 };

public:
    void initialize();
    auto dequeue(std::size_t size) -> std::tuple<GLuint, std::span<uint8_t>>;
    void enqueue(std::tuple<GLuint, GLsync> pbo);
    void destroy();

}; // class PboRing

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTPBORING_H
