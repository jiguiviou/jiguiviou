/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTGREYTRANSFORM_H
#define GVSPQTGREYTRANSFORM_H

#include "gvspqtpixeltransform.h"

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Gvsp::QtBackend {

class GreyTransform final : public PixelTransform {
public:
    explicit GreyTransform(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~GreyTransform() override = default;
    GreyTransform(const GreyTransform&) = delete;
    GreyTransform(GreyTransform&&) = delete;
    auto operator=(const GreyTransform&) -> GreyTransform& = delete;
    auto operator=(GreyTransform &&) -> GreyTransform& = delete;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTGREYTRANSFORM_H
