/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTTIMECODEPAINTER_H
#define GVSPQTTIMECODEPAINTER_H

#include <memory>
#include <tuple>

namespace Jgv::Gvsp::QtBackend {

struct PboMetas;
class TimecodePainterPrivate;
class TimecodePainter final {
public:
    TimecodePainter();
    ~TimecodePainter();
    TimecodePainter(const TimecodePainter&) = delete;
    TimecodePainter(TimecodePainter&&) = delete;
    auto operator=(const TimecodePainter&) -> TimecodePainter& = delete;
    auto operator=(TimecodePainter &&) -> TimecodePainter& = delete;

    void setFboSize(std::tuple<uint32_t, uint32_t> size) noexcept;
    void initialize();
    void render(int64_t timestamp, int32_t site, int32_t gisement) noexcept;
    void destroy();

private:
    const std::unique_ptr<TimecodePainterPrivate> _impl;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTTIMECODEPAINTER_H
