/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTJPEGRECORDER_P_H
#define GVSPQTJPEGRECORDER_P_H

#include "gvspqtrecordermetas.h"
#include "moodycamel/readerwriterqueue.h"

#include <atomic>
#include <epoxy/gl.h>
#include <future>
#include <queue>
#include <span>
#include <thread>

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Gvsp::QtBackend {

enum class JpegRecorderFormat;

struct RecorderJob {
    GLuint pbo { 0U };
    std::span<uint8_t> span;
    GLsync sync { nullptr };
    int64_t timestamp { 0 };
    int32_t site { 0 };
    int32_t gisement { 0 };
    std::string record;
    std::string snapshot;
    std::shared_ptr<RecorderMetas> xmp;
};

struct PboMetas;

class JpegRecorderPrivate {
    using Render = void (JpegRecorderPrivate::*)(int64_t, int32_t, int32_t, std::string_view, std::string_view, std::shared_ptr<RecorderMetas>);

public:
    explicit JpegRecorderPrivate(JpegRecorderFormat format, std::string_view sub, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);

    const JpegRecorderFormat format;
    const std::string subpath;
    const Channel::Type channel;
    std::shared_ptr<GlobalIPC::Object> globalIpc;

    // le thread de download des pbos
    std::thread renderThread;
    // la queue synchrone
    moodycamel::BlockingReaderWriterQueue<RecorderJob> jobQueue;
    // la queue synchrone des pbo libres
    moodycamel::ReaderWriterQueue<RecorderJob> pbosQueue;
    uint32_t width {};
    uint32_t height {};

    Render render;

    //    void renderAsYuv(GLuint fbo, const PboMetas &metas, QOpenGLContext *context);

    void loop(std::promise<void> barrier);
    void setFboSize(uint32_t width, uint32_t height);
    void renderAsGrey(int64_t timestamp, int32_t site, int32_t gisement, std::string_view record, std::string_view snapshot, std::shared_ptr<RecorderMetas>);
    void destroy();
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTJPEGRECORDER_P_H
