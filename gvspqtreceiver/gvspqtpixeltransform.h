/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTPIXELTRANSFORM_H
#define GVSPQTPIXELTRANSFORM_H

#include <epoxy/gl.h>

#include <chrono>
#include <memory>
#include <span>

namespace Jgv::Gvsp {
struct Image;
} // namespace Jgv::Gvsp

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Gvsp::QtBackend {

struct PixelTransformPrivate;
class PixelTransform {

protected:
    explicit PixelTransform(std::unique_ptr<PixelTransformPrivate>&& dd);

public:
    virtual ~PixelTransform();
    PixelTransform(const PixelTransform&) = delete;
    PixelTransform(PixelTransform&&) = delete;
    auto operator=(const PixelTransform&) -> PixelTransform& = delete;
    auto operator=(PixelTransform &&) -> PixelTransform& = delete;

    static auto make(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc) -> std::unique_ptr<PixelTransform>;

    [[nodiscard]] auto handleImageFormat(uint32_t width, uint32_t height, uint32_t pixelFormat) const noexcept -> bool;
    void initialize();
    auto dequeue(std::size_t size) -> std::tuple<GLuint, std::span<uint8_t>>;
    void requeue(GLuint pbo, GLsync fence);
    void push(GLuint pbo, GLsync fence, int64_t timestamp, int32_t site, int32_t gisement);

protected:
    const std::unique_ptr<PixelTransformPrivate> _impl;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTPIXELTRANSFORM_H
