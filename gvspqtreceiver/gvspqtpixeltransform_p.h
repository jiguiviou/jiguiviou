/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTPIXELTRANSFORM_P_H
#define GVSPQTPIXELTRANSFORM_P_H

#include <epoxy/gl.h>

#include "gvspqtpboring.h"
#include "gvspqtrecordermetas.h"
#include "moodycamel/readerwriterqueue.h"

#include <atomic>
#include <functional>
#include <future>
#include <globalipc/vtoconfig.h>
#include <list>
#include <map>
#include <memory>
#include <thread>
#include <tuple>

#include <iostream>
namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

//namespace Jgv::Gvsp {
//struct Image;
//} // namespace Jgv::Gvsp

namespace Jgv::Gvsp::QtBackend {

enum class JpegRecorderFormat {
    Grey,
    Yuv
};

enum class GlEffectType {
    HistogramGetter,
    HistogramEqualization,
    Sobel,
    RectangleMask,
    HistogramPainter,
    Crosshair,
    JpegRecorder,
    JpegRecorderOut,
    StateOverlay,
    TimecodePainter
};

using Point = GlobalIPC::Pair<int32_t>;
inline auto operator!=(const Point& lhs, const Point& rhs) -> bool
{
    return lhs.first != rhs.first || lhs.second != rhs.second;
}

struct FboSize {
    uint32_t width;
    uint32_t height;
};
inline auto operator!=(const FboSize& lhs, const FboSize& rhs) -> bool
{
    return lhs.width != rhs.width || lhs.height != rhs.height;
}

struct TextureInfos {
    uint id;
    uint32_t width;
    uint32_t height;
};

// Le SyncType ne fonctionne que si un seul thread, modifie
// la valeur, et qu'un seul thread consomme cette valeur.
// La convergence des états est assuré par la boucle render.
template <typename T>
struct SyncType {
    std::atomic<T> reg {};
    T to {};
    auto changed() noexcept
    {
        return (std::exchange(to, reg.load()) != to);
    }
};

template <typename T>
struct SyncSharedPtr {
    std::shared_ptr<T> reg { std::make_shared<T>() };
    std::shared_ptr<T> to { std::make_shared<T>() };
    auto changed() noexcept
    {
        return (std::atomic_exchange(&to, reg) != to);
    }

    auto operator()() noexcept -> T&
    {
        return (*to);
    }

    auto operator()() const noexcept -> const T&
    {
        return (*to);
    }
};

class IPCVtoMetas {

    SyncSharedPtr<RecorderMetas> _datas;

public:
    std::shared_ptr<std::function<void(const std::shared_ptr<RecorderMetas>&)>> setter;

    void set(const std::shared_ptr<RecorderMetas>& metas, std::string_view emetterName)
    {
        // crée une copie writable
        metas->setMeta(CAMERA, emetterName);
        _datas.reg = metas;
    }

    [[nodiscard]] auto get() const noexcept -> std::shared_ptr<RecorderMetas>
    {
        return _datas.to;
    }

    auto changed() noexcept
    {
        return _datas.changed();
    }

    explicit operator RecorderMetas&() noexcept
    {
        return _datas();
    }

    explicit operator const RecorderMetas&() const noexcept
    {
        return _datas();
    }
};

class IPCSnapshot {
    struct S {
        uint count { 0 };
        std::string name;
        S() = default;
        explicit S(uint count, std::string_view name)
            : count { count }
            , name { name }
        {
        }
    };

    uint _count { 0 };
    std::string _name;
    SyncSharedPtr<S> _datas;

public:
    std::shared_ptr<std::function<void(uint, std::string_view)>> setter;

    void set(uint count, std::string_view name)
    {
        _datas.reg = std::make_shared<S>(count, name);
    }

    explicit operator bool() noexcept
    {
        if (_datas.changed()) {
            _count = _datas().count;
            _name = _datas().name;
        }
        return _count > 0;
    }

    void operator--(int) noexcept
    {
        if (_count > 0) {
            --_count;
        }
    }

    [[nodiscard]] auto name() const noexcept -> std::string_view
    {
        return _name;
    }
};

class IPCBoolTogglerWithEvent {
    SyncType<bool> _boolToggler;

public:
    std::shared_ptr<std::function<void(void)>> toggler {};
    std::shared_ptr<std::function<bool(void)>> getter {};
    std::function<void(bool)> emitEvent;

    // demande le changement d'état
    void toggle() noexcept
    {
        const auto state { _boolToggler.reg.load() };
        _boolToggler.reg.store(state ^ true);
    }

    // renvoie la valeur synchronisée
    [[nodiscard]] auto get() const noexcept
    {
        return _boolToggler.to;
    }

    // teste et renvoie la valeur synchronisée
    explicit operator bool() noexcept
    {
        if (_boolToggler.changed()) {
            emitEvent(_boolToggler.to);
        }
        return _boolToggler.to;
    }

    // teste et renvoie le résultat du test, et la valeur synchronisée
    auto changed() noexcept
    {
        if (_boolToggler.changed()) {
            emitEvent(_boolToggler.to);
            return std::tuple<bool, bool>(true, _boolToggler.to);
        }
        return std::tuple<bool, bool>(false, _boolToggler.to);
    }
};

template <typename T>
class IPCType {
    SyncType<T> _syncType;

public:
    std::shared_ptr<std::function<void(T)>> setter {};
    std::shared_ptr<std::function<T(void)>> getter {};

    void set(T value) noexcept
    {
        _syncType.reg.store(value);
    }

    [[nodiscard]] auto get() const noexcept -> T
    {
        return _syncType.to;
    }

    auto changed() noexcept
    {
        return _syncType.changed();
    }

    explicit operator T&() noexcept
    {
        return _syncType.to;
    }
};

class RecorderMetas;
class HistogramGetter;
class HistogramEqualization;
class ContourEnhancer;
class HistogramPainter;
class JpegRecorder;
class TimecodePainter;
class Crosshair;

template <typename T>
class AtomicVector;

struct PixelTransformPrivate {
    explicit PixelTransformPrivate(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    virtual ~PixelTransformPrivate();
    PixelTransformPrivate(const PixelTransformPrivate&) = delete;
    PixelTransformPrivate(PixelTransformPrivate&&) = delete;
    auto operator=(const PixelTransformPrivate&) -> PixelTransformPrivate& = delete;
    auto operator=(PixelTransformPrivate &&) -> PixelTransformPrivate& = delete;

    const uint32_t width;
    const uint32_t height;
    const uint32_t pixelFormat;
    const Channel::Type channel;
    const std::shared_ptr<GlobalIPC::Object> globalIpc;

    std::shared_ptr<std::function<std::tuple<uint, uint32_t, uint32_t>(void)>> textureGetter;
    JpegRecorderFormat jpegFormat { JpegRecorderFormat::Grey };

    // Equalization
    std::unique_ptr<HistogramGetter> histogramGetter;
    std::unique_ptr<HistogramEqualization> histogramEq;
    std::unique_ptr<ContourEnhancer> contourEnhancer;
    std::unique_ptr<HistogramPainter> histogramPainter;
    // Enregistrement
    std::unique_ptr<JpegRecorder> recorderIn;
    std::unique_ptr<JpegRecorder> recorderOut;
    // Timecode
    std::unique_ptr<TimecodePainter> timecode;
    // Réticule
    std::unique_ptr<Crosshair> crosshair;

    bool widthEqualization { false };
    std::string recordSubDirectory;
    std::string cameraName;

    IPCBoolTogglerWithEvent record;
    IPCBoolTogglerWithEvent equalize;
    IPCType<GlobalIPC::Pair<int32_t>> crosshairCenter;
    IPCType<float> equalizeBalance;
    IPCSnapshot snapshot;
    IPCVtoMetas vtoMetas;

    GLuint vao { 0U }; // pour le rendu du quad image
    GLuint vbo { 0U }; // les vertices du quad
    GLuint indexes { 0U }; // rendu par index
    GLuint gbo { 0U }; // buffer object qui contient la taille en pixels du quad, commun à tous les shaders

    GLuint renderFbo { 0U }; // le framebuffer de rendu offscreen
    GLuint renderTexture { 0U }; // la texture du fbo
    GLuint blitFbo { 0U }; // le framebuffer offscreen pour le double buffer
    GLuint blitTexture { 0U }; // la texture du fbo de blit

    PboRing pool; // le ring de pixel buffer object pour l'upload
    std::thread thread; // le thread upload -> transform -> effects
    moodycamel::BlockingReaderWriterQueue<std::tuple<GLuint, GLsync, int64_t, int32_t, int32_t>> pboQueue;

    [[nodiscard]] auto handleImageFormat(uint32_t width, uint32_t height, uint32_t pixelFormat) const noexcept -> bool;
    void initialize() noexcept;
    void destroy() noexcept;

    void initializeEffects() noexcept;
    void destroyEffects() noexcept;

    void loop(std::promise<void> barrier) noexcept;

    virtual void initializeTransform() = 0;
    virtual void destroyTransform() = 0;

    virtual auto transform(GLuint pbo) -> GLuint = 0;
    void render(std::tuple<GLuint, GLsync, int64_t, int32_t, int32_t> metas);
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTPIXELTRANSFORM_P_H
