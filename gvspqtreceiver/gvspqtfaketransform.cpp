/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtfaketransform.h"
#include "gvspqtfaketransform_p.h"

namespace Jgv::Gvsp::QtBackend {

const int FONT_SIZE { 48 };
constexpr auto BACKGROUND_COLOR { std::make_tuple<GLfloat, GLfloat, GLfloat, GLfloat>(0.1F, 0.1F, 0.1F, 1.F) };

FakeTransformPrivate::FakeTransformPrivate(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : PixelTransformPrivate(width, height, pixelFormat, channel, std::move(globalIpc))
{
    jpegFormat = JpegRecorderFormat::Grey;
}

void FakeTransformPrivate::initializeTransform()
{
    textPainter = std::make_unique<OpenGLUtils::TextPainter>();
    textPainter->initializeGL(FONT_SIZE, "monospace", true);

    auto text { textPainter->createObject() };
    textPainter->setText(text, std::string { "Ce format de pixels" });
    textPainter->setColor(text, { 1.F, 1.F, 1.F, 1.F });
    texts.push_back(text);

    text = textPainter->createObject();
    textPainter->setText(text, std::string { "n'est pas pris en charge !" });
    textPainter->setColor(text, { 1.F, 1.F, 1.F, 1.F });
    texts.push_back(text);

    textPainter->resizeViewGL(width, height);

    {
        const auto [left, bottom, right, top] { textPainter->boundingBox(texts[1]) };
        auto x0 { 0.5F * (width - right) }; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
        auto y0 { 0.5F * (height - top) }; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
        textPainter->setPosition(texts[1], { x0, y0, 0.F });
    }
    {
        const auto [left, bottom, right, top] { textPainter->boundingBox(texts[0]) };
        auto x0 { 0.5F * (width - right) }; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
        auto y0 { 0.5F * (height - top) }; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
        y0 += textPainter->interline();
        textPainter->setPosition(texts[0], { x0, y0, 0.F });
    }
}

auto FakeTransformPrivate::transform(GLuint /*pbo*/) -> GLuint
{
    const auto [r, v, b, a] { BACKGROUND_COLOR };
    glClearColor(r, v, b, a);
    glClear(GL_COLOR_BUFFER_BIT);
    textPainter->renderGL(texts);
    return 0;
}

void FakeTransformPrivate::destroyTransform()
{
    textPainter->destroyGL();
}

FakeTransform::FakeTransform(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : PixelTransform(std::make_unique<FakeTransformPrivate>(width, height, pixelFormat, channel, std::move(globalIpc)))
{
}

} // namespace Jgv::Gvsp::QtBackend
