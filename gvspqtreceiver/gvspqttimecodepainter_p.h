/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTTIMECODEPAINTER_P_H
#define GVSPQTTIMECODEPAINTER_P_H

#include <epoxy/gl.h>

#include <openglutils/rectanglepainter.h>
#include <openglutils/textpainter.h>

namespace Jgv::Gvsp::QtBackend {

struct PboMetas;

class TimecodePainterPrivate {
public:
    std::unique_ptr<OpenGLUtils::TextPainter> textPainter;
    std::unique_ptr<OpenGLUtils::RectanglePainter> rectanglePainter;
    OpenGLUtils::TextPainterObject text;
    OpenGLUtils::RectanglePainterObject rectangle;

    void setFboSize(std::tuple<uint32_t, uint32_t> size) noexcept;
    void initialize();
    void render(int64_t timestamp, int32_t site, int32_t gisement) noexcept;
};

} // namespace Jgv

#endif // GVSPQTTIMECODEPAINTER_P_H
