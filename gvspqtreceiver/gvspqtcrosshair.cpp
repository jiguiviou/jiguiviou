/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtcrosshair.h"
#include "gvspqtcrosshair_p.h"

#include <openglutils/openglprogram.h>
#include <span>

namespace Jgv::Gvsp::QtBackend {

constexpr auto RETICLE_COLOR { std::make_tuple(0.8F, 0.8F, 0.8F, 0.5F) };
constexpr auto VIEW_WIDTH { 2.F };
constexpr auto VIEW_HEIGHT { 2.F };

void CrosshairPrivate::initialize() noexcept
{
    const std::string vSource =
#include "shader/default.vert"
        ;
    const std::string fSource =
#include "shader/primitive.frag"
        ;

    program = OpenGLUtils::Program::create(vSource, fSource);

    glUseProgram(program);
    auto color { glGetUniformLocation(program, "color") };
    const auto [r, g, b, a] { RETICLE_COLOR };
    glUniform4f(color, r, g, b, a);
    glUseProgram(0);

    glGenBuffers(1, &vbo);
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), OpenGLUtils::offsetPtr<GLfloat>(0));
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void CrosshairPrivate::render() noexcept
{
    glUseProgram(program);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBlendEquation(GL_FUNC_ADD);
    glBindVertexArray(vao);
    glDrawArrays(GL_LINES, 0, 4);
    glBindVertexArray(0);
    glDisable(GL_BLEND);
    glUseProgram(0);
}

void CrosshairPrivate::destroy() noexcept
{
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteProgram(program);
}

void CrosshairPrivate::updateBufferObjects() noexcept
{
    // on normalise
    x *= VIEW_WIDTH / w;
    y *= VIEW_HEIGHT / h;

    // une simple croix
    const std::array<GLfloat, 2 * 4> vertices {
        x, -1.F, x, 1.F,
        1.F, y, -1.F, y
    };

    const std::span<const GLfloat> span { vertices };
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, span.size_bytes(), span.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void CrosshairPrivate::setCenter(int32_t centerX, int32_t centerY) noexcept
{
    x = centerX;
    y = centerY;
    updateBufferObjects();
}

void CrosshairPrivate::setSize(int32_t width, int32_t height) noexcept
{
    w = width;
    h = height;
    updateBufferObjects();
}

Crosshair::Crosshair()
    : _impl(std::make_unique<CrosshairPrivate>())
{
}

Crosshair::~Crosshair() = default;

void Crosshair::setCenter(int32_t centerX, int32_t centerY) noexcept
{
    _impl->setCenter(centerX, centerY);
}

void Crosshair::setFboSize(int32_t width, int32_t height) noexcept
{
    _impl->setSize(width, height);
}

void Crosshair::initialize() noexcept
{
    _impl->initialize();
}

void Crosshair::render() noexcept
{
    _impl->render();
}

void Crosshair::destroy() noexcept
{
    _impl->destroy();
}

} // namespace Jgv::Gvsp::QtBackend
