/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtpboring.h"

#include <iostream>
#include <openglutils/openglprogram.h>

namespace Jgv::Gvsp::QtBackend {
constexpr GLuint64 FENCE_TIMEOUT { 100000000U }; // 100 ms

void PboRing::initialize()
{
    glGenBuffers(PBO_COUNT, _pbos.data());
    // remplie le queue avec les pbos sans barrière
    for (auto pbo : _pbos) {
        _queue.enqueue({ pbo, nullptr });
    }
}

auto PboRing::dequeue(std::size_t size) -> std::tuple<GLuint, std::span<uint8_t>>
{
    // la taille est nulle
    if (size == 0) {
        std::cerr << "Gvsp::PboRing: demande d'allocation d'un tampon vide.";
        return {};
    }

    std::tuple<GLuint, GLsync> pboFence;
    if (_queue.try_dequeue(pboFence)) {
        auto [pbo, fence] { pboFence };
        if (fence != nullptr) {
            const auto state { glClientWaitSync(fence, GL_SYNC_FLUSH_COMMANDS_BIT, FENCE_TIMEOUT) };
            glDeleteSync(fence);
            if ((state != GL_ALREADY_SIGNALED) && (state != GL_CONDITION_SATISFIED)) {
                std::cout << OpenGLUtils::FENCE_STATE_TO_STRING(state) << std::endl;
            }
        }
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
        glBufferData(GL_PIXEL_UNPACK_BUFFER, size, nullptr, GL_DYNAMIC_DRAW);
        auto ptr { static_cast<uint8_t*>(glMapBufferRange(GL_PIXEL_UNPACK_BUFFER, 0, size, GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT)) };
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
        if (ptr != nullptr) {
            return { pbo, { ptr, static_cast<std::size_t>(size) } };
        }
    }

    std::clog << "Gvsp::PboRing:buffer: le pool est vide." << std::endl;
    return { 0, {} };
}

void PboRing::enqueue(std::tuple<GLuint, GLsync> pbo)
{
    _queue.enqueue(std::move(pbo));
}

void PboRing::destroy()
{
    glDeleteBuffers(PBO_COUNT, _pbos.data());
    _pbos.fill(0);
}

} // namespace Jgv::Gvsp::QtBackend
