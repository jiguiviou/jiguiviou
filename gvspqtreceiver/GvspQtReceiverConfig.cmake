get_filename_component(SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(${SELF_DIR}/GvspQtReceiver.cmake)
get_filename_component(gvspqtreceiver_INCLUDE_DIRS "${SELF_DIR}/../../include/gvspqtreceiver" ABSOLUTE)

message(STATUS "GvspQtReceiverConfig: include : ${gvspqtreceiver_INCLUDE_DIRS}")
