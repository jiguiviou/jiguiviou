/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTGREY12TRANSFORM_H
#define GVSPQTGREY12TRANSFORM_H

#include "gvspqtpixeltransform.h"

namespace Jgv::Gvsp::QtBackend {

class Grey12Transform final : public PixelTransform {
public:
    explicit Grey12Transform(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~Grey12Transform() override = default;
    Grey12Transform(const Grey12Transform&) = delete;
    Grey12Transform(Grey12Transform&&) = delete;
    auto operator=(const Grey12Transform&) -> Grey12Transform& = delete;
    auto operator=(Grey12Transform &&) -> Grey12Transform& = delete;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTGREY12TRANSFORM_H
