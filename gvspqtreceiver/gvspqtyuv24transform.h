/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTYUV24TRANSFORM_H
#define GVSPQTYUV24TRANSFORM_H

#include "gvspqtpixeltransform.h"

namespace Jgv::Gvsp::QtBackend {

class Yuv24Transform final : public PixelTransform {
public:
    explicit Yuv24Transform(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    Yuv24Transform(const Yuv24Transform&) = delete;
    Yuv24Transform(Yuv24Transform&&) = delete;
    auto operator=(const Yuv24Transform&) -> Yuv24Transform& = delete;
    auto operator=(Yuv24Transform &&) -> Yuv24Transform& = delete;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTYUV24TRANSFORM_H
