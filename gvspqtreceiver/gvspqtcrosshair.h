/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTCROSSHAIR_H
#define GVSPQTCROSSHAIR_H

#include <memory>

namespace Jgv::Gvsp::QtBackend {

struct CrosshairPrivate;
class Crosshair final {
public:
    explicit Crosshair();
    ~Crosshair();
    Crosshair(const Crosshair&) = delete;
    Crosshair(Crosshair&&) = delete;
    auto operator=(const Crosshair&) -> Crosshair& = delete;
    auto operator=(Crosshair &&) -> Crosshair& = delete;

    void setCenter(int32_t centerX, int32_t centerY) noexcept;
    void setFboSize(int32_t width, int32_t height) noexcept;
    void initialize() noexcept;
    void render() noexcept;
    void destroy() noexcept;

private:
    const std::unique_ptr<CrosshairPrivate> _impl;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTCROSSHAIR_H
