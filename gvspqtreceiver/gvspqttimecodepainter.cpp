/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqttimecodepainter.h"
#include "gvspqttimecodepainter_p.h"

#include "date-master/date.h"
#include "gvspqtpixeltransform.h"
#include "gvspqtpixeltransform_p.h"

#include <cstdlib>
#include <globalipc/globalipc.h>
#include <iostream>
#include <openglutils/openglprogram.h>

namespace {

constexpr std::string_view DEFAULT_TIMECODE { "00:00:00.000  -123.456  123.456" };
constexpr auto SITE_OFFSET { 14 };
constexpr auto GISEMENT_OFFSET { 24 };
constexpr auto BACKGROUND_COLOR { std::make_tuple(0.25F, 0.25F, 0.25F, 0.4F) };
constexpr auto TEXT_COLOR { std::make_tuple(1.F, 1.F, 1.F, 0.8F) };
constexpr auto FONT_SIZE { 30U };
constexpr auto BASE_100000 { 100000 };
constexpr auto BASE_10000 { 10000 };
constexpr auto BASE_1000 { 1000 };
constexpr auto BASE_100 { 100 };
constexpr auto BASE_10 { 10 };

void siteString(int32_t value, std::span<char> text)
{
    const auto abs { std::abs(value) };
    text[0] = (value < 0) ? '-' : '+'; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    auto div { std::div(abs, BASE_100000) };
    text[1] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    div = std::div(div.rem, BASE_10000);
    text[2] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    div = std::div(div.rem, BASE_1000);
    text[3] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    div = std::div(div.rem, BASE_100);
    text[5] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    div = std::div(div.rem, BASE_10);
    text[6] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    text[7] = '0' + static_cast<char>(div.rem); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
}

void gisementString(int32_t value, std::span<char> text)
{
    auto div { std::div(value, BASE_100000) };
    text[0] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    div = std::div(div.rem, BASE_10000);
    text[1] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    div = std::div(div.rem, BASE_1000);
    text[2] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    div = std::div(div.rem, BASE_100);
    text[4] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    div = std::div(div.rem, BASE_10);
    text[5] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    text[6] = '0' + static_cast<char>(div.rem); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
}

} // namespace

namespace Jgv::Gvsp::QtBackend {

void TimecodePainterPrivate::setFboSize(std::tuple<uint32_t, uint32_t> size) noexcept
{
    const auto [width, height] { size };
    const auto [leftTimecode, bottomTimecode, rightTimecode, topTimecode] { textPainter->boundingBox(text) };

    const float x { 0.5F * (static_cast<float>(width) - rightTimecode) };

    // les coordonnées sont inversées
    rectanglePainter->setRectangle(rectangle, { x, height - (topTimecode - bottomTimecode), x + rightTimecode, height }, 0.F);
    rectanglePainter->resizeViewGL(width, height);
    textPainter->setPosition(text, { x, -bottomTimecode, 0.F });
    textPainter->resizeViewGL(width, height);
}

void TimecodePainterPrivate::initialize()
{
    textPainter = std::make_unique<OpenGLUtils::TextPainter>();
    textPainter->initializeGL(FONT_SIZE, "monospace", true);

    text = textPainter->createObject();
    textPainter->setText(text, DEFAULT_TIMECODE);
    textPainter->setColor(text, TEXT_COLOR);

    rectanglePainter = std::make_unique<OpenGLUtils::RectanglePainter>();
    rectanglePainter->initializeGL();

    rectangle = rectanglePainter->createObject();
    rectanglePainter->setColor(rectangle, BACKGROUND_COLOR);
}

void TimecodePainterPrivate::render(int64_t timestamp, int32_t site, int32_t gisement) noexcept
{
    std::string all { DEFAULT_TIMECODE };
    std::span<char> span { all };

    GlobalIPC::Utils::timestampToString(timestamp, span);
    siteString(site, span.subspan(SITE_OFFSET));
    gisementString(gisement, span.subspan(GISEMENT_OFFSET));

    textPainter->setText(text, all);
    rectanglePainter->renderGL(rectangle);
    textPainter->renderGL(text);
}

TimecodePainter::TimecodePainter()
    : _impl(std::make_unique<TimecodePainterPrivate>())
{
}

TimecodePainter::~TimecodePainter() = default;

void TimecodePainter::setFboSize(std::tuple<uint32_t, uint32_t> size) noexcept
{
    _impl->setFboSize(size);
}

void TimecodePainter::initialize()
{
    _impl->initialize();
}

void TimecodePainter::render(int64_t timestamp, int32_t site, int32_t gisement) noexcept
{
    _impl->render(timestamp, site, gisement);
}

void TimecodePainter::destroy()
{
    _impl->textPainter->destroyGL();
}

} // namespace Jgv
