/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtcontourenhancer.h"
#include "gvspqtcontourenhancer_p.h"

#include <openglutils/openglprogram.h>

namespace Jgv::Gvsp::QtBackend {

void ContourEnhancerPrivate::initialize(GLuint vbo, GLuint indexes) noexcept
{
    const std::string vSource =
#include "shader/texelfetch.vert"
        ;
    const std::string fSource =
#include "shader/highfilter.frag"
        ;

    program = OpenGLUtils::Program::create(vSource, fSource);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), OpenGLUtils::offsetPtr<GLfloat>(0));
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), OpenGLUtils::offsetPtr<GLfloat>(2));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexes);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void ContourEnhancerPrivate::render(GLuint texture) noexcept
{
    glUseProgram(program);
    glBindTexture(GL_TEXTURE_2D, texture);
    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, OpenGLUtils::QUAD_ELEMENTS_COUNT, GL_UNSIGNED_BYTE, nullptr);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glUseProgram(0);
}

void ContourEnhancerPrivate::destroy() noexcept
{
    glDeleteBuffers(1, &vao);
    glDeleteProgram(program);
}

ContourEnhancer::ContourEnhancer()
    : _impl(std::make_unique<ContourEnhancerPrivate>())
{
}

ContourEnhancer::~ContourEnhancer() = default;

void ContourEnhancer::initialize(GLuint vbo, GLuint indexes) noexcept
{
    _impl->initialize(vbo, indexes);
}

void ContourEnhancer::render(GLuint texture) noexcept
{
    _impl->render(texture);
}

void ContourEnhancer::destroy() noexcept
{
    _impl->destroy();
}

} // namespace Jgv::Gvsp::QtBackend
