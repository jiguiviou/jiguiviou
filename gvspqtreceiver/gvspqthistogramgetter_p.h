/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTHISTOGRAMGETTER_P_H
#define GVSPQTHISTOGRAMGETTER_P_H

#include "gvspqtatomicvector.h"
#include "moodycamel/readerwriterqueue.h"

#include <epoxy/gl.h>

#include <functional>
#include <future>
#include <span>
#include <thread>

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Gvsp::QtBackend {

constexpr auto DEFAULT_RELATIVE_SIZE { 0.5F };
constexpr auto CRITER_FACTOR { 3U };
constexpr auto HISTOGRAM_SIZE { 256U };

class ReadPixelsSize {
    GLint _x { 0 };
    GLint _y { 0 };
    GLsizei _width { 0 };
    GLsizei _height { 0 };
    uint32_t _sourceWidth { 0 };
    uint32_t _sourceHeight { 0 };
    float _ratio { 1.F };
    void apply() noexcept
    {
        _width = _sourceWidth * _ratio;
        _height = _sourceHeight * _ratio;
        _x = 0.5 * (_sourceWidth - _width);
        _y = 0.5 * (_sourceHeight - _height);
    }

public:
    void setRatio(float ratio)
    {
        _ratio = std::clamp(ratio, 0.F, 1.F);
        apply();
    };
    void setSourceSize(uint32_t width, uint32_t height)
    {
        _sourceWidth = width;
        _sourceHeight = height;
        apply();
    }

    auto x() const noexcept { return _x; }
    auto y() const noexcept { return _y; }
    auto width() const noexcept { return _width; }
    auto height() const noexcept { return _height; }
    auto bufferSize() const noexcept { return _width * _height; }
};

class HistogramGetterPrivate {

public:
    explicit HistogramGetterPrivate(GLenum downloadType, std::function<void(AtomicVector<float>&&)>&& histogramSetter, Channel::Type channel, std::shared_ptr<GlobalIPC::Object>&& globalIpc);
    const GLenum downLoadType;
    const std::function<void(AtomicVector<float>&&)> histogramSetter;
    const Channel::Type channel;
    const std::shared_ptr<GlobalIPC::Object> globalIpc;

    // le thread de dowload des pbos
    std::thread renderThread;
    // la queue synchrone
    moodycamel::BlockingReaderWriterQueue<std::tuple<GLuint, GLsync>> jobsQueue;
    // la queue synchrone des pbo libres
    moodycamel::ReaderWriterQueue<std::tuple<GLuint, GLsync>> pbosQueue;

    ReadPixelsSize size;

    std::atomic<double> balance { 0. };

    virtual void download(std::size_t size) = 0;

    void loop(std::promise<void> barrier, std::size_t bufferSize);
    void render();
    void destroy();
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTHISTOGRAMGETTER_P_H
