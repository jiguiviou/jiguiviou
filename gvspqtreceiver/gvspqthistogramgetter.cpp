/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqthistogramgetter.h"
#include "gvspqthistogramgetter_p.h"

#include "gvspqthistogrampainter.h"
#include "gvspqtpixeltransform.h"

#include <epoxy/gl.h>

#include <QOffscreenSurface>
#include <QOpenGLContext>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>

namespace {

constexpr auto HISTOGRAM_PBO_COUNT { 3 };
constexpr GLuint64 FENCE_TIMEOUT { 100000000U };

class AutoPbos {
    QOpenGLContext _context;
    QOffscreenSurface _surface;
    std::array<GLuint, HISTOGRAM_PBO_COUNT> _pbos {};

public:
    AutoPbos(QOpenGLContext* context, std::size_t size)
    {
        QSurfaceFormat fmt;
        fmt.setMajorVersion(4);
        fmt.setMinorVersion(2);
        fmt.setProfile(QSurfaceFormat::CoreProfile);
        _context.setFormat(fmt);
        _context.setShareContext(context);
        _context.create();
        _surface.setFormat(fmt);
        _surface.create();

        _context.makeCurrent(&_surface);
        glGenBuffers(HISTOGRAM_PBO_COUNT, _pbos.data());
        for (const auto& pbo : _pbos) {
            glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo);
            glBufferData(GL_PIXEL_PACK_BUFFER, size, nullptr, GL_DYNAMIC_READ);
            glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
        }
        _context.doneCurrent();
    }

    ~AutoPbos()
    {
        _context.makeCurrent(&_surface);
        glDeleteBuffers(HISTOGRAM_PBO_COUNT, _pbos.data());
        _context.doneCurrent();
        _surface.destroy();
    }

    AutoPbos(const AutoPbos&) = delete;
    AutoPbos(AutoPbos&&) = delete;
    auto operator=(const AutoPbos&) -> AutoPbos& = delete;
    auto operator=(AutoPbos &&) -> AutoPbos& = delete;

    void makeCurrent() { _context.makeCurrent(&_surface); }
    void doneCurrent() { _context.doneCurrent(); }
    auto begin() { return _pbos.begin(); }
    auto end() { return _pbos.end(); }
};

} // namespace

namespace Jgv::Gvsp::QtBackend {

HistogramGetterPrivate::HistogramGetterPrivate(GLenum _downloadType, std::function<void(AtomicVector<float>&&)>&& histogramSetter, Channel::Type _channel, std::shared_ptr<GlobalIPC::Object>&& _globalIpc)
    : downLoadType { _downloadType }
    , histogramSetter { std::move(histogramSetter) }
    , channel { _channel }
    , globalIpc { std::move(_globalIpc) }

{
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        size.setRatio(vtoConfig->histogramDownloadRatio(channel));
    }
}

void HistogramGetterPrivate::loop(std::promise<void> barrier, std::size_t bufferSize)
{
    AutoPbos pbos { globalIpc->sharedContext(), bufferSize };
    for (auto pbo : pbos) {
        pbosQueue.enqueue({ pbo, nullptr });
    }

    // on est initiliasé, on lève la barrière
    barrier.set_value();

    while (true) {
        std::tuple<GLuint, GLsync> job;
        jobsQueue.wait_dequeue(job);
        const auto [pbo, fence] { job };
        if (pbo > 0) {
            pbos.makeCurrent();
            // se met en attente de la barrière
            const auto state { glClientWaitSync(fence, GL_SYNC_FLUSH_COMMANDS_BIT, FENCE_TIMEOUT) };
            glDeleteSync(fence);

            if (state == GL_CONDITION_SATISFIED || state == GL_ALREADY_SIGNALED) {
                glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo);
                download(bufferSize);
                glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
            }

            pbos.doneCurrent();

            // traité ou pas le job retourne à la queue
            pbosQueue.enqueue(job);
        } else {
            break;
        }
    }
}

void HistogramGetterPrivate::render()
{
    if (std::tuple<GLuint, GLsync> job; pbosQueue.try_dequeue(job)) {
        auto& [pbo, fence] { job };
        // lecture 16 bits
        glReadBuffer(GL_COLOR_ATTACHMENT1);
        // bind le pbo
        glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo);
        // transfert asynchrone vers le pbo
        glReadPixels(size.x(), size.y(), size.width(), size.height(), GL_RED_INTEGER, downLoadType, nullptr);
        // relache le pbo
        glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
        // crée une barrière sur le download
        fence = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
        // pousse le job
        jobsQueue.enqueue(job);
    } else {
        std::cout << "HistogramGetterPrivate::render: no free pbo available." << std::endl;
    }
}

void HistogramGetterPrivate::destroy()
{
    // on met fin à la boucle de download
    if (renderThread.joinable()) {
        jobsQueue.enqueue({ 0, nullptr });
        renderThread.join();
    }
}

HistogramGetter::HistogramGetter(std::unique_ptr<HistogramGetterPrivate>&& impl)
    : _impl(std::move(impl))
{
}

HistogramGetter::~HistogramGetter()
{
    // on met fin à la boucle de download
    destroy();
}

void HistogramGetter::setBalance(float balance)
{
    _impl->balance.store(std::clamp(static_cast<double>(balance), 0., 1.));
}

void HistogramGetter::setFboSize(uint32_t width, uint32_t height)
{
    // stop le rendu
    if (_impl->renderThread.joinable()) {
        _impl->jobsQueue.enqueue({ 0, nullptr });
        _impl->renderThread.join();
    }

    _impl->size.setSourceSize(width, height);

    std::promise<void> barrier;
    std::future<void> barrierFuture { barrier.get_future() };
    _impl->renderThread = std::thread(&HistogramGetterPrivate::loop, _impl.get(), std::move(barrier), _impl->size.bufferSize());
    pthread_setname_np(_impl->renderThread.native_handle(), "glGetHistogram");

    barrierFuture.wait();
}

void HistogramGetter::render()
{
    _impl->render();
}

void HistogramGetter::destroy()
{
    _impl->destroy();
}

} // namespace Jgv::Gvsp::QtBackend
