/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtrecordermetas.h"

#include "gvspqtpixeltransform.h"

#include <filesystem>
#include <iostream>

namespace Jgv::Gvsp::QtBackend {

constexpr std::filesystem::perms DEFAULT_PERMS {
    std::filesystem::perms::owner_read | std::filesystem::perms::owner_write
    | std::filesystem::perms::group_read | std::filesystem::perms::group_write
    | std::filesystem::perms::others_read | std::filesystem::perms::others_write
};

RecorderMetas::RecorderMetas()
{
    auto value = Exiv2::Value::create(Exiv2::xmpText);
    Exiv2::XmpProperties::registerNs("http://videotheodolite.fr/1.0/", VTO_NAMESPACE.data());
    value->read("vto_2");
    _xmpData.add(Exiv2::XmpKey("Xmp.dc.source"), value.get());
    value->read("");
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), TRIEDRE.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), LYRE.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), CAMPAGNE.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), CRI.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), FOCALE.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), CAMERA.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), TIMESTAMP.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), DATE.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), UTC.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), UNITE_ANGLE.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), SITE.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), GISEMENT.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), CENTRE_X.data()), value.get());
    _xmpData.add(Exiv2::XmpKey(VTO_NAMESPACE.data(), CENTRE_Y.data()), value.get());
}

void RecorderMetas::setPath(std::string_view path) noexcept
{
    _path = path;
}

auto RecorderMetas::path() const noexcept -> std::string
{
    return _path;
}

void RecorderMetas::setMeta(std::string_view key, std::string_view value)
{
    // teste si la clé existe dans le namespace
    auto it { _xmpData.findKey(Exiv2::XmpKey(std::string { VTO_NAMESPACE }, std::string { key })) };
    if (it != _xmpData.end()) {
        it->setValue(std::string { value });
    }
}

auto RecorderMetas::meta(std::string_view key) const -> std::string
{
    // teste si la clé existe dans le namespace
    const Exiv2::XmpKey xmpKey { std::string { VTO_NAMESPACE }, std::string { key } };

    auto it { _xmpData.findKey(xmpKey) };
    if (it != _xmpData.end()) {
        return it->value().toString();
    }
    return {};
}

void RecorderMetas::tag(const XmpTags& tags)
{
    std::ostringstream value;
    value << tags.timestamp;
    setMeta(TIMESTAMP, value.str());

    value.str(std::string());
    value << tags.timecode.year
          << " " << std::setw(2) << std::setfill('0') << tags.timecode.month
          << " " << std::setw(2) << std::setfill('0') << tags.timecode.day;
    setMeta(DATE, value.str());

    value.str(std::string());
    value << std::setw(2) << std::setfill('0') << tags.timecode.hour
          << ':' << std::setw(2) << std::setfill('0') << tags.timecode.minute
          << ':' << std::setw(2) << std::setfill('0') << tags.timecode.second
          << '.' << std::setw(3) << std::setfill('0') << tags.timecode.milli;
    setMeta(UTC, value.str());

    const auto site { static_cast<double>(tags.site) / 1000.0 };
    value.str(std::string());
    if (site < 0) {
        value << "-";
    }
    value << std::fixed << std::setw(7) << std::setprecision(3) << std::setfill('0') << std::abs(site); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    setMeta(SITE, value.str());

    const auto gisement { static_cast<double>(tags.gisement) / 1000.0 };
    value.str(std::string());
    if (gisement < 0) {
        value << "-";
    }
    value << std::fixed << std::setw(7) << std::setprecision(3) << std::setfill('0') << std::abs(gisement); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    setMeta(GISEMENT, value.str());
}

void RecorderMetas::saveToFile(const uint8_t* mem, std::size_t size, const std::string& filename, const RecorderMetas& xmp)
{
    auto image { Exiv2::ImageFactory::open(mem, static_cast<int64_t>(size)) };
    image->setXmpData(xmp._xmpData);
    image->writeMetadata();

    std::string xmpPacket;
    if (0 != Exiv2::XmpParser::encode(xmpPacket, xmp._xmpData)) {
        //throw Exiv2::Error(1, "Failed to serialize XMP data");
    }

    Exiv2::FileIo file { filename };
    file.open();
    file.transfer(image->io());
    file.close();

    std::error_code ec;
    std::filesystem::permissions(filename, DEFAULT_PERMS, ec);
    if (ec != std::error_code()) {
        std::clog << "RecorderMetas::saveToFile failed to change permissions on " << filename << " error: " << ec.message() << std::endl;
    }
}

} // namespace Jgv::Gvsp::QtBackend
