/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQOPENGLPIPELINE_H
#define GVSPQOPENGLPIPELINE_H

#include <gvspdevices/gvspreceiver.h>

namespace Jgv::Gvsp {
struct Image;
} // namespace Jgv::Gvsp

namespace Jgv::Gvsp::QtBackend {

class OpenGLPipelinePrivate;
class OpenGLPipeline final : public Gvsp::MemoryAllocator {

public:
    explicit OpenGLPipeline(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~OpenGLPipeline() override;
    OpenGLPipeline(const OpenGLPipeline&) = delete;
    OpenGLPipeline(OpenGLPipeline&&) = delete;
    auto operator=(const OpenGLPipeline&) -> OpenGLPipeline& = delete;
    auto operator=(OpenGLPipeline &&) -> OpenGLPipeline& = delete;

    void initialize() noexcept final;
    auto allocate(uint32_t id, std::size_t size) noexcept -> std::span<uint8_t> override;
    void push(const Image& image) noexcept final;
    void trash(uint32_t id) noexcept override;
    void destroy() noexcept final;

private:
    const std::unique_ptr<OpenGLPipelinePrivate> _impl;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQOPENGLPIPELINE_H
