/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqthistogramuint12equalization.h"
#include "gvspqthistogramequalization_p.h"

#include <openglutils/openglprogram.h>

namespace Jgv::Gvsp::QtBackend {

constexpr auto UINT12_COUNT { 4096U };
constexpr auto UINT12_MAX { 4095.F };

void HistogramUint12Equalization::pushLut(AtomicVector<uint16_t>&& lut) noexcept
{
    _lut = std::move(lut);
}

void HistogramUint12Equalization::initialize() noexcept
{
    const std::string vSource =
#include "shader/texelfetch.vert"
        ;
    const std::string fSource =
#include "shader/histogrameq.frag"
        ;

    _impl->program = OpenGLUtils::Program::create(vSource, fSource);
    glUseProgram(_impl->program);
    auto maxId { glGetUniformLocation(_impl->program, "max") };
    glUniform1f(maxId, UINT12_MAX);
    glUseProgram(0);

    // la taille de la texture LUT ne changera jamais
    glGenTextures(1, &_impl->lutTexture);
    glBindTexture(GL_TEXTURE_1D, _impl->lutTexture);
    glTexStorage1D(GL_TEXTURE_1D, 1, GL_R16UI, UINT12_COUNT);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_1D, 0);
}

void HistogramUint12Equalization::uploadAndBind() noexcept
{
    if (const auto lut { std::unique_ptr<std::vector<uint16_t>>(_lut.release()) }) {
        glTexSubImage1D(GL_TEXTURE_1D, 0, 0, lut->size(), GL_RED_INTEGER, GL_UNSIGNED_SHORT, lut->data());
    }
}

} // namespace Jgv::Gvsp::QtBackend
