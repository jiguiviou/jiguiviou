/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTHISTOGRAMEQUALIZATION_P_H
#define GVSPQTHISTOGRAMEQUALIZATION_P_H

#include <epoxy/gl.h>

namespace Jgv::Gvsp::QtBackend {

struct HistogramEqualizationPrivate {
    GLuint program { 0U };
    GLuint fbo { 0U };
    GLuint renderTexture { 0U };
    GLuint lutTexture { 0U };
    GLuint vao { 0U };
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTHISTOGRAMEQUALIZATION_P_H
