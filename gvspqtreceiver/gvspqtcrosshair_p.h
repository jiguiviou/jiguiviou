/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTCROSSHAIR_P_H
#define GVSPQTCROSSHAIR_P_H

#include <epoxy/gl.h>

#include <tuple>

namespace Jgv::Gvsp::QtBackend {

struct CrosshairPrivate final {

    GLuint program { 0U };
    GLuint vao { 0U };
    GLuint vbo { 0U };

    GLfloat x;
    GLfloat y;
    GLfloat w;
    GLfloat h;
    //    std::tuple<GLfloat, GLfloat> center;
    //    std::tuple<GLfloat, GLfloat> size;

    void initialize() noexcept;
    void render() noexcept;
    void destroy() noexcept;

    void updateBufferObjects() noexcept;
    void setCenter(int32_t centerX, int32_t centerY) noexcept;
    void setSize(int32_t width, int32_t height) noexcept;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTCROSSHAIR_P_H
