/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqthistogramuint12getter.h"
#include "gvspqthistogramuint12getter_p.h"

#include <cmath>

namespace {

constexpr auto UINT12_MAX { 4095U };
constexpr auto HISTOGRAM_UINT12_SIZE { 1U + UINT12_MAX };
constexpr auto LUT_UINT12_SIZE { HISTOGRAM_UINT12_SIZE };

struct Histogram {
    std::array<uint32_t, HISTOGRAM_UINT12_SIZE> array;

    // value < 4096 est garanti par le shader
    void fill(std::span<const uint16_t> buffer) noexcept
    {
        for (const auto value : buffer) {
            max = std::max(max, ++array[value]); // NOLINT (cppcoreguidelines-pro-bounds-constant-array-index)
        }
    }
    uint32_t max;
};

constexpr auto AVERAGE_MAX { 16U };
class Average {
    int64_t _sum { 0 };
    uint _count { 0 };

public:
    auto push(uint_fast32_t value) noexcept -> bool
    {
        _sum += value;
        return (++_count == AVERAGE_MAX);
    }
    auto reset() noexcept -> float
    {
        const auto out { static_cast<float>(_sum) };
        _count = 0;
        _sum = 0;
        return out / AVERAGE_MAX;
    }
};

} // namespace

namespace Jgv::Gvsp::QtBackend {

void HistogramUint12GetterPrivate::download(std::size_t bufferSize)
{
    if (const auto buffer { glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY) }; buffer != nullptr) {

        // convient jusque image 65535 x 65535
        Histogram pmf {};
        pmf.fill({ static_cast<const uint16_t*>(buffer), static_cast<std::span<uint16_t>::size_type>(bufferSize) });
        const auto clip { std::pow(balance, CRITER_FACTOR) };
        const auto pixelsCount { static_cast<double>(bufferSize) };

        // on normalise l'histogramme et on applique le limite de dynamique
        const double clipLevel { static_cast<double>(pmf.max) / pixelsCount * clip };

        double extra { 0. };
        std::vector<double> normalizedHistogram;
        normalizedHistogram.reserve(HISTOGRAM_UINT12_SIZE);
        for (const auto value : pmf.array) {
            const auto normValue { static_cast<double>(value) / pixelsCount };
            if (normValue > clipLevel) {
                normalizedHistogram.push_back(clipLevel);
                extra += normValue - clipLevel;
            } else {
                normalizedHistogram.push_back(normValue);
            }
        }

        AtomicVector<float> histogram;
        histogram->reserve(HISTOGRAM_SIZE);
        AtomicVector<uint16_t> lut;
        lut->reserve(LUT_UINT12_SIZE);
        Average average;

        auto histogramIt { pmf.array.cbegin() };
        const auto histogramEnd { pmf.array.cend() };
        auto normalizedHistogramIt { normalizedHistogram.begin() };

        // diffusion de l'exces;
        const auto more { extra / static_cast<double>(HISTOGRAM_UINT12_SIZE) };
        *normalizedHistogramIt += more;

        average.push(*histogramIt);
        ++histogramIt;
        lut->push_back(static_cast<uint16_t>(lround(*normalizedHistogramIt * UINT12_MAX)));
        ++normalizedHistogramIt;
        while (histogramIt < histogramEnd) {
            if (average.push(*histogramIt)) {
                histogram->push_back(average.reset());
            }
            ++histogramIt;
            // on ajoute la valeur précédente ainsi que la valeur de diffusion
            *normalizedHistogramIt += *std::prev(normalizedHistogramIt) + more;
            lut->push_back(static_cast<uint16_t>(lround(*normalizedHistogramIt * UINT12_MAX)));
            ++normalizedHistogramIt;
        }

        if (histogramSetter) {
            histogramSetter(std::move(histogram));
        }
        if (lutSetter) {
            lutSetter(std::move(lut));
        }
    }
}

HistogramUint12Getter::HistogramUint12Getter(std::function<void(AtomicVector<uint16_t>&&)>&& lutSetter, std::function<void(AtomicVector<float>&&)>&& histogramSetter, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : HistogramGetter(std::make_unique<HistogramUint12GetterPrivate>(std::move(lutSetter), std::move(histogramSetter), channel, std::move(globalIpc)))
{
}

} // namespace Jgv::Gvsp::QtBackend
