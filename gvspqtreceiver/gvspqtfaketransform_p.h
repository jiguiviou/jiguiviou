/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTFAKETRANSFORM_P_H
#define GVSPQTFAKETRANSFORM_P_H

#include "gvspqtpixeltransform_p.h"
#include <openglutils/textpainter.h>
#include <vector>

namespace Jgv::Gvsp::QtBackend {

class FakeTransformPrivate final : public PixelTransformPrivate {
public:
    explicit FakeTransformPrivate(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);

    std::unique_ptr<OpenGLUtils::TextPainter> textPainter;
    std::vector<OpenGLUtils::TextPainterObject> texts;

    void initializeTransform() override;
    auto transform(GLuint pbo) -> GLuint override;
    void destroyTransform() override;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTFAKETRANSFORM_P_H
