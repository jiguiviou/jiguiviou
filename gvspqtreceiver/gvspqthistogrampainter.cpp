/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqthistogrampainter.h"
#include "gvspqthistogrampainter_p.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>
#include <iostream>
#include <openglutils/openglprogram.h>

namespace {
// l'histogramme est composé de 256 colonnes, soit 512 points
constexpr auto VERTICES_COUNT = 256 * 2;
// le nombre de triangle pour dessiner l'histogramme
constexpr auto TRIANGLES_COUNT = VERTICES_COUNT - 2;
// le nombre d'indices pour dessiner tous les triangles
constexpr auto TRIANGLES_INDICES_COUNT = TRIANGLES_COUNT * 3;

enum class BufferObject {
    BaselineVertices = 0,
    HistogramVertices = 1,
    HistogramElements = 2
};
enum class Vaos {
    BaseLine = 0,
    Histogram = 1
};

auto fill(const std::unique_ptr<std::vector<float>>& histogram, GLfloat* const dest) -> float
{
    float max { 0. };
    auto p { dest + 1 };

    auto it { histogram->cbegin() };
    for (; it != histogram->cend(); ++it, p += 2) {
        *p = *it;
        max = (max < *p) ? *p : max;
    }
    return max;
}

} // namespace

namespace Jgv::Gvsp::QtBackend {

HistogramPainter::HistogramPainter()
    : _impl(std::make_unique<HistogramPainterPrivate>())
{
}

HistogramPainter::~HistogramPainter() = default;

void HistogramPainter::pushHistogram(AtomicVector<float>&& histogram)
{
    _impl->hist = std::move(histogram);
}

void HistogramPainter::initialize()
{
    /*! Les indices vont permettre de modifier l'ordre des points
     * On groupe d'abord les points qui varient à chaque image,
     * puis les points sur l'axe des absisses (qui ne bougent jamais).
     * Cela permettra de faire des map partiels du tampon de vertices
     * et de réduire les transferts vers le serveur opengl. */
    auto indices = [](GLushort* buffer) {
        for (GLushort i = 0; i < 255; i++) {
            const int x = 6 * i;
            buffer[x] = i;
            buffer[x + 1] = i + 256;
            buffer[x + 2] = i + 257;
            buffer[x + 3] = buffer[x];
            buffer[x + 4] = buffer[x + 2];
            buffer[x + 5] = i + 1;
        }
    };

    const std::string vSource =
#include "shader/histogram.vert"
        ;
    const std::string fSource =
#include "shader/primitive.frag"
        ;
    _impl->program = OpenGLUtils::Program::create(vSource, fSource);

    const auto flip { glm::scale(glm::mat4(1.0F), glm::vec3(1.0F, -1.0F, 1.0F)) };
    const auto center { glm::translate(glm::mat4(1.0F), glm::vec3(-0.5F, -0.5F, 0.F)) };
    const auto scale { glm::scale(glm::mat4(1.0F), glm::vec3(0.8F, 0.8F, 1.F)) };
    const auto translate { glm::translate(glm::mat4(1.0F), glm::vec3(-0.5F, -0.5F, 0.F)) };
    const auto projection { flip * center * scale * translate };

    glUseProgram(_impl->program);
    const auto transform { glGetUniformLocation(_impl->program, "transform") };
    glUniformMatrix4fv(transform, 1, GL_FALSE, &projection[0][0]);
    const auto color { glGetUniformLocation(_impl->program, "color") };
    glUniform4f(color, 0.973F, 1.0F, 0.39F, 0.5F);
    _impl->max = glGetUniformLocation(_impl->program, "max");
    glUseProgram(0);

    glGenBuffers(static_cast<GLsizei>(_impl->bufferObjects.size()), _impl->bufferObjects.data());
    glGenVertexArrays(static_cast<GLsizei>(_impl->vaos.size()), _impl->vaos.data());

    // la ligne de base
    const std::array<GLfloat, 4> baseLinePoints { { 0.F, 0.F, 1.F, 0.F } };
    // les sommets
    glBindBuffer(GL_ARRAY_BUFFER, _impl->bufferObjects[static_cast<int>(BufferObject::BaselineVertices)]);
    glBufferData(GL_ARRAY_BUFFER, baseLinePoints.size() * sizeof(GLfloat), baseLinePoints.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(_impl->vaos[static_cast<int>(Vaos::BaseLine)]);
    glBindBuffer(GL_ARRAY_BUFFER, _impl->bufferObjects[static_cast<int>(BufferObject::BaselineVertices)]);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), OpenGLUtils::offsetPtr<GLfloat>(0));
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);

    std::array<GLfloat, VERTICES_COUNT * 2> vBuffer {};
    for (size_t i = 0; i < 256; ++i) {
        const auto x = 2 * i;
        vBuffer.at(x) = static_cast<GLfloat>(i) / 255.F;
        vBuffer.at(x + 1) = 0.5;
        vBuffer.at(x + 512) = vBuffer.at(x);
        vBuffer.at(x + 512 + 1) = 0.0;
    }

    glBindBuffer(GL_ARRAY_BUFFER, _impl->bufferObjects[static_cast<int>(BufferObject::HistogramVertices)]);
    glBufferData(GL_ARRAY_BUFFER, VERTICES_COUNT * 2 * sizeof(GLfloat), vBuffer.data(), GL_STREAM_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    GLushort iBuffer[TRIANGLES_INDICES_COUNT];
    indices(iBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _impl->bufferObjects[static_cast<int>(BufferObject::HistogramElements)]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, (TRIANGLES_INDICES_COUNT) * sizeof(GLushort), iBuffer, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glBindVertexArray(_impl->vaos[static_cast<int>(Vaos::Histogram)]);
    glBindBuffer(GL_ARRAY_BUFFER, _impl->bufferObjects[static_cast<int>(BufferObject::HistogramVertices)]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _impl->bufferObjects[static_cast<int>(BufferObject::HistogramElements)]);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), OpenGLUtils::offsetPtr<GLfloat>(0));
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void HistogramPainter::render()
{
    glUseProgram(_impl->program);
    if (auto h { std::unique_ptr<std::vector<float>>(_impl->hist.release()) }) {
        glBindBuffer(GL_ARRAY_BUFFER, _impl->bufferObjects[static_cast<int>(BufferObject::HistogramVertices)]);
        auto p { glMapBufferRange(GL_ARRAY_BUFFER, 0, 512 * sizeof(GLfloat), GL_MAP_WRITE_BIT) };
        if (p != nullptr) {
            const auto max { fill(h, static_cast<GLfloat*>(p)) };
            glUniform1f(_impl->max, max);
            glUnmapBuffer(GL_ARRAY_BUFFER);
        }
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBlendEquation(GL_FUNC_ADD);
    glBindVertexArray(_impl->vaos[static_cast<int>(Vaos::BaseLine)]);
    glDrawArrays(GL_LINES, 0, 4);
    glBindVertexArray(0);
    glBindVertexArray(_impl->vaos[static_cast<int>(Vaos::Histogram)]);
    glDrawElements(GL_TRIANGLES, TRIANGLES_INDICES_COUNT, GL_UNSIGNED_SHORT, nullptr);
    glBindVertexArray(0);
    glDisable(GL_BLEND);

    glUseProgram(0);
}

void HistogramPainter::destroy()
{
    glDeleteBuffers(static_cast<GLsizei>(_impl->bufferObjects.size()), _impl->bufferObjects.data());
    glDeleteVertexArrays(static_cast<GLsizei>(_impl->vaos.size()), _impl->vaos.data());
    glDeleteProgram(_impl->program);
}

} // namespace Jgv::Gvsp::QtBackend
