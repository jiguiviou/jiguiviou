/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtgrey12transform.h"
#include "gvspqtgrey12transform_p.h"

#include <openglutils/openglprogram.h>

namespace Jgv::Gvsp::QtBackend {

Grey12TransformPrivate::Grey12TransformPrivate(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : PixelTransformPrivate(width, height, pixelFormat, channel, std::move(globalIpc))
{
    jpegFormat = JpegRecorderFormat::Grey;
}

void Grey12TransformPrivate::initializeTransform()
{
    const std::string vSource =
#include "shader/texelfetch.vert"
        ;
    const std::string fSource =
#include "shader/mono12packed2mono16.frag"
        ;
    program = OpenGLUtils::Program::create(vSource, fSource);

    glGenTextures(1, &uploadTexture);
    glGenTextures(1, &integerTexture);

    // la texture pour l'upload
    glBindTexture(GL_TEXTURE_2D, uploadTexture);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8UI, (width * 3) / 2, height);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);

    // la texture 16 bits mono, pour le download
    glBindTexture(GL_TEXTURE_2D, integerTexture);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R16UI, width, height);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);

    // on attache au fbo
    glBindFramebuffer(GL_FRAMEBUFFER, renderFbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, integerTexture, 0);
    std::array<GLenum, 2> bufs { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
    glDrawBuffers(2, bufs.data());
    auto status { glCheckFramebufferStatus(GL_FRAMEBUFFER) };
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        std::cerr << " Grey12Transform::resize: create fbo failed." << std::endl;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

auto Grey12TransformPrivate::transform(GLuint pbo) -> GLuint
{
    glBindTexture(GL_TEXTURE_2D, uploadTexture);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, (width * 3) / 2, height, GL_RED_INTEGER, GL_UNSIGNED_BYTE, nullptr);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    glUseProgram(program);
    glBindVertexArray(vao);
    glDrawElements(GL_TRIANGLES, OpenGLUtils::QUAD_ELEMENTS_COUNT, GL_UNSIGNED_BYTE, nullptr);
    glBindVertexArray(0);
    glUseProgram(0);
    glBindTexture(GL_TEXTURE_2D, 0);
    return integerTexture;
}

void Grey12TransformPrivate::destroyTransform()
{
    glDeleteTextures(1, &uploadTexture);
    glDeleteTextures(1, &integerTexture);
    glDeleteProgram(program);
}

Grey12Transform::Grey12Transform(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : PixelTransform(std::make_unique<Grey12TransformPrivate>(width, height, pixelFormat, channel, std::move(globalIpc)))
{
}

} // namespace Jgv::Gvsp::QtBackend
