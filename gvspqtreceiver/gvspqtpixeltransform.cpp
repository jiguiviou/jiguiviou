/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtpixeltransform.h"
#include "gvspqtpixeltransform_p.h"

#include "gvspqtbayertransform.h"
#include "gvspqtcontourenhancer.h"
#include "gvspqtcrosshair.h"
#include "gvspqtfaketransform.h"
#include "gvspqtgrey12transform.h"
#include "gvspqtgreytransform.h"
#include "gvspqthistogrampainter.h"
#include "gvspqthistogramuint12equalization.h"
#include "gvspqthistogramuint12getter.h"
#include "gvspqthistogramuint8equalization.h"
#include "gvspqthistogramuint8getter.h"
#include "gvspqtjpegrecorder.h"
#include "gvspqtpboring.h"
#include "gvspqtreceiver.h"
#include "gvspqtrgbtransform.h"
#include "gvspqttimecodepainter.h"
#include "gvspqtyuv12transform.h"
#include "gvspqtyuv16transform.h"
#include "gvspqtyuv24transform.h"

#include <epoxy/gl.h>

#include <QOffscreenSurface>
#include <QOpenGLContext>

#include <filesystem>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <gvspdevices/gvsp.h>
#include <openglutils/openglprogram.h>
#include <span>

namespace {
constexpr GLuint64 FENCE_TIMEOUT { 100000000U }; // 100 ms
constexpr std::string_view IN_DIRECTORY { "in" };
constexpr std::string_view OUT_DIRECTORY { "out" };

struct ShaderDatas {
    std::array<float, 2> texSize;
};

constexpr std::array<GLfloat, 16> VERTICES {
    1.F, 1.F, 1.F, 1.F,
    1.F, -1.F, 1.F, 0.F,
    -1.F, -1.F, 0.F, 0.F,
    -1.F, 1.F, 0.F, 1.F
};

constexpr std::array<GLubyte, 6> ELEMENTS {
    0, 1, 3, 1, 2, 3
};

constexpr auto centerFromSize(uint32_t width, uint32_t height, const Jgv::GlobalIPC::Pair<int32_t>& center) -> std::tuple<int32_t, int32_t>
{
    auto [x, y] { center };
    // le système de coordonnées de l'image commence à (0, 0), et l'axe des y est inversé
    // le centre est donc ((width / 2) - 1, (height / 2) - 1)
    return { static_cast<int32_t>(width / 2) - 1 + x, static_cast<int32_t>(height / 2) - 1 - y };
}

constexpr auto stateIsValid(GLenum state) noexcept
{
    return (state == GL_CONDITION_SATISFIED || state == GL_ALREADY_SIGNALED);
}

} // namespace

namespace Jgv::Gvsp::QtBackend {

PixelTransformPrivate::PixelTransformPrivate(uint32_t _width, uint32_t _height, uint32_t _pixelFormat, Channel::Type _channel, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : width { _width }
    , height { _height }
    , pixelFormat { _pixelFormat }
    , channel { _channel }
    , globalIpc { std::move(_globalIpc) }
{
    textureGetter = std::make_shared<std::function<std::tuple<uint, uint32_t, uint32_t>(void)>>([this]() -> std::tuple<uint, uint32_t, uint32_t> { return { blitTexture, width, height }; });

    // il n'y a d'effets que dans le context vto_2
    if (auto vtoConfig { globalIpc->vtoConfig() }) {
        // l'equalization n'est disponible que pour les images monochoromes 8 et 12 bit
        auto pixelFormatEqualizationIsSupported = [](uint32_t pixelFormat) {
            return (pixelFormat == GVSP_PIX_MONO8) || (pixelFormat == GVSP_PIX_MONO12_PACKED);
        };

        widthEqualization = pixelFormatEqualizationIsSupported(pixelFormat) && vtoConfig->histogramEqualizationEnabled(channel);
        const bool recIn { vtoConfig->recorderInEnabled(channel) };
        const bool recOut { vtoConfig->recorderOutEnabled(channel) };
        const bool ch { vtoConfig->crosshairEnabled(channel) };

        if (widthEqualization) {
            if (pixelFormat == GVSP_PIX_MONO8) {
                histogramGetter = std::make_unique<HistogramUint8Getter>(
                    [this](AtomicVector<uint8_t>&& lut) { histogramEq->pushLut(std::move(lut)); },
                    [this](AtomicVector<float>&& histogram) { histogramPainter->pushHistogram(std::move(histogram)); },
                    channel, globalIpc);

                histogramEq = std::make_unique<HistogramUint8Equalization>();
            } else if (pixelFormat == GVSP_PIX_MONO12_PACKED) {
                histogramGetter = std::make_unique<HistogramUint12Getter>(
                    [this](AtomicVector<uint16_t>&& lut) { histogramEq->pushLut(std::move(lut)); },
                    [this](AtomicVector<float>&& histogram) { histogramPainter->pushHistogram(std::move(histogram)); },
                    channel, globalIpc);

                histogramEq = std::make_unique<HistogramUint12Equalization>();
            }

            contourEnhancer = std::make_unique<ContourEnhancer>();
            histogramPainter = std::make_unique<HistogramPainter>();

            { // balance
                auto setter = [this](float value) {
                    equalizeBalance.set(value);
                };
                auto getter = [this]() -> float {
                    return equalizeBalance.get();
                };
                equalizeBalance.setter = std::make_shared<std::function<void(float)>>(setter);
                equalizeBalance.getter = std::make_shared<std::function<float(void)>>(getter);
                vtoConfig->setEqualizeFactorSetter(channel, equalizeBalance.setter);
                vtoConfig->setEqualizeFactorGetter(channel, equalizeBalance.getter);
            }

            { // equalize

                auto emitChanged = [this, vtoConfig](bool value) {
                    vtoConfig->emitEqualizeChanged(channel, value);
                };
                auto toggle = [this, vtoConfig]() {
                    equalize.toggle();
                };
                auto getter = [this]() -> bool {
                    return equalize.get();
                };
                equalize.emitEvent = emitChanged;
                equalize.toggler = std::make_shared<std::function<void(void)>>(toggle);
                equalize.getter = std::make_shared<std::function<bool(void)>>(getter);
                vtoConfig->setEqualizeToggler(channel, equalize.toggler);
                vtoConfig->setEqualizeGetter(channel, equalize.getter);
            }
        }

        if (recIn) {
            recorderIn = std::make_unique<JpegRecorder>(jpegFormat, IN_DIRECTORY, channel, globalIpc);
        }

        if (recOut) {
            std::cout << "RECOUT " << std::endl;
            recorderOut = std::make_unique<JpegRecorder>(jpegFormat, OUT_DIRECTORY, channel, globalIpc);
        }

        if (recOut || recIn) {
            { //  toggler
                auto emitChanged = [this, vtoConfig](bool value) {
                    vtoConfig->emitRecordChanged(channel, value);
                };
                auto toggle = [this, vtoConfig]() {
                    record.toggle();
                };
                auto getter = [this]() -> bool {
                    return record.get();
                };
                record.emitEvent = emitChanged;
                record.toggler = std::make_shared<std::function<void(void)>>(toggle);
                record.getter = std::make_shared<std::function<bool(void)>>(getter);
                vtoConfig->setRecordToggler(channel, record.toggler);
                vtoConfig->setRecordGetter(channel, record.getter);
            }

            { // snapshot
                auto setter = [this](uint count, std::string_view directory) {
                    snapshot.set(count, directory);
                };
                snapshot.setter = std::make_shared<std::function<void(uint, std::string_view)>>(setter);
                vtoConfig->setSnapshotCommand(channel, snapshot.setter);
            }
        }

        if (ch) {
            crosshair = std::make_unique<Crosshair>();
            crosshairCenter.set(vtoConfig->reticleOffsets(channel));
            { //center
                auto setter = [this](const GlobalIPC::Pair<int32_t>& center) {
                    crosshairCenter.set(center);
                };
                crosshairCenter.setter = std::make_shared<std::function<void(GlobalIPC::Pair<int32_t>)>>(setter);
                vtoConfig->setReticleOffsetsSetter(channel, crosshairCenter.setter);
            }
        }

        { // metas
            cameraName = globalIpc->emetterName(channel);
            // si une campagne est en cours (restaurée)
            if (auto restored { vtoConfig->vtoXmp() }) {
                vtoMetas.set(restored, cameraName);
            }
            auto setter = [this](const std::shared_ptr<RecorderMetas>& metas) {
                vtoMetas.set(metas, cameraName);
            };

            vtoMetas.setter = std::make_shared<std::function<void(const std::shared_ptr<RecorderMetas>&)>>(setter);
            vtoConfig->setVtoXmpSetter(vtoMetas.setter);
        }

        timecode = std::make_unique<TimecodePainter>();
    }
}

PixelTransformPrivate::~PixelTransformPrivate() = default;

auto PixelTransformPrivate::handleImageFormat(uint32_t _width, uint32_t _height, uint32_t _pixelFormat) const noexcept -> bool
{
    return (width == _width) && (height == _height) && (pixelFormat == _pixelFormat);
}

void PixelTransformPrivate::initialize() noexcept
{
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &indexes);
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &gbo);
    glGenTextures(1, &renderTexture);
    glGenTextures(1, &blitTexture);
    glGenFramebuffers(1, &renderFbo);
    glGenFramebuffers(1, &blitFbo);

    // les sommets
    const auto v { VERTICES };
    std::span<const GLfloat> vSpan { v };
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, vSpan.size_bytes(), vSpan.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // les indices
    const auto e { ELEMENTS };
    std::span<const GLubyte> eSpan { e };
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexes);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, eSpan.size_bytes(), eSpan.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // le vao
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexes);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), OpenGLUtils::offsetPtr<GLfloat>(0));
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), OpenGLUtils::offsetPtr<GLfloat>(2));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    ShaderDatas shaderDatas { { static_cast<GLfloat>(width), static_cast<GLfloat>(height) } };

    glBindBuffer(GL_UNIFORM_BUFFER, gbo);
    glBufferData(GL_UNIFORM_BUFFER, sizeof shaderDatas, &shaderDatas, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    GLuint binding_point_index { 0U };
    glBindBufferBase(GL_UNIFORM_BUFFER, binding_point_index, gbo);

    // la texture de rendu
    glBindTexture(GL_TEXTURE_2D, renderTexture);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, width, height);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);

    // le fbo de rendu
    glBindFramebuffer(GL_FRAMEBUFFER, renderFbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderTexture, 0);
    auto status { glCheckFramebufferStatus(GL_FRAMEBUFFER) };
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        std::cerr << "Gvsp::QtBackend::PixelTransform::initialize: create fbo failed." << std::endl;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // la texture pour le blit
    glBindTexture(GL_TEXTURE_2D, blitTexture);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width, height);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);

    // fbo double buffer
    glBindFramebuffer(GL_FRAMEBUFFER, blitFbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, blitTexture, 0);
    status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        std::cerr << " PixelTransform::resize: create fbo failed." << std::endl;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    pool.initialize();

    // diffuse la fin d'initialisation
    globalIpc->setTextureGetter(channel, textureGetter);
    globalIpc->textureChanged(channel, { blitTexture, width, height });
}

void PixelTransformPrivate::destroy() noexcept
{
    glDeleteFramebuffers(1, &blitFbo);
    glDeleteFramebuffers(1, &renderFbo);
    glDeleteTextures(1, &blitTexture);
    glDeleteTextures(1, &renderTexture);
    glDeleteBuffers(1, &gbo);
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &indexes);
    glDeleteBuffers(1, &vbo);

    pool.destroy();
}

void PixelTransformPrivate::initializeEffects() noexcept
{
    if (histogramGetter) {
        histogramGetter->setFboSize(width, height);
    }
    if (histogramEq) {
        histogramEq->initialize(vbo, indexes);
        histogramEq->setFboSize(width, height);
    }
    if (contourEnhancer) {
        contourEnhancer->initialize(vbo, indexes);
    }
    if (histogramPainter) {
        histogramPainter->initialize();
    }
    if (timecode) {
        timecode->initialize();
        timecode->setFboSize({ width, height });
    }
    if (crosshair) {
        crosshair->initialize();
    }
    if (recorderIn) {
        recorderIn->setFboSize(width, height);
    }
    if (recorderOut) {
        recorderOut->setFboSize(width, height);
    }
    if (crosshair) {
        crosshair->setFboSize(width, height);
    }
}

void PixelTransformPrivate::destroyEffects() noexcept
{
    if (histogramGetter) {
        histogramGetter->destroy();
    }
    if (histogramEq) {
        histogramEq->destroy();
    }
    if (contourEnhancer) {
        contourEnhancer->destroy();
    }
    if (histogramPainter) {
        histogramPainter->destroy();
    }
    if (timecode) {
        timecode->destroy();
    }
    if (crosshair) {
        crosshair->destroy();
    }
    if (recorderIn) {
        recorderIn->destroy();
    }
    if (recorderOut) {
        recorderOut->destroy();
    }
    if (crosshair) {
        crosshair->destroy();
    }
}

void PixelTransformPrivate::loop(std::promise<void> barrier) noexcept
{
    // on a besoin d'un context sur le thread,
    QSurfaceFormat fmt;
    fmt.setMajorVersion(4);
    fmt.setMinorVersion(2);
    fmt.setProfile(QSurfaceFormat::CoreProfile);

    QOffscreenSurface surface;
    surface.setFormat(fmt);
    surface.create();

    QOpenGLContext context;
    context.setFormat(fmt);
    context.setShareContext(globalIpc->sharedContext());
    if (!context.create()) {
        std::cerr << "Gvsp::QtBackend::PixelTransform: create context failed." << std::endl;
        return;
    }

    context.makeCurrent(&surface);
    initialize();
    initializeTransform();
    initializeEffects();

    glViewport(0, 0, width, height);

    context.doneCurrent();

    std::tuple<GLuint, GLsync, int64_t, int32_t, int32_t> metas {};

    //    // on met à jour le centre dans les métas
    //    const auto [x, y] { centerFromSize(width, height, crosshairCenter.get()) };
    //    // on met à jour, les métas
    //    RecorderMetas& metas(vtoMetas);
    //    metas.setMeta(CENTRE_X, std::to_string(x));
    //    metas.setMeta(CENTRE_Y, std::to_string(y));

    // on est initiliasé, on lève la barrière
    barrier.set_value();

    while (true) {
        pboQueue.wait_dequeue(metas);
        const auto [pbo, fence, timestamp, site, gisement] { metas };
        if (pbo > 0) {
            context.makeCurrent(&surface);
            // se met en attente de la barrière
            const auto state { glClientWaitSync(fence, GL_SYNC_FLUSH_COMMANDS_BIT, FENCE_TIMEOUT) };
            glDeleteSync(fence);
            if (stateIsValid(state)) {
                render(metas);
            }
            context.doneCurrent();
        } else {
            break;
        }
    }
    context.makeCurrent(&surface);
    destroyEffects();
    destroyTransform();
    destroy();

    context.doneCurrent();
    surface.destroy();
}

void PixelTransformPrivate::render(std::tuple<GLuint, GLsync, int64_t, int32_t, int32_t> metas)
{
    auto& [pbo, fence, timestamp, site, gisement] { metas };
    glBindFramebuffer(GL_FRAMEBUFFER, renderFbo);

    auto integerTexture { transform(pbo) };
    pool.enqueue({ pbo, glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0) });

    const auto [recChanged, rec] { record.changed() };
    if (recChanged && rec) {
        recordSubDirectory = JpegRecorder::timecodeToString(timestamp);
    }

    if (equalizeBalance.changed()) {
        if (histogramGetter) {
            histogramGetter->setBalance(equalizeBalance.get());
        }
    }

    if (vtoMetas.changed()) {
        // on met à jour le centre dans les métas
        const auto [x, y] { centerFromSize(width, height, crosshairCenter.get()) };
        // on met à jour, les métas
        RecorderMetas& metas(vtoMetas);
        metas.setMeta(CENTRE_X, std::to_string(x));
        metas.setMeta(CENTRE_Y, std::to_string(y));
    }

    if (crosshairCenter.changed()) {
        const GlobalIPC::Pair<int32_t>& center(crosshairCenter);
        const auto [x, y] { centerFromSize(width, height, center) };
        // on met à jour, les métas
        RecorderMetas& metas(vtoMetas);
        metas.setMeta(CENTRE_X, std::to_string(x));
        metas.setMeta(CENTRE_Y, std::to_string(y));

        // puis le dessin du réticule
        if (crosshair) {
            crosshair->setCenter(center.first, center.second);
        }
    }

    if (recorderIn) {
        if (rec) {
            recorderIn->render(timestamp, site, gisement, recordSubDirectory, snapshot.name(), vtoMetas.get());
        }
    }
    if (widthEqualization) {
        histogramGetter->render();
        if (equalize) {
            const auto floatTexture { histogramEq->render(integerTexture) };
            glBindFramebuffer(GL_FRAMEBUFFER, renderFbo);
            contourEnhancer->render(floatTexture);
        }
    }

    if (crosshair) {
        crosshair->render();
    }
    if (timecode) {
        timecode->render(timestamp, site, gisement);
    }
    if (recorderOut) {
        if (rec || snapshot) {
            recorderOut->render(timestamp, site, gisement, recordSubDirectory, snapshot.name(), vtoMetas.get());
            snapshot--;
        }
    }
    if (histogramPainter) {
        histogramPainter->render();
    }

    glReadBuffer(GL_COLOR_ATTACHMENT0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, blitFbo);
    glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void PixelTransformPrivate::destroyTransform()
{
    glDeleteBuffers(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &indexes);
    glDeleteFramebuffers(1, &blitFbo);
}

PixelTransform::PixelTransform(std::unique_ptr<PixelTransformPrivate>&& dd)
    : _impl { std::move(dd) }
{
}

PixelTransform::~PixelTransform()
{
    if (_impl->thread.joinable()) {
        _impl->pboQueue.enqueue({});
        _impl->thread.join();
    }
}

auto PixelTransform::make(uint32_t width, uint32_t height, uint32_t pixelFormat, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc) -> std::unique_ptr<PixelTransform>
{
    switch (pixelFormat) {
    case GVSP_PIX_MONO8:
        return std::make_unique<GreyTransform>(width, height, pixelFormat, channel, std::move(globalIpc));
    case GVSP_PIX_MONO12_PACKED:
        return std::make_unique<Grey12Transform>(width, height, pixelFormat, channel, std::move(globalIpc));
    case GVSP_PIX_BAYGR8:
    case GVSP_PIX_BAYRG8:
    case GVSP_PIX_BAYGB8:
    case GVSP_PIX_BAYBG8:
        return std::make_unique<BayerTransform>(width, height, pixelFormat, channel, std::move(globalIpc));
    case GVSP_PIX_RGB8_PACKED:
    case GVSP_PIX_BGR8_PACKED:
        return std::make_unique<RgbTransform>(width, height, pixelFormat, channel, std::move(globalIpc));
    case GVSP_PIX_YUV411_PACKED:
        return std::make_unique<Yuv12Transform>(width, height, pixelFormat, channel, std::move(globalIpc));
    case GVSP_PIX_YUV422_PACKED:
        return std::make_unique<Yuv16Transform>(width, height, pixelFormat, channel, std::move(globalIpc));
    case GVSP_PIX_YUV444_PACKED:
        return std::make_unique<Yuv24Transform>(width, height, pixelFormat, channel, std::move(globalIpc));
    default:
        return std::make_unique<FakeTransform>(width, height, pixelFormat, channel, std::move(globalIpc));
    }
}

auto PixelTransform::handleImageFormat(uint32_t width, uint32_t height, uint32_t pixelFormat) const noexcept -> bool
{
    return _impl->handleImageFormat(width, height, pixelFormat);
}

void PixelTransform::initialize()
{
    std::promise<void> barrier;
    std::future<void> barrierFuture { barrier.get_future() };
    _impl->thread = std::thread(&PixelTransformPrivate::loop, _impl.get(), std::move(barrier));
    pthread_setname_np(_impl->thread.native_handle(), "glPixTrans");
    barrierFuture.wait();
}

auto PixelTransform::dequeue(std::size_t size) -> std::tuple<GLuint, std::span<uint8_t>>
{
    return _impl->pool.dequeue(size);
}

void PixelTransform::requeue(GLuint pbo, GLsync fence)
{
    _impl->pool.enqueue({ pbo, fence });
}

void PixelTransform::push(GLuint pbo, GLsync fence, int64_t timestamp, int32_t site, int32_t gisement)
{
    _impl->pboQueue.enqueue({ pbo, fence, timestamp, site, gisement });
}

} // namespace Jgv::Gvsp::QtBackend
