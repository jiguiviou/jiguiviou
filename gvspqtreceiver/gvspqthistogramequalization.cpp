/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqthistogramequalization.h"
#include "gvspqthistogramequalization_p.h"

#include <openglutils/openglprogram.h>

namespace Jgv::Gvsp::QtBackend {

HistogramEqualization::HistogramEqualization()
    : _impl { std::make_unique<HistogramEqualizationPrivate>() }
{
}

HistogramEqualization::~HistogramEqualization() = default;

void HistogramEqualization::initialize(GLuint quadVbo, GLuint quadIVbo)
{
    initialize();
    glGenVertexArrays(1, &_impl->vao);
    // vao pour le rendu
    glBindVertexArray(_impl->vao);
    glBindBuffer(GL_ARRAY_BUFFER, quadVbo);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), OpenGLUtils::offsetPtr<GLfloat>(0));
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), OpenGLUtils::offsetPtr<GLfloat>(2));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, quadIVbo);
    glBindVertexArray(0);
}

void HistogramEqualization::setFboSize(uint32_t width, uint32_t height)
{
    // on détruit le fbo et sa texture
    glDeleteFramebuffers(1, &_impl->fbo);
    glDeleteTextures(1, &_impl->renderTexture);

    // regénère
    glGenFramebuffers(1, &_impl->fbo);
    glGenTextures(1, &_impl->renderTexture);

    // la texture pour le rendu vers le filtrage
    glBindTexture(GL_TEXTURE_2D, _impl->renderTexture);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, width, height);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);
    // attache au fbo de rendu intermédiaire
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _impl->fbo);
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, _impl->renderTexture, 0);
    auto status { glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER) };
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        std::cerr << "HistogramEqualization::resize: create fbo failed." << std::endl;
    }

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

auto HistogramEqualization::render(GLuint sourceTexture) -> GLuint
{
    // le rendu
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _impl->fbo);
    glUseProgram(_impl->program);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_1D, _impl->lutTexture);
    uploadAndBind();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, sourceTexture);

    glBindVertexArray(_impl->vao);
    glDrawElements(GL_TRIANGLES, OpenGLUtils::QUAD_ELEMENTS_COUNT, GL_UNSIGNED_BYTE, nullptr);
    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindTexture(GL_TEXTURE_1D, 0);

    glUseProgram(0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    return _impl->renderTexture;
}

void HistogramEqualization::destroy()
{
    glDeleteTextures(1, &_impl->renderTexture);
    glDeleteTextures(1, &_impl->lutTexture);
    glDeleteBuffers(1, &_impl->vao);
    glDeleteProgram(_impl->program);
    glDeleteFramebuffers(1, &_impl->fbo);
}

} // namespace Jgv::Gvsp::QtBackend
