/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtopenglpipeline.h"
#include "gvspqtopenglpipeline_p.h"

#include "gvspqtpixeltransform.h"

#include <QOpenGLContext>
#include <globalipc/globalipc.h>

#include <iostream>

namespace Jgv::Gvsp::QtBackend {

OpenGLPipelinePrivate::OpenGLPipelinePrivate(Channel::Type _channel, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : channel { _channel }
    , globalIpc { std::move(_globalIpc) }
{
    auto changePixelFormat = [this](uint32_t width, uint32_t height, uint32_t pixelFormat) {
        auto newOne { PixelTransform::make(width, height, pixelFormat, channel, globalIpc) };
        newOne->initialize();
        newTransform.reset(newOne.release());
    };
    changePixelFormatFunction = std::make_shared<std::function<void(uint32_t, uint32_t, uint32_t)>>(changePixelFormat);
    globalIpc->addPixelFormatChangedListener(channel, changePixelFormatFunction);
}

auto OpenGLPipelinePrivate::allocate(uint32_t id, std::size_t size) noexcept -> std::span<uint8_t>
{
    auto newOne { newTransform.release() };
    if (newOne != nullptr) {
        transform.reset(newOne);
    }
    if (transform) {
        context->makeCurrent(&surface);
        const auto& [pbo, span] { transform->dequeue(size) };
        context->doneCurrent();
        imagePbos[id] = pbo;
        return span;
    }
    return {};
}

void OpenGLPipelinePrivate::push(const Image& image) noexcept
{
    // récupère le pbo
    if (transform->handleImageFormat(image.width, image.height, image.pixelFormat)) {
        const auto search { imagePbos.find(image.number) };
        if (search != imagePbos.cend()) {
            const auto [number, pbo] { *search };
            context->makeCurrent(&surface);
            glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
            glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
            glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
            auto fence { glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0) };
            // on flush pour la barrière
            glFlush();
            context->doneCurrent();
            transform->push(pbo, fence, image.timestamp, image.site, image.gisement);
            imagePbos.erase(search);
        }
    } else {
        trash(image.number);
    }
}

void OpenGLPipelinePrivate::trash(uint32_t id) noexcept
{
    const auto search { imagePbos.find(id) };
    if (search != imagePbos.cend()) {
        const auto [number, pbo] { *search };
        context->makeCurrent(&surface);
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
        glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
        auto fence { glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0) };
        // on flush pour la barrière
        glFlush();
        context->doneCurrent();
        transform->requeue(pbo, fence);
        imagePbos.erase(search);
    }
}

OpenGLPipeline::OpenGLPipeline(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl(std::make_unique<OpenGLPipelinePrivate>(channel, std::move(globalIpc)))
{
}

OpenGLPipeline::~OpenGLPipeline() = default;

void OpenGLPipeline::initialize() noexcept
{
    QSurfaceFormat fmt;
    fmt.setMajorVersion(4);
    fmt.setMinorVersion(2);
    fmt.setProfile(QSurfaceFormat::CoreProfile);

    _impl->surface.setFormat(fmt);
    _impl->surface.create();

    _impl->context = std::make_unique<QOpenGLContext>();
    _impl->context->setFormat(_impl->surface.format());
    _impl->context->setShareContext(_impl->globalIpc->sharedContext());
    _impl->context->create();
}

auto OpenGLPipeline::allocate(uint32_t id, std::size_t size) noexcept -> std::span<uint8_t>
{
    return _impl->allocate(id, size);
}

void OpenGLPipeline::push(const Image& image) noexcept
{
    _impl->push(image);
}

void OpenGLPipeline::trash(uint32_t id) noexcept
{
    _impl->trash(id);
}

void OpenGLPipeline::destroy() noexcept
{
    if (_impl->surface.isValid()) {
        _impl->surface.destroy();
    }
}

} // namespace Jgv::Gvsp::QtBackend
