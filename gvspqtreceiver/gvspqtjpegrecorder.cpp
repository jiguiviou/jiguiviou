/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqtjpegrecorder.h"
#include "gvspqtjpegrecorder_p.h"

#include "date-master/date.h"
#include "gvspqtpixeltransform_p.h"
#include "gvspqtrecordermetas.h"

#include <epoxy/gl.h>

#include <QOffscreenSurface>
#include <QOpenGLContext>

#include <charconv>
#include <filesystem>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <iostream>
#include <turbojpeg.h>

namespace {

constexpr auto RECORDER_PBO_COUNT { 2 };
constexpr auto FILENAME_SIZE { 22 };
constexpr auto DEFAULT_JPEG_QUALITY { 85 };

auto parseTimecode(int64_t timestamp) -> Jgv::Gvsp::QtBackend::ParsedTimecode
{
    std::chrono::system_clock::time_point tp;
    tp += std::chrono::nanoseconds(timestamp);

    auto const dp = date::floor<date::days>(tp);
    auto const ymd = date::year_month_day(dp);
    auto const year = ymd.year();
    auto const month = ymd.month();
    auto const day = ymd.day();
    auto left = tp - dp;
    auto const hour = std::chrono::duration_cast<std::chrono::hours>(left);
    left -= hour;
    auto const minute = std::chrono::duration_cast<std::chrono::minutes>(left);
    left -= minute;
    auto const second = std::chrono::duration_cast<std::chrono::seconds>(left);
    left -= second;
    auto const milli = std::chrono::duration_cast<std::chrono::milliseconds>(left);

    return Jgv::Gvsp::QtBackend::ParsedTimecode { static_cast<uint32_t>(static_cast<int>(year)), static_cast<uint32_t>(month), static_cast<uint32_t>(day),
        static_cast<uint32_t>(hour.count()), static_cast<uint32_t>(minute.count()),
        static_cast<uint32_t>(second.count()), static_cast<uint32_t>(milli.count()) };
}

auto toFilename(const Jgv::Gvsp::QtBackend::ParsedTimecode& timecode) -> std::string
{
    constexpr auto DIZAINE { 10 };
    constexpr auto CENTAINE { 100 };

    std::array<char, 4> year {};
    std::array<char, 2> month {};
    std::array<char, 2> day {};
    std::array<char, 2> hour {};
    std::array<char, 2> minute {};
    std::array<char, 2> second {};
    std::array<char, 3> milli {};

    std::string out;
    // c'est pas clair si il fut prévoir la place pour \0
    // on le met au cas où
    out.reserve(FILENAME_SIZE + 1);

    // gcc ne supporte pas chars_format, on magouille
    if (auto [p, ec] = std::to_chars(year.data(), year.data() + year.size(), timecode.year); ec == std::errc()) {
        out.append(std::string_view(year.data(), p - year.data()));
    }
    if (auto [p, ec] = std::to_chars(month.data(), month.data() + month.size(), timecode.month); ec == std::errc()) {
        if (timecode.month < DIZAINE) {
            month[1] = month[0];
            month[0] = '0';
        }
        out.append(std::string_view(month.data(), month.size()));
    }
    if (auto [p, ec] = std::to_chars(day.data(), day.data() + day.size(), timecode.day); ec == std::errc()) {
        if (timecode.day < DIZAINE) {
            day[1] = day[0];
            day[0] = '0';
        }
        out.append(std::string_view(day.data(), day.size()));
    }
    out.append("_");
    if (auto [p, ec] = std::to_chars(hour.data(), hour.data() + hour.size(), timecode.hour); ec == std::errc()) {
        if (timecode.hour < DIZAINE) {
            hour[1] = hour[0];
            hour[0] = '0';
        }
        out.append(std::string_view(hour.data(), hour.size()));
    }
    if (auto [p, ec] = std::to_chars(minute.data(), minute.data() + minute.size(), timecode.minute); ec == std::errc()) {
        if (timecode.minute < DIZAINE) {
            minute[1] = minute[0];
            minute[0] = '0';
        }
        out.append(std::string_view(minute.data(), minute.size()));
    }
    if (auto [p, ec] = std::to_chars(second.data(), second.data() + second.size(), timecode.second); ec == std::errc()) {
        if (timecode.second < DIZAINE) {
            second[1] = second[0];
            second[0] = '0';
        }
        out.append(std::string_view(second.data(), second.size()));
    }
    if (auto [p, ec] = std::to_chars(milli.data(), milli.data() + milli.size(), timecode.milli); ec == std::errc()) {
        if (timecode.milli < DIZAINE) {
            milli[2] = milli[0];
            milli[1] = '0';
            milli[0] = milli[1];
        } else if (timecode.milli < CENTAINE) {
            milli[2] = milli[1];
            milli[1] = milli[0];
            milli[0] = '0';
        }
        out.append(std::string_view(milli.data(), milli.size()));
    }
    out.append(".jpg");

    return out;
}

//void compressGreyImage(int quality, int width, int height, const Jgv::Gvsp::QtBackend::RecorderMetas& metas, const QString& filename, const uint8_t* buffer)
//{
//    tjhandle handle = tjInitCompress();
//    if (handle == nullptr) {
//        const char* err = tjGetErrorStr();
//        std::cout << "QtReceiver compress GreyImage: failed to create Turbo Jpeg instance " << err << std::endl;
//        return;
//    }

//    unsigned long jpegSize = 0;
//    unsigned char* buff = nullptr;

//    if (0 == tjCompress2(handle, buffer, width, 0, height, TJPF_GRAY, &buff, &jpegSize, TJSAMP_GRAY, quality, 0)) {
//        Jgv::Gvsp::QtBackend::RecorderMetas::saveToFile(buff, jpegSize, filename.toStdString(), metas);
//    } else {
//        const char* err = tjGetErrorStr();
//        qWarning("Failed to turbo compress monochrome image : %s", err);
//    }

//    tjDestroy(handle);
//    tjFree(buff);
//}

//struct GreyCompressDatas {
//    int quality;
//    int width;
//    int height;
//    std::shared_ptr<Jgv::Gvsp::QtBackend::RecorderMetas> metas;
//    std::filesystem::path firstRecord;
//    std::filesystem::path secondRecord;
//    const uint8_t* buffer;
//};

void compressGreyImage2(const Jgv::Gvsp::QtBackend::RecorderJob& job, int width, int height)
{

    tjhandle handle = tjInitCompress();
    if (handle == nullptr) {
        const char* err = tjGetErrorStr();
        std::cout << "QtReceiver compress GreyImage: failed to create Turbo Jpeg instance " << err << std::endl;
        return;
    }

    auto jpegSize { 0LU };
    unsigned char* buff { nullptr };

    if (0 == tjCompress2(handle, job.span.data(), width, 0, height, TJPF_GRAY, &buff, &jpegSize, TJSAMP_GRAY, DEFAULT_JPEG_QUALITY, 0)) {
        if (!job.record.empty()) {
            Jgv::Gvsp::QtBackend::RecorderMetas::saveToFile(buff, jpegSize, job.record, (*job.xmp));
        }
        if (!job.snapshot.empty()) {
            Jgv::Gvsp::QtBackend::RecorderMetas::saveToFile(buff, jpegSize, job.snapshot, (*job.xmp));
        }
    } else {
        const char* err = tjGetErrorStr();
        std::cout << "QtReceiver compress GreyImage: failed to create Turbo Jpeg instance " << err << std::endl;
    }

    tjDestroy(handle);
    tjFree(buff);
}

//void compressYuvImage(uint32_t quality, uint32_t width, uint32_t height, const QString &filename, const std::vector<std::shared_ptr<uint8_t>> buffers)
//{
//    tjhandle handle = tjInitCompress();
//    if (handle == nullptr) {
//        const char *err = tjGetErrorStr();
//        qWarning("Failed to create Turbo Jpeg I420 instance: %s", err);
//        return;
//    }
//    unsigned char * buff = nullptr;
//    unsigned long jpegSize = 0;
//    const unsigned char * srcPlanes[3];
//    srcPlanes[0] = buffers[0].get();
//    srcPlanes[1] = buffers[1].get();
//    srcPlanes[2] = buffers[2].get();
//    if (0 == tjCompressFromYUVPlanes(handle, srcPlanes, static_cast<int>(width), nullptr, static_cast<int>(height), TJSAMP_420, &buff, &jpegSize, static_cast<int>(quality), 0)) {
//        QFile f(filename);
//        f.open(QIODevice::WriteOnly);
//        f.write(reinterpret_cast<const char *>(buff), static_cast<qint64>(jpegSize));
//        f.close();
//    }
//    else {
//        const char *err = tjGetErrorStr();
//        qWarning("Failed to turbo compress I420 image : %s", err);
//    }

//    tjDestroy(handle);
//    tjFree(buff);
//}

} // namespace

namespace Jgv::Gvsp::QtBackend {

JpegRecorderPrivate::JpegRecorderPrivate(JpegRecorderFormat format, std::string_view sub, Channel::Type _channel, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : format(format)
    , subpath(sub)
    , channel(_channel)
    , globalIpc(std::move(_globalIpc))
{
    render = /*(format == JpegRecorderFormat::Grey)?*/ &JpegRecorderPrivate::renderAsGrey /*:&JpegRecorderPrivate::renderAsYuv*/;
}

//void JpegRecorderPrivate::renderAsYuv(GLuint fbo, const PboMetas &metas, QOpenGLContext *context)
//{
//    // on rejette les images sans date
//    if (metas.timestamp == 0) {
//        qDebug("Jpeg recorder failed to save 0 timestamp image");
//        return;
//    }

//    const ParsedTimecode parsed = parseTimecode(metas.timestamp);
//    std::array<SharedMemory, 3> buffers { pool.memory(), uvPool.memory(), uvPool.memory() };

//
//    glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
//    // on bind la texture du fbo principal
//    //glBindTexture(GL_TEXTURE_2D, fbo.outTexture);

//    // le rendu dans notre fbo
//    fboPtr->bind();
//    // premier rendu full size pour l'extraction de Y

//    programPtr->bind();
//    vao.bind();
//    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, reinterpret_cast<const GLvoid *>(0));
//    vao.release();
//    programPtr->release();
//    GLsizei width = static_cast<GLsizei>(metas.width);
//    GLsizei height = static_cast<GLsizei>(metas.height);
//    glReadBuffer(GL_COLOR_ATTACHMENT0);
//    glReadPixels(0, 0, width, height, QOpenGLTexture::Red, QOpenGLTexture::UInt8, buffers[0]->data());

//    // deuxième rendu 1/2 size pour U et V
//    scaledProgramPtr->bind();
//    vao.bind();
//    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, reinterpret_cast<const GLvoid *>(0));
//    vao.release();
//    scaledProgramPtr->release();
//    // le shader scale à 1/2 et place la texture en haut à gauche
//    width /= 2;
//    height /= 2;
//    glReadBuffer(GL_COLOR_ATTACHMENT1);
//    glReadPixels(0, 0, width, height, QOpenGLTexture::Red, QOpenGLTexture::UInt8, buffers[1]->data());
//    glReadBuffer(GL_COLOR_ATTACHMENT2);
//    glReadPixels(0, 0, width, height, QOpenGLTexture::Red, QOpenGLTexture::UInt8, buffers[2]->data());

//    // restaure le fbo
//    glBindFramebuffer(GL_FRAMEBUFFER, 0);
//#ifdef SYLVIX
//    VtoXmp xmp { config->sylvixXmp() };
//    xmp.tag(metas, parsed);
//    if (config->sylvixIsRecording()) {
//        const QString filename = config->sylvixRecordingDir().filePath(toFilename(parsed));
//        std::thread(compressYuvImage, 85, metas.width, metas.height, xmp, filename, buffers).detach();
//    }
//    if (config->sylvixWantSnapshot()) {
//        const QString filename = config->sylvixNextSnapshotName();
//        std::thread(compressYuvImage, 85, metas.width, metas.height, xmp, filename, buffers).detach();
//    }

//#else
//    const QString filename = config->jpegEffectiveRecordingDir().filePath(toFilename(parsed));
//    std::thread(compressGreyImage, config->jpegQuality(), metas.width, metas.height, filename, buffers).detach();
//#endif

//}

void JpegRecorderPrivate::loop(std::promise<void> barrier)
{
    // on a besoin d'un context sur le thread,
    QSurfaceFormat fmt;
    fmt.setMajorVersion(4);
    fmt.setMinorVersion(2);
    fmt.setProfile(QSurfaceFormat::CoreProfile);

    QOpenGLContext context;
    context.setFormat(fmt);
    context.setShareContext(globalIpc->sharedContext());
    context.create();

    QOffscreenSurface surface;
    surface.setFormat(fmt);
    surface.create();

    const auto imageSize { width * height };

    context.makeCurrent(&surface);
    // construction de nos 2 pbos
    std::array<GLuint, RECORDER_PBO_COUNT> pbos {};
    glGenBuffers(static_cast<GLsizei>(pbos.size()), pbos.data());
    //    // les jobs
    //    std::array<RecorderJob, RECORDER_PBO_COUNT> jobs;
    // construction du buffer pixels
    std::vector<uint8_t> buffer(imageSize * RECORDER_PBO_COUNT);
    std::span<uint8_t> span { buffer };
    std::size_t offset { 0 };

    // initialisation de la queue
    for (auto pbo : pbos) {
        glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo);
        glBufferData(GL_PIXEL_PACK_BUFFER, imageSize, nullptr, GL_DYNAMIC_READ);
        glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
        pbosQueue.enqueue({ pbo, span.subspan(offset, width * height), nullptr, 0, 0, 0, "", "", nullptr });
        offset += width * height;
    }
    context.doneCurrent();

    auto validatePath = [](std::string_view path, std::string_view subPath) -> std::string {
        const auto desiredPath { std::filesystem::path(path) / subPath };
        if (!std::filesystem::exists(desiredPath)) {
            std::error_code ec;
            if (!std::filesystem::create_directory(desiredPath, ec)) {
                std::cerr << "Gvsp::QtBackend::JpegRecorder::loop failed to create directory " << desiredPath << " error: " << ec.message() << std::endl;
                return {};
            }
            std::filesystem::permissions(desiredPath, std::filesystem::perms::all, ec);
            if (ec != std::error_code()) {
                std::clog << "Gvsp::QtBackend::JpegRecorder::loop failed to change permissions on " << desiredPath << " error: " << ec.message() << std::endl;
            }
        }
        return desiredPath;
    };

    RecorderJob job;

    // on est initiliasé, on lève la barrière
    barrier.set_value();

    while (true) {
        jobQueue.wait_dequeue(job);
        if (job.pbo > 0) {

            const auto parsedTimecode { parseTimecode(job.timestamp) };

            // résolution des chemins record
            if (!job.record.empty()) {
                auto desiredPath { validatePath(job.xmp->path(), job.record) };
                if (!desiredPath.empty()) {
                    desiredPath = validatePath(desiredPath, subpath);
                }

                if (!desiredPath.empty()) {
                    job.record = std::filesystem::path(desiredPath) / toFilename(parsedTimecode);
                } else {
                    job.record.clear();
                }
            }

            // résolution des chemins snapshot
            if (!job.snapshot.empty()) {
                auto desiredPath { validatePath(job.xmp->path(), job.snapshot) };
                if (!desiredPath.empty()) {
                    job.snapshot = std::filesystem::path(desiredPath) / toFilename(parsedTimecode);
                } else {
                    job.snapshot.clear();
                }
            }

            context.makeCurrent(&surface);

            auto state { glClientWaitSync(job.sync, GL_SYNC_FLUSH_COMMANDS_BIT, 100000000) };
            glDeleteSync(job.sync);
            if (state == GL_CONDITION_SATISFIED || state == GL_ALREADY_SIGNALED) {
                glBindBuffer(GL_PIXEL_PACK_BUFFER, job.pbo);
                glGetBufferSubData(GL_PIXEL_PACK_BUFFER, 0, job.span.size_bytes(), job.span.data());
                glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
                job.xmp->tag({ job.timestamp, parsedTimecode, job.site, job.gisement });
                compressGreyImage2(job, width, height);
            }

            // on remet le pbo dans la queue
            pbosQueue.enqueue(job);
            context.doneCurrent();
        } else {
            break;
        }
    }
    context.makeCurrent(&surface);
    glDeleteBuffers(static_cast<GLsizei>(pbos.size()), pbos.data());
    surface.destroy();
    context.doneCurrent();
}

void JpegRecorderPrivate::setFboSize(uint32_t _width, uint32_t _height)
{
    width = _width;
    height = _height;

    std::promise<void> barrier;
    std::future<void> barrierFuture { barrier.get_future() };
    // relance le thread d'enregistrement
    renderThread = std::thread(&JpegRecorderPrivate::loop, this, std::move(barrier));
    pthread_setname_np(renderThread.native_handle(), "glGetJpeg");
    barrierFuture.wait();
}

void JpegRecorderPrivate::renderAsGrey(int64_t timestamp, int32_t site, int32_t gisement, std::string_view record, std::string_view snapshot, std::shared_ptr<RecorderMetas> vtoXmp)
{
    // on rejette les images sans date
    if (timestamp == 0) {
        std::cerr << "Jpeg recorder failed to save 0 timestamp image\n";
        return;
    }

    RecorderJob job;
    if (pbosQueue.try_dequeue(job)) {
        // lit le rendu 8 bits
        glReadBuffer(GL_COLOR_ATTACHMENT0);
        // bind le pbo
        glBindBuffer(GL_PIXEL_PACK_BUFFER, job.pbo);
        // transfert asynchrone vers le pbo
        glReadPixels(0, 0, width, height, GL_RED, GL_UNSIGNED_BYTE, nullptr);
        // relache le pbo
        glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

        job.sync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
        job.timestamp = timestamp;
        job.site = site;
        job.gisement = gisement;
        job.record = record;
        job.snapshot = snapshot;
        job.xmp = std::move(vtoXmp);

        // il est important de flusher dans le thread qui a créé le fence
        glFlush();

        jobQueue.enqueue(job);
    } else {
        std::clog << "JpegRecorderPrivate::renderAsGrey: no free pbo for pack." << std::endl;
    }
}

void JpegRecorderPrivate::destroy()
{
    // on met fin à la boucle de download
    if (renderThread.joinable()) {
        jobQueue.enqueue({});
        renderThread.join();
    }
}

JpegRecorder::JpegRecorder(JpegRecorderFormat format, std::string_view sub, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl(std::make_unique<JpegRecorderPrivate>(format, sub, channel, std::move(globalIpc)))
{
}

JpegRecorder::~JpegRecorder() = default;

void JpegRecorder::setFboSize(uint32_t width, uint32_t height)
{
    _impl->setFboSize(width, height);
}

void JpegRecorder::render(int64_t timestamp, int32_t site, int32_t gisement, std::string_view record, std::string_view snapshot, std::shared_ptr<RecorderMetas> vtoXmp)
{
    (_impl.get()->*(_impl->render))(timestamp, site, gisement, record, snapshot, std::move(vtoXmp));
}

void JpegRecorder::destroy()
{
    _impl->destroy();
}

auto JpegRecorder::timecodeToString(int64_t timecode) noexcept -> std::string
{
    return toFilename(parseTimecode(timecode)).substr(0, FILENAME_SIZE - 4);
}
} // namespace Jgv::Gvsp::QtBackend
