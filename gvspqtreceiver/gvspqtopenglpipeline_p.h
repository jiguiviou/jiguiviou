﻿/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQOPENGLPIPELINE_P_H
#define GVSPQOPENGLPIPELINE_P_H

#include "gvspqtatomicvector.h"
#include "gvspqtpixeltransform.h"

#include <QOffscreenSurface>
#include <functional>
#include <unordered_map>

class QOpenGLContext;

namespace Jgv::Gvsp::QtBackend {

class PixelTransform;
class GLVideoRenderer;

struct OpenGLPipelinePrivate {
    explicit OpenGLPipelinePrivate(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    const Channel::Type channel;
    const std::shared_ptr<GlobalIPC::Object> globalIpc;
    QOffscreenSurface surface;

    AtomicPointer<PixelTransform> newTransform;
    std::unique_ptr<PixelTransform> transform;
    std::unique_ptr<QOpenGLContext> context;
    std::unordered_map<uint32_t, GLuint> imagePbos;
    std::shared_ptr<std::function<void(uint32_t, uint32_t, uint32_t)>> changePixelFormatFunction;

    auto allocate(uint32_t id, std::size_t size) noexcept -> std::span<uint8_t>;
    void push(const Image& image) noexcept;
    void trash(uint32_t id) noexcept;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQOPENGLPIPELINE_P_H
