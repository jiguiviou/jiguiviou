/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTHISTOGRAMUINT8GETTER_H
#define GVSPQTHISTOGRAMUINT8GETTER_H

#include "gvspqthistogramgetter.h"

namespace Jgv::Gvsp::QtBackend {

class HistogramUint8Getter : public HistogramGetter {
public:
    explicit HistogramUint8Getter(std::function<void(AtomicVector<uint8_t>&&)>&& lutSetter, std::function<void(AtomicVector<float>&&)>&& histogramSetter, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTHISTOGRAMUINT8GETTER_H
