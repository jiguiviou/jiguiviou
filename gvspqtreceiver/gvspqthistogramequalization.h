/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTHISTOGRAMEQUALIZATION_H
#define GVSPQTHISTOGRAMEQUALIZATION_H

#include <epoxy/gl.h>

#include <memory>

namespace Jgv::Gvsp::QtBackend {

template <typename T>
class AtomicVector;
struct HistogramEqualizationPrivate;

class HistogramEqualization {

protected:
    HistogramEqualization();

public:
    ~HistogramEqualization();
    HistogramEqualization(const HistogramEqualization&) = delete;
    HistogramEqualization(HistogramEqualization&&) = delete;
    auto operator=(const HistogramEqualization&) -> HistogramEqualization& = delete;
    auto operator=(HistogramEqualization &&) -> HistogramEqualization& = delete;

    virtual void pushLut(AtomicVector<uint8_t>&& lut) noexcept { (void)lut; }
    virtual void pushLut(AtomicVector<uint16_t>&& lut) noexcept { (void)lut; }

    void initialize(GLuint quadVbo, GLuint quadIVbo);
    void setFboSize(uint32_t width, uint32_t height);
    auto render(GLuint sourceTexture) -> GLuint;
    void destroy();

protected:
    virtual void initialize() noexcept = 0;
    virtual void uploadAndBind() noexcept = 0;
    const std::unique_ptr<HistogramEqualizationPrivate> _impl;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTHISTOGRAMEQUALIZATION_H
