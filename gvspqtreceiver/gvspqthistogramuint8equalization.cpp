/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspqthistogramuint8equalization.h"
#include "gvspqthistogramequalization_p.h"

#include <openglutils/openglprogram.h>

namespace {
constexpr auto UINT8_COUNT { 256U };
} // namespace

namespace Jgv::Gvsp::QtBackend {

void HistogramUint8Equalization::pushLut(AtomicVector<uint8_t>&& lut) noexcept
{
    _lut = std::move(lut);
}

void HistogramUint8Equalization::initialize() noexcept
{
    const std::string vSource =
#include "shader/texelfetch.vert"
        ;
    const std::string fSource =
#include "shader/histogrameq.frag"
        ;
    _impl->program = OpenGLUtils::Program::create(vSource, fSource);

    glUseProgram(_impl->program);
    auto maxId { glGetUniformLocation(_impl->program, "max") };
    glUniform1f(maxId, UINT8_MAX);
    glUseProgram(0);

    glGenTextures(1, &_impl->lutTexture);
    glBindTexture(GL_TEXTURE_1D, _impl->lutTexture);
    glTexStorage1D(GL_TEXTURE_1D, 1, GL_R8UI, UINT8_COUNT);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_1D, 0);
}

void HistogramUint8Equalization::uploadAndBind() noexcept
{
    if (const auto lut { std::unique_ptr<std::vector<uint8_t>>(_lut.release()) }) {
        glTexSubImage1D(GL_TEXTURE_1D, 0, 0, lut->size(), GL_RED_INTEGER, GL_UNSIGNED_BYTE, lut->data());
    }
}

} // namespace Jgv::Gvsp::QtBackend
