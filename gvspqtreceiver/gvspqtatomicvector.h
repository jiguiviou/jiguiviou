/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTATOMICVECTOR_H
#define GVSPQTATOMICVECTOR_H

#include <atomic>
#include <iostream>
#include <memory>
#include <vector>

namespace Jgv::Gvsp::QtBackend {

template <typename T>
class AtomicPointer {
    using value_type = T;
    using pointer = value_type*;
    std::atomic<pointer> ptr;

public:
    explicit AtomicPointer() noexcept
        : ptr(nullptr)
    {
    }
    ~AtomicPointer()
    {
        reset();
    }

    AtomicPointer(const AtomicPointer&) = delete;
    AtomicPointer(AtomicPointer&& p) noexcept = delete;
    auto operator=(const AtomicPointer&) noexcept -> AtomicPointer& = delete;
    auto operator=(AtomicPointer&& p) noexcept -> AtomicPointer& = delete;

    void reset(pointer p = pointer()) noexcept
    {
        const auto old { ptr.exchange(p) };
        if (old != nullptr) {
            delete old; // NOLINT(cppcoreguidelines-owning-memory)
        }
    }

    auto release() noexcept -> pointer
    {
        return ptr.exchange(pointer());
    }
};

template <typename T>
class AtomicVector final {
    using value_type = std::vector<T>;
    using pointer = value_type*;
    std::atomic<pointer> ptr;

public:
    explicit AtomicVector() noexcept
        : ptr(new value_type())
    {
    }
    ~AtomicVector()
    {
        reset();
    }

    AtomicVector(const AtomicVector&) = delete;
    AtomicVector(AtomicVector&& p) noexcept = delete;
    auto operator=(const AtomicVector&) noexcept -> AtomicVector& = delete;
    auto operator=(AtomicVector&& p) noexcept -> AtomicVector&
    {
        reset(p.release());
        return *this;
    }

    void reset(pointer p = pointer()) noexcept
    {
        const auto old { ptr.exchange(p) };
        if (old != nullptr) {
            delete old; // NOLINT(cppcoreguidelines-owning-memory)
        }
    }

    auto operator-> () const noexcept -> pointer
    {
        return ptr;
    }

    auto release() noexcept -> pointer
    {
        return ptr.exchange(pointer());
    }
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTATOMICVECTOR_H
