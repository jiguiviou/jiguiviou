/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTRECORDERMETAS_H
#define GVSPQTRECORDERMETAS_H

#include <exiv2/exiv2.hpp>

namespace Jgv::Gvsp::QtBackend {

constexpr std::string_view VTO_NAMESPACE { "vto" };
constexpr std::string_view TRIEDRE { "Triedre" };
constexpr std::string_view LYRE { "Lyre" };
constexpr std::string_view CAMPAGNE { "Campagne" };
constexpr std::string_view CRI { "CRI" };
constexpr std::string_view FOCALE { "Focale" };
constexpr std::string_view CAMERA { "Camera" };
constexpr std::string_view TIMESTAMP { "Timestamp" };
constexpr std::string_view DATE { "Date" };
constexpr std::string_view UTC { "UTC" };
constexpr std::string_view UNITE_ANGLE { "UniteAngle" };
constexpr std::string_view SITE { "Site" };
constexpr std::string_view GISEMENT { "Gisement" };
constexpr std::string_view CENTRE_X { "CentreX" };
constexpr std::string_view CENTRE_Y { "CentreY" };

struct ParsedTimecode {
    uint32_t year;
    uint32_t month;
    uint32_t day;
    uint32_t hour;
    uint32_t minute;
    uint32_t second;
    uint32_t milli;
};

struct XmpTags {
    int64_t timestamp;
    ParsedTimecode timecode;
    int32_t site;
    int32_t gisement;
};

class RecorderMetas final {
    Exiv2::XmpData _xmpData;
    std::string _path;

public:
    explicit RecorderMetas();
    void setPath(std::string_view path) noexcept;
    [[nodiscard]] auto path() const noexcept -> std::string;

    void setMeta(std::string_view key, std::string_view value);
    [[nodiscard]] auto meta(std::string_view key) const -> std::string;
    void tag(const XmpTags& tags);
    static void saveToFile(const uint8_t* mem, std::size_t size, const std::string& filename, const RecorderMetas& xmp);

}; // class RecorderMetas

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTRECORDERMETAS_H
