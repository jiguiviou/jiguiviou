R"(
#version 420 core
layout(binding = 0) uniform sampler2D tex;
layout(location = 0) out vec4 outColor;

in vec2 vTexCoord;

void main()
{
    outColor = texture(tex, vTexCoord);
}
)"
