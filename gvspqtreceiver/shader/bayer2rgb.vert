R"(
#version 420 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 texCoord;
layout(std140, binding = 0) uniform shaderdatas
{
  vec2 texSize;
};

uniform vec2 firstRed;

out     vec4 center;
out     vec4 xCoord;
out     vec4 yCoord;

void main(void) {
    center.xy = texCoord * texSize;
    center.zw = center.xy + firstRed;
    xCoord = center.xxxx + vec4(-2.0, -1.0, 1.0, 2.0);
    yCoord = center.yyyy + vec4(-2.0, -1.0, 1.0, 2.0);
    gl_Position = position;
}
)"
