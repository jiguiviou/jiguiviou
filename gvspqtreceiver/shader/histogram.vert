R"(
#version 420 core

layout(location = 0) in vec2 position;

uniform mat4 transform;
uniform float max;

void main()
{
    vec4 flipScale = vec4(position * vec2(1.0, 1.0/max), 0.0, 1.0);
    gl_Position = transform * flipScale;
}
)"
