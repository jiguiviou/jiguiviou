R"(
#version 420 core

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 texCoord;
layout(std140, binding = 0) uniform shaderdatas
{
  vec2 texSize;
};

out vec2 fTexCoord;
out vec2 vTexCoord;

void main()
{
    fTexCoord = texCoord;
    vTexCoord = texCoord * texSize;
    gl_Position = position;
}
)"
