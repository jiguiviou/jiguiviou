R"(
#version 420 core

layout(binding = 0) uniform sampler2D tex;
layout(location = 0) out vec3 outColor;

in vec2 vTexCoord;

/*
   R = 1.1643(Y - 0.0625)                    + 1.5958(V - 0.5)
   G = 1.1643(Y - 0.0625) - 0.39173(U - 0.5) - 0.8129(V - 0.5)
   B = 1.1643(Y - 0.0625) + 2.01700(U - 0.5)
*/
const   mat3 YUVtoRGB = mat3( 1.16430,  1.16430,  1.16430,
                              0.00000, -0.39173,  2.01800,
                              1.59580, -0.81290,  0.00000 );
const   vec3 offset =   vec3(     0.0,     -0.5,     -0.5 );

void main() {
    vec3 yuv = texelFetch(tex, ivec2(vTexCoord), 0).grb;
    outColor = clamp(YUVtoRGB * (yuv + offset), 0.0, 1.0);
}
)"

