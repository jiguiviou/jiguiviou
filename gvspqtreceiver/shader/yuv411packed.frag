R"(
#version 420 core

layout(binding = 0) uniform sampler2D tex;
layout(location = 0) out vec3 outColor;

in vec2 vTexCoord;
/*
   R = 1.1643(Y - 0.0625)                    + 1.5958(V - 0.5)
   G = 1.1643(Y - 0.0625) - 0.39173(U - 0.5) - 0.8129(V - 0.5)
   B = 1.1643(Y - 0.0625) + 2.01700(U - 0.5)
*/
const   mat3 YUVtoRGB = mat3( 1.16430,  1.16430,  1.16430,
                              0.00000, -0.39173,  2.01800,
                              1.59580, -0.81290,  0.00000 );
const   vec3 offset =   vec3(     0.0,     -0.5,     -0.5 );

void main()
{
    ivec2 coord = ivec2(vTexCoord);
    int quarkIndex = coord.x % 4;
    coord.x = (coord.x / 4) * 6;
    ivec2 xy =  (quarkIndex==0)?ivec2(1,0):(quarkIndex==1)?ivec2(2,0):(quarkIndex==2)?ivec2(4,0):ivec2(5,0);
    vec3 yuv = vec3(texelFetch(tex, coord + xy, 0).r, texelFetch(tex, coord, 0).r, texelFetch(tex, coord + ivec2(3,0), 0).r);
    outColor = clamp(YUVtoRGB * (yuv + offset), 0.0, 1.0);
}
)"
