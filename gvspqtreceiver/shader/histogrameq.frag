R"(
#version 420 core

layout (binding = 0) uniform usampler2D tex;
layout (binding = 1) uniform usampler1D lutTex;
layout(location = 0) out float render;
uniform float max;
in vec2 vTexCoord;

void main()
{
    // la valeur du pixel [0, 4095], l'index doit être de type int
    const int index = int(texelFetch(tex, ivec2(vTexCoord), 0).r);
    // la valeur du lut normalisée [0, 1]
    render = float(texelFetch(lutTex, index, 0).r) / max;
}
)"
