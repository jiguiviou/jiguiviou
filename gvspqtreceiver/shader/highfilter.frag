R"(
#version 420 core

layout (binding = 0) uniform sampler2D tex;
layout (location = 0) out vec3 outcolor;

in vec2 vTexCoord;


float contrastConvolve(ivec2 point)
{
    return texelFetch(tex, point, 0).r * 5.
         - texelFetch(tex, point + ivec2(-1,-1), 0).r
         - texelFetch(tex, point + ivec2( 1,-1), 0).r
         - texelFetch(tex, point + ivec2(-1, 1), 0).r
         - texelFetch(tex, point + ivec2( 1, 1), 0).r;
}

//float highContrastConvolve(ivec2 point)
//{
//    return texelFetch(tex, point, 0).r * 9.
//            - texelFetch(tex, point + ivec2(-1,-1), 0).r - texelFetch(tex, point + ivec2( 1,-1), 0).r
//            - texelFetch(tex, point + ivec2(-1, 1), 0).r - texelFetch(tex, point + ivec2( 1, 1), 0).r
//            - texelFetch(tex, point + ivec2( 0,-1), 0).r - texelFetch(tex, point + ivec2(-1, 0), 0).r
//            - texelFetch(tex, point + ivec2( 0, 1), 0).r - texelFetch(tex, point + ivec2( 1, 0), 0).r ;
//}

void main()
{

//outcolor = vec3(0.5, 0., 0.);
    outcolor = vec3(contrastConvolve(ivec2(vTexCoord)));
}
)"
