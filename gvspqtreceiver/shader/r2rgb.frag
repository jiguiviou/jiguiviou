R"(
#version 420 core

layout(binding = 0) uniform usampler2D tex;
layout(location = 0) out vec3 outColor;
layout(location = 1) out uint grey;

in vec2 vTexCoord;
const float PIXEL_MAX = float(0xFF);

void main()
{
    uint pixel = texture(tex, vTexCoord).r;
    float val = float(pixel) / PIXEL_MAX;
    outColor = vec3(val, val, val);
    grey = pixel;
}
)"
