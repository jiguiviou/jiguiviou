R"(
#version 420 core
layout(location = 0) out vec4 outColor;
uniform float time;
in vec2 vTexCoord;

void main()
{
    float pct = 1. - step(0.5, distance(vTexCoord, vec2(0.5)));
    float var = sin(3.1416 * time);
    float alpha = 0.6 - pow(max(0., abs(var) * 2. -1.), 3.);
    outColor = vec4(vec3(1., 0., 0.), alpha * pct);
}
)"
