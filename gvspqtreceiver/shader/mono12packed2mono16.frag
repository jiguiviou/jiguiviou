R"(
#version 420 core

layout(binding = 0) uniform usampler2D tex;
layout(location = 0) out vec3 outColor;
layout(location = 1) out uint grey;

in vec2 vTexCoord;
void main()
{
    ivec2 coord = ivec2(vTexCoord);
    int index = (coord.x % 2);
    coord.x = (coord.x / 2) * 3;
    // [0, 16]
    const uint LSB = texelFetch(tex, coord + ivec2(1,0), 0).r;
    // pixel [0, 4095]
    const uint VAL = (index==0)?(texelFetch(tex, coord, 0).r << 4) + ((LSB & 0xF0u)>>4):(texelFetch(tex, coord + ivec2(2,0), 0).r << 4) + (LSB & 0x0Fu);
    // rendu simple
    outColor = vec3(float(VAL) / 4095.);
    grey = (index==0)?(texelFetch(tex, coord, 0).r << 4) + ((LSB & 0xF0u)>>4):(texelFetch(tex, coord + ivec2(2,0), 0).r << 4) + (LSB & 0x0Fu);

}
)"
