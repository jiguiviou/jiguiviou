/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTJPEGRECORDER_H
#define GVSPQTJPEGRECORDER_H

#include <epoxy/gl.h>

#include <memory>

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Gvsp::QtBackend {

enum class JpegRecorderFormat;

class RecorderMetas;
struct PboMetas;
class JpegRecorderPrivate;
class JpegRecorder final {
public:
    explicit JpegRecorder(JpegRecorderFormat format, std::string_view sub, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~JpegRecorder();
    JpegRecorder(const JpegRecorder&) = delete;
    JpegRecorder(JpegRecorder&&) = delete;
    auto operator=(const JpegRecorder&) -> JpegRecorder& = delete;
    auto operator=(JpegRecorder &&) -> JpegRecorder& = delete;

    void setFboSize(uint32_t width, uint32_t height);
    void render(int64_t timestamp, int32_t site, int32_t gisement, std::string_view record, std::string_view snapshot, std::shared_ptr<RecorderMetas> vtoXmp);
    void destroy();

    static auto timecodeToString(int64_t timecode) noexcept -> std::string;

private:
    const std::unique_ptr<JpegRecorderPrivate> _impl;
};

} // namespace Jgv::Gvsp::QtBackend

#endif // GVSPQTJPEGRECORDER_H
