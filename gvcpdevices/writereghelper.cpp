/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "writereghelper.h"
#include "gvcpclient.h"

#include <arpa/inet.h> // htonl

namespace Jgv::Gvcp {

struct WRITEREG_CMD {
    CMD_HEADER header;
    ADDRESS_VALUE address_value;
};

struct WRITEREG_ACK {
    ACK_HEADER header;
    uint16_t reserved;
    uint16_t index;
};

constexpr auto WRITEREG_ACK_LENGTH { sizeof(WRITEREG_ACK) - sizeof(ACK_HEADER) };

WriteregCmdHelper::WriteregCmdHelper(std::span<uint8_t> memoryView, uint16_t length)
    : HeaderCmdHelper(memoryView, GVCP_CMD_WRITEREG, length)
{
}

void WriteregCmdHelper::setRegsValList(const AddrValPairList& values) noexcept
{
    auto datasSpan { subspan(offsetof(WRITEREG_CMD, address_value.address)) };
    for (const auto& pair : values) {
        toUnaligned(htonl(pair.address), datasSpan);
        datasSpan = datasSpan.subspan(spanIndex_cast(sizeof(uint32_t)));
        toUnaligned(htonl(pair.value), datasSpan);
        datasSpan = datasSpan.subspan(spanIndex_cast(sizeof(uint32_t)));
    }
}

void WriteregCmdHelper::setReg(uint32_t address, uint32_t value) noexcept
{
    auto datasSpan { subspan(offsetof(WRITEREG_CMD, address_value.address)) };
    toUnaligned(htonl(address), datasSpan);
    datasSpan = datasSpan.subspan(spanIndex_cast(sizeof(uint32_t)));
    toUnaligned(htonl(value), datasSpan);
}

void WriteregCmdHelper::setReg(uint32_t address, int32_t value) noexcept
{
    auto datasSpan { subspan(offsetof(WRITEREG_CMD, address_value.address)) };
    toUnaligned(htonl(address), datasSpan);
    datasSpan = datasSpan.subspan(spanIndex_cast(sizeof(uint32_t)));
    toUnaligned(htonl(value), datasSpan);
}

WriteregAckHelper::WriteregAckHelper(std::span<uint8_t> memoryView, uint16_t lenght)
    : HeaderAckHelper(memoryView, GVCP_ACK_WRITEREG, lenght)
{
}

void WriteregAckHelper::setIndex(uint16_t index) noexcept
{
    toUnaligned(htons(index), subspan(offsetof(WRITEREG_ACK, index)));
}

auto WriteregAckHelper::index(std::span<uint8_t> memoryView) -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(WRITEREG_ACK, index))));
}

} // namespace Jgv::Gvcp
