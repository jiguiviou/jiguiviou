/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "writememhelper.h"

#include <arpa/inet.h> // ntoh
#include <iostream>

namespace Jgv::Gvcp {

WritememCmdHelper::WritememCmdHelper(std::span<uint8_t> memoryView, uint16_t length)
    : HeaderCmdHelper(memoryView, GVCP_CMD_WRITEMEM, (length % 4 == 0) ? length : length + 4 - (length % 4))
{
}

void WritememCmdHelper::setAddress(uint32_t address) noexcept
{
    toUnaligned(htonl(address), subspan(offsetof(WRITEMEM_CMD, address)));
}

void WritememCmdHelper::setDatas(std::span<const uint8_t> datas) noexcept
{
    auto dest { subspan(offsetof(WRITEMEM_CMD, datas)) };
    if (dest.size() < datas.size()) {
        std::cerr << "Gvcp::WritememCmdHelper::setDatas(): failed (dest.size() < datas.size()) " << dest.size() << " " << datas.size() << std::endl;
        return;
    }

    std::copy(datas.begin(), datas.end(), dest.begin());
}

auto WritememCmdHelper::address(std::span<const uint8_t> memoryView) noexcept -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(WRITEMEM_CMD, address))));
}

auto WritememCmdHelper::datas(std::span<const uint8_t> memoryView) noexcept -> std::span<const uint8_t>
{
    // obtient la taille
    const auto length { HeaderCmdHelper::length(memoryView) - static_cast<int>(sizeof(WRITEMEM_CMD::address)) };
    return memoryView.subspan(offsetof(WRITEMEM_CMD, datas), length);
}

WritememAckHelper::WritememAckHelper(std::span<uint8_t> memoryView, uint16_t lenght)
    : HeaderAckHelper(memoryView, GVCP_ACK_WRITEMEM, lenght)
{
}

void WritememAckHelper::setIndex(uint16_t index) noexcept
{
    toUnaligned(htons(index), subspan(offsetof(WRITEMEM_ACK, index)));
}

auto WritememAckHelper::index(std::span<const uint8_t> memoryView) noexcept -> unsigned
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(WRITEMEM_ACK, index))));
}

} // namespace Jgv::Gvcp
