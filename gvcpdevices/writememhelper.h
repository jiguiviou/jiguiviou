/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef WRITEMEMHELPER_H
#define WRITEMEMHELPER_H

#include "headerhelper.h"

namespace Jgv::Gvcp {

constexpr uint16_t GVCP_CMD_WRITEMEM = 0x0086;
constexpr uint16_t GVCP_ACK_WRITEMEM = 0x0087;

struct WRITEMEM_CMD {
    CMD_HEADER header;
    uint32_t address;
    uint8_t datas;
};

struct WRITEMEM_ACK {
    ACK_HEADER header;
    uint16_t reserved;
    uint16_t index;
};

class WritememCmdHelper final : public HeaderCmdHelper {

public:
    WritememCmdHelper(std::span<uint8_t> memoryView, uint16_t length);

    void setAddress(uint32_t address) noexcept;
    void setDatas(std::span<const uint8_t> datas) noexcept;

    static auto address(std::span<const uint8_t> memoryView) noexcept -> uint32_t;
    static auto datas(std::span<const uint8_t> memoryView) noexcept -> std::span<const uint8_t>;
};

constexpr auto WRITEMEM_ACK_LENGTH = sizeof(WRITEMEM_ACK) - sizeof(ACK_HEADER);

class WritememAckHelper final : public HeaderAckHelper {
public:
    WritememAckHelper(std::span<uint8_t> memoryView, uint16_t lenght);

    void setIndex(uint16_t index) noexcept;
    static auto index(std::span<const uint8_t> memoryView) noexcept -> unsigned;
};

} // namespace Jgv::Gvcp

#endif // WRITEMEMHELPER_H
