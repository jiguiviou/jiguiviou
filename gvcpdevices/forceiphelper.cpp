/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "forceiphelper.h"

#include <arpa/inet.h> // ntoh

namespace Jgv::Gvcp {

ForceipCmdHelper::ForceipCmdHelper(std::span<uint8_t> memoryView, uint16_t length)
    : HeaderCmdHelper(memoryView, GVCP_CMD_FORCEIP, length)
{
}

void ForceipCmdHelper::setMacHigh(uint16_t mac)
{
    toUnaligned(htons(mac), subspan(offsetof(FORCEIP_CMD, MACAddressHigh)));
}

void ForceipCmdHelper::setMacLow(uint32_t mac)
{
    toUnaligned(htonl(mac), subspan(offsetof(FORCEIP_CMD, MACAddressLow)));
}

void ForceipCmdHelper::setStaticIP(uint32_t ip)
{
    toUnaligned(htonl(ip), subspan(offsetof(FORCEIP_CMD, staticIP)));
}

void ForceipCmdHelper::setStaticNetmask(uint32_t netmask)
{
    toUnaligned(htonl(netmask), subspan(offsetof(FORCEIP_CMD, staticSubnetMask)));
}

void ForceipCmdHelper::setStaticDefaultGateway(uint32_t gateway)
{
    toUnaligned(htonl(gateway), subspan(offsetof(FORCEIP_CMD, staticDefaultGateway)));
}

auto ForceipCmdHelper::macHigh(std::span<const uint8_t> memoryView) -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(FORCEIP_CMD, MACAddressHigh))));
}

auto ForceipCmdHelper::macLow(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(FORCEIP_CMD, MACAddressLow))));
}

auto ForceipCmdHelper::staticIP(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(FORCEIP_CMD, staticIP))));
}

auto ForceipCmdHelper::staticNetmask(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(FORCEIP_CMD, staticSubnetMask))));
}

auto ForceipCmdHelper::staticDefaultGateway(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(FORCEIP_CMD, staticDefaultGateway))));
}

ForceipAckHelper::ForceipAckHelper(std::span<uint8_t> memoryView, uint16_t length)
    : HeaderAckHelper(memoryView, GVCP_CMD_FORCEIP, length)
{
}

} // namespace Jgv::Gvcp
