/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVCPCLIENT_P_H
#define GVCPCLIENT_P_H

#include <globalipc/globalipc.h>
#include <memory>
#include <mutex>
#include <span>
#include <thread>

namespace Jgv::Gvcp {

enum class State {
    none,
    controller,
    monitor
};
constexpr auto toString(State state) -> std::string_view
{
    switch (state) {
    case State::none:
        return "Libre";
    case State::controller:
        return "Controlleur";
    case State::monitor:
        return "Moniteur";
    default:
        return "inconnu";
    }
}

class HeaderCmdHelper;

struct GvcpResponse {
    bool isValid;
    int64_t beginTime;
    int64_t endTime;
};

struct ClientPrivate {
    explicit ClientPrivate(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    std::shared_ptr<GlobalIPC::Object> globalIpc;
    Channel::Type channel;
    uint16_t id { 1 };
    int cmdSocket { -1 };
    int eventSocket { -1 };

    std::mutex mutex;

    volatile bool listenEventsLoop { true };
    std::unique_ptr<std::thread> listenEventThread;

    State state = State::none;
    bool hwTimestamp { false };

    auto proceed(HeaderCmdHelper cmd, std::span<uint8_t> receiveBuffer, bool timing) -> GvcpResponse;
    void listenForEvents(uint16_t port);
}; // struct ClientPrivate

} // namespace Jgv::Gvcp

#endif // GVCPCLIENT_P_H
