/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "eventhelper.h"

#include <arpa/inet.h> // htonl

namespace Jgv::Gvcp {

struct EVENT {
    uint16_t reserved;
    uint16_t identifier;
    uint16_t streamChannelIndex;
    uint16_t blockId;
    uint32_t timestampHigh;
    uint32_t timestampLow;
};

struct EVENT_CMD {
    CMD_HEADER header;
    EVENT firstEvent;
};

struct EVENT_ACK {
    ACK_HEADER header;
};

constexpr auto EVENT_ACK_LENGTH { sizeof(EVENT_ACK) - sizeof(ACK_HEADER) };

auto EventHelper::eventIdentifier() const noexcept -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(subspan(offsetof(EVENT, identifier))));
}

auto EventHelper::streamChannelIndex() const noexcept -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(subspan(offsetof(EVENT, streamChannelIndex))));
}

auto EventHelper::blockId() const noexcept -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(subspan(offsetof(EVENT, blockId))));
}

auto EventHelper::timestampHigh() const noexcept -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(subspan(offsetof(EVENT, timestampHigh))));
}

auto EventHelper::timestampLow() const noexcept -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(subspan(offsetof(EVENT, timestampLow))));
}

EventCmdHelper::EventCmdHelper(std::span<uint8_t> memoryView, uint16_t length)
    : HeaderCmdHelper(memoryView, GVCP_CMD_EVENT, length)
{
}

auto EventCmdHelper::eventsCount(std::span<const uint8_t> memoryView) noexcept -> uint16_t
{
    return length(memoryView) / sizeof(EVENT);
}

auto EventCmdHelper::event(uint16_t index, std::span<const uint8_t> memoryView) noexcept -> EventHelper
{
    return { memoryView.subspan(offsetof(EVENT_CMD, firstEvent) + (index * sizeof(EVENT)), sizeof(EVENT)) };
}

EventAckHelper::EventAckHelper(std::span<uint8_t> memoryView, uint16_t lenght)
    : HeaderAckHelper(memoryView, GVCP_ACK_EVENT, lenght)
{
}

} // namespace Jgv::Gvcp
