/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "discoveryhelper.h"

#include <arpa/inet.h> // ntoh
#include <iostream>

namespace {
constexpr uint8_t ALLOW_BROADCAST_ON { 0x10 };
} // namespace

namespace Jgv::Gvcp {

DiscoveryCmdHelper::DiscoveryCmdHelper(std::span<uint8_t> memoryView, uint16_t length)
    : HeaderCmdHelper(memoryView, GVCP_CMD_DISCOVERY, length)
{
}

void DiscoveryCmdHelper::allowBroadcastAck()
{
    const auto flag { fromUnaligned<uint8_t>(subspan(offsetof(CMD_HEADER, flag))) };
    std::cout << "current flag " << (int)flag << " new flag " << (int)(ALLOW_BROADCAST_ON | flag) << std::endl;

    toUnaligned<uint8_t>(ALLOW_BROADCAST_ON | flag, subspan(offsetof(CMD_HEADER, flag)));
}

DiscoveryAckHelper::DiscoveryAckHelper(std::span<uint8_t> memoryView, uint16_t length)
    : HeaderAckHelper(memoryView, GVCP_ACK_DISCOVERY, length)
{
}

//void DiscoveryAckHelper::setSpecVersionMajor(uint16_t value)
//{
//    reinterpret_cast<DISCOVERY_ACK*>(data())->specVersionMajor = native_to_big(value);
//}

//void DiscoveryAckHelper::setSpecVersionMinor(uint16_t value)
//{
//    reinterpret_cast<DISCOVERY_ACK*>(data())->specVersionMinor = native_to_big(value);
//}

//void DiscoveryAckHelper::setDeviceMode(uint32_t mode)
//{
//    reinterpret_cast<DISCOVERY_ACK*>(data())->deviceMode = native_to_big(mode);
//}

//void DiscoveryAckHelper::setDeviceMACaddressHigh(uint16_t mac)
//{
//    reinterpret_cast<DISCOVERY_ACK*>(data())->deviceMACAddressHigh = native_to_big(mac);
//}

//void DiscoveryAckHelper::setDeviceMACaddressLow(uint32_t mac)
//{
//    reinterpret_cast<DISCOVERY_ACK*>(data())->deviceMACAddressLow = native_to_big(mac);
//}

//void DiscoveryAckHelper::setIPconfigOptions(uint32_t options)
//{
//    reinterpret_cast<DISCOVERY_ACK*>(data())->ipConfigOptions = native_to_big(options);
//}

//void DiscoveryAckHelper::setIPconfigCurrent(uint32_t current)
//{
//    reinterpret_cast<DISCOVERY_ACK*>(data())->ipConfigCurrent = native_to_big(current);
//}

//void DiscoveryAckHelper::setCurrentIP(uint32_t IP)
//{
//    reinterpret_cast<DISCOVERY_ACK*>(data())->currentIP = native_to_big(IP);
//}

//void DiscoveryAckHelper::setCurrentSubnetMask(uint32_t mask)
//{
//    reinterpret_cast<DISCOVERY_ACK*>(data())->currentSubnetMask = native_to_big(mask);
//}

//void DiscoveryAckHelper::setDefaultGateway(uint32_t gateway)
//{
//    reinterpret_cast<DISCOVERY_ACK*>(data())->defaultGateway = native_to_big(gateway);
//}

//void DiscoveryAckHelper::setManufacturerName(const char* name)
//{
//    std::memcpy(reinterpret_cast<DISCOVERY_ACK*>(data())->manufacturerName, name, sizeof(reinterpret_cast<DISCOVERY_ACK*>(data())->manufacturerName));
//}

//void DiscoveryAckHelper::setModelName(const char* name)
//{
//    std::memcpy(reinterpret_cast<DISCOVERY_ACK*>(data())->modelName, name, sizeof(reinterpret_cast<DISCOVERY_ACK*>(data())->modelName));
//}

//void DiscoveryAckHelper::setDeviceVersion(const char* name)
//{
//    std::memcpy(reinterpret_cast<DISCOVERY_ACK*>(data())->deviceVersion, name, sizeof(reinterpret_cast<DISCOVERY_ACK*>(data())->deviceVersion));
//}

//void DiscoveryAckHelper::setManufacturerInformation(const char* name)
//{
//    std::memcpy(reinterpret_cast<DISCOVERY_ACK*>(data())->manufacturerSpecificInformation, name, sizeof(reinterpret_cast<DISCOVERY_ACK*>(data())->manufacturerSpecificInformation));
//}

//void DiscoveryAckHelper::setSerialNumber(const char* name)
//{
//    std::memcpy(reinterpret_cast<DISCOVERY_ACK*>(data())->serialNumber, name, sizeof(reinterpret_cast<DISCOVERY_ACK*>(data())->serialNumber));
//}

//void DiscoveryAckHelper::setUserName(const char* name)
//{
//    std::memcpy(reinterpret_cast<DISCOVERY_ACK*>(data())->userDefinedName, name, sizeof(reinterpret_cast<DISCOVERY_ACK*>(data())->userDefinedName));
//}

auto DiscoveryAckHelper::specVersionMajor(std::span<const uint8_t> memoryView) -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(DISCOVERY_ACK, specVersionMajor))));
}

auto DiscoveryAckHelper::specVersionMinor(std::span<const uint8_t> memoryView) -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(DISCOVERY_ACK, specVersionMinor))));
}

auto DiscoveryAckHelper::deviceMode(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(DISCOVERY_ACK, deviceMode))));
}

auto DiscoveryAckHelper::deviceClass(uint32_t deviceMode) -> uint32_t
{
    return (0x70000000 & deviceMode) >> 28; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
}

auto DiscoveryAckHelper::deviceMACaddressHigh(std::span<const uint8_t> memoryView) -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(DISCOVERY_ACK, deviceMACAddressHigh))));
}

auto DiscoveryAckHelper::deviceMACaddressLow(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(DISCOVERY_ACK, deviceMACAddressLow))));
}

auto DiscoveryAckHelper::IPconfigOptions(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(DISCOVERY_ACK, ipConfigOptions))));
}

auto DiscoveryAckHelper::IPconfigCurrent(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(DISCOVERY_ACK, ipConfigCurrent))));
}

auto DiscoveryAckHelper::currentIP(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(DISCOVERY_ACK, currentIP))));
}

auto DiscoveryAckHelper::currentSubnetMask(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(DISCOVERY_ACK, currentSubnetMask))));
}

auto DiscoveryAckHelper::defaultGateway(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(DISCOVERY_ACK, defaultGateway))));
}

auto DiscoveryAckHelper::manufacturerName(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>
{
    return memoryView.subspan(offsetof(DISCOVERY_ACK, manufacturerName), sizeof DISCOVERY_ACK::manufacturerName);
}

auto DiscoveryAckHelper::modelName(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>
{
    return memoryView.subspan(offsetof(DISCOVERY_ACK, modelName), sizeof DISCOVERY_ACK::modelName);
}

auto DiscoveryAckHelper::deviceVersion(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>
{
    return memoryView.subspan(offsetof(DISCOVERY_ACK, deviceVersion), sizeof DISCOVERY_ACK::deviceVersion);
}

auto DiscoveryAckHelper::manufacturerSpecificInformation(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>
{
    return memoryView.subspan(offsetof(DISCOVERY_ACK, manufacturerSpecificInformation), sizeof DISCOVERY_ACK::manufacturerSpecificInformation);
}

auto DiscoveryAckHelper::serialNumber(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>
{
    return memoryView.subspan(offsetof(DISCOVERY_ACK, serialNumber), sizeof DISCOVERY_ACK::serialNumber);
}

auto DiscoveryAckHelper::userDefinedName(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>
{
    return memoryView.subspan(offsetof(DISCOVERY_ACK, userDefinedName), sizeof DISCOVERY_ACK::userDefinedName);
}

} // namespace Jgv::Gvcp
