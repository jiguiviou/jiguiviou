/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVCPCLIENT_H
#define GVCPCLIENT_H

#include <memory>
#include <span>
#include <vector>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::Gvcp::Bootstrap {
enum class Address : uint32_t;
enum class BlockSize : uint16_t;
} // namespace Jgv::Gvcp::Bootstrap

namespace Jgv::Gvcp {

struct ADDRESS_VALUE {
    uint32_t address;
    uint32_t value;
};
using AddrValPairList = std::vector<ADDRESS_VALUE>;

class ClientPrivate;
class Client {

public:
    explicit Client(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    virtual ~Client();
    Client(const Client&) = delete;
    Client(Client&&) = delete;
    auto operator=(const Client&) -> Client& = delete;
    auto operator=(Client &&) -> Client& = delete;

    auto controleDevice() -> bool;
    auto monitorDevice() -> bool;
    auto listenEvents(uint16_t deviceMessageChannelPort) -> uint16_t;
    void releaseDevice();

    auto readRegister(Bootstrap::Address address) -> uint32_t;
    auto readRegister(uint32_t address) -> uint32_t;
    auto readRegisters(const std::vector<uint32_t>& addresses) -> std::vector<uint32_t>;

    auto writeRegisters(const AddrValPairList& values) -> bool;
    void writeRegister(uint32_t address, uint32_t value);
    void writeRegister(uint32_t address, int32_t value);

    auto readMemory(uint32_t address, uint16_t count) -> std::vector<uint8_t>;
    auto writeMemory(uint32_t address, std::span<uint8_t> memoryView) -> bool;

    auto xmlFile() -> std::string;
    auto xmlFilename() -> std::string;

    auto getDatation() -> std::tuple<int64_t, int64_t, int64_t>;

private:
    const std::unique_ptr<ClientPrivate> _impl;
};

} // namespace Jgv::Gvcp

#endif // GVCPCLIENT_H
