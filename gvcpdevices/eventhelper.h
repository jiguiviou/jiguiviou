/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef EVENTHELPER_H
#define EVENTHELPER_H

#include "headerhelper.h"

namespace Jgv::Gvcp {

constexpr uint16_t GVCP_CMD_EVENT { 0x00C0 };
constexpr uint16_t GVCP_ACK_EVENT { 0x00C1 };

class EventHelper : public std::span<const uint8_t> {
public:
    [[nodiscard]] auto eventIdentifier() const noexcept -> uint16_t;
    [[nodiscard]] auto streamChannelIndex() const noexcept -> uint16_t;
    [[nodiscard]] auto blockId() const noexcept -> uint16_t;
    [[nodiscard]] auto timestampHigh() const noexcept -> uint32_t;
    [[nodiscard]] auto timestampLow() const noexcept -> uint32_t;
};

class EventCmdHelper final : public HeaderCmdHelper {
public:
    EventCmdHelper(std::span<uint8_t> memoryView, uint16_t length);

    static auto eventsCount(std::span<const uint8_t> memoryView) noexcept -> uint16_t;
    static auto event(uint16_t index, std::span<const uint8_t> memoryView) noexcept -> EventHelper;
};

class EventAckHelper final : public HeaderAckHelper {
public:
    EventAckHelper(std::span<uint8_t> memoryView, uint16_t lenght);
};

} // namespace Jgv::Gvcp

#endif // EVENTHELPER_H
