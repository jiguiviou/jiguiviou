/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef READREGHELPER_H
#define READREGHELPER_H

#include "headerhelper.h"
#include <vector>

namespace Jgv::Gvcp {

constexpr uint16_t GVCP_CMD_READREG = 0x0080;
constexpr uint16_t GVCP_ACK_READREG = 0x0081;

struct READREG_CMD {
    CMD_HEADER header;
    uint32_t addresses;
};

struct READREG_ACK {
    ACK_HEADER header;
    uint32_t registerData;
};

class ReadregCmdHelper final : public HeaderCmdHelper {
public:
    ReadregCmdHelper(std::span<uint8_t> memoryView, uint16_t length);

    void setAddress(uint32_t address);

    void setAddresses(std::span<const uint32_t> addresses);
    static auto addresses(std::span<const uint8_t> memoryView) -> std::vector<uint32_t>;
};

class ReadregAckHelper final : public HeaderAckHelper {
public:
    ReadregAckHelper(std::span<uint8_t> memoryView, uint16_t lenght);

    void setRegistersData(const std::vector<uint32_t>& registersData);
    static auto registersData(std::span<uint8_t> memoryView) -> std::vector<uint32_t>;
};

} // namespace Jgv::Gvcp

#endif // READREGHELPER_H
