﻿/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVCPDISCOVERER_H
#define GVCPDISCOVERER_H

#include <functional>
#include <memory>
#include <span>
#include <string>

namespace Jgv::Gvcp {

struct DeviceInfos {
    std::string model;
    std::string manufacturer;
    std::string version;
    std::string serial;
    std::string userName;
    uint32_t deviceClass;
    uint32_t ip;
    uint32_t netmask;
    uint32_t gateway;
    uint64_t mac;
};

class DiscovererPrivate;
class Discoverer final {
public:
    Discoverer();
    ~Discoverer();
    Discoverer(const Discoverer&) = delete;
    Discoverer(Discoverer&&) = delete;
    auto operator=(const Discoverer&) -> Discoverer& = delete;
    auto operator=(Discoverer &&) -> Discoverer& = delete;

    auto listen(uint32_t srcIP) -> bool;
    void stop();
    void discover(uint32_t peerIP);
    void forceIP(uint64_t mac, uint32_t newIP, uint32_t newNetmask, uint32_t newGateway);
    void addDeviceFoundedListener(std::weak_ptr<std::function<void(const DeviceInfos&)>> listener);
    void addForceIpSuccesListener(std::weak_ptr<std::function<void(void)>> listener);

private:
    const std::unique_ptr<DiscovererPrivate> _impl;
};

} // namespace Jgv::Gvcp

#endif // GVCPDISCOVERER_H
