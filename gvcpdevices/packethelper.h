/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef PACKETHELPER_H
#define PACKETHELPER_H

#include <array>
#include <cstdint>
#include <vector>

namespace Jgv::Gvcp {

using PacketSize = std::vector<uint8_t>::size_type;
const PacketSize GVCP_PACKET_MAX_SIZE = 548;
using EventBuffer = std::array<uint8_t, GVCP_PACKET_MAX_SIZE>;

} // namespace Jgv::Gvcp

#endif // PACKETHELPER_H
