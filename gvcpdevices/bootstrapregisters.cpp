/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "bootstrapregisters.h"
#include "bootstrapregisters_p.h"

namespace Jgv::Gvcp::Bootstrap {

auto Registers::streamChannelTranspose(uint channel, Bootstrap::Address address) noexcept -> uint32_t
{
    return (channel * 0x40) + static_cast<uint32_t>(address);
}

auto Registers::align(int val) -> int
{
    const int r = val % 4;
    return r == 0 ? val : val + (4 - r);
}

auto CCP::privilegeFromCCP(uint32_t CCP) -> CCPPrivilege
{
    if ((CCP & 0x01) != 0)
        return CCPPrivilege::ExclusiveAcces;
    if ((CCP & 0x02) == 0)
        return CCPPrivilege::OpenAcces;
    if ((CCP & 0x04) == 0)
        return CCPPrivilege::ControlAcces;
    return CCPPrivilege::ControlAccesWithSwitchoverEnabled;
}

auto CCP::CCPfromPrivilege(CCPPrivilege privilege) -> uint32_t
{
    if (privilege == CCPPrivilege::OpenAcces)
        return 0x00;
    if (privilege == CCPPrivilege::ControlAcces)
        return 0x02;
    if (privilege == CCPPrivilege::ControlAccesWithSwitchoverEnabled)
        return 0x06;
    return 0x01;
}

auto CCP::privilegeToString(CCPPrivilege priv) -> std::string
{
    if (priv == CCPPrivilege::OpenAcces)
        return "OpenAcces";
    if (priv == CCPPrivilege::ControlAcces)
        return "ControlAcces";
    if (priv == CCPPrivilege::ControlAccesWithSwitchoverEnabled)
        return "ControlAccesWithSwitchoverEnabled";
    if (priv == CCPPrivilege::ExclusiveAcces)
        return "ExclusiveAcces";

    return "AccesDenied";
}

//int bitFromRegister(uint32_t reg, unsigned bit) {
//    return 0x00000001 & (reg >> (31-bit));
//}

} // namespace Jgv::Gvcp::Bootstrap
