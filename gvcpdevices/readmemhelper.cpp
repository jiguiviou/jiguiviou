/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "readmemhelper.h"

#include <arpa/inet.h> // ntoh

namespace Jgv::Gvcp {

ReadmemCmdHelper::ReadmemCmdHelper(std::span<uint8_t> memoryView, uint16_t length)
    : HeaderCmdHelper(memoryView, GVCP_CMD_READMEM, length)
{
}

void ReadmemCmdHelper::setAddress(uint32_t address)
{
    toUnaligned(htonl(address), subspan(offsetof(READMEM_CMD, address)));
}

void ReadmemCmdHelper::setDataCount(uint16_t count)
{
    toUnaligned(htons(count), subspan(offsetof(READMEM_CMD, count)));
}

auto ReadmemCmdHelper::address(std::span<const uint8_t> memoryView) -> unsigned
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(READMEM_CMD, address))));
}

auto ReadmemCmdHelper::dataCount(std::span<const uint8_t> memoryView) -> unsigned
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(READMEM_CMD, count))));
}

ReadmemAckHelper::ReadmemAckHelper(std::span<uint8_t> memoryView, uint16_t lenght)
    : HeaderAckHelper(memoryView, GVCP_ACK_READMEM, lenght)
{
}

void ReadmemAckHelper::setAddress(uint32_t address)
{
    toUnaligned(htonl(address), subspan(offsetof(READMEM_ACK, address)));
}

//void ReadmemAckHelper::addMem(const uint8_t* mem, uint size)
//{
//    std::memcpy(reinterpret_cast<READMEM_ACK*>(data())->datas, mem, size);
//}

auto ReadmemAckHelper::address(std::span<const uint8_t> memoryView) -> uint32_t
{
    return ntohl(fromUnaligned<uint32_t>(memoryView.subspan(offsetof(READMEM_ACK, address))));
}

auto ReadmemAckHelper::datas(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>
{
    // obtient la taille
    const auto length { HeaderAckHelper::length(memoryView) - static_cast<int>(sizeof(READMEM_ACK::address)) };
    return memoryView.subspan(offsetof(READMEM_ACK, datas), length);
}

} // namespace Jgv::Gvcp
