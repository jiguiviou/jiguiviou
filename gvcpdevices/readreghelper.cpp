/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "readreghelper.h"

#include <arpa/inet.h> // hton

namespace Jgv::Gvcp {

ReadregCmdHelper::ReadregCmdHelper(std::span<uint8_t> memoryView, uint16_t length)
    : HeaderCmdHelper(memoryView, GVCP_CMD_READREG, length)
{
}

void ReadregCmdHelper::setAddress(uint32_t address)
{
    toUnaligned(htonl(address), subspan(offsetof(READREG_CMD, addresses)));
}

void ReadregCmdHelper::setAddresses(std::span<const uint32_t> addresses)
{
    SpanIndex index { offsetof(READREG_CMD, addresses) };
    for (const auto address : addresses) {
        toUnaligned(htonl(address), subspan(index));
        index += sizeof address;
    }
}

auto ReadregCmdHelper::addresses(std::span<const uint8_t> memoryView) -> std::vector<uint32_t>
{
    // la taille en octets des données
    const auto length { HeaderCmdHelper::length(memoryView) };

    // notre span sur toutes les données
    auto datasSpan { memoryView.subspan(offsetof(READREG_CMD, addresses), length) };

    std::vector<uint32_t> result;
    while (datasSpan.size_bytes() >= spanIndex_cast(sizeof(uint32_t))) {
        result.push_back(ntohl(fromUnaligned<uint32_t>(datasSpan)));
        datasSpan = datasSpan.subspan(spanIndex_cast(sizeof(uint32_t)));
    }

    return result;
}

ReadregAckHelper::ReadregAckHelper(std::span<uint8_t> memoryView, uint16_t lenght)
    : HeaderAckHelper(memoryView, GVCP_ACK_READREG, lenght)
{
}

void ReadregAckHelper::setRegistersData(const std::vector<uint32_t>& answers)
{
    auto datasSpan { subspan(offsetof(READREG_ACK, registerData)) };
    for (auto answer : answers) {
        toUnaligned(htonl(answer), datasSpan);
        datasSpan = datasSpan.subspan(spanIndex_cast(sizeof(uint32_t)));
    }
}

auto ReadregAckHelper::registersData(std::span<uint8_t> memoryView) -> std::vector<uint32_t>
{
    // la taille en octets des données
    const auto length { HeaderAckHelper::length(memoryView) };

    // notre span sur toutes les données
    auto datasSpan { memoryView.subspan(offsetof(READREG_ACK, registerData), length) };

    std::vector<uint32_t> result;
    while (datasSpan.size_bytes() >= spanIndex_cast(sizeof(uint32_t))) {
        result.push_back(ntohl(fromUnaligned<uint32_t>(datasSpan)));
        datasSpan = datasSpan.subspan(spanIndex_cast(sizeof(uint32_t)));
    }

    return result;
}

} // namespace Jgv::Gvcp
