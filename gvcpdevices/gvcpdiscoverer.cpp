/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvcpdiscoverer.h"
#include "gvcpdiscoverer_p.h"

#include "discoveryhelper.h"
#include "forceiphelper.h"
#include "gvcp.h"

#include <arpa/inet.h>
#include <globalipc/globalipc.h>
#include <iostream>
#include <netinet/in.h>
#include <poll.h>
#include <sys/fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

namespace {

void sendFrom(int sd, std::span<uint8_t> cmd, uint32_t from, uint32_t to)
{
    sockaddr_in dest = {};
    dest.sin_family = AF_INET;
    dest.sin_port = htons(Jgv::Gvcp::GVCP_PORT);
    dest.sin_addr.s_addr = htonl(to);

    iovec iov { cmd.data(), static_cast<std::size_t>(cmd.size_bytes()) };

    // construction du tampon des données de service
    std::array<char, CMSG_SPACE(sizeof(in_pktinfo))> buffer {};
    // construction du message
    msghdr msg {};
    msg.msg_name = &dest;
    msg.msg_namelen = sizeof(dest);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = &buffer;
    msg.msg_controllen = sizeof(buffer);
    msg.msg_flags = 0;

    // le pointeur sur les données de service
    cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_IP;
    cmsg->cmsg_type = IP_PKTINFO;
    cmsg->cmsg_len = CMSG_LEN(sizeof(in_pktinfo));
    auto pktinfo = reinterpret_cast<in_pktinfo*>(CMSG_DATA(cmsg));
    pktinfo->ipi_ifindex = 0;
    pktinfo->ipi_spec_dst.s_addr = htonl(from); // on ne traite que les paquets vers notre controleur

    if (sendmsg(sd, &msg, 0) < 0) {
        std::perror("GvcpDiscoverer::discover sendmsg");
    }
}

} // namespace

namespace Jgv::Gvcp {

using PacketBuffer = std::array<uint8_t, GVCP_PACKET_MAX_SIZE>;

void DiscovererPrivate::listenSocket()
{
    pollfd pfd {};
    pfd.fd = sd;
    pfd.events = POLLIN;

    in_addr addr {};
    addr.s_addr = htonl(srcIP);
    char saddr[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &addr.s_addr, saddr, INET_ADDRSTRLEN);
    std::clog << "Gvcp::Discoverer::listenSocket(): starts listening on " << saddr << std::endl;

    while (listen) {
        const int pollResult = poll(&pfd, 1, 500);
        if (pollResult == -1) {
            std::perror("Gvcp::DiscovererPrivate::listenSocket poll()");
        } else if (pollResult == 0) {
            // timeout
        } else {

            sockaddr fromAddr {};
            socklen_t fromlen = sizeof fromAddr;
            PacketBuffer packet;

            const auto size = recvfrom(sd, packet.data(), packet.size(), 0, &fromAddr, &fromlen);
            if (size < static_cast<int>(sizeof(ACK_HEADER))) {
                continue;
            }

            if (HeaderAckHelper::status(packet) != enumType(Status::SUCCESS)) {
                std::clog << "GvcpDiscovererPrivate::listenSocket() : Status unsucces " << HeaderAckHelper::status(packet) << std::endl;
                continue;
            }

            if (HeaderAckHelper::ackId(packet) != id) {
                continue;
            }

            if (HeaderAckHelper::acknowledge(packet) == GVCP_ACK_DISCOVERY) {
                if ((HeaderAckHelper::length(packet) != DISCOVERY_ACK_LENGTH) || (size != sizeof(DISCOVERY_ACK))) {
                    std::clog << "GvcpDiscovererPrivate::listenSocket() : Discover mal formatted packet !" << std::endl;
                    continue;
                }

                for (auto& listener : deviceFoundedListeners) {
                    if (auto p { listener.lock() }) {
                        DeviceInfos infos {};
                        infos.model = Jgv::spanToString(DiscoveryAckHelper::modelName(packet));
                        infos.manufacturer = Jgv::spanToString(DiscoveryAckHelper::manufacturerName(packet));
                        infos.version = Jgv::spanToString(DiscoveryAckHelper::deviceVersion(packet));
                        infos.serial = Jgv::spanToString(DiscoveryAckHelper::serialNumber(packet));
                        infos.userName = Jgv::spanToString(DiscoveryAckHelper::userDefinedName(packet));
                        infos.deviceClass = DiscoveryAckHelper::deviceClass(DiscoveryAckHelper::deviceMode(packet));
                        infos.ip = DiscoveryAckHelper::currentIP(packet);
                        infos.netmask = DiscoveryAckHelper::currentSubnetMask(packet);
                        infos.gateway = DiscoveryAckHelper::defaultGateway(packet);
                        infos.mac = DiscoveryAckHelper::deviceMACaddressHigh(packet);
                        infos.mac <<= 32;
                        infos.mac += DiscoveryAckHelper::deviceMACaddressLow(packet);
                        (*p)(infos);
                    }
                }

            } else if (HeaderAckHelper::acknowledge(packet) == GVCP_ACK_FORCEIP) {
                if ((HeaderAckHelper::length(packet) != FORCEIP_ACK_LENGTH) || (size != sizeof(FORCEIP_ACK))) {
                    std::clog << "GvcpDiscovererPrivate::listenSocket() : Discover mal formatted packet !" << std::endl;
                    continue;
                }

                for (auto& listener : forceIpSuccesListeners) {
                    if (auto p { listener.lock() }) {
                        (*p)();
                    }
                }
            }
        }
    }

    std::clog << "Gvcp::Discoverer::listenSocket(): stops listening on " << saddr << std::endl;
}

Discoverer::Discoverer()
    : _impl(std::make_unique<DiscovererPrivate>())
{
}

Discoverer::~Discoverer()
{
    stop();
}

bool Discoverer::listen(uint32_t srcIP)
{
    if (_impl->threadPtr && _impl->threadPtr->joinable() && _impl->srcIP == srcIP) {
        in_addr addr = {};
        addr.s_addr = htonl(srcIP);
        char saddr[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &addr.s_addr, saddr, INET_ADDRSTRLEN);

        std::clog << "GvcpDiscoverer: allready listening " << saddr << std::endl;
        return true;
    }

    _impl->srcIP = srcIP;

    // descripteur du socket UDP
    _impl->sd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (_impl->sd < 0) {
        std::perror("GvcpDiscoverer::listen");
        return false;
    }

    // ancillary data
    int on = 1;
    setsockopt(_impl->sd, SOL_IP, IP_PKTINFO, &on, sizeof(on));
    // autorise le broadcast
    setsockopt(_impl->sd, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on));

    // bind sur port alléatoire
    sockaddr_in localAddress = {};
    localAddress.sin_family = AF_INET;
    localAddress.sin_port = htons(0); // port aléatoire
    localAddress.sin_addr.s_addr = htonl(INADDR_ANY); // écoute sur tous les NIC
    if (bind(_impl->sd, reinterpret_cast<const sockaddr*>(&localAddress), sizeof(sockaddr_in)) == -1) {
        std::perror("GvcpDiscoverer::listen bind()");
        close(_impl->sd);
        return false;
    }

    // socket non bloquant
    fcntl(_impl->sd, F_SETFL, O_NONBLOCK);

    // notre descripteur écoute, on lance le thread de polling
    _impl->listen = true;
    _impl->threadPtr = std::unique_ptr<std::thread>(new std::thread(&DiscovererPrivate::listenSocket, _impl.get()));
    pthread_setname_np(_impl->threadPtr->native_handle(), "GvcpDiscoverer");

    return true;
}

void Discoverer::stop()
{
    if (_impl->threadPtr && _impl->threadPtr->joinable()) {
        _impl->listen = false;
        _impl->threadPtr->join();
    }
    if (_impl->sd != -1) {
        close(_impl->sd);
        _impl->sd = -1;
    }
}

void Discoverer::discover(uint32_t peerIP)
{
    _impl->id = (_impl->id == 0xFFFFu) ? 1 : _impl->id + 1;

    std::array<uint8_t, GVCP_PACKET_MAX_SIZE> buffer {};
    DiscoveryCmdHelper cmd(buffer, DISCOVERY_CMD_LENGTH);
    if (peerIP == INADDR_BROADCAST) {
        cmd.allowBroadcastAck();
    }
    cmd.setReqId(_impl->id);

    sendFrom(_impl->sd, cmd, _impl->srcIP, peerIP);
}

void Discoverer::forceIP(uint64_t mac, uint32_t newIP, uint32_t newNetmask, uint32_t newGateway)
{
    _impl->id = (_impl->id == 0xFFFF) ? 1 : _impl->id + 1;

    std::array<uint8_t, GVCP_PACKET_MAX_SIZE> buffer;
    ForceipCmdHelper cmd(buffer, FORCEIP_CMD_LENGTH);
    cmd.setReqId(_impl->id);
    cmd.setMacHigh(0xFFFF & (mac >> 32));
    cmd.setMacLow(0xFFFFFFFF & mac);
    cmd.setStaticIP(newIP);
    cmd.setStaticNetmask(newNetmask);
    cmd.setStaticDefaultGateway(newGateway);

    sendFrom(_impl->sd, cmd, _impl->srcIP, INADDR_BROADCAST);
}

void Discoverer::addDeviceFoundedListener(std::weak_ptr<std::function<void(const DeviceInfos&)>> listener)
{
    _impl->deviceFoundedListeners.emplace_back(listener);
}

void Discoverer::addForceIpSuccesListener(std::weak_ptr<std::function<void(void)>> listener)
{
    _impl->forceIpSuccesListeners.emplace_back(listener);
}

} // namespace Jgv::Gvcp
