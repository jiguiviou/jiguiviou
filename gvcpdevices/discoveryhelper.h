/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DISCOVERYHELPER_H
#define DISCOVERYHELPER_H

#include "headerhelper.h"

#include <array>

namespace Jgv::Gvcp {

using ByteArray16 = std::array<std::byte, 16>; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
using ByteArray32 = std::array<std::byte, 32>; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
using ByteArray48 = std::array<std::byte, 48>; // NOLINT(cppcoreguidelines-avoid-magic-numbers)

struct DISCOVERY_CMD {
    CMD_HEADER header;
};

struct DISCOVERY_ACK {
    ACK_HEADER header;
    uint16_t specVersionMajor;
    uint16_t specVersionMinor;
    uint32_t deviceMode;
    uint16_t reserved1;
    uint16_t deviceMACAddressHigh;
    uint32_t deviceMACAddressLow;
    uint32_t ipConfigOptions;
    uint32_t ipConfigCurrent;
    std::array<uint32_t, 3> reserved2;
    uint32_t currentIP;
    std::array<uint32_t, 3> reserved3;
    uint32_t currentSubnetMask;
    std::array<uint32_t, 3> reserved4;
    uint32_t defaultGateway;
    ByteArray32 manufacturerName;
    ByteArray32 modelName;
    ByteArray32 deviceVersion;
    ByteArray48 manufacturerSpecificInformation;
    ByteArray16 serialNumber;
    ByteArray16 userDefinedName;
};

constexpr uint16_t GVCP_CMD_DISCOVERY = 0x0002;
constexpr uint16_t GVCP_ACK_DISCOVERY = 0x0003;
constexpr int DISCOVERY_CMD_LENGTH = sizeof(DISCOVERY_CMD) - sizeof(CMD_HEADER);
constexpr int DISCOVERY_ACK_LENGTH = sizeof(DISCOVERY_ACK) - sizeof(ACK_HEADER);

class DiscoveryCmdHelper final : public HeaderCmdHelper {
public:
    DiscoveryCmdHelper(std::span<uint8_t> memoryView, uint16_t length);

    void allowBroadcastAck();
};

class DiscoveryAckHelper final : public HeaderAckHelper {
public:
    DiscoveryAckHelper(std::span<uint8_t> memoryView, uint16_t length);

    //    void setSpecVersionMajor(uint16_t value);
    //    void setSpecVersionMinor(uint16_t value);
    //    void setDeviceMode(uint32_t mode);
    //    void setDeviceMACaddressHigh(uint16_t mac);
    //    void setDeviceMACaddressLow(uint32_t mac);
    //    void setIPconfigOptions(uint32_t options);
    //    void setIPconfigCurrent(uint32_t current);
    //    void setCurrentIP(uint32_t IP);
    //    void setCurrentSubnetMask(uint32_t mask);
    //    void setDefaultGateway(uint32_t gateway);
    //    void setManufacturerName(const char* name);
    //    void setModelName(const char* name);
    //    void setDeviceVersion(const char* name);
    //    void setManufacturerInformation(const char* name);
    //    void setSerialNumber(const char* name);
    //    void setUserName(const char* name);

    static auto specVersionMajor(std::span<const uint8_t> memoryView) -> uint16_t;
    static auto specVersionMinor(std::span<const uint8_t> memoryView) -> uint16_t;
    static auto deviceMode(std::span<const uint8_t> memoryView) -> uint32_t;

    static auto deviceClass(uint32_t deviceMode) -> uint32_t;
    static auto deviceMACaddressHigh(std::span<const uint8_t> memoryView) -> uint16_t;
    static auto deviceMACaddressLow(std::span<const uint8_t> memoryView) -> uint32_t;
    static auto IPconfigOptions(std::span<const uint8_t> memoryView) -> uint32_t;
    static auto IPconfigCurrent(std::span<const uint8_t> memoryView) -> uint32_t;
    static auto currentIP(std::span<const uint8_t> memoryView) -> uint32_t;
    static auto currentSubnetMask(std::span<const uint8_t> memoryView) -> uint32_t;
    static auto defaultGateway(std::span<const uint8_t> memoryView) -> uint32_t;
    static auto manufacturerName(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>;
    static auto modelName(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>;
    static auto deviceVersion(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>;
    static auto manufacturerSpecificInformation(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>;
    static auto serialNumber(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>;
    static auto userDefinedName(std::span<const uint8_t> memoryView) -> std::span<const uint8_t>;
};

} // namespace Jgv::Gvcp

#endif // READREGHELPER_H
