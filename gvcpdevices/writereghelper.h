/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef WRITEREGHELPER_H
#define WRITEREGHELPER_H

#include "headerhelper.h"

namespace Jgv::Gvcp {

constexpr uint16_t GVCP_CMD_WRITEREG { 0x0082 };
constexpr uint16_t GVCP_ACK_WRITEREG { 0x0083 };

struct ADDRESS_VALUE;
using AddrValPairList = std::vector<ADDRESS_VALUE>;

class WriteregCmdHelper final : public HeaderCmdHelper {
public:
    WriteregCmdHelper(std::span<uint8_t> memoryView, uint16_t length);

    void setRegsValList(const AddrValPairList& values) noexcept;
    void setReg(uint32_t address, uint32_t value) noexcept;
    void setReg(uint32_t address, int32_t value) noexcept;
};

class WriteregAckHelper final : public HeaderAckHelper {
public:
    WriteregAckHelper(std::span<uint8_t> memoryView, uint16_t lenght);

    void setIndex(uint16_t index) noexcept;

    static auto index(std::span<uint8_t> memoryView) -> uint16_t;
};

} // namespace Jgv::Gvcp

#endif // WRITEREGHELPER_H
