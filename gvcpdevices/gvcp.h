/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVCP_H
#define GVCP_H

#include <cstdint>
#include <iostream>
#include <string>
#include <type_traits>

namespace Jgv::Gvcp {

constexpr uint16_t GVCP_PORT { 3956 };

enum class Status : uint16_t {
    SUCCESS = 0x0000,
    PACKET_RESEND = 0x0100,
    NOT_IMPLEMENTED = 0x8001,
    INVALID_PARAMETER = 0x8002,
    INVALID_ADDRESS = 0x8003,
    WRITE_PROTECT = 0x8004,
    BAD_ALIGNMENT = 0x8005,
    ACCESS_DENIED = 0x8006,
    GEV_STATUS_BUSY = 0x8007,
    PACKET_UNAVAILABLE = 0x800C,
    DATA_OVERUN = 0x800D,
    INVALID_HEADER = 0x800E,
    PACKET_NOT_YET_AVAILABLE = 0x8010,
    PACKET_AND_PREV_REMOVED_FROM_MEMORY = 0x8011,
    PACKET_REMOVED_FROM_MEMORY = 0x8012,
    NO_REF_TIME = 0x8013,
    PACKET_TEMPORARILY_UNAVAILABLE = 0x0814,
    STATUS_OVERFLOW = 0x0815,
    ACTION_LATE = 0x0816,
    ERROR = 0x8FFF
};

template <typename T>
constexpr auto enumType(T enumerator) noexcept -> typename std::underlying_type<T>::type
{
    return static_cast<typename std::underlying_type<T>::type>(enumerator);
}

static auto statusToString(uint16_t status) -> std::string
{

    switch (status) {
    case enumType(Status::SUCCESS):
        return "SUCCESS";
    case enumType(Status::PACKET_RESEND):
        return "PACKET_RESEND";
    case enumType(Status::NOT_IMPLEMENTED):
        return "NOT_IMPLEMENTED";
    case enumType(Status::INVALID_PARAMETER):
        return "INVALID_PARAMETER";
    case enumType(Status::INVALID_ADDRESS):
        return "INVALID_ADDRESS";
    case enumType(Status::WRITE_PROTECT):
        return "WRITE_PROTECT";
    case enumType(Status::BAD_ALIGNMENT):
        return "BAD_ALIGNMENT";
    case enumType(Status::ACCESS_DENIED):
        return "ACCESS_DENIED";
    case enumType(Status::GEV_STATUS_BUSY):
        return "GEV_STATUS_BUSY";
    case enumType(Status::PACKET_UNAVAILABLE):
        return "PACKET_UNAVAILABLE";
    case enumType(Status::DATA_OVERUN):
        return "DATA_OVERUN";
    case enumType(Status::INVALID_HEADER):
        return "INVALID_HEADER";
    case enumType(Status::PACKET_NOT_YET_AVAILABLE):
        return "PACKET_NOT_YET_AVAILABLE";
    case enumType(Status::PACKET_AND_PREV_REMOVED_FROM_MEMORY):
        return "PACKET_AND_PREV_REMOVED_FROM_MEMORY";
    case enumType(Status::PACKET_REMOVED_FROM_MEMORY):
        return "PACKET_REMOVED_FROM_MEMORY";
    case enumType(Status::NO_REF_TIME):
        return "NO_REF_TIME";
    case enumType(Status::PACKET_TEMPORARILY_UNAVAILABLE):
        return "PACKET_TEMPORARILY_UNAVAILABLE";
    case enumType(Status::STATUS_OVERFLOW):
        return "STATUS_OVERFLOW";
    case enumType(Status::ACTION_LATE):
        return "ACTION_LATE";
    case enumType(Status::ERROR):
        return "ERROR";
    default:
        return "unknow status " + std::to_string(status);
    }
}

} // namespace Jgv::Gvcp

#endif // GVCP_H
