/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "headerhelper.h"

#include "gvcp.h"

#include <arpa/inet.h> // ntoh
#include <iostream>

namespace {
constexpr uint8_t ACK_FLAG_ON { 0x01 };
} // namespace

namespace Jgv::Gvcp {

HeaderCmdHelper::HeaderCmdHelper(std::span<uint8_t> buffer, uint16_t command, uint16_t payloadSize)
    : std::span<uint8_t>(buffer.subspan(0, sizeof(CMD_HEADER) + payloadSize))
{
    toUnaligned(GVCP_HARD_KEY, subspan(offsetof(CMD_HEADER, hardKey)));
    // par défaut, on veut l'ack
    toUnaligned(ACK_FLAG_ON, subspan(offsetof(CMD_HEADER, flag)));
    toUnaligned(htons(command), subspan(offsetof(CMD_HEADER, command)));
    toUnaligned(htons(payloadSize), subspan(offsetof(CMD_HEADER, length)));
}

void HeaderCmdHelper::disableAcknowledge() noexcept
{
    toUnaligned(ACK_FLAG_ON & ~(0x01U), subspan(offsetof(CMD_HEADER, flag)));
}

void HeaderCmdHelper::setReqId(uint16_t id) noexcept
{
    toUnaligned(htons(id), subspan(offsetof(CMD_HEADER, reqId)));
}

auto HeaderCmdHelper::needAcknoledgement() const noexcept -> bool
{
    return (fromUnaligned<uint8_t>(subspan(offsetof(CMD_HEADER, flag))) & ACK_FLAG_ON) > 0;
}

auto HeaderCmdHelper::isValid(std::span<uint8_t> memoryView) noexcept -> bool
{
    if (fromUnaligned<uint8_t>(memoryView.subspan(offsetof(CMD_HEADER, hardKey))) == GVCP_HARD_KEY) {
        return true;
    }
    std::cerr << "HeaderHelper: Bad cmd header flag: " << fromUnaligned<uint8_t>(memoryView.subspan(offsetof(CMD_HEADER, hardKey))) << std::endl;
    return false;
}

auto HeaderCmdHelper::acknowledgeIsSet(std::span<const uint8_t> memoryView) noexcept -> bool
{
    return (fromUnaligned<uint8_t>(memoryView.subspan(offsetof(CMD_HEADER, flag))) & ACK_FLAG_ON) > 0;
}

auto HeaderCmdHelper::command(std::span<const uint8_t> memoryView) noexcept -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(CMD_HEADER, command))));
}

auto HeaderCmdHelper::length(std::span<const uint8_t> memoryView) noexcept -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(CMD_HEADER, length))));
}

auto HeaderCmdHelper::reqId(std::span<const uint8_t> memoryView) noexcept -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(CMD_HEADER, reqId))));
}

HeaderAckHelper::HeaderAckHelper(std::span<uint8_t> buffer, uint16_t acknowledge, uint16_t payloadSize)
    : std::span<uint8_t>(buffer.subspan(0, sizeof(ACK_HEADER) + payloadSize))
{
    toUnaligned(htons(static_cast<uint16_t>(Status::SUCCESS)), subspan(offsetof(ACK_HEADER, status)));
    toUnaligned(htons(acknowledge), subspan(offsetof(ACK_HEADER, acknowledge)));
    toUnaligned(htons(payloadSize), subspan(offsetof(ACK_HEADER, length)));
}

void HeaderAckHelper::setStatus(uint16_t newStatus) noexcept
{
    toUnaligned(htons(newStatus), subspan(offsetof(ACK_HEADER, status)));
}

void HeaderAckHelper::setAckID(uint16_t id) noexcept
{
    toUnaligned(htons(id), subspan(offsetof(ACK_HEADER, ackId)));
}

auto HeaderAckHelper::status(std::span<const uint8_t> memoryView) noexcept -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(ACK_HEADER, status))));
}

auto HeaderAckHelper::acknowledge(std::span<const uint8_t> memoryView) noexcept -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(ACK_HEADER, acknowledge))));
}

auto HeaderAckHelper::length(std::span<const uint8_t> memoryView) noexcept -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(ACK_HEADER, length))));
}

auto HeaderAckHelper::ackId(std::span<const uint8_t> memoryView) noexcept -> uint16_t
{
    return ntohs(fromUnaligned<uint16_t>(memoryView.subspan(offsetof(ACK_HEADER, ackId))));
}

} // namespace Jgv::Gvcp
