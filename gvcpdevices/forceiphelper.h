/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef FORCEIPHELPER_H
#define FORCEIPHELPER_H

#include "headerhelper.h"

namespace Jgv::Gvcp {

using uint32Array3 = std::array<uint32_t, 3>; // NOLINT(cppcoreguidelines-avoid-magic-numbers)

struct FORCEIP_CMD {
    CMD_HEADER header;
    uint16_t reserved1;
    uint16_t MACAddressHigh;
    uint32_t MACAddressLow;
    uint32Array3 reserved2;
    uint32_t staticIP;
    uint32Array3 reserved3;
    uint32_t staticSubnetMask;
    uint32Array3 reserved4;
    uint32_t staticDefaultGateway;
};

struct FORCEIP_ACK {
    ACK_HEADER header;
};

constexpr uint16_t GVCP_CMD_FORCEIP = 0x0004;
constexpr uint16_t GVCP_ACK_FORCEIP = 0x0005;
constexpr int FORCEIP_CMD_LENGTH = sizeof(FORCEIP_CMD) - sizeof(CMD_HEADER);
constexpr int FORCEIP_ACK_LENGTH = sizeof(FORCEIP_ACK) - sizeof(ACK_HEADER);

class ForceipCmdHelper final : public HeaderCmdHelper {
public:
    ForceipCmdHelper(std::span<uint8_t> memoryView, uint16_t length);

    void setMacHigh(uint16_t mac);
    void setMacLow(uint32_t mac);
    void setStaticIP(uint32_t ip);
    void setStaticNetmask(uint32_t netmask);
    void setStaticDefaultGateway(uint32_t gateway);

    static auto macHigh(std::span<const uint8_t> memoryView) -> uint16_t;
    static auto macLow(std::span<const uint8_t> memoryView) -> uint32_t;
    static auto staticIP(std::span<const uint8_t> memoryView) -> uint32_t;
    static auto staticNetmask(std::span<const uint8_t> memoryView) -> uint32_t;
    static auto staticDefaultGateway(std::span<const uint8_t> memoryView) -> uint32_t;
};

class ForceipAckHelper final : public HeaderAckHelper {
public:
    ForceipAckHelper(std::span<uint8_t> memoryView, uint16_t length);
};

} // namespace Jgv::Gvcp

#endif // FORCEIPHELPER_H
