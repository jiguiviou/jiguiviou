/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvcpclient.h"
#include "gvcpclient_p.h"

#include "bootstrapregisters.h"
#include "discoveryhelper.h"
#include "eventhelper.h"
#include "gvcp.h"
#include "headerhelper.h"
#include "readmemhelper.h"
#include "readreghelper.h"
#include "writememhelper.h"
#include "writereghelper.h"

#include <algorithm>
#include <arpa/inet.h>
#include <charconv>
#include <cstring>
#include <ctime>
#include <globalipc/globalipc.h>
#include <ifaddrs.h>
#include <iostream>
#include <linux/errqueue.h>
#include <linux/net_tstamp.h>
#include <net/if.h>
#include <netinet/in.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>

namespace {

constexpr int SIOCSHWTSTAMP { 0x89b0 };
constexpr int POLL_TIMEOUT { 1000 };
constexpr int PROCEED_RETRY { 3 };

constexpr std::string_view LOCAL_STRING { "Local:" };
constexpr auto UINT32_HEX_SIZE { 8 };
constexpr auto HEX_BASE { 16 };

auto is32BitsAligned(uint32_t value) -> bool
{
    constexpr uint32_t ALIGN_MASK { 0b00000011 };
    return (value & ALIGN_MASK) == 0;
}

template <typename T, typename U>
auto convert(U* p) -> T*
{
    void* tmp { p };
    return static_cast<T*>(tmp);
}

constexpr auto toTimestamp(uint32_t high, uint32_t low) -> uint64_t
{
    constexpr auto LSHIFT_32 { 32U };
    return (static_cast<uint64_t>(high) << LSHIFT_32) + low;
}

//auto spanToString(std::span<const uint8_t> view) -> std::string
//{
//    const auto first { std::find(view.cbegin(), view.cend(), 0) };
//    const auto distance { std::distance(view.cbegin(), first) };
//    std::string out;
//    std::copy_n(view.cbegin(), distance, std::back_inserter(out));
//    return out;
//};

auto splitUrl(std::string_view url) -> std::vector<std::string_view>
{
    std::vector<std::string_view> out {};
    std::string_view::size_type current { 0 };
    std::string_view::size_type previous { 0 };
    current = url.find_first_of(';');
    while (current != std::string::npos) {
        out.push_back(url.substr(previous, current - previous));
        previous = current + 1;
        current = url.find_first_of(';', previous);
    }
    out.push_back(url.substr(previous, current - previous));
    return out;
}

auto ipToString(uint32_t ip) -> std::string
{
    const uint32_t MASK { 0xFF };
    const uint32_t BYTE_SWITCH { 8 };
    const auto d { ip & MASK };
    ip >>= BYTE_SWITCH;
    const auto c { ip & MASK };
    ip >>= BYTE_SWITCH;
    const auto b { ip & MASK };
    ip >>= BYTE_SWITCH;
    const auto a { ip & MASK };
    const std::string IP_SEPARATOR { "." };
    return std::to_string(a) + IP_SEPARATOR + std::to_string(b) + IP_SEPARATOR + std::to_string(c) + IP_SEPARATOR + std::to_string(d);
}

auto cmsgFirstHdr(msghdr* msg) -> cmsghdr*
{
    if (msg->msg_controllen >= sizeof(cmsghdr)) {
        return static_cast<cmsghdr*>(msg->msg_control);
    }
    return nullptr;
};

struct Answer {
    ssize_t readedSize;
    scm_timestamping timestamp;
};

//auto nameFromIpV4(uint32_t ip) -> std::string
//{
//    std::string name { "empty" };
//    ifaddrs* addrs { nullptr };
//    if (getifaddrs(&addrs) == -1) {
//        std::perror("GvcpClient: getifaddrs");
//        return name;
//    }
//    for (ifaddrs* ifa = addrs; ifa != nullptr; ifa = ifa->ifa_next) {
//        if (ifa->ifa_addr == nullptr) {
//            continue;
//        }
//        if (ifa->ifa_addr->sa_family != AF_INET) {
//            continue;
//        }
//        if (convert<sockaddr_in>(ifa->ifa_addr)->sin_addr.s_addr == htonl(ip)) {
//            name = std::string { ifa->ifa_name };
//        }
//    }
//    freeifaddrs(addrs);
//    return name;
//}

//auto nicCanHardwareTimestamping(std::string_view nicName) -> bool
//{
//    auto sd { socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP) };
//    if (sd < 0) {
//        std::perror("Gvcp::Client: create socket for timestamping.");
//        return false;
//    }

//    hwtstamp_config hwconfig { 0, HWTSTAMP_TX_ON, HWTSTAMP_FILTER_ALL };
//    hwconfig.tx_type = HWTSTAMP_TX_ON;
//    hwconfig.rx_filter = HWTSTAMP_FILTER_ALL;

//    ifreq hwtstamp {};
//    std::span<char> sp { hwtstamp.ifr_name }; // NOLINT(cppcoreguidelines-pro-type-union-access)
//    std::copy(nicName.cbegin(), nicName.cend(), sp.begin());

//    hwtstamp.ifr_data = convert<char>(&hwconfig); // NOLINT(cppcoreguidelines-pro-type-union-access)

//    bool hwTsSupport { false };
//    if (ioctl(sd, SIOCSHWTSTAMP, &hwtstamp) < 0) { // NOLINT(cppcoreguidelines-pro-type-vararg)
//    } else {
//        hwTsSupport = true;
//    }

//    close(sd);
//    return hwTsSupport;
//}

auto timestampFromAncillaryData(msghdr* msg) -> scm_timestamping
{
    for (auto cmhdr { cmsgFirstHdr(msg) }; cmhdr != nullptr; cmhdr = CMSG_NXTHDR(msg, cmhdr)) {
        if (cmhdr->cmsg_level == SOL_SOCKET) {
            if (cmhdr->cmsg_type == SCM_TIMESTAMPING) {
                scm_timestamping out {};
                __builtin_memcpy(&out, &cmhdr->__cmsg_data, sizeof out);
                return out;
            }
        }
    }
    return {};
}

auto sendPacketWithFlags(int sd, std::span<uint8_t> memoryView, uint32_t destIP, int flags) -> ssize_t
{
    sockaddr_in dest = {};
    dest.sin_family = AF_INET;
    dest.sin_port = htons(Jgv::Gvcp::GVCP_PORT);
    dest.sin_addr.s_addr = htonl(destIP);

    iovec iov { memoryView.data(), static_cast<std::size_t>(memoryView.size_bytes()) };

    msghdr smsg {}; // all to zero
    smsg.msg_iov = &iov;
    smsg.msg_iovlen = 1;
    smsg.msg_name = &dest;
    smsg.msg_namelen = sizeof dest;

    std::array<char, CMSG_LEN(sizeof(__u32))> ancillaryBuf {};
    if (flags > 0) {

        smsg.msg_control = ancillaryBuf.data();
        smsg.msg_controllen = ancillaryBuf.size();
        auto cmsg { cmsgFirstHdr(&smsg) };
        if (cmsg != nullptr) {
            cmsg->cmsg_level = SOL_SOCKET;
            cmsg->cmsg_type = SO_TIMESTAMPING;
            cmsg->cmsg_len = ancillaryBuf.size();

            auto out { static_cast<__u32>(flags) };
            __builtin_memcpy(&cmsg->__cmsg_data, &out, sizeof out);
        }
    }

    return sendmsg(sd, &smsg, 0);
}

auto receiveFromErrorQueue(int sd) -> Answer
{
    std::array<uint8_t, CMSG_LEN(sizeof(scm_timestamping))> control {};
    msghdr msg { nullptr, 0, nullptr, 0, control.data(), control.size(), 0 };

    const auto recvlen { recvmsg(sd, &msg, MSG_ERRQUEUE | MSG_DONTWAIT) };
    if (recvlen == 0) {
        return { 0, timestampFromAncillaryData(&msg) };
    }
    if (recvlen < 0) {
        std::perror("Gvcp::Client::receiveFromErrorQueue() receive from MSG_ERRQUEUE");
    } else if (recvlen > 0) {
        std::cerr << "Gvcp::Client::receiveFromErrorQueue() receive from MSG_ERRQUEUE with payload ? " << recvlen << std::endl;
    }
    return {};
}

auto receivePayload(int sd, std::span<uint8_t> buffer) -> Answer
{
    sockaddr_in sin {};
    iovec iov { buffer.data(), static_cast<std::size_t>(buffer.size_bytes()) };
    std::array<uint8_t, CMSG_LEN(sizeof(scm_timestamping))> control {};
    msghdr msg { &sin, sizeof(sin), &iov, 1, control.data(), control.size(), 0 };

    // réceptionne le paquet
    // on passe le flag MSG_DONTWAIT car c'est epoll qui gère les timeouts
    const auto size { recvmsg(sd, &msg, MSG_DONTWAIT) };

    return { size, timestampFromAncillaryData(&msg) };
}
} // anonymous namespace

namespace Jgv::Gvcp {

ClientPrivate::ClientPrivate(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : globalIpc(std::move(_globalIpc))
    , channel(channel)
{
}

auto ClientPrivate::proceed(HeaderCmdHelper cmd, std::span<uint8_t> receiveBuffer, bool timing) -> GvcpResponse
{
    const std::lock_guard<std::mutex> lock(mutex);
    const uint16_t currentId = id;
    // pas d'id nulle
    if (++id == 0) {
        id = 1;
    }
    cmd.setReqId(currentId);

    auto timespecToInt64 = [](const timespec& ts) {
        return static_cast<int64_t>(ts.tv_sec) * INT64_C(1000000000) + static_cast<int64_t>(ts.tv_nsec);
    };

    int flags { 0 };
    if (timing) {
        if (hwTimestamp) {
            flags = SOF_TIMESTAMPING_TX_HARDWARE | SOF_TIMESTAMPING_TX_SOFTWARE;
        } else {
            flags = SOF_TIMESTAMPING_TX_SOFTWARE;
        }
    }

    // On entre dans la boucle retry.
    // Le processus complet est l'emmission de la commande et la réception
    // de son acquiescement.
    for (int i = 0; i < PROCEED_RETRY; ++i) {
        // emmission
        const auto sendLen { sendPacketWithFlags(cmdSocket, cmd, globalIpc->gvcpTransmitterIP(channel), flags) };
        if (sendLen < 0) {
            std::perror("Gvcp::Client::Proceed(): sendPacketWithFlags failed.");
            continue;
        }

        // peu probable
        if (sendLen != cmd.size_bytes()) {
            std::cerr << "Gvcp::Client::Proceed(): sendlen != command size" << std::endl;
            continue;
        }

        // si la commande n'a pas besoin d'acquiescement, on quitte
        if (!cmd.needAcknoledgement()) {
            //std::clog << "cmd sans ack" << std::endl;
            return { true, 0, 0 };
        }

        int64_t hwStart { 0 };
        int64_t hwStop { 0 };
        int64_t swStart { 0 };
        int64_t swStop { 0 };

        while (true) {
            pollfd pfd { cmdSocket, POLLIN, 0 };
            const auto pollResult = poll(&pfd, 1, POLL_TIMEOUT);

            if (pollResult < 0) {
                // peu probable
                std::perror("Gvcp::Client::Proceed(): poll()");
                // on sort du while
                break;
            }

            // timeout, on relance une procédure complète
            if (pollResult == 0) {
                const auto command { HeaderCmdHelper::command(cmd) };
                std::clog << "Gvcp::Client::Proceed(): poll() timeout. command : " << std::hex << command << std::endl;
                // on sort du while
                break;
            }

            if ((pfd.revents & POLLERR) > 0) {
                auto response { receiveFromErrorQueue(cmdSocket) };
                // on récupère les timestamps
                swStart += timespecToInt64(response.timestamp.ts[0]);
                hwStart += timespecToInt64(response.timestamp.ts[2]);
            }

            if ((pfd.revents & POLLIN) > 0) {
                const auto response { receivePayload(cmdSocket, receiveBuffer) };
                if (response.readedSize < static_cast<ssize_t>(sizeof(ACK_HEADER))) {
                    std::cerr << "Gvcp::Client::Proceed(): bad packet size: " << response.readedSize << std::endl;
                    // on sort du while;
                    break;
                }
                if (Gvcp::HeaderAckHelper::ackId(receiveBuffer) != currentId) {
                    std::cerr << "Gvcp::Client::Proceed(): bad ackID " << Gvcp::HeaderAckHelper::ackId(receiveBuffer) << " waiting for " << currentId << std::endl;
                    // on sort du while;
                    break;
                }
                // si pas de timing, on ne contrôle pas les timestamps
                if (!timing) {
                    return { true, 0, 0 };
                }
                swStop += timespecToInt64(response.timestamp.ts[0]);
                hwStop += timespecToInt64(response.timestamp.ts[2]);
            }

            if (hwTimestamp) {
                if (hwStop > 0 && hwStart > 0) {
                    return { true, swStart, swStart + (hwStop - hwStart) };
                }
            } else if (swStop > 0 && swStart > 0) {
                return { true, swStart, swStop };
            }
        }
    }

    return { false, 0, 0 };
}

void ClientPrivate::listenForEvents(uint16_t port)
{
    sockaddr_in sin {};
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = htonl(globalIpc->gvcpTransmitterIP(channel));
    sin.sin_port = htons(port);

    sockaddr from = {};
    socklen_t fromSize = sizeof(from);

    while (listenEventsLoop) {
        pollfd pfd { eventSocket, POLLIN, 0 };
        const auto pollResult = poll(&pfd, 1, 500);

        //std::cout << "ClientPrivate::listenForEvents poll " << pollResult << std::endl;

        if (pollResult < 0) {
            std::perror("ClientPrivate::listenForEvents(): poll()");
            break;
        }

        if ((pfd.revents & POLLIN) > 0) {
            EventBuffer buffer;
            std::array<uint8_t, sizeof(ACK_HEADER)> ackBuffer {};

            const auto size { recvfrom(eventSocket, buffer.data(), buffer.size(), 0, &from, &fromSize) };
            if (size < static_cast<ssize_t>(sizeof(CMD_HEADER))) {
                std::cout << "Gvcp::Client: bad event size: " << size << std::endl;
                continue;
            }

            if (EventCmdHelper::command(buffer) != GVCP_CMD_EVENT) {
                std::cout << "Gvcp::Client: not a Event Command: 0x" << std::hex << EventCmdHelper::command(buffer) << std::endl;
                continue;
            }

            if (EventCmdHelper::acknowledgeIsSet(buffer)) {
                EventAckHelper ack { ackBuffer, 0 };
                ack.setAckID(EventCmdHelper::reqId(buffer));

                auto sended { sendto(eventSocket, ackBuffer.data(), ackBuffer.size(), 0, &from, fromSize) };
                //std::cout << "ACK SENDED SIZE " << sended << std::endl;
            }

            const auto count { EventCmdHelper::eventsCount(buffer) };

            for (uint16_t index = 0; index < count; ++index) {
                auto event { EventHelper { EventCmdHelper::event(index, buffer) } };
                if (globalIpc) {
                    globalIpc->emitGvcpEvent(channel, event.eventIdentifier(), toTimestamp(event.timestampHigh(), event.timestampLow()));
                }
            }
        }
    }
}

Client::Client(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl(std::make_unique<ClientPrivate>(channel, globalIpc))
{
}

Client::~Client()
{
    if (_impl->listenEventThread && _impl->listenEventThread->joinable()) {
        _impl->listenEventsLoop = false;
        _impl->listenEventThread->join();
    }
    // ferme le socket
    if (close(_impl->cmdSocket) == 0) {
        std::clog << "Gvcp::~Client: socket closed for " << Utils::ipToString(_impl->globalIpc->gvcpControllerIP(_impl->channel)) << std::endl;
        _impl->cmdSocket = -1;
    } else {
        std::perror("GvcpClient failed to close socket");
    }
}

auto Client::controleDevice() -> bool
{
    const auto controllerIP { _impl->globalIpc->gvcpControllerIP(_impl->channel) };
    const auto deviceIP { _impl->globalIpc->gvcpTransmitterIP(_impl->channel) };
    _impl->globalIpc->setGvcpTransmitterPort(_impl->channel, GVCP_PORT);

    if (_impl->state != State::none) {
        std::clog << "Gvcp::Client::controleDevice(): allready in use " << ipToString(_impl->globalIpc->gvcpTransmitterIP(_impl->channel))
                  << " status " << toString(_impl->state) << std::endl;
        return false;
    }

    // descripteur du socket UDP
    _impl->cmdSocket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (_impl->cmdSocket < 0) {
        std::perror("Gvcp::Client::controleDevice(): failed at socket()");
        return false;
    }

    // bind sur port alléatoire
    sockaddr_in localAddress = {};
    localAddress.sin_family = AF_INET;
    localAddress.sin_port = htons(0); // port aléatoire
    localAddress.sin_addr.s_addr = htonl(controllerIP);
    if (bind(_impl->cmdSocket, convert<sockaddr>(&localAddress), sizeof(sockaddr_in)) == -1) {
        std::perror("Gvcp::Client::controleDevice(): failed at bind()");
        close(_impl->cmdSocket);
        _impl->cmdSocket = -1;
        return false;
    }

    // on récupère le numéro de port affecté
    sockaddr bindAddress {};
    socklen_t bindAddressSize = sizeof(sockaddr);
    getsockname(_impl->cmdSocket, &bindAddress, &bindAddressSize);
    _impl->globalIpc->setGvcpControllerPort(_impl->channel, ntohs(convert<sockaddr_in>(&bindAddress)->sin_port));

    int timestampingFlags { SOF_TIMESTAMPING_RX_SOFTWARE | SOF_TIMESTAMPING_SOFTWARE };
    if (setsockopt(_impl->cmdSocket, SOL_SOCKET, SO_TIMESTAMPING, &timestampingFlags, sizeof(timestampingFlags)) < 0) {
        std::perror("Gvcp::Client::controleDevice(): failed at setsockopt SO_TIMESTAMPING");
    }

    // control
    socklen_t len = sizeof(timestampingFlags);
    if (getsockopt(_impl->cmdSocket, SOL_SOCKET, SO_TIMESTAMPING, &timestampingFlags, &len) < 0) {
        std::perror("Gvcp::Client::controleDevice(): failed at getsockopt SO_TIMESTAMPING");
    }
    _impl->state = State::controller;

    // obtient CCP, si lecture impossible on a un soucis de communication
    auto regs { readRegisters({ enumType(Bootstrap::Address::ControlChannelPrivilege) }) };

    if (regs.empty()) {
        std::cerr << "Gvcp::Client::controleDevice(): failed: can't read CCP (exclusive access ?)." << std::endl;
        releaseDevice();
        return false;
    }

    const auto devicePriv { Bootstrap::CCP::privilegeFromCCP(regs.at(0)) };
    // s'assure qu'on a le droit de se connecter
    if (devicePriv != Bootstrap::CCPPrivilege::OpenAcces) {
        std::cerr << "Gvcp::Client::controleDevice(): failed to control device, not in OpenAcces" << std::endl;
        releaseDevice();
        return false;
    }

    // demande le privilège control
    if (!writeRegisters({ ADDRESS_VALUE { enumType(Bootstrap::Address::ControlChannelPrivilege), Bootstrap::CCP::CCPfromPrivilege(Bootstrap::CCPPrivilege::ControlAcces) } })) {
        std::cerr << "Gvcp::Client::controleDevice(): failed to control device, can write privilege." << std::endl;
        releaseDevice();
        return false;
    }

    const auto manufacturerName { readMemory(enumType(Bootstrap::Address::ManufacturerName), enumType(Bootstrap::BlockSize::ManufacturerName)) };
    const auto modelName { readMemory(enumType(Bootstrap::Address::ModelName), enumType(Bootstrap::BlockSize::ModelName)) };
    std::clog << "Gvcp::Client::controleDevice(): device " << ipToString(deviceIP) << " is " << spanToString(manufacturerName) << " " << spanToString(modelName) << std::endl;

    return true;
}

auto Client::monitorDevice() -> bool
{
    if (_impl->state != State::none) {
        std::clog << "Gvcp::Client::monitorDevice(): allready in use: " << ipToString(_impl->globalIpc->gvcpTransmitterIP(_impl->channel))
                  << " status " << toString(_impl->state) << std::endl;
        return true;
    }

    // descripteur du socket UDP
    _impl->cmdSocket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (_impl->cmdSocket < 0) {
        std::perror("Gvcp::Client::monitorDevice(): failed at socket().");
        return false;
    }
    // bind sur port alléatoire
    sockaddr_in localAddress = {};
    localAddress.sin_family = AF_INET;
    localAddress.sin_port = htons(0); // port aléatoire
    localAddress.sin_addr.s_addr = htonl(_impl->globalIpc->gvcpControllerIP(_impl->channel));
    if (bind(_impl->cmdSocket, convert<sockaddr>(&localAddress), sizeof(sockaddr_in)) == -1) {
        std::perror("Gvcp::Client::monitorDevice(): failed at bind()");
        if (close(_impl->cmdSocket) == 0) {
            _impl->cmdSocket = -1;
            _impl->state = State::none;
        } else {
            std::perror("Gvcp::Client::monitorDevice(): failed to close socket");
        }
        return false;
    }

    _impl->state = State::monitor;

    // on a un socket en écoute on essaie de lire CCP
    std::vector<uint32_t> regs = readRegisters({ enumType(Bootstrap::Address::ControlChannelPrivilege) });
    if (regs.empty()) {
        std::clog << "Gvcp::Client::monitorDevice(): can't read CCP (exclusive access ?)." << std::endl;
        releaseDevice();
        return false;
    }

    const auto manufacturerName { readMemory(enumType(Bootstrap::Address::ManufacturerName), enumType(Bootstrap::BlockSize::ManufacturerName)) };
    const auto modelName { readMemory(enumType(Bootstrap::Address::ModelName), enumType(Bootstrap::BlockSize::ModelName)) };
    std::clog << "Gvcp::Client::monitorDevice(): device " << ipToString(_impl->globalIpc->gvcpTransmitterIP(_impl->channel)) << " is " << spanToString(manufacturerName) << " " << spanToString(modelName) << std::endl;

    return true;
}

auto Client::listenEvents(uint16_t deviceMessageChannelPort) -> uint16_t
{
    if (_impl->state != State::controller) {
        std::cerr << "Gvcp::Client::listenEvents(): client not in controller mode." << std::endl;
        return 0;
    }

    // descripteur du socket UDP
    _impl->eventSocket = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (_impl->eventSocket < 0) {
        std::perror("Gvcp::Client::listenEvents(): failed to create socket.");
        return 0;
    }

    // bind
    sockaddr_in addr {};
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(_impl->globalIpc->gvcpControllerIP(_impl->channel));
    addr.sin_port = htons(0);
    auto p { bind(_impl->eventSocket, convert<sockaddr>(&addr), sizeof addr) };
    if (p < 0) {
        std::perror("Gvcp::Client::listenEvents(): failed to bind socket.");
        return 0;
    }

    // on récupère le numéro de port affecté
    sockaddr bindAddress {};
    socklen_t bindAddressSize = sizeof(sockaddr);
    getsockname(_impl->eventSocket, &bindAddress, &bindAddressSize);
    _impl->listenEventThread = std::make_unique<std::thread>(&ClientPrivate::listenForEvents, _impl.get(), deviceMessageChannelPort);
    pthread_setname_np(_impl->listenEventThread->native_handle(), "pollGvcpEvent");

    return ntohs(convert<sockaddr_in>(&bindAddress)->sin_port);
}

void Client::releaseDevice()
{
    if (_impl->state == State::none) {
        return;
    }

    // on relache les privilèges seulement si on est controlleur
    if (_impl->state == State::controller) {
        if (!writeRegisters({ ADDRESS_VALUE { enumType(Bootstrap::Address::ControlChannelPrivilege), 0x00000000 } })) {
            std::cerr << "GvcpClient fails to clear Control Channel Privilege" << std::endl;
        }
    }

    _impl->state = State::none;

    std::clog << "Gvcp::Client::releaseDevice(): device " << ipToString(_impl->globalIpc->gvcpTransmitterIP(_impl->channel)) << std::endl;
}

auto Client::readRegister(Bootstrap::Address address) -> uint32_t
{
    return readRegister(enumType(address));
}

uint32_t Client::readRegister(uint32_t address)
{
    // on crée un tampon pouvant contenir la commande et la réponse
    std::array<uint8_t, 2 * GVCP_PACKET_MAX_SIZE> buffer {};
    // forge la commande
    ReadregCmdHelper read(buffer, sizeof(uint32_t));
    read.setAddresses(std::vector<uint32_t> { address });

    auto receiveBuffer { std::span<uint8_t>(buffer).subspan(GVCP_PACKET_MAX_SIZE) };
    const auto response { _impl->proceed(read, receiveBuffer, false) };
    if (response.isValid && ReadregAckHelper::acknowledge(receiveBuffer) == GVCP_ACK_READREG) {
        const auto regs { ReadregAckHelper::registersData(receiveBuffer) };
        if (!regs.empty()) {
            return regs[0];
        }
    }

    std::cerr << "Gvcp::Client::readRegister(uint32_t): failed." << std::endl;

    return 0;
}

auto Client::readRegisters(const std::vector<uint32_t>& addresses) -> std::vector<uint32_t>
{
    if (_impl->state == State::none) {
        std::cerr << "Gvcp::Client::readRegisters(): no device configured." << std::endl;
        return {};
    }

    // on s'assure que les adresses sont allignées
    for (auto address : addresses) {
        if (!is32BitsAligned(address)) {
            std::cerr << "Gvcp::Client::readRegisters(): failed, unaligned adress." << std::endl;
            std::array<char, UINT32_HEX_SIZE> addr {};
            auto [pAddr, ecAddr] = std::to_chars(addr.begin(), addr.end(), address, HEX_BASE);
            if (ecAddr == std::errc()) {
                std::cerr << "Gvcp::Client::readRegisters(): unaligned : 0x" << std::string_view(addr.data(), pAddr - addr.data()) << std::endl;
            }
            return {};
        }
    }

    // on crée un tampon pouvant contenir la commande et la réponse
    std::array<uint8_t, 2 * GVCP_PACKET_MAX_SIZE> buffer {};
    // forge la commande
    ReadregCmdHelper read(buffer, addresses.size() * sizeof(uint32_t));
    read.setAddresses(addresses);

    auto receiveBuffer { std::span<uint8_t>(buffer).subspan(GVCP_PACKET_MAX_SIZE) };
    const auto response { _impl->proceed(read, receiveBuffer, false) };
    if (response.isValid && ReadregAckHelper::acknowledge(receiveBuffer) == GVCP_ACK_READREG) {
        return ReadregAckHelper::registersData(receiveBuffer);
    }

    std::cerr << "Gvcp::Client::readRegisters(): failed." << std::endl;

    return {};
}

auto Client::writeRegisters(const AddrValPairList& values) -> bool
{
    if (_impl->state != State::controller) {
        std::cerr << "GvcpClient failed to write register, we are not controller" << std::endl;
        return false;
    }

    auto outputValues = [&values]() {
        auto index { 0 };
        for (const auto addrVals : values) {
            std::array<char, UINT32_HEX_SIZE> id {};
            std::array<char, UINT32_HEX_SIZE> addr {};
            std::array<char, UINT32_HEX_SIZE> val {};
            auto [pId, ecID] = std::to_chars(id.begin(), id.end(), index, HEX_BASE);
            auto [pAddr, ecAddr] = std::to_chars(addr.begin(), addr.end(), addrVals.address, HEX_BASE);
            auto [pVal, ecVal] = std::to_chars(val.begin(), val.end(), addrVals.value, HEX_BASE);
            if (ecID == std::errc() && ecAddr == std::errc() && ecVal == std::errc()) {
                std::cerr << "Gvcp::Client::writeRegisters():"
                          << " index: 0x" << std::string_view(id.data(), pId - id.data())
                          << " adresse: 0x" << std::string_view(addr.data(), pAddr - addr.data())
                          << " value: 0x" << std::string_view(val.data(), pVal - val.data()) << std::endl;
            }
            ++index;
        }
    };

    // on crée un tampon pouvant contenir la commande et la réponse
    std::array<uint8_t, 2 * GVCP_PACKET_MAX_SIZE> buffer {};
    // forge la commande
    WriteregCmdHelper write(buffer, static_cast<uint16_t>(values.size() * 2 * sizeof(uint32_t)));
    write.setRegsValList(values);

    auto receiveBuffer { std::span<uint8_t>(buffer).subspan(GVCP_PACKET_MAX_SIZE) };

    const auto response { _impl->proceed(write, receiveBuffer, false) };
    if (!response.isValid) {
        std::cerr << "Gvcp::Client::writeRegisters(): failed, bad response." << std::endl;
        outputValues();
        return false;
    }

    if (WriteregAckHelper::acknowledge(receiveBuffer) != GVCP_ACK_WRITEREG) {
        std::cerr << "Gvcp::Client::writeRegisters(): failed, bad ack " << std::hex << WriteregAckHelper::acknowledge(receiveBuffer) << " must be " << std::hex << GVCP_ACK_WRITEREG << std::endl;
        outputValues();
        return false;
    }

    if (WriteregAckHelper::status(receiveBuffer) != enumType(Status::SUCCESS)) {
        std::cerr << "Gvcp::Client::writeRegisters(): failed with status " << statusToString(WriteregAckHelper::status(receiveBuffer)) << std::endl;
        outputValues();
        std::array<char, UINT32_HEX_SIZE> id {};
        auto [pId, ecID] = std::to_chars(id.begin(), id.end(), WriteregAckHelper::index(receiveBuffer), HEX_BASE);
        if (ecID == std::errc()) {
            std::cerr << "Gvcp::Client::writeRegisters(): failed index : 0x" << std::string_view(id.data(), pId - id.data()) << std::endl;
        }
        return false;
    }

    return true;
}

void Client::writeRegister(uint32_t address, uint32_t value)
{
    if (_impl->state != State::controller) {
        std::cerr << "GvcpClient failed to write register, we are not controller" << std::endl;
        return;
    }

    // on crée un tampon pouvant contenir la commande et la réponse
    std::array<uint8_t, 2 * GVCP_PACKET_MAX_SIZE> buffer {};
    // forge la commande
    WriteregCmdHelper write(buffer, static_cast<uint16_t>(2 * sizeof(uint32_t)));
    write.setReg(address, value);

    auto receiveBuffer { std::span<uint8_t>(buffer).subspan(GVCP_PACKET_MAX_SIZE) };

    const auto response { _impl->proceed(write, receiveBuffer, false) };
    if (!response.isValid) {
        std::cerr << "Gvcp::Client::writeRegister(): failed, bad response." << std::endl;
        return;
    }

    if (WriteregAckHelper::acknowledge(receiveBuffer) != GVCP_ACK_WRITEREG) {
        std::cerr << "Gvcp::Client::writeRegister(): failed, bad ack " << std::hex << WriteregAckHelper::acknowledge(receiveBuffer) << " must be " << std::hex << GVCP_ACK_WRITEREG << std::endl;
        return;
    }

    if (WriteregAckHelper::status(receiveBuffer) != enumType(Status::SUCCESS)) {
        std::cerr << "Gvcp::Client::writeRegister(uint32_t): failed with status " << statusToString(WriteregAckHelper::status(receiveBuffer))
                  << "\n       address: 0x" << std::hex << address << " value: " << value << std::endl;
    }
}

void Client::writeRegister(uint32_t address, int32_t value)
{
    if (_impl->state != State::controller) {
        std::cerr << "GvcpClient failed to write register, we are not controller" << std::endl;
        return;
    }

    // on crée un tampon pouvant contenir la commande et la réponse
    std::array<uint8_t, 2 * GVCP_PACKET_MAX_SIZE> buffer {};
    // forge la commande
    WriteregCmdHelper write(buffer, static_cast<uint16_t>(2 * sizeof(int32_t)));
    write.setReg(address, value);

    auto receiveBuffer { std::span<uint8_t>(buffer).subspan(GVCP_PACKET_MAX_SIZE) };

    const auto response { _impl->proceed(write, receiveBuffer, false) };
    if (!response.isValid) {
        std::cerr << "Gvcp::Client::writeRegister(): failed, bad response." << std::endl;
        return;
    }

    if (WriteregAckHelper::acknowledge(receiveBuffer) != GVCP_ACK_WRITEREG) {
        std::cerr << "Gvcp::Client::writeRegister(): failed, bad ack " << std::hex << WriteregAckHelper::acknowledge(receiveBuffer) << " must be " << std::hex << GVCP_ACK_WRITEREG << std::endl;
        return;
    }

    if (WriteregAckHelper::status(receiveBuffer) != enumType(Status::SUCCESS)) {
        std::cerr << "Gvcp::Client::writeRegister(int32_t): failed with status " << statusToString(WriteregAckHelper::status(receiveBuffer)) << std::endl;
    }
}

auto Client::readMemory(uint32_t address, uint16_t count) -> std::vector<uint8_t>
{
    if (_impl->state == State::none) {
        std::cerr << "Gvcp::Client::readMemory(): no device configured." << std::endl;
        return {};
    }

    // la taille à lire doit être allignée, si ce n'est pas le cas on rejette
    if (!is32BitsAligned(count)) {
        std::cerr << "Gvcp::Client::readMemory(): can't read memory, size not 32 bit aligned: " << count << std::endl;
        return {};
    }

    // on crée un tampon pouvant contenir la commande et la réponse
    std::array<uint8_t, 2 * GVCP_PACKET_MAX_SIZE> buffer {};
    // forge la commande
    ReadmemCmdHelper readmem(buffer, READMEM_CMD_LENGTH);
    readmem.setAddress(address);
    readmem.setDataCount(count);

    auto receiveBuffer { std::span<uint8_t>(buffer).subspan(GVCP_PACKET_MAX_SIZE) };

    const auto response { _impl->proceed(readmem, receiveBuffer, false) };
    if (!response.isValid) {
        std::cerr << "Gvcp::Client::readMemory(): failed, bad response." << std::endl;
        return {};
    }

    if (ReadmemAckHelper::acknowledge(receiveBuffer) != GVCP_ACK_READMEM) {
        std::cerr << "Gvcp::Client::readMemory(): failed, bad ack " << std::hex << ReadmemAckHelper::acknowledge(receiveBuffer) << " must be " << std::hex << GVCP_ACK_READMEM << std::endl;
        return {};
    }

    if (ReadmemAckHelper::status(receiveBuffer) != enumType(Status::SUCCESS)) {
        std::cerr << "Gvcp::Client::readMemory(): failed with status " << statusToString(WriteregAckHelper::status(receiveBuffer))
                  << " address " << std::hex << address << std::dec << std::endl;

        return {};
    }

    // on contrôle l'adresse quoique normalement pas necessaire
    if (ReadmemAckHelper::address(receiveBuffer) != address) {
        std::cerr << "Gvcp::Client::readMemory(): failed, ack address diffrent from command." << std::endl;

        std::array<char, UINT32_HEX_SIZE> addr {};
        std::array<char, UINT32_HEX_SIZE> ackAddr {};
        auto [pAddr, ecAddr] = std::to_chars(addr.begin(), addr.end(), address, HEX_BASE);
        auto [pAckAddr, ecAckAdddr] = std::to_chars(ackAddr.begin(), ackAddr.end(), ReadmemAckHelper::address(receiveBuffer), HEX_BASE);
        if (ecAddr == std::errc() && ecAckAdddr == std::errc()) {
            std::cerr << "Gvcp::Client::readMemory():"
                      << " adresse: 0x" << std::string_view(addr.data(), pAddr - addr.data())
                      << " ack adresse: 0x" << std::string_view(ackAddr.data(), pAckAddr - ackAddr.data()) << std::endl;
        }
    }

    const auto memorySpan { ReadmemAckHelper::datas(receiveBuffer) };
    return { memorySpan.begin(), memorySpan.end() };
}

auto Client::writeMemory(uint32_t address, std::span<uint8_t> memoryView) -> bool
{
    if (_impl->state != State::controller) {
        std::cerr << "Gvcp::Client::writeMemory(): failed, we are not controller." << std::endl;
        return false;
    }

    // la taille des données doit être alignée
    if (!is32BitsAligned(memoryView.size_bytes())) {
        std::cerr << "Gvcp::Client::writeMemory(): failed, datas size is not 32 bit aligned: 0x" << memoryView.size_bytes() << std::endl;
        return false;
    }

    // l'adresse doit être alignée
    if (!is32BitsAligned(address)) {
        std::cerr << "Gvcp::Client::writeMemory(): failed, address is not 32 bit aligned: 0x" << std::hex << address << std::endl;
        return false;
    }

    // on crée un tampon pouvant contenir la commande et la réponse
    std::array<uint8_t, 2 * GVCP_PACKET_MAX_SIZE> buffer {};
    // forge la commande
    WritememCmdHelper writemem(buffer, memoryView.size_bytes() + sizeof WRITEMEM_CMD::address);
    writemem.setAddress(address);
    writemem.setDatas(memoryView);

    auto receiveBuffer { std::span<uint8_t>(buffer).subspan(GVCP_PACKET_MAX_SIZE) };

    const auto response { _impl->proceed(writemem, receiveBuffer, false) };
    if (!response.isValid) {
        std::cerr << "Gvcp::Client::writeMemory(): failed, bad response." << std::endl;
        return false;
    }

    if (WritememAckHelper::acknowledge(receiveBuffer) != GVCP_ACK_WRITEMEM) {
        std::cerr << "Gvcp::Client::writeMemory(): failed, bad ack " << std::hex << WritememAckHelper::acknowledge(receiveBuffer) << " must be " << std::hex << GVCP_ACK_WRITEMEM << std::endl;
        return false;
    }

    if (WritememAckHelper::status(receiveBuffer) != enumType(Status::SUCCESS)) {
        std::cerr << "Gvcp::Client::writeMemory(): to 0x" << std::hex << address << " failed with status " << statusToString(WriteregAckHelper::status(receiveBuffer)) << std::endl;
        // index nous montre où se trouve l'erreur
        std::cerr << "Gvcp::Client::writeMemory(): failed index: " << WritememAckHelper::index(receiveBuffer) << std::endl;
        return false;
    }

    // un écriture complète
    if (WritememAckHelper::index(receiveBuffer) != memoryView.size_bytes()) {
        std::cerr << "Gvcp::Client::writeMemory(): failed, sizes differe, transmit: " << memoryView.size_bytes()
                  << " really writted " << WritememAckHelper::index(receiveBuffer) << std::endl;
    }

    return true;
}

auto Client::xmlFile() -> std::string
{

    // on récupère la l'URL
    const auto urlMemory { readMemory(enumType(Bootstrap::Address::FirstURL), enumType(Bootstrap::BlockSize::FirstURL)) };

    // l'url est splittée en 3 parties
    const auto url { spanToString(urlMemory) };
    auto splittedUrl { splitUrl(url) };
    if (splittedUrl.size() != 3) {
        std::cerr << "Gvcp::Client::xmlFile(): failed to split FirstUrl: " << url << std::endl;
        return {};
    }

    uint32_t address { 0 };
    {
        const auto str { splittedUrl[1] };
        auto [p, ec] { std::from_chars(str.data(), str.data() + str.size(), address, HEX_BASE) };
        if (ec != std::errc()) {
            std::cerr << "Gvcp::Client::xmlFile(): failed to read address: " << std::string { str } << " " << std::hex << address << std::endl;
            return {};
        }
    }

    uint32_t size { 0 };
    {
        const auto str { splittedUrl[2] };
        auto [p, ec] { std::from_chars(str.data(), str.data() + str.size(), size, HEX_BASE) };
        if (ec != std::errc()) {
            std::cerr << "Gvcp::Client::xmlFile(): failed read memory size: " << std::string { str } << std::endl;
            return {};
        }
    }

    // on utilise un tampon unique pour toute la transaction
    std::array<uint8_t, 2 * GVCP_PACKET_MAX_SIZE> buffer {};
    // forge la commande
    ReadmemCmdHelper readmem(buffer, READMEM_CMD_LENGTH);
    auto receiveBuffer { std::span<uint8_t>(buffer).subspan(GVCP_PACKET_MAX_SIZE) };
    auto dataLeft { static_cast<int>(size) };
    auto currentAddress { address };
    std::string out;
    out.reserve(size);
    while (dataLeft > 0) {
        const uint16_t sizeToRead = dataLeft > READMEM_ACK_PAYLOAD_MAX_SIZE ? READMEM_ACK_PAYLOAD_MAX_SIZE : Bootstrap::Registers::align(dataLeft);
        readmem.setAddress(currentAddress);
        readmem.setDataCount(sizeToRead);

        const auto response { _impl->proceed(readmem, receiveBuffer, false) };
        if (response.isValid && ReadmemAckHelper::acknowledge(receiveBuffer) == GVCP_ACK_READMEM) {
            // on s'assure que la payload est présent
            if (ReadmemAckHelper::length(receiveBuffer) == 0) {
                std::cerr << "Gvcp::Client::xmlFile(): failed: null payload." << std::endl;
                return {};
            }

            // que l'address lue est bien celle demandée
            if (ReadmemAckHelper::address(receiveBuffer) != currentAddress) {
                std::cerr << "Gvcp::Client::xmlFile(): failed: address." << std::endl;
                return {};
            }

            // on copie dans le tampon
            out.append(spanToString(ReadmemAckHelper::datas(receiveBuffer)));
        } else {
            std::cerr << "Gvcp::Client::xmlFile(): failed: invalid response." << std::endl;
            return {};
        }
        currentAddress += sizeToRead;
        dataLeft -= sizeToRead;
    }

    return out;
}

auto Client::xmlFilename() -> std::string
{
    // on récupère la l'URL
    const auto urlMemory { readMemory(enumType(Bootstrap::Address::FirstURL), enumType(Bootstrap::BlockSize::FirstURL)) };

    // l'url est splittée en 3 parties
    const auto url { spanToString(urlMemory) };
    auto splittedUrl { splitUrl(url) };
    if (splittedUrl.size() != 3) {
        std::cerr << "Gvcp::Client::xmlFilename(): failed to split FirstUrl: " << url << std::endl;
        return {};
    }

    // extrait le début, et le compare
    const auto start { splittedUrl[0].substr(0, LOCAL_STRING.size()) };
    if (start != LOCAL_STRING) {
        std::cerr << "Gvcp::Client::xmlFilename(): start string error: " << start << std::endl;
        return std::string();
    }

    return std::string { splittedUrl[0].substr(LOCAL_STRING.size()) };
}

auto Client::getDatation() -> std::tuple<int64_t, int64_t, int64_t>
{
    constexpr int REGISTER_SIZE { 32 };
    // on crée les tampons pouvant contenir la commande et la réponse
    std::array<uint8_t, 2 * GVCP_PACKET_MAX_SIZE> latchBuffer {};
    std::array<uint8_t, 2 * GVCP_PACKET_MAX_SIZE> readTsBuffer {};
    auto latchReceiveBuffer { std::span<uint8_t>(latchBuffer).subspan(GVCP_PACKET_MAX_SIZE) };
    auto readTsReceiveBuffer { std::span<uint8_t>(readTsBuffer).subspan(GVCP_PACKET_MAX_SIZE) };

    // demande le latch du timestamp courant
    WriteregCmdHelper latch(latchBuffer, sizeof(ADDRESS_VALUE));
    latch.setRegsValList({ ADDRESS_VALUE { enumType(Bootstrap::Address::TimestampControl), 0x00000002 } });
    // lecture des 2 registres timestamp
    ReadregCmdHelper readTS(readTsBuffer, 2 * sizeof(uint32_t));
    const std::array<uint32_t, 2> addresses { enumType(Bootstrap::Address::TimestampValueHigh), enumType(Bootstrap::Address::TimestampValueLow) };
    readTS.setAddresses(addresses);

    // latch le compteur
    const auto latchResponse { _impl->proceed(latch, latchReceiveBuffer, true) };

    int64_t timestamp { 0 };
    if (latchResponse.isValid) {
        // on lance la lecture
        const auto readResponse { _impl->proceed(readTS, readTsReceiveBuffer, false) };
        if (readResponse.isValid) {
            const auto result { ReadregAckHelper::registersData(readTsReceiveBuffer) };
            timestamp = result[0];
            timestamp <<= REGISTER_SIZE;
            timestamp += result[1];
        }
    }

    return { timestamp, latchResponse.beginTime, latchResponse.endTime };
}

} // namespace Jgv::Gvcp
