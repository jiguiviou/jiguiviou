/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef HEADERHELPER_H
#define HEADERHELPER_H

#include "packethelper.h"

#include <span>

namespace Jgv::Gvcp {

constexpr uint8_t GVCP_HARD_KEY = 0x42;

struct CMD_HEADER {
    uint8_t hardKey;
    uint8_t flag;
    uint16_t command;
    uint16_t length;
    uint16_t reqId;
};

struct ACK_HEADER {
    uint16_t status;
    uint16_t acknowledge;
    uint16_t length;
    uint16_t ackId;
};

template <typename T>
constexpr auto fromUnaligned(std::span<const uint8_t> src) -> T
{
    T dest;
    __builtin_memcpy(&dest, src.data(), sizeof(T));
    return dest;
}

template <typename T>
auto toUnaligned(T value, std::span<uint8_t> dest) -> void
{
    __builtin_memcpy(dest.data(), &value, sizeof(T));
}

using SpanIndex = std::size_t; //std::span<uint8_t>::index_type;

template <typename T>
constexpr auto spanIndex_cast(T value) { return static_cast<SpanIndex>(value); }

class HeaderCmdHelper : public std::span<uint8_t> {

public:
    HeaderCmdHelper(std::span<uint8_t> buffer, uint16_t command, uint16_t payloadSize);

    void disableAcknowledge() noexcept;
    void setReqId(uint16_t id) noexcept;
    [[nodiscard]] auto needAcknoledgement() const noexcept -> bool;

    static auto isValid(std::span<uint8_t> memoryView) noexcept -> bool;
    static auto acknowledgeIsSet(std::span<const uint8_t> memoryView) noexcept -> bool;
    static auto command(std::span<const uint8_t> memoryView) noexcept -> uint16_t;
    static auto length(std::span<const uint8_t> memoryView) noexcept -> uint16_t;
    static auto reqId(std::span<const uint8_t> memoryView) noexcept -> uint16_t;
};

class HeaderAckHelper : public std::span<uint8_t> {
public:
    HeaderAckHelper(std::span<uint8_t> buffer, uint16_t acknowledge, uint16_t length);

    void setStatus(uint16_t newStatus) noexcept;
    void setAckID(uint16_t id) noexcept;

    static auto status(std::span<const uint8_t> memoryView) noexcept -> uint16_t;
    static auto acknowledge(std::span<const uint8_t> memoryView) noexcept -> uint16_t;
    static auto length(std::span<const uint8_t> memoryView) noexcept -> uint16_t;
    static auto ackId(std::span<const uint8_t> memoryView) noexcept -> uint16_t;
};

} // namespace Jgv::Gvcp

#endif // HEADERHELPER_H
