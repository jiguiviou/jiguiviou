/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef BOOTSTRAPREGISTERS_H
#define BOOTSTRAPREGISTERS_H

#include <bitset>
#include <cstdint>
#include <string>

namespace Jgv::Gvcp::Bootstrap {

enum class Address : uint32_t {
    Version = 0x0000,
    DeviceMode = 0x0004,
    DeviceMacAddressHigh_0 = 0x0008,
    DeviceMacAddressLow_0 = 0x000C,
    SupportedIPconfiguration_0 = 0x0010,
    CurrentIPconfiguration_0 = 0x0014,
    CurrentIPAddress_0 = 0x0024,
    CurrentSubnetMask_0 = 0x0034,
    CurrentDefaultGateway_0 = 0x0044,
    ManufacturerName = 0x0048,
    ModelName = 0x0068,
    DeviceVersion = 0x0088,
    ManufacturerInfo = 0x00A8,
    SerialNumber = 0x00D8,
    UserDefinedName = 0x00E8,
    FirstURL = 0x0200,
    SecondURL = 0x0400,
    NumberOfNetworkInterfaces = 0x0600,
    PersistentIPAddress_0 = 0x064C,
    PersistentSubnetMask_0 = 0x065C,
    PersistentDefaultGateway_0 = 0x066C,
    LinkSpeed_0 = 0x0670,
    DeviceMacAddressHigh_1 = 0x0680,
    DeviceMacAddressLow_1 = 0x0684,
    NetworkInterfaceCapability_1 = 0x0688,
    NetworkInterfaceConfiguration_1 = 0x068C,
    CurrentIPAddress_1 = 0x069C,
    CurrentSubnetMask_1 = 0x06AC,
    CurrentDefaultGateway_1 = 0x06BC,
    PersistentIPAddress_1 = 0x06CC,
    PersistentSubnetMask_1 = 0x06DC,
    PersistentDefaultGateway_1 = 0x06EC,
    LinkSpeed_1 = 0x06F0,
    DeviceMacAddressHigh_2 = 0x0700,
    DeviceMacAddressLow_2 = 0x0704,
    NetworkInterfaceCapability_2 = 0x0708,
    NetworkInterfaceConfiguration_2 = 0x070C,
    CurrentIPAddress_2 = 0x071C,
    CurrentSubnetMask_2 = 0x072C,
    CurrentDefaultGateway_2 = 0x073C,
    PersistentIPAddress_2 = 0x074C,
    PersistentSubnetMask_2 = 0x075C,
    PersistentDefaultGateway_2 = 0x076C,
    LinkSpeed_2 = 0x0770,
    DeviceMacAddressHigh_3 = 0x0780,
    DeviceMacAddressLow_3 = 0x0784,
    NetworkInterfaceCapability_3 = 0x0788,
    NetworkInterfaceConfiguration_3 = 0x078C,
    CurrentIPAddress_3 = 0x079C,
    CurrentSubnetMask_3 = 0x07AC,
    CurrentDefaultGateway_3 = 0x07BC,
    PersistentIPAddress_3 = 0x07CC,
    PersistentSubnetMask_3 = 0x07DC,
    PersistentDefaultGateway_3 = 0x07EC,
    LinkSpeed_3 = 0x07F0,
    NumberOfMessageChannels = 0x0900,
    NumberOfStreamChannel = 0x0904,
    NumberOfActionSignals = 0x0908,
    ActionDeviceKey = 0x090C,
    NumberOfActiveLinks = 0x0910,
    GVSPCapability = 0x092C,
    MessageChannelCapability = 0x0930,
    GVCPCapability = 0x0934,
    HeartbeatTimeout = 0x0938,
    TimestampTickFrequencyHigh = 0x093C,
    TimestampTickFrequencyLow = 0x0940,
    TimestampControl = 0x0944,
    TimestampValueHigh = 0x0948,
    TimestampValueLow = 0x094C,
    DiscoveryACKDelay = 0x0950,
    GVCPConfiguration = 0x0954,
    PendingTimeout = 0x0958,
    ControlSwitchoverKey = 0x095C,
    GVSPConfiguration = 0x0960,
    PhysicalLinkConfigurationCapability = 0x0964,
    PhysicalLinkConfiguration = 0x0968,
    IEEE1588Status = 0x096C,
    SheduledActionCommandQueueSize = 0x0970,
    ControlChannelPrivilege = 0x0A00,
    PrimaryApplicationPort = 0x0A04,
    PrimaryApplicationIPAddress = 0x0A14,
    MessageChannelPort = 0x0B00,
    MessageChannelDestinationAddress = 0x0B10,
    MessageChannelTransmissionTimeout = 0x0B14,
    MessageChannelRetryCount = 0x0B18,
    MessageChannelSourcePort = 0x0B1C,

    // chaque canal stream possède ce schéma et prend 64 octets (0x40)
    StreamChannelPort = 0x0D00,
    StreamChannelPacketSize = 0x0D04,
    StreamChannelPacketDelay = 0x0D08,
    StreamChannelDestinationAddress = 0x0D18,
    StreamChannelSourcePort = 0x0D1C,
    StreamChannelCapability = 0x0D20,
    StreamChannelConfiguration = 0x0D24,
    StreamChannelZone = 0x0D28,
    StreamChannelZoneDirection = 0x0D2C,
    ManifestTable = 0x9000,

    // chaque action group possède ce schéma et prend 16 octets (0x10)
    ActionGroupKey = 0x9800,
    ActionGroupMask = 0x9814,

    SpecificManufacturerRegister = 0xA000
};

enum class BlockSize : uint16_t {
    ManufacturerName = 32,
    ModelName = 32,
    DeviceVersion = 32,
    ManufacturerInfo = 48,
    SerialNumber = 16,
    UserDefinedName = 16,
    FirstURL = 512,
    SecondURL = 512,
    ManifestTable = 512
};

enum class DeviceClass : uint8_t {
    TRANSMITTER = 0,
    RECEIVER = 1,
    TRANSCEIVER = 2,
    PERIPHERAL = 3
};

enum class Endianness {
    LITTLE = 0,
    BIG = 1
};

enum class CharacterSetIndex : uint8_t {
    UTF8 = 1
};

enum class GVCPCapabilities : unsigned {
    UN = 0, // user-defined name
    SN = 1, // serial number support
    HD = 2, // heartbeat can be disable
    LS = 3, // link speed register support
    CAP = 4, // CCP application and IP register support
    MT = 5, // manifest table support
    TD = 6, // test data is filled with data from LSFR
    DD = 7, // discovery ack delay support
    WD = 8, // discovery ack delay writable
    ES = 9, // extented status code support
    PAS = 10, // primary swithover support
    A = 25, // action are supported
    P = 26, // pending ack support
    ED = 27, // event data support
    E = 28, // event support
    PR = 29, // packetresend support
    W = 30, // writemem support
    C = 31 // multipple operation in a single message support
};

enum class CCPPrivilege {
    OpenAcces,
    ControlAcces,
    ControlAccesWithSwitchoverEnabled,
    ExclusiveAcces,
    AccesDenied
};

struct CCP {
    static auto privilegeFromCCP(uint32_t CCP) -> CCPPrivilege;
    static auto CCPfromPrivilege(CCPPrivilege privilege) -> uint32_t;
    static auto privilegeToString(CCPPrivilege priv) -> std::string;
};

namespace SCPSx {
    constexpr auto packetSize(uint32_t scps) -> uint16_t { return 0x0000FFFF & scps; }
}

namespace SCPx {
    enum class Direction {
        Transmitter,
        Receiver
    };
    constexpr auto direction(uint32_t scpx) -> Direction { return (scpx >= 0x80000000) ? Direction::Receiver : Direction::Transmitter; }
    constexpr auto networkInterfaceIndex(uint32_t scp) -> uint { return 0x00000003 & (scp >> 16); }
    constexpr auto hostPort(uint32_t scpx) -> uint16_t { return 0x0000FFFF & scpx; }
}; // namespace SCPx

namespace SCSPx {
    constexpr auto sourcePort(uint32_t scpx) -> uint16_t { return 0x0000FFFF & scpx; }
}; // namespace SCPx

constexpr uint32_t BE_BIT_ZERO { 0x80000000U };
constexpr uint32_t LOW_PART { 0x0000FFFFU };
constexpr uint32_t HIGH_PART { 0xFFFF0000U };
constexpr auto SHIFT_16 { 16 };
class Register {
    uint32_t _value;

public:
    constexpr explicit Register(uint32_t value)
        : _value(value) {};
    void setValue(uint32_t value) noexcept
    {
        _value = value;
    }
    template <typename T>
    [[nodiscard]] auto test(T bit) const noexcept -> bool
    {
        return (_value & (BE_BIT_ZERO >> enumType_cast(bit))) > 0;
    }
    template <typename T>
    auto set(T bit) const noexcept -> uint32_t
    {
        return _value | (BE_BIT_ZERO >> enumType_cast(bit));
    }
    template <typename T>
    auto reset(T bit) const noexcept -> uint32_t
    {
        return _value & ((~BE_BIT_ZERO) >> enumType_cast(bit));
    }
    [[nodiscard]] auto lowPart() const noexcept -> uint16_t
    {
        return _value & LOW_PART;
    }
    [[nodiscard]] auto highPart() const noexcept -> uint16_t
    {
        return (_value & HIGH_PART) >> SHIFT_16;
    }
};

//class BootstrapRegistersPrivate;
class Registers {

public:
    static auto streamChannelTranspose(uint channel, Bootstrap::Address address) noexcept -> uint32_t;

    static auto align(int val) -> int;
};

} // namespace Jgv::Gvcp::Bootstrap

#endif // BOOTSTRAPREGISTERS_H
