﻿/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspreceiver.h"
#include "gvspreceiver_p.h"

#include "gvspfakememoryallocator.h"
#include "gvsppacket.h"

#include <cap-ng.h>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <ifaddrs.h>
#include <iostream>
#include <linux/filter.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <net/if.h>
#include <netinet/in.h>
#include <poll.h>
#include <sys/mman.h>
#include <unistd.h>

namespace {

const uint GVSP_BUFFER_SIZE { 10000 };
constexpr int POLL_TIMEOUT { 250 };

struct BlockDesc {
    uint32_t version;
    uint32_t userOffset;
    tpacket_hdr_v1 hdr;
};

struct MapSpan {
    std::span<uint8_t> span;
    MapSpan* nextSpan;
};

template <typename T>
constexpr auto fromUnaligned(std::span<const uint8_t> src) -> T
{
    T dest;
    __builtin_memcpy(&dest, src.data(), sizeof(T));
    return dest;
}

constexpr auto blockStatus(std::span<uint8_t> span) -> std::uint_fast32_t
{
    return fromUnaligned<__u32>(span.subspan(offsetof(BlockDesc, hdr) + offsetof(tpacket_hdr_v1, block_status)));
}

constexpr void resetBlockStatus(std::span<uint8_t> span)
{
    auto blockStatus { span.subspan(offsetof(BlockDesc, hdr) + offsetof(tpacket_hdr_v1, block_status)) };
    blockStatus[0] = blockStatus[1] = blockStatus[2] = blockStatus[3] = 0; // TP_STATUS_KERNEL
}

constexpr auto numOfPkts(std::span<const uint8_t> span) -> std::uint_fast32_t
{
    return fromUnaligned<__u32>(span.subspan(offsetof(BlockDesc, hdr) + offsetof(tpacket_hdr_v1, num_pkts)));
}

constexpr auto offsetToFirstPkt(std::span<const uint8_t> span) -> std::intptr_t
{
    return fromUnaligned<__u32>(span.subspan(offsetof(BlockDesc, hdr) + offsetof(tpacket_hdr_v1, offset_to_first_pkt)));
}

constexpr auto offsetToNextPkt(std::span<const uint8_t> span) -> std::intptr_t
{
    return fromUnaligned<__u32>(span.subspan(offsetof(tpacket3_hdr, tp_next_offset)));
}

constexpr auto offsetToNet(std::span<const uint8_t> span) -> std::intptr_t
{
    return fromUnaligned<__u16>(span.subspan(offsetof(tpacket3_hdr, tp_net)));
}

constexpr auto ipTotLen(std::span<const uint8_t> span) -> uint16_t
{
    return fromUnaligned<__be16>(span.subspan(offsetof(iphdr, tot_len)));
}

enum class PacketFormat : uint_fast8_t {
    DATA_LEADER = 1,
    DATA_TRAILER = 2,
    DATA_PAYLOAD = 3,
    ALL_IN = 4
};

void setFilter(int sd, uint32_t srcIP, uint32_t srcPort, uint32_t dstIP, uint32_t dstPort)
{
    // tcpdump udp and src host <srcIP> and src port <srcPort> dst host <dstIP> and dst port <dstPort> -dd
    sock_filter bpf_filter[] = {
        { 0x28, 0, 0, 0x0000000c },
        { 0x15, 15, 0, 0x000086dd },
        { 0x15, 0, 14, 0x00000800 },
        { 0x30, 0, 0, 0x00000017 },
        { 0x15, 0, 12, 0x00000011 },
        { 0x20, 0, 0, 0x0000001a },
        { 0x15, 0, 10, srcIP },
        { 0x28, 0, 0, 0x00000014 },
        { 0x45, 8, 0, 0x00001fff },
        { 0xb1, 0, 0, 0x0000000e },
        { 0x48, 0, 0, 0x0000000e },
        { 0x15, 0, 5, srcPort },
        { 0x20, 0, 0, 0x0000001e },
        { 0x15, 0, 3, dstIP },
        { 0x48, 0, 0, 0x00000010 },
        { 0x15, 0, 1, dstPort },
        { 0x6, 0, 0, 0x00040000 },
        { 0x6, 0, 0, 0x00000000 }
    };

    const sock_fprog bpf_prog = { sizeof(bpf_filter) / sizeof(struct sock_filter), static_cast<sock_filter*>(bpf_filter) };
    if (setsockopt(sd, SOL_SOCKET, SO_ATTACH_FILTER, &bpf_prog, sizeof(bpf_prog)) != 0) {
        std::perror("GvspSocket failed to set filter");
    }
}

void setRealtime()
{
    if (capng_have_capability(CAPNG_EFFECTIVE, CAP_SYS_NICE) > 0) {
        sched_param param {};
        param.sched_priority = 2;
        if (sched_setscheduler(0, SCHED_FIFO | SCHED_RESET_ON_FORK, &param) < 0) {
            std::perror("GvspSocket failed to set socket thread realtime");
        }
    }
}

auto sockaddr2sockaddr_in(sockaddr* sa) -> sockaddr_in*
{
    void* v { sa };
    return static_cast<sockaddr_in*>(v);
}

auto sockaddr_ll2sockaddr(sockaddr_ll* sa_ll) -> sockaddr*
{
    void* v { sa_ll };
    return static_cast<sockaddr*>(v);
}

auto nicIndexFromAddress(uint32_t ip) -> unsigned
{
    unsigned index { 0 };

    // obtient la liste des interfaces
    ifaddrs* ifaddr { nullptr };
    if (getifaddrs(&ifaddr) == -1) {
        return index;
    }

    // on parcourt la liste
    for (const ifaddrs* ifa = ifaddr; ifa != nullptr; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr == nullptr) {
            continue;
        }

        // IPv4
        if (AF_INET != ifa->ifa_addr->sa_family) {
            continue;
        }

        auto* sa = sockaddr2sockaddr_in(ifa->ifa_addr);
        // contient bien notre IP
        if (ip != ntohl(sa->sin_addr.s_addr)) {
            continue;
        }

        // l'index de l'interface
        index = if_nametoindex(ifa->ifa_name);
    }

    freeifaddrs(ifaddr);

    return index;
}

constexpr auto HIGH_BITWISE { 32U };
constexpr auto timestamp(int64_t low, int64_t high) noexcept -> int64_t
{
    return low | (high << HIGH_BITWISE);
}

} // anonymous namespace

namespace Jgv::Gvsp {

ReceiverPrivate::ReceiverPrivate(std::unique_ptr<MemoryAllocator> _allocator, Channel::Type _channel, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : allocator(std::move(_allocator))
    , channel(_channel)
    , globalIpc(std::move(_globalIpc))
    , vtoConfig(globalIpc->vtoConfig())
{
    auto setGvspFormat = [this](int64_t segment, int64_t payload) {
        payloadFrom.store({ static_cast<int32_t>(segment), static_cast<int32_t>(payload) });
    };
    gvspFormatListenerFunction = std::make_shared<std::function<void(int64_t, int64_t)>>(setGvspFormat);
    globalIpc->addGvspPacketChangedListener(channel, gvspFormatListenerFunction);
}

void ReceiverPrivate::userStack()
{
    std::clog << "GvspReceiver listening with UDP socket" << std::endl;

    // préparation du polling sur le descripteur de socket
    pollfd pfd { sd, POLLIN, 0 };
    sockaddr from = {};
    socklen_t fromSize = sizeof(from);
    std::array<uint8_t, GVSP_BUFFER_SIZE> buffer {};

    while (run) {
        // attend un évènement à lire
        int pollResult = poll(&pfd, 1, POLL_TIMEOUT);
        if (pollResult > 0) { // oki, on lit
            const auto read = recvfrom(sd, buffer.data(), buffer.size(), MSG_DONTWAIT, &from, &fromSize);
            if (read > 0) {
                //auto addr { sockaddr2sockaddr_in(&from) };
                //                if (params.transmitterIP == 0 || ntohl(addr->sin_addr.s_addr) == params.transmitterIP) {
                //                    if (params.transmitterPort == 0 || ntohs(addr->sin_port) == params.transmitterPort) {
                doBuffer(std::span<uint8_t>(buffer.data(), read));
                //                    }
                //                }
            } else if (read < 0) {
                std::perror("GvspSocket recv:");
            } else {
                std::clog << "GvspSocket recv: 0 byte readed" << std::endl;
            }
        }
    }
}

void ReceiverPrivate::ringStack()
{
    std::clog << "GvspReceiver listening with PACKET socket" << std::endl;

    raw = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (raw < 0) {
        std::perror("GvspSocket failed to setup socket");
        return;
    }

    // on utilise la version 3 des socket packet
    tpacket_versions version = TPACKET_V3;
    if (setsockopt(raw, SOL_PACKET, PACKET_VERSION, &version, sizeof(version)) < 0) {
        std::perror("GvspSocket failed to set packet version");
        return;
    }

    // configure le ring
    tpacket_req3 req {};
    req.tp_frame_size = 1 << 14; // la puissance de 2 directement superieur à la taille du packet (~9000 soit 16384)
    req.tp_block_size = req.tp_frame_size * 32;
    req.tp_block_nr = 16; // 16 blocs
    req.tp_frame_nr = (req.tp_block_size * req.tp_block_nr) / req.tp_frame_size;
    req.tp_sizeof_priv = 0;
    req.tp_retire_blk_tov = 5; // timeout 5 ms
    req.tp_feature_req_word = TP_FT_REQ_FILL_RXHASH;
    if (setsockopt(raw, SOL_PACKET, PACKET_RX_RING, &req, sizeof(req)) < 0) {
        std::perror("GvspSocket failed to set packet rx ring");
        return;
    }

    //    if (Utils::ipIsMusticast(globalIpc->setGvcpTransmitterIP(channel))) {
    //        setFilter(raw, params.transmitterIP, params.transmitterPort, params.multicastGroup, params.multicastPort);
    //    } else {
    //        setFilter(raw, params.transmitterIP, params.transmitterPort, params.receiverIP, params.receiverPort);
    //    }

    std::clog << "Gvsp::ReceiverPrivate::ringStack(): filtering from "
              << Utils::ipToString(globalIpc->gvspTransmitterIP(channel)) << ":" << globalIpc->gvspTransmitterPort(channel)
              << " to " << Utils::ipToString(globalIpc->gvspReceiverIP(channel)) << ":" << globalIpc->gvspReceiverPort(channel) << std::endl;

    setFilter(raw, globalIpc->gvspTransmitterIP(channel), globalIpc->gvspTransmitterPort(channel),
        globalIpc->gvspReceiverIP(channel), globalIpc->gvspReceiverPort(channel));

    // map le ring
    const auto mapSize { req.tp_block_size * req.tp_block_nr };
    auto map { static_cast<uint8_t*>(mmap(nullptr, mapSize, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_POPULATE, raw, 0)) };
    if (static_cast<uint8_t*>(nullptr) > map) {
        std::perror("GvspSocket failed to map ring");
        return;
    }

    MapSpan span1 { { map, mapSize }, nullptr };
    MapSpan span2 { span1.span.subspan(req.tp_block_size), nullptr };
    MapSpan span3 { span2.span.subspan(req.tp_block_size), nullptr };
    MapSpan span4 { span3.span.subspan(req.tp_block_size), nullptr };
    MapSpan span5 { span4.span.subspan(req.tp_block_size), nullptr };
    MapSpan span6 { span5.span.subspan(req.tp_block_size), nullptr };
    MapSpan span7 { span6.span.subspan(req.tp_block_size), nullptr };
    MapSpan span8 { span7.span.subspan(req.tp_block_size), nullptr };
    MapSpan span9 { span8.span.subspan(req.tp_block_size), nullptr };
    MapSpan span10 { span9.span.subspan(req.tp_block_size), nullptr };
    MapSpan span11 { span10.span.subspan(req.tp_block_size), nullptr };
    MapSpan span12 { span11.span.subspan(req.tp_block_size), nullptr };
    MapSpan span13 { span12.span.subspan(req.tp_block_size), nullptr };
    MapSpan span14 { span13.span.subspan(req.tp_block_size), nullptr };
    MapSpan span15 { span14.span.subspan(req.tp_block_size), nullptr };
    MapSpan span16 { span15.span.subspan(req.tp_block_size), nullptr };
    span1.nextSpan = &span2;
    span2.nextSpan = &span3;
    span3.nextSpan = &span4;
    span4.nextSpan = &span5;
    span5.nextSpan = &span6;
    span6.nextSpan = &span7;
    span7.nextSpan = &span8;
    span8.nextSpan = &span9;
    span9.nextSpan = &span10;
    span10.nextSpan = &span11;
    span11.nextSpan = &span12;
    span12.nextSpan = &span13;
    span13.nextSpan = &span14;
    span14.nextSpan = &span15;
    span15.nextSpan = &span16;
    span16.nextSpan = &span1;

    // bind le raw socket
    sockaddr_ll localAddress {};
    localAddress.sll_family = AF_PACKET;
    localAddress.sll_protocol = htons(ETH_P_IP);
    localAddress.sll_ifindex = static_cast<int>(nicIndexFromAddress(globalIpc->gvspReceiverIP(channel)));
    localAddress.sll_hatype = 0;
    localAddress.sll_pkttype = 0;
    localAddress.sll_halen = 0;
    if (bind(raw, sockaddr_ll2sockaddr(&localAddress), sizeof(localAddress)) == -1) {
        std::perror("GvspSocket failed to bind packet socket");
        return;
    }

    // préparation du polling sur le descripteur de socket
    pollfd pfd = {};
    pfd.fd = raw;
    pfd.events = POLLIN;

    // on commence par le premier block
    //auto mapping { &firstMapping };
    auto current { &span1 };
    while (run) {
        //auto data { mapping->data };
        // si le bloc est indisponible
        if ((blockStatus(current->span) & static_cast<uint_fast32_t>(TP_STATUS_USER)) == 0) {
            // on attends un évènement sur le socket packet
            poll(&pfd, 1, POLL_TIMEOUT);
            // le poll est déclenché on recommence la procédure
            continue;
        }
        doBlock(current->span);
        resetBlockStatus(current->span);
        current = current->nextSpan;
    }

    munmap(map, req.tp_block_size * req.tp_block_nr);
    close(raw);
}

void ReceiverPrivate::doBlock(std::span<const uint8_t> block)
{
    // obtient le nombre de packet
    const auto count { numOfPkts(block) };
    // l'emplacement du premier packet
    auto tpacket3 { block.subspan(offsetToFirstPkt(block)) };
    // on parcourt toutes les trames décrites dans le bloc
    for (uint_fast32_t index { 0 }; index < count; ++index) {
        // il nous faut la position du datagram
        auto dgram { tpacket3.subspan(offsetToNet(tpacket3)) };
        // le datagram commence par l'entête IP, on va y chercher la taille du packet
        const auto size { static_cast<std::span<const uint8_t>::size_type>(ntohs(ipTotLen(dgram)) - sizeof(iphdr) - sizeof(udphdr)) };
        // on transmet le payload
        doBuffer(dgram.subspan(sizeof(iphdr) + sizeof(udphdr), size));
        // le prochain packet
        tpacket3 = tpacket3.subspan(offsetToNextPkt(tpacket3));
    }
}

void ReceiverPrivate::loop(std::promise<void> barrier)
{
    // descripteur du socket UDP
    sd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sd < 0) {
        std::perror("GvspSocket: failed to create socket");
    }

    // bind du socket UDP
    sockaddr localAddress = {};
    auto localAddressIn { sockaddr2sockaddr_in(&localAddress) };
    localAddressIn->sin_family = AF_INET;
    localAddressIn->sin_port = htons(0); // port aléatoire
    localAddressIn->sin_addr.s_addr = htonl(0);

    if (bind(sd, &localAddress, sizeof(struct sockaddr_in)) == -1) {
        std::perror("GvspSocket: failed to bind socket");
    }

    if (Utils::ipIsMusticast(globalIpc->gvspTransmitterIP(channel))) {
        ip_mreq group {};
        group.imr_multiaddr.s_addr = htonl(globalIpc->gvspTransmitterIP(channel));
        group.imr_interface.s_addr = htonl(globalIpc->gvspReceiverIP(channel));

        if (setsockopt(sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &group, sizeof(group)) < 0) {
            std::perror("Gvsp::Receiver::loop: failed to set multicast option");
        }
    }

    // on récupère le numéro de port affecté
    sockaddr bindAddress {};
    socklen_t bindAddressSize = sizeof(sockaddr);
    getsockname(sd, &bindAddress, &bindAddressSize);
    globalIpc->setGvspReceiverPort(channel, ntohs(sockaddr2sockaddr_in(&bindAddress)->sin_port));

    // obtient l'ordonnancement temps réel
    setRealtime();

    // init sur le thread de réception
    allocator->initialize();

    // on lève la barrière
    barrier.set_value();

    // si on a la capacity on démarre un socket mmap ring buffer
    if (capng_have_capability(CAPNG_EFFECTIVE, CAP_NET_RAW) > 0) {
        ringStack();
    } else {
        userStack();
    }

    // détruit sur le thread d'init
    allocator->destroy();

    close(sd);
}

void ReceiverPrivate::handleLeader(Packet gvsp)
{
    // cette version ne gère pas les resends

    // les blocks non finis sont jetés
    if (!currentBlock.memoryView.empty()) {
        std::cout << "GvspReceiver::handleLeader: trashing non empty block " << image.number << std::endl;
        allocator->trash(image.number);
    }

    if (!payloadFrom.compare_exchange_weak(payloadTo, payloadFrom.load(), std::memory_order_relaxed)) {
        const auto segmentSize { payloadTo.segmentSize - static_cast<int>(sizeof(iphdr)) - static_cast<int>(sizeof(udphdr)) - static_cast<int>(sizeof(DataPacketHeader)) };
        const auto [quot, rem] { std::div(payloadTo.size, segmentSize) };
        // id commence à 0 pour le leader, on passe -1 pour avoir une id payload [0, n]
        currentBlock.blockSizes.lastPacketId = static_cast<uint32_t>(rem == 0 ? quot - 1 : quot);
        currentBlock.blockSizes.segmentSize = static_cast<uint16_t>(segmentSize);
        currentBlock.blockSizes.lastSegmentSize = static_cast<uint16_t>(rem);
    }

    // oki le block est disponible
    const auto number { gvsp.headerBlockId() };
    currentBlock.memoryView = allocator->allocate(number, static_cast<std::size_t>(payloadTo.size));
    if (!currentBlock.memoryView.empty()) {
        image.number = number;
        image.width = gvsp.leaderImageSizeX();
        image.height = gvsp.leaderImageSizeY();
        image.pixelFormat = gvsp.leaderImagePixelFormat();

        // on date avec le timestamp GVSP
        image.timestamp = timestamp(gvsp.leaderTimestampLow(), gvsp.leaderTimestampHigh());
        // les données angulaires
        if (vtoConfig) {
            const auto [site, gisement] { vtoConfig->positions() };
            image.site = site;
            image.gisement = gisement;
        }

    } else {
        std::cout << number << " not allocated" << std::endl;
        // L'allocation a loupé, ca peut être normal si l'allocateur a changé sa configuration.
        // On doit invalider toute les vues précédentes, sachant que l'allocateur s'occupe de libérer la mémoire.

        // Ce receveur ne gère qu'une image à la fois.
        //currentBlock.reset();
        //std::cerr << "GvspReceiver::handleLeader: block " << gvsp.headerBlockId() << " failed to allocate memory size: " << blockSize(width, height, pixelFormat) << std::endl;
    }
}

void ReceiverPrivate::handleTrailer(Packet gvsp)
{
    if (image.number == gvsp.headerBlockId()) {
        image.timestamp = globalIpc->timestamp(channel, image.timestamp);
        currentBlock.memoryView = {};
        allocator->push(image);
    }
}

void ReceiverPrivate::doBuffer(std::span<const uint8_t> span)
{
    Packet gvsp(span);
    if (gvsp.statusIsSucces()) {
        const auto packetFormat { gvsp.headerPacketFormat() };
        if (packetFormat == enumType(PacketFormat::DATA_PAYLOAD)) {
            // on drop silencieusement les payloads qui n'appartienent pas au bloc courant
            if (image.number == gvsp.headerBlockId()) {
                currentBlock.insert(gvsp);
            }
            return;
        }

        if (packetFormat == enumType(PacketFormat::DATA_LEADER)) {
            handleLeader(gvsp);
            return;
        }

        if (packetFormat == enumType(PacketFormat::DATA_TRAILER)) {
            handleTrailer(gvsp);
            return;
        }

        std::cerr << "ReceiverPrivate::doBuffer packet format not handled " << std::hex << static_cast<int>(packetFormat) << std::endl;

    } else if (gvsp.headerStatus() == enumType(Status::Resend)) {
        //blockPtr->insertSegment(packetID, gvsp.imageData(), gvsp.imageDataSize());
        std::cout << "GvspSocket: resend" << std::endl;
    } else {
        std::cerr << "ReceiverPrivate::doBuffer packet unsuccess." << std::endl;
    }
}

Receiver::Receiver(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl(std::make_unique<ReceiverPrivate>(std::make_unique<FakeMemoryAllocator>(channel, globalIpc), channel, globalIpc))
{
}

Receiver::Receiver(std::unique_ptr<MemoryAllocator> allocator, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl(std::make_unique<ReceiverPrivate>(std::move(allocator), channel, globalIpc))
{
}

Receiver::~Receiver()
{
    if (_impl->thread.joinable()) {
        _impl->run = false;
        _impl->thread.join();
    }
}

void Receiver::setAllocatorEventsListener(std::weak_ptr<AllocatorEvents> listener)
{
    _impl->allocator->setAllocatorEventsListener(std::move(listener));
}

/*!
 * \brief Receiver::listenOn
 * Place le receveur en écoute.
 * \param bindAddress L'addresse IPV4 utilisée par le receveur.
 */
void Receiver::listenUnicast()
{
    if (_impl->thread.joinable()) {
        std::clog << "Receiver allready listening" << std::endl;
        return;
    }

    std::promise<void> barrier;
    std::future<void> barrierFuture { barrier.get_future() };

    // on lance le thread
    _impl->thread = std::thread(&ReceiverPrivate::loop, _impl.get(), std::move(barrier));
    pthread_setname_np(_impl->thread.native_handle(), "GvspSocket");

    // on attend que la barrière soit levée
    barrierFuture.wait();
}

void Receiver::listenMulticast()
{
    //    _impl->params.multicastGroup = multicastgroup;
    //    _impl->params.multicastPort = multicastPort;
    //    _impl->params.isMulticast = true;
    //    listenUnicast(bindAddress, transmitterIP, transmitterPort);

    std::clog << "Listen multicast " << std::endl;
}

auto Receiver::allocator() -> MemoryAllocator&
{
    return *_impl->allocator;
}

auto Receiver::allocator() const -> const MemoryAllocator&
{
    return *_impl->allocator;
}

} // namespace Jgv::Gvsp
