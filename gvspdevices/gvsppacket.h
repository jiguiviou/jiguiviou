/***************************************************************************
 *   Copyright (C) 2014-2018 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPPACKET_H
#define GVSPPACKET_H

#include <cstdint>
#include <span>

namespace Jgv::Gvsp {

using SIMPLET = std::array<uint8_t, 1>;
using DOUBLET = std::array<uint8_t, 2>;
using TRIPLET = std::array<uint8_t, 3>;
using QUADLET = std::array<uint8_t, 4>;

struct DataPacketHeader {
    DOUBLET status;
    DOUBLET blockId;
    SIMPLET packetFormat;
    TRIPLET packetid;
};

struct DataLeader {
    DataPacketHeader header;
    DOUBLET reserved;
    DOUBLET payloadType;
    QUADLET timestampHighPart;
    QUADLET timestampLowPart;
};

struct DataLeaderImage {
    DataLeader leader;
    QUADLET pixelFormat;
    QUADLET sizeX;
    QUADLET sizeY;
    QUADLET offsetX;
    QUADLET offsetY;
    DOUBLET paddingX;
    DOUBLET paddingY;
};

struct PayloadImage {
    DataPacketHeader header;
    QUADLET data;
};

struct DataTrailer {
    DataPacketHeader header;
    DOUBLET reserved;
    DOUBLET payloadType;
};

struct DataTrailerImage {
    DataTrailer trailer;
    QUADLET sizeY;
};

class Packet final {
    const std::span<const uint8_t> _span;

public:
    explicit Packet(std::span<const uint8_t> span) noexcept;

    [[nodiscard]] auto headerStatus() const noexcept -> uint16_t;
    [[nodiscard]] auto headerBlockId() const noexcept -> uint16_t;
    [[nodiscard]] auto headerPacketFormat() const noexcept -> uint8_t;
    [[nodiscard]] auto headerPacketId() const noexcept -> uint32_t;
    [[nodiscard]] auto leaderPayloadType() const noexcept -> uint16_t;
    [[nodiscard]] auto leaderTimestampHigh() const noexcept -> uint32_t;
    [[nodiscard]] auto leaderTimestampLow() const noexcept -> uint32_t;
    [[nodiscard]] auto leaderImagePixelFormat() const noexcept -> uint32_t;
    [[nodiscard]] auto leaderImageSizeX() const noexcept -> uint32_t;
    [[nodiscard]] auto leaderImageSizeY() const noexcept -> uint32_t;
    [[nodiscard]] auto leaderImageOffsetX() const noexcept -> uint32_t;
    [[nodiscard]] auto leaderImageOffsetY() const noexcept -> uint32_t;
    [[nodiscard]] auto leaderImagePaddingX() const noexcept -> uint16_t;
    [[nodiscard]] auto leaderImagePaddingY() const noexcept -> uint16_t;
    [[nodiscard]] auto payloadImageData() const noexcept -> const uint8_t*;
    [[nodiscard]] auto imageDataSize() const noexcept -> std::size_t;
    [[nodiscard]] auto trailerPayloadType() const noexcept -> uint16_t;
    [[nodiscard]] auto trailerImageSizeY() const noexcept -> uint32_t;

    [[nodiscard]] auto statusIsSucces() const -> bool;

}; // class Packet

} // namespace Jgv::Gvsp
#endif // GVSPPACKET_H
