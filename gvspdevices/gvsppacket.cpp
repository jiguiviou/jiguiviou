/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvsppacket.h"
#include "gvsp.h"
#include <endian.h>

namespace {

template <typename T>
constexpr auto fromUnaligned(std::span<const uint8_t> src) noexcept -> T
{
    T dest;
    __builtin_memcpy(&dest, src.data(), sizeof(T));
    return dest;
}

} // anonymous namespace

namespace Jgv::Gvsp {

Packet::Packet(std::span<const uint8_t> span) noexcept
    : _span(span)
{
}

auto Packet::headerStatus() const noexcept -> uint16_t
{
    return be16toh(fromUnaligned<uint16_t>(_span.subspan(__builtin_offsetof(DataPacketHeader, status))));
}

auto Packet::headerBlockId() const noexcept -> uint16_t
{
    return be16toh(fromUnaligned<uint16_t>(_span.subspan(__builtin_offsetof(DataPacketHeader, blockId))));
}

auto Packet::headerPacketFormat() const noexcept -> uint8_t
{
    return fromUnaligned<uint8_t>(_span.subspan(__builtin_offsetof(DataPacketHeader, packetFormat)));
}

auto Packet::headerPacketId() const noexcept -> uint32_t
{
    return UINT32_C(0x00FFFFFF) & be32toh(fromUnaligned<uint32_t>(_span.subspan(__builtin_offsetof(DataPacketHeader, packetFormat))));
}

auto Packet::leaderPayloadType() const noexcept -> uint16_t
{
    return be16toh(fromUnaligned<uint16_t>(_span.subspan(__builtin_offsetof(DataLeader, payloadType))));
}

auto Packet::leaderTimestampHigh() const noexcept -> uint32_t
{
    return be32toh(fromUnaligned<uint32_t>(_span.subspan(__builtin_offsetof(DataLeader, timestampHighPart))));
}

auto Packet::leaderTimestampLow() const noexcept -> uint32_t
{
    return be32toh(fromUnaligned<uint32_t>(_span.subspan(__builtin_offsetof(DataLeader, timestampLowPart))));
}

auto Packet::leaderImagePixelFormat() const noexcept -> uint32_t
{
    return be32toh(fromUnaligned<uint32_t>(_span.subspan(__builtin_offsetof(DataLeaderImage, pixelFormat))));
}

auto Packet::leaderImageSizeX() const noexcept -> uint32_t
{
    return be32toh(fromUnaligned<uint32_t>(_span.subspan(__builtin_offsetof(DataLeaderImage, sizeX))));
}

auto Packet::leaderImageSizeY() const noexcept -> uint32_t
{
    return be32toh(fromUnaligned<uint32_t>(_span.subspan(__builtin_offsetof(DataLeaderImage, sizeY))));
}

auto Packet::leaderImageOffsetX() const noexcept -> uint32_t
{
    return be32toh(fromUnaligned<uint32_t>(_span.subspan(__builtin_offsetof(DataLeaderImage, offsetX))));
}

auto Packet::leaderImageOffsetY() const noexcept -> uint32_t
{
    return be32toh(fromUnaligned<uint32_t>(_span.subspan(__builtin_offsetof(DataLeaderImage, offsetY))));
}

auto Packet::leaderImagePaddingX() const noexcept -> uint16_t
{
    return be16toh(fromUnaligned<uint16_t>(_span.subspan(__builtin_offsetof(DataLeaderImage, paddingX))));
}

auto Packet::leaderImagePaddingY() const noexcept -> uint16_t
{
    return be16toh(fromUnaligned<uint16_t>(_span.subspan(__builtin_offsetof(DataLeaderImage, paddingY))));
}

auto Packet::payloadImageData() const noexcept -> const uint8_t*
{
    return _span.subspan(__builtin_offsetof(PayloadImage, data)).data();
}

auto Packet::imageDataSize() const noexcept -> std::size_t
{
    return static_cast<std::size_t>(_span.size_bytes()) - sizeof(DataPacketHeader);
}

auto Packet::trailerPayloadType() const noexcept -> uint16_t
{
    return be16toh(fromUnaligned<uint16_t>(_span.subspan(__builtin_offsetof(DataTrailer, payloadType))));
}

auto Packet::trailerImageSizeY() const noexcept -> uint32_t
{
    return be32toh(fromUnaligned<uint32_t>(_span.subspan(__builtin_offsetof(DataTrailerImage, sizeY))));
}

auto Packet::statusIsSucces() const -> bool
{
    // SUCCES == 0
    return (_span[__builtin_offsetof(DataPacketHeader, status)] == 0) && (_span[__builtin_offsetof(DataPacketHeader, status) + 1] == 0);
}

} // namespace Jgv::Gvsp
