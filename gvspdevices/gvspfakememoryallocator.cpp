/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspfakememoryallocator.h"

#include <chrono>
#include <globalipc/globalipc.h>
#include <iostream>

std::string timestampString(uint64_t ts)
{
    std::chrono::nanoseconds epoch { ts };
    std::chrono::system_clock::time_point tp { epoch };
    auto tt { std::chrono::system_clock::to_time_t(tp) };
    std::string ss = std::ctime(&tt);
    ss.resize(ss.size() - 1);
    return ss;
}

namespace Jgv::Gvsp {

FakeMemoryAllocator::FakeMemoryAllocator(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _channel { channel }
    , _globalIpc { std::move(globalIpc) }
{

    auto pixelFormatChanged = [this](uint32_t width, uint32_t height, uint32_t format) {
        if (auto events { _events.lock() }) {
            events->imageFormat(width, height, format);
        }
    };

    _pixelFormatChangedListener = std::make_shared<std::function<void(uint32_t, uint32_t, uint32_t)>>(pixelFormatChanged);
    _globalIpc->addPixelFormatChangedListener(_channel, _pixelFormatChangedListener);
}

FakeMemoryAllocator::~FakeMemoryAllocator() = default;

void FakeMemoryAllocator::initialize() noexcept {}

auto FakeMemoryAllocator::allocate(uint32_t id, std::size_t size) noexcept -> std::span<uint8_t>
{
    if (auto events { _events.lock() }) {
        events->allocate(id, size);
    }
    _buffer.resize(size);
    return _buffer;
}

void FakeMemoryAllocator::push(const Jgv::Gvsp::Image& image) noexcept
{
    std::cout << "Fake push " << image.width << "x" << image.height << " " << timestampString(image.timestamp) << std::endl;
}

void FakeMemoryAllocator::trash(uint32_t id) noexcept
{
    std::cout << "Fake trash image " << id << std::endl;
}

void FakeMemoryAllocator::destroy() noexcept {}

void FakeMemoryAllocator::setAllocatorEventsListener(std::weak_ptr<AllocatorEvents> listener)
{
    _events = std::move(listener);
}

} // namespace Jgv::Gvsp
