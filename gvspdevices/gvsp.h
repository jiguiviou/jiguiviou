/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSP_H
#define GVSP_H

#include <cstdint>
#include <type_traits>

//===================================================
// PIXEL FORMATS
//===================================================
// Indicate if pixel is monochrome or RGB
constexpr auto GVSP_PIX_MONO { 0x01000000U };
constexpr auto GVSP_PIX_RGB { 0x02000000U }; // deprecated in version 1.1
constexpr auto GVSP_PIX_COLOR { 0x02000000U };
constexpr auto GVSP_PIX_CUSTOM { 0x80000000U };
constexpr auto GVSP_PIX_COLOR_MASK { 0xFF000000U };
// Indicate effective number of bits occupied by the pixel (including padding).
// This can be used to compute amount of memory required to store an image.
constexpr auto GVSP_PIX_OCCUPY8BIT { 0x00080000U };
constexpr auto GVSP_PIX_OCCUPY12BIT { 0x000C0000U };
constexpr auto GVSP_PIX_OCCUPY16BIT { 0x00100000U };
constexpr auto GVSP_PIX_OCCUPY24BIT { 0x00180000U };
constexpr auto GVSP_PIX_OCCUPY32BIT { 0x00200000U };
constexpr auto GVSP_PIX_OCCUPY36BIT { 0x00240000U };
constexpr auto GVSP_PIX_OCCUPY48BIT { 0x00300000U };
constexpr auto GVSP_PIX_EFFECTIVE_PIXEL_SIZE_MASK { 0x00FF0000U };
constexpr auto GVSP_PIX_EFFECTIVE_PIXEL_SIZE_SHIFT { 16U };
// Pixel ID: lower 16-bit of the pixel formats
constexpr auto GVSP_PIX_ID_MASK { 0x0000FFFFU };
// Mono buffer format defines
constexpr auto GVSP_PIX_MONO8 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY8BIT | 0x0001U };
constexpr auto GVSP_PIX_MONO8_SIGNED { GVSP_PIX_MONO | GVSP_PIX_OCCUPY8BIT | 0x0002U };
constexpr auto GVSP_PIX_MONO10 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x0003U };
constexpr auto GVSP_PIX_MONO10_PACKED { GVSP_PIX_MONO | GVSP_PIX_OCCUPY12BIT | 0x0004U };
constexpr auto GVSP_PIX_MONO12 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x0005U };
constexpr auto GVSP_PIX_MONO12_PACKED { GVSP_PIX_MONO | GVSP_PIX_OCCUPY12BIT | 0x0006U };
constexpr auto GVSP_PIX_MONO14 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x0025U };
constexpr auto GVSP_PIX_MONO16 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x0007U };
// Bayer buffer format defines
constexpr auto GVSP_PIX_BAYGR8 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY8BIT | 0x0008U };
constexpr auto GVSP_PIX_BAYRG8 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY8BIT | 0x0009U };
constexpr auto GVSP_PIX_BAYGB8 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY8BIT | 0x000AU };
constexpr auto GVSP_PIX_BAYBG8 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY8BIT | 0x000BU };
constexpr auto GVSP_PIX_BAYGR10 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x000CU };
constexpr auto GVSP_PIX_BAYRG10 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x000DU };
constexpr auto GVSP_PIX_BAYGB10 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x000EU };
constexpr auto GVSP_PIX_BAYBG10 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x000FU };
constexpr auto GVSP_PIX_BAYGR12 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x0010U };
constexpr auto GVSP_PIX_BAYRG12 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x0011U };
constexpr auto GVSP_PIX_BAYGB12 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x0012U };
constexpr auto GVSP_PIX_BAYBG12 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x0013U };
constexpr auto GVSP_PIX_BAYGR10_PACKED { GVSP_PIX_MONO | GVSP_PIX_OCCUPY12BIT | 0x0026U };
constexpr auto GVSP_PIX_BAYRG10_PACKED { GVSP_PIX_MONO | GVSP_PIX_OCCUPY12BIT | 0x0027U };
constexpr auto GVSP_PIX_BAYGB10_PACKED { GVSP_PIX_MONO | GVSP_PIX_OCCUPY12BIT | 0x0028U };
constexpr auto GVSP_PIX_BAYBG10_PACKED { GVSP_PIX_MONO | GVSP_PIX_OCCUPY12BIT | 0x0029U };
constexpr auto GVSP_PIX_BAYGR12_PACKED { GVSP_PIX_MONO | GVSP_PIX_OCCUPY12BIT | 0x002AU };
constexpr auto GVSP_PIX_BAYRG12_PACKED { GVSP_PIX_MONO | GVSP_PIX_OCCUPY12BIT | 0x002BU };
constexpr auto GVSP_PIX_BAYGB12_PACKED { GVSP_PIX_MONO | GVSP_PIX_OCCUPY12BIT | 0x002CU };
constexpr auto GVSP_PIX_BAYBG12_PACKED { GVSP_PIX_MONO | GVSP_PIX_OCCUPY12BIT | 0x002DU };
constexpr auto GVSP_PIX_BAYGR16 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x002EU };
constexpr auto GVSP_PIX_BAYRG16 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x002FU };
constexpr auto GVSP_PIX_BAYGB16 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x0030U };
constexpr auto GVSP_PIX_BAYBG16 { GVSP_PIX_MONO | GVSP_PIX_OCCUPY16BIT | 0x0031U };
// RGB Packed buffer format defines
constexpr auto GVSP_PIX_RGB8_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY24BIT | 0x0014U };
constexpr auto GVSP_PIX_BGR8_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY24BIT | 0x0015U };
constexpr auto GVSP_PIX_RGBA8_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY32BIT | 0x0016U };
constexpr auto GVSP_PIX_BGRA8_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY32BIT | 0x0017U };
constexpr auto GVSP_PIX_RGB10_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0018U };
constexpr auto GVSP_PIX_BGR10_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0019U };
constexpr auto GVSP_PIX_RGB12_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x001AU };
constexpr auto GVSP_PIX_BGR12_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x001BU };
constexpr auto GVSP_PIX_RGB16_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0033U };
constexpr auto GVSP_PIX_RGB10V1_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY32BIT | 0x001CU };
constexpr auto GVSP_PIX_RGB10V2_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY32BIT | 0x001DU };
constexpr auto GVSP_PIX_RGB12V1_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY36BIT | 0X0034U };
constexpr auto GVSP_PIX_RGB565_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x0035U };
constexpr auto GVSP_PIX_BGR565_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0X0036U };
// YUV Packed buffer format defines
constexpr auto GVSP_PIX_YUV411_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY12BIT | 0x001EU };
constexpr auto GVSP_PIX_YUV422_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x001FU };
constexpr auto GVSP_PIX_YUV422_YUYV_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x0032U };
constexpr auto GVSP_PIX_YUV444_PACKED { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY24BIT | 0x0020U };
// RGB Planar buffer format defines
constexpr auto GVSP_PIX_RGB8_PLANAR { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY24BIT | 0x0021U };
constexpr auto GVSP_PIX_RGB10_PLANAR { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0022U };
constexpr auto GVSP_PIX_RGB12_PLANAR { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0023U };
constexpr auto GVSP_PIX_RGB16_PLANAR { GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0024U };

constexpr auto GVSP_PIX_PIXEL_SIZE(unsigned int GVSP_PIX) { return (GVSP_PIX_EFFECTIVE_PIXEL_SIZE_MASK & GVSP_PIX) >> GVSP_PIX_EFFECTIVE_PIXEL_SIZE_SHIFT; }

namespace Jgv::Gvsp {

enum class Status : uint16_t {
    Succes = 0x0000,
    Resend = 0x0100
};

template <typename T>
constexpr auto enumType(T enumerator) noexcept -> typename std::underlying_type<T>::type
{
    return static_cast<typename std::underlying_type<T>::type>(enumerator);
}

} // namespace Jgv::Gvsp

#endif // GVSP_H
