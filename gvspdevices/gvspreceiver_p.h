/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPRECEIVER_P_H
#define GVSPRECEIVER_P_H

#include "gvspblock.h"

#include "gvsp.h"

#include <atomic>
#include <future>
#include <memory>
#include <span>
#include <thread>

namespace Jgv::GlobalIPC {
class Object;
class VtoConfig;
} // namespace Jgv::GlobalIPC

namespace Jgv::Gvsp {

class MemoryAllocator;

struct Payload {
    int32_t segmentSize;
    int32_t size;
};

struct ReceiverPrivate {
    explicit ReceiverPrivate(std::unique_ptr<MemoryAllocator> allocator, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    const std::unique_ptr<MemoryAllocator> allocator;
    const Channel::Type channel;
    const std::shared_ptr<GlobalIPC::Object> globalIpc;
    const std::shared_ptr<GlobalIPC::VtoConfig> vtoConfig;

    std::atomic<Payload> payloadFrom {};
    Payload payloadTo {};
    Block currentBlock;
    Image image;

    std::shared_ptr<std::function<void(int64_t, int64_t)>> gvspFormatListenerFunction;
    int sd = -1;
    int raw = -1;

    volatile bool run { true }; // contrôle la sortie de boucle
    std::thread thread;

    void userStack();
    void ringStack();
    void loop(std::promise<void> barrier);
    void doBuffer(std::span<const uint8_t> span);
    void doBlock(std::span<const uint8_t> block);
    void handleLeader(Packet gvsp);
    void handleTrailer(Packet gvsp);

}; // ReceiverPrivate

} // namespace Jgv::Gvsp

#endif // GVSPRECEIVER_P_H
