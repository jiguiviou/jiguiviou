/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPFAKEMEMORYALLOCATOR_H
#define GVSPFAKEMEMORYALLOCATOR_H

#include "gvspreceiver.h"

#include <cinttypes>
#include <functional>
#include <span>
#include <vector>

namespace Jgv::Gvsp {

struct Image;

class FakeMemoryAllocator : public MemoryAllocator {
    const Channel::Type _channel;
    const std::shared_ptr<GlobalIPC::Object> _globalIpc;
    std::shared_ptr<std::function<void(uint32_t, uint32_t, uint32_t)>> _pixelFormatChangedListener;
    std::vector<uint8_t> _buffer {};
    std::weak_ptr<AllocatorEvents> _events;

public:
    FakeMemoryAllocator(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~FakeMemoryAllocator() override;
    void initialize() noexcept override;
    auto allocate(uint32_t id, std::size_t size) noexcept -> std::span<uint8_t> override;
    void push(const Image& image) noexcept override;
    void trash(uint32_t id) noexcept override;
    void destroy() noexcept override;
    void setAllocatorEventsListener(std::weak_ptr<AllocatorEvents> listener) override;
};

} // namespace Jgv::Gvsp

#endif // GVSPFAKEMEMORYALLOCATOR_H
