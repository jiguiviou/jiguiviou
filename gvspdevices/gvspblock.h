/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPBLOCK_H
#define GVSPBLOCK_H

#include "gvspreceiver.h"

#include <span>

namespace Jgv::Gvsp {

class Packet;

struct BlockSizes {
    uint32_t lastPacketId;
    uint16_t segmentSize;
    uint16_t lastSegmentSize;
};

struct Block {
    BlockSizes blockSizes { 0, 0, 0 };
    std::span<uint8_t> memoryView {};

    void insert(Packet packet) const noexcept;
};

} // namespace Jgv::Gvsp

#endif // GVSPBLOCK_H
