/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPRECEIVER_H
#define GVSPRECEIVER_H

#include <memory>
#include <span>

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Gvsp {

struct AllocatorEvents {
    virtual void imageFormat(uint32_t width, uint32_t height, uint32_t format) = 0;
    virtual void allocate(uint32_t number, std::size_t size) = 0;
};

struct Image {
    int64_t timestamp { 0 };
    uint32_t number { 0 };
    uint32_t width { 0 };
    uint32_t height { 0 };
    uint32_t pixelFormat { 0 };
    int32_t site { 0 };
    int32_t gisement { 0 };
};

class MemoryAllocator {

public:
    virtual ~MemoryAllocator() = default;
    virtual void initialize() noexcept = 0;
    virtual auto allocate(uint32_t number, std::size_t size) noexcept -> std::span<uint8_t> = 0;
    virtual void push(const Image& image) noexcept = 0;
    virtual void trash(uint32_t number) noexcept = 0;
    virtual void destroy() noexcept = 0;
    virtual void setAllocatorEventsListener(std::weak_ptr<AllocatorEvents> /*listener*/) {};
};

struct ReceiverPrivate;
class Receiver {

public:
    explicit Receiver(Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    explicit Receiver(std::unique_ptr<MemoryAllocator> allocator, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    Receiver(const Receiver&) = delete;
    Receiver(Receiver&&) = delete;
    auto operator=(const Receiver&) -> Receiver& = delete;
    auto operator=(Receiver &&) -> Receiver& = delete;
    virtual ~Receiver();

    void setAllocatorEventsListener(std::weak_ptr<AllocatorEvents> listener);

    void listenUnicast();
    void listenMulticast();

protected:
    auto allocator() -> MemoryAllocator&;
    [[nodiscard]] auto allocator() const -> const MemoryAllocator&;

private:
    const std::unique_ptr<ReceiverPrivate> _impl;

}; // class Receiver

} // namespace Jgv::Gvsp

#endif // GVSPRECEIVER_H
