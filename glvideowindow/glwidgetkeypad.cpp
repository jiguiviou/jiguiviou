/**************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetkeypad.h"
#include "glwidgetkeypad_p.h"

#include "glwidgetkey.h"
#include "glwidgetkeyspecial.h"
#include "gnuinstalldir.h"

namespace {
constexpr uint COLUMNS_COUNT { 3 };
constexpr uint LINES_COUNT { 4 };
constexpr auto KEY_SCALE_FACTOR { 0.9F };
} // namespace

namespace Jgv::GLWidget {

KeypadPrivate::KeypadPrivate(float backgroundDepth, GLPainters* const painters)
    : depth(backgroundDepth)
    , painters(painters)
{
    keys.emplace_back(std::make_unique<Key>(depth, '1', '1', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '2', '2', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '3', '3', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '4', '4', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '5', '5', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '6', '6', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '7', '7', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '8', '8', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '9', '9', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '0', '0', painters));

    auto supprClicked = [this]() {
        if (text && setText) {
            std::string t { text() };
            if (!t.empty()) {
                t.pop_back();
                setText(std::move(t));
            }
        }
    };
    supprKey = std::make_unique<KeySpecial>(depth, GnuIntallDirs::assetsDir() + "/cleanup.png", painters);
    supprKey->setClickedFunction(supprClicked);

    auto keypadClicked = [this](char character) {
        if (text && setText) {
            std::string t { text() };
            setText(t + character);
        }
    };
    for (auto& key : keys) {
        key->setClickedFunction(keypadClicked);
    }
}

auto KeypadPrivate::rectangles() const -> OU::RectanglePainterObjectsVector
{
    OU::RectanglePainterObjectsVector vector;
    std::transform(keys.cbegin(), keys.cend(), std::back_inserter(vector),
        [](const auto& value) { return value->rectangleObject(); });
    vector.emplace_back(supprKey->rectangleObject());
    return vector;
}

auto KeypadPrivate::textures() const -> OU::TexturePainterObjectsVector
{
    return { supprKey->textureObject() };
}

auto KeypadPrivate::texts() const -> OU::TextPainterObjectVector
{
    OU::TextPainterObjectVector vector;
    std::transform(keys.cbegin(), keys.cend(), std::back_inserter(vector),
        [](const auto& value) { return value->textObject(); });
    return vector;
}

void KeypadPrivate::resize(const Rect& rect)
{
    const auto keySize { std::min(rect.width() / COLUMNS_COUNT, rect.height() / LINES_COUNT) };
    Rect keypadRect { 0, 0, COLUMNS_COUNT * keySize, LINES_COUNT * keySize };
    keypadRect.alignCenter(rect);

    auto resize = [&](const char& key, float c, float l) {
        auto it = std::find_if(keys.begin(), keys.end(), [&](const auto& value) {
            return value->normal() == key;
        });
        Rect keyRect { 0.f, 0.f, keySize, keySize };
        keyRect.translate(keypadRect.left() + c * keySize, keypadRect.bottom() + l * keySize);
        (*it)->resize(keyRect.scaled(KEY_SCALE_FACTOR));
    };

    resize('0', 0.F, 0.F);
    resize('1', 0.F, 1.F);
    resize('2', 1.F, 1.F);
    resize('3', 2.F, 1.F);
    resize('4', 0.F, 2.F);
    resize('5', 1.F, 2.F);
    resize('6', 2.F, 2.F);
    resize('7', 0.F, 3.F);
    resize('8', 1.F, 3.F);
    resize('9', 2.F, 3.F);

    Rect supprRect { 0.F, 0.F, 1.5F * keySize, keySize };
    supprRect.translate(keypadRect.left() + 1.5F * keySize, keypadRect.bottom());
    supprKey->resize(supprRect.scaled(0.75F));
}

void KeypadPrivate::handlePress(float x, float y)
{
    for (auto& key : keys) {
        key->handlePress(x, y);
    }
    supprKey->handlePress(x, y);
}

void KeypadPrivate::handleRelease(float x, float y)
{
    for (auto& key : keys) {
        key->handleRelease(x, y);
    }
    supprKey->handleRelease(x, y);
}

void KeypadPrivate::handleMove(float x, float y)
{
    for (auto& key : keys) {
        key->handleMove(x, y);
    }
    supprKey->handleMove(x, y);
}

Keypad::Keypad(float backgroundDepth, GLPainters* const painters)
    : _impl(std::make_unique<KeypadPrivate>(backgroundDepth, painters))
{
}

Keypad::~Keypad() = default;

void Keypad::setSetTextFunction(std::function<void(std::string_view)> setText)
{
    _impl->setText = std::move(setText);
}

void Keypad::setGetTextFunction(std::function<std::string_view(void)> text)
{
    _impl->text = std::move(text);
}

auto Keypad::rectangles() const -> OU::RectanglePainterObjectsVector
{
    return _impl->rectangles();
}

auto Keypad::textures() const -> OU::TexturePainterObjectsVector
{
    return _impl->textures();
}

auto Keypad::texts() const -> OU::TextPainterObjectVector
{
    return _impl->texts();
}

void Keypad::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Keypad::handlePress(float x, float y)
{
    _impl->handlePress(x, y);
}

void Keypad::handleRelease(float x, float y)
{
    _impl->handleRelease(x, y);
}

void Keypad::handleMove(float x, float y)
{
    _impl->handleMove(x, y);
}

} // namespace Jgv
