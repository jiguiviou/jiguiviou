/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLVIDEOWINDOW_H
#define GLVIDEOWINDOW_H

#include <epoxy/gl.h>

#include <QOpenGLWindow>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv {

struct GLVideoWindowPrivate;
class GLVideoWindow : public QOpenGLWindow {

public:
    explicit GLVideoWindow(std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~GLVideoWindow() override;
    GLVideoWindow(const GLVideoWindow&) = delete;
    GLVideoWindow(GLVideoWindow&&) = delete;
    auto operator=(const GLVideoWindow&) -> GLVideoWindow& = delete;
    auto operator=(GLVideoWindow &&) -> GLVideoWindow& = delete;

private:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void mousePressEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;
    void mouseMoveEvent(QMouseEvent* event) override;

private:
    const std::unique_ptr<GLVideoWindowPrivate> _impl;

}; // class VideoWindow

} // namespace Jgv

#endif // GLVIDEOWINDOW_H
