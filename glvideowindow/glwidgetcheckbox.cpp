/**************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetcheckbox.h"
#include "glwidgetcheckbox_p.h"

#include "glpagesmanager.h"
#include "theme.h"

#include <iostream>

namespace Jgv::GLWidget {

CheckboxPrivate::CheckboxPrivate(float backgroundDepth, GLPainters* const p)
    : depth(backgroundDepth)
    , painters(p)
{
    backgroundFar = painters->rectanglePainter.createObject();
    backgroundNear = painters->rectanglePainter.createObject();
    textObject = painters->textPainter.createObject();
    textClipObject = painters->textPainter.createClipObject();

    painters->rectanglePainter.setColor(backgroundFar, DECORATION_COLOR);
    painters->rectanglePainter.setColor(backgroundNear, RADIO_BACKGROUND_COLOR);
    painters->textPainter.setColor(textObject, RADIO_TEXT_COLOR);
    painters->textPainter.setClippingRectangle(textObject, textClipObject);
}

CheckboxPrivate::~CheckboxPrivate()
{
    painters->textPainter.destroyObject(textObject);
    painters->textPainter.destroyClipObject(textClipObject);
    painters->rectanglePainter.destroyObject(backgroundNear);
    painters->rectanglePainter.destroyObject(backgroundFar);
}

void CheckboxPrivate::resize(const Rect& rect)
{
    // la case à cocher est carrée, le côté vaut une hauteur
    auto checkRect { rect };
    checkRect.setRight(rect.left() + rect.height());
    painters->rectanglePainter.setRectangle(backgroundFar, checkRect.tuple(), depth);
    painters->rectanglePainter.setRectangle(backgroundNear, checkRect.reduced(DECORATION_WIDTH).tuple(), depth + INTER_LAYOUT_DEPTH);

    // le texte commence à droite de la case + INTER_WIDGET

    const auto [left, bottom, right, top] { painters->textPainter.boundingBox(textObject) };
    Rect textRect { right, top };
    textRect.alignCenter(rect);
    painters->textPainter.setPosition(textObject, { checkRect.right() + INTER_WIDGET_SPACE, textRect.bottom(), depth + INTER_LAYOUT_DEPTH + INTER_LAYOUT_DEPTH });
    painters->textPainter.updateClipObjectRectangle(textClipObject, { rect.left<int>(), rect.bottom<int>(), rect.width<int>(), rect.height<int>() });
}

Checkbox::Checkbox(float backgroundDepth, GLPainters* const painters)
    : _impl(std::make_unique<CheckboxPrivate>(backgroundDepth, painters))
{
}

Checkbox::~Checkbox() = default;

void Checkbox::setText(std::string_view text)
{
    _impl->painters->textPainter.setText(_impl->textObject, std::string(text));
}

void Checkbox::setChecked(bool checked)
{
    _impl->checked = checked;
    if (checked) {
        _impl->painters->rectanglePainter.setColor(_impl->backgroundNear, FOCUS_BACKGROUND_COLOR);
    } else {
        _impl->painters->rectanglePainter.setColor(_impl->backgroundNear, RADIO_BACKGROUND_COLOR);
    }
}

auto Checkbox::checked() const -> bool
{
    return _impl->checked;
}

auto Checkbox::widthHint(float height) const -> float
{
    const auto [textLeft, textBottom, textRight, textTop] { _impl->painters->textPainter.boundingBox(_impl->textObject) };
    return height + INTER_WIDGET_SPACE + (textRight - textLeft);
}

auto Checkbox::rectangles() const -> OU::RectanglePainterObjectsVector
{
    return { _impl->backgroundFar, _impl->backgroundNear };
}

auto Checkbox::texts() const -> OU::TextPainterObjectVector
{
    return { _impl->textObject };
}

void Checkbox::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Checkbox::clicked()
{
    setChecked(!_impl->checked);
}

} // namespace Jgv::GLWidget
