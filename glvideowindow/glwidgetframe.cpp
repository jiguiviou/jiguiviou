/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetframe.h"

namespace Jgv::GLWidget {

void Frame::setEnabled(bool enabled) noexcept
{
    _enabled = enabled;
}

auto Frame::isEnabled() const noexcept -> bool
{
    return _enabled;
}

void Frame::setActived(bool actived) noexcept
{
    _actived = actived;
}

auto Frame::isActived() const noexcept -> bool
{
    return _actived;
}

void Frame::handlePress(float x, float y)
{
    if (_enabled && contains(x, y)) {
        _pressed = true;
        pressed();
    }
}

void Frame::handleRelease(float x, float y)
{
    if (_enabled && contains(x, y)) {
        released();
        if (_pressed) {
            _pressed = false;
            clicked();
        }
    }
}

void Frame::handleMove(float x, float y)
{
    // si la frame était pressée, la sortie provoque le relachement
    if (_enabled && _pressed && !contains(x, y)) {
        _pressed = false;
        released();
    }
}

void Frame::clicked()
{
}

void Frame::pressed()
{
}

void Frame::released()
{
}

void Frame::moved()
{
}

} // namespace Jgv
