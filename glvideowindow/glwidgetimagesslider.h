/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETIMAGESSLIDER_H
#define GLWIDGETIMAGESSLIDER_H

#include "glwidgetframe.h"

#include <filesystem>
#include <functional>
#include <memory>
#include <string>
#include <vector>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

class ImagesSliderPrivate;
class ImagesSlider : public Frame {

public:
    ImagesSlider(float backgroundDepth, GLPainters* painters, std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc);
    ~ImagesSlider() override;
    ImagesSlider(const ImagesSlider&) = delete;
    ImagesSlider(ImagesSlider&&) = delete;
    auto operator=(const ImagesSlider&) -> ImagesSlider& = delete;
    auto operator=(ImagesSlider &&) -> ImagesSlider& = delete;

    void setBackgroundColor(const Color& color);

    void setPath(std::filesystem::path path);
    void setSelectfunction(std::function<void(const std::string&)> selectImageFunction);

    [[nodiscard]] auto rectangles() const -> std::vector<std::vector<OU::RectanglePainterObject>>;
    [[nodiscard]] auto textures() const -> std::vector<std::vector<OU::TexturePainterObject>>;

    void resize(const Rect& rect) noexcept override;

    void handlePress(float x, float y) override;
    void handleRelease(float x, float y) override;
    void handleMove(float x, float y) override;

private:
    const std::unique_ptr<ImagesSliderPrivate> _impl;

}; // class ImagesSlider

} // namespace Jgv::GLWidget

#endif // GLWIDGETIMAGESSLIDER_H
