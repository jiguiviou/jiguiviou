﻿/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETRECT_H
#define GLWIDGETRECT_H

#include <tuple>

namespace Jgv::GLWidget {

using Bbox = std::tuple<float, float, float, float>;

class Rect {
    float _left { 0.F };
    float _bottom { 0.F };
    float _right { 0.F };
    float _top { 0.F };

public:
    explicit Rect() = default;
    explicit Rect(float left, float bottom, float right, float top);
    explicit Rect(float width, float height);
    virtual ~Rect() = default;
    Rect(const Rect&) = default;
    Rect(Rect&&) = default;
    auto operator=(const Rect&) -> Rect& = default;
    auto operator=(Rect &&) -> Rect& = default;

    void setLeft(float left) noexcept;
    void moveLeft(float left) noexcept;
    [[nodiscard]] auto left() const noexcept -> float;
    template <typename T>
    auto left() const noexcept -> T { return static_cast<T>(left()); }

    void setBottom(float bottom) noexcept;
    void moveBottom(float bottom) noexcept;
    [[nodiscard]] auto bottom() const noexcept -> float;
    template <typename T>
    auto bottom() const noexcept -> T { return static_cast<T>(bottom()); }

    void setRight(float right);
    [[nodiscard]] auto right() const noexcept -> float;
    template <typename T>
    auto right() const noexcept -> T { return static_cast<T>(right()); }

    void setTop(float top) noexcept;
    void moveTop(float top) noexcept;
    [[nodiscard]] auto top() const noexcept -> float;
    template <typename T>
    auto top() const noexcept -> T { return static_cast<T>(top()); }

    [[nodiscard]] auto width() const noexcept -> float;
    template <typename T>
    auto width() const noexcept -> T { return static_cast<T>(width()); }

    [[nodiscard]] auto height() const noexcept -> float;
    template <typename T>
    auto height() const noexcept -> T { return static_cast<T>(height()); }

    [[nodiscard]] auto centerX() const noexcept -> float;
    [[nodiscard]] auto centerY() const noexcept -> float;

    [[nodiscard]] auto contains(float x, float y) const -> bool;
    [[nodiscard]] auto intersect(const Rect& other) const -> bool;

    void alignLeft(const Rect& other);
    void alignCenter(const Rect& other);
    void setCenter(float x, float y);
    void translate(float x, float y);
    void scaleWidth(float k);
    void scaleKeepAspectRatio(const Rect& other);
    void scaleKeepAspectRatio(Rect&& other);

    [[nodiscard]] auto square() const noexcept -> Rect;
    [[nodiscard]] auto scaled(float k) const noexcept -> Rect;
    [[nodiscard]] auto reduced(float length) const noexcept -> Rect;

    virtual void resize(const Rect& rect) noexcept;

    auto operator+(const Rect& other) const noexcept -> Rect;

    [[nodiscard]] auto rect() const noexcept -> Rect;
    [[nodiscard]] auto tuple() const noexcept -> std::tuple<float, float, float, float>;

}; // class Frame::GLWidget

} // namespace Jgv

#endif // GLWIDGETRECT_H
