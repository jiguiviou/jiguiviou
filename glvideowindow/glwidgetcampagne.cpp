/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetcampagne.h"
#include "glwidgetcampagne_p.h"

#include "glpagesmanager.h"
#include "glwidgetcheckbox.h"
#include "glwidgetkeyboard.h"
#include "glwidgetkeypad.h"
#include "glwidgetlabel.h"
#include "glwidgettextedit.h"
#include "glwidgetvolumes.h"
#include "theme.h"

#include <filesystem>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <iostream>
#include <sstream>

namespace Jgv::GLWidget {

CampagnePrivate::CampagnePrivate(float backgroundDepth, GLPainters* const p, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : depth(backgroundDepth)
    , painters(p)
    , globalIpc(std::move(_globalIpc))
{
    title = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH, painters);
    title->setTextColor(TITLE_TEXT_COLOR);
    title->setBackgroundColor(TITLE_BACKGROUND_COLOR);

    const auto vtoConfig { globalIpc->vtoConfig() };
    if (!vtoConfig) {
        return;
    }

    const auto [x, name, cri] { vtoConfig->campagne() };

    nameEditor = std::make_unique<TextEdit>(depth + INTER_WIDGET_DEPTH, painters);
    nameEditor->reserve(EDITOR_TEXT_MAX_SIZE);
    nameEditor->setText(name);
    nameEditor->setTextColor(TITLE_TEXT_COLOR);
    nameEditor->setBackground(MAIN_BACKGROUND_COLOR);
    nameEditor->setPrefix(" nom: ", MAIN_TEXT_COLOR);

    auto setNameText = [this](std::string_view text) {
        nameEditor->setText(text);
    };
    auto getNameText = [this]() {
        return nameEditor->text();
    };

    keyboard = std::make_unique<Keyboard>(depth + INTER_WIDGET_DEPTH, painters);
    keyboard->setSetTextFunction(setNameText);
    keyboard->setGetTextFunction(getNameText);

    criEditor = std::make_unique<TextEdit>(depth + INTER_WIDGET_DEPTH, painters);
    criEditor->reserve(EDITOR_TEXT_MAX_SIZE);
    criEditor->setText(cri);
    criEditor->setTextColor(TITLE_TEXT_COLOR);
    criEditor->setBackground(MAIN_BACKGROUND_COLOR);
    criEditor->setPrefix(" CRI: ", MAIN_TEXT_COLOR);
    auto setCriText = [this](std::string_view text) {
        criEditor->setText(text);
    };
    auto getCriText = [this]() {
        return criEditor->text();
    };

    keypad = std::make_unique<Keypad>(depth + INTER_WIDGET_DEPTH, painters);
    keypad->setSetTextFunction(setCriText);
    keypad->setGetTextFunction(getCriText);

    updateTitle();

    volumes = std::make_unique<Volumes>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
}

void CampagnePrivate::updateTitle()
{
    switch (currentState) {
    case State::medium:
        title->setText("Choix du support d'enregistrement");
        break;
    case State::name:
        title->setText("Nom de la campagne");
        break;
    case State::cri:
        title->setText("Numéro de CRI");
        break;
    }
}

auto CampagnePrivate::rectangles() -> OU::RectanglePainterObjectsProxy
{
    std::vector<std::vector<OU::RectanglePainterObject>> proxy { title->rectangles() };

    switch (currentState) {
    case State::medium:
        if (volumes) {
            const auto volumesRectangles { volumes->rectangles() };
            std::copy(volumesRectangles.cbegin(), volumesRectangles.cend(), std::back_inserter(proxy));
        }
        break;
    case State::name:
        proxy.emplace_back(nameEditor->rectangles());
        proxy.emplace_back(keyboard->rectangles());
        break;
    case State::cri:
        proxy.emplace_back(criEditor->rectangles());
        proxy.emplace_back(keypad->rectangles());
        break;
    }

    return proxy;
}

auto CampagnePrivate::textures() -> OU::TexturePainterObjectsProxy
{
    switch (currentState) {
    case State::medium:
        return volumes->textures();
    case State::name:
        return { keyboard->textures() };
    case State::cri:
        return { keypad->textures() };
    }

    return {};
}

auto CampagnePrivate::texts() -> OU::TextPainterObjectProxy
{
    OU::TextPainterObjectProxy proxy { title->texts() };

    switch (currentState) {
    case State::medium:
        if (volumes) {
            const auto volumesTexts { volumes->texts() };
            std::copy(volumesTexts.cbegin(), volumesTexts.cend(), std::back_inserter(proxy));
        }
        break;
    case State::name:
        proxy.emplace_back(nameEditor->texts());
        proxy.emplace_back(keyboard->texts());
        break;
    case State::cri:
        proxy.emplace_back(criEditor->texts());
        proxy.emplace_back(keypad->texts());
        break;
    }

    return proxy;
}

void CampagnePrivate::resize(const Rect& rect) noexcept
{
    auto titleRect { rect };
    titleRect.setBottom(rect.top() - TITLE_HEIGHT);
    title->resize(titleRect);

    // claviers se réservent la surface moitié inférieure
    auto keybordSize { rect };
    keybordSize.setTop(rect.bottom() + HALF * rect.height());
    keyboard->resize(keybordSize);
    keypad->resize(keybordSize);

    // l'éditeur
    Rect editorRect { 0.F, 0.F, HALF * rect.width(), LABEL_HEIGHT };
    editorRect.setCenter(rect.centerX(), keyboard->top() + HALF * (title->bottom() - keyboard->top()));
    nameEditor->resize(editorRect);
    criEditor->resize(editorRect);

    // le widget des volumes
    if (volumes) {
        Rect volumesRect { rect };
        volumesRect.setTop(title->bottom());
        volumes->resize(volumesRect);
    }
}

auto CampagnePrivate::validateCurrentState() -> bool
{
    switch (currentState) {
    case State::medium:
        return !volumes->blockUUID().empty();
    case State::name:
        return !nameEditor->text().empty();
    case State::cri:
        return !criEditor->text().empty();
    }

    return false;
}

auto CampagnePrivate::previous(const Rect& rect) -> bool
{
    switch (currentState) {
    case State::medium:
        return true;
    case State::name:
        currentState = State::medium;
        updateTitle();
        resize(rect);
        break;
    case State::cri:
        currentState = State::name;
        updateTitle();
        resize(rect);
        break;
    }
    return false;
}

auto CampagnePrivate::next(const Rect& rect) -> bool
{
    if (!validateCurrentState()) {
        return false;
    }

    switch (currentState) {
    case State::medium:
        currentState = State::name;
        updateTitle();
        resize(rect);
        return false;
    case State::name:
        currentState = State::cri;
        updateTitle();
        resize(rect);
        break;
    case State::cri:
        currentState = State::medium;
        return true;
    }
    return false;
}

void CampagnePrivate::handlePress(float x, float y)
{
    switch (currentState) {
    case State::medium:
        volumes->handlePress(x, y);
        break;
    case State::name:
        keyboard->handlePress(x, y);
        break;
    case State::cri:
        keypad->handlePress(x, y);
        break;
    }
}

void CampagnePrivate::handleRelease(float x, float y)
{
    switch (currentState) {
    case State::medium:
        if (volumes) {
            volumes->handleRelease(x, y);
        }
        break;
    case State::name:
        keyboard->handleRelease(x, y);
        break;
    case State::cri:
        keypad->handleRelease(x, y);
        break;
    }
}

void CampagnePrivate::handleMove(float x, float y)
{
    switch (currentState) {
    case State::medium:
        if (volumes) {
            volumes->handleMove(x, y);
        }
        break;
    case State::name:
        keyboard->handleMove(x, y);
        break;
    case State::cri:
        keypad->handleMove(x, y);
        break;
    }
}

Campagne::Campagne(float backgroundDepth, GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl { std::make_unique<CampagnePrivate>(backgroundDepth, painters, std::move(globalIpc)) }
{
}

Campagne::~Campagne() = default;

auto Campagne::blockUUID() const -> std::string
{
    return (_impl->volumes) ? _impl->volumes->blockUUID() : "";
}

auto Campagne::name() const -> std::string
{
    return (_impl->nameEditor) ? _impl->nameEditor->text() : std::string {};
}

auto Campagne::cri() const -> std::string
{
    return (_impl->criEditor) ? _impl->criEditor->text() : std::string {};
}

auto Campagne::abord() const noexcept -> bool
{
    return _impl->previous(*this);
}

auto Campagne::exit() const noexcept -> bool
{
    return _impl->next(*this);
}

auto Campagne::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    return _impl->rectangles();
}

auto Campagne::textures() const -> OU::TexturePainterObjectsProxy
{
    return _impl->textures();
}

auto Campagne::texts() const -> OU::TextPainterObjectProxy
{
    return _impl->texts();
}

void Campagne::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Campagne::handlePress(float x, float y)
{
    if (_enabled) {
        _impl->handlePress(x, y);
    }
}

void Campagne::handleRelease(float x, float y)
{
    if (_enabled) {
        _impl->handleRelease(x, y);
    }
}

void Campagne::handleMove(float x, float y)
{
    if (_enabled) {
        _impl->handleMove(x, y);
    }
}

} // namespace Jgv::GLWidget
