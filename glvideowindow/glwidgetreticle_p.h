/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETRETICLEALIGN_P_H
#define GLWIDGETRETICLEALIGN_P_H

#include "glpainters.h"

#include <functional>
#include <memory>
#include <vector>

namespace Jgv {
class StreamController;
} // namespace Jgv

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

class ImagesSlider;
class Label;
class Rect;
class Texture;

struct ReticlePrivate {
    explicit ReticlePrivate(float backgroundDepth, GLPainters* p, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~ReticlePrivate();
    ReticlePrivate(const ReticlePrivate&) = delete;
    ReticlePrivate(ReticlePrivate&&) = delete;
    auto operator=(const ReticlePrivate&) -> ReticlePrivate& = delete;
    auto operator=(ReticlePrivate &&) -> ReticlePrivate& = delete;

    const float depth;
    GLPainters* const painters;
    const Channel::Type channel;
    std::shared_ptr<GlobalIPC::Object> globalIpc;
    std::shared_ptr<std::function<void(float)>> zoomFactorEventListener;
    std::shared_ptr<std::function<void(float)>> exposureValueEventListener;
    std::shared_ptr<std::function<void(float)>> gainValueEventListener;

    std::unique_ptr<Label> title;
    std::unique_ptr<Label> zoom;
    std::unique_ptr<Label> gain;
    std::unique_ptr<Label> exposure;
    std::unique_ptr<Texture> video;
    std::unique_ptr<Texture> capture;
    std::unique_ptr<ImagesSlider> slider;
    std::unique_ptr<Texture> arrowUp;
    std::unique_ptr<Texture> arrowLeft;
    std::unique_ptr<Texture> arrowDown;
    std::unique_ptr<Texture> arrowRight;
    std::unique_ptr<Texture> zoomPlus;
    std::unique_ptr<Texture> zoomMoins;
    std::unique_ptr<Texture> exposurePlus;
    std::unique_ptr<Texture> exposureMoins;
    std::unique_ptr<Texture> gainPlus;
    std::unique_ptr<Texture> gainMoins;

    std::unique_ptr<Texture> snapshot;

    auto rectangles() -> OU::RectanglePainterObjectsProxy;
    auto textures() -> OU::TexturePainterObjectsProxy;
    auto texts() -> OU::TextPainterObjectProxy;

    void resize(const Rect& rect) noexcept;

    void handlePress(float x, float y);
    void handleRelease(float x, float y);
    void handleMove(float x, float y);

}; // struct CampagnePrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETRETICLEALIGN_P_H
