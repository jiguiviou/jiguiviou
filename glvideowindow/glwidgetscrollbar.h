/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETSCROLLBAR_H
#define GLWIDGETSCROLLBAR_H

#include "glwidgetframe.h"

#include <memory>
#include <string>
#include <vector>

namespace Jgv::GLWidget {

using Color = std::tuple<float, float, float, float>;

class ScrollBarPrivate;
class ScrollBar : public Frame {

public:
    ScrollBar(float backgroundDepth, GLPainters* painters);
    ~ScrollBar() override;
    ScrollBar(const ScrollBar&) = delete;
    ScrollBar(ScrollBar&&) = delete;
    auto operator=(const ScrollBar&) -> ScrollBar& = delete;
    auto operator=(ScrollBar &&) -> ScrollBar& = delete;

    void setCursorLength(float length, float maximum);
    void scrollCursor(float scroll);
    void reset();

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsVector;

    void resize(const Rect& rect) noexcept override;

private:
    const std::unique_ptr<ScrollBarPrivate> _impl;

}; // class Texture

} // namespace Jgv::GLWidget

#endif // GLWIDGETSCROLLBAR_H
