/**************************************************************************
*   Copyright (C) 2020 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetterminal.h"
#include "glwidgetterminal_p.h"

#include "glpagesmanager.h"
#include "theme.h"

#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>

#include <iostream>

namespace Jgv::GLWidget {

constexpr float FONT_SCALE { 0.6F };

TerminalPrivate::TerminalPrivate(float backgroundDepth, GLPainters* const p, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : depth(backgroundDepth)
    , painters(p)
    , globalIpc { _globalIpc }
{
    if (auto vtoConfig { globalIpc->vtoConfig() }) {
        auto output = [this](GlobalIPC::OutputLevel level, std::string_view text) {
            messagesQueue.enqueue({ level, std::string { text } });
        };
        logOuput = std::make_shared<std::function<void(GlobalIPC::OutputLevel, std::string_view)>>(output);
        vtoConfig->addOutputListener(logOuput);
    }
}

TerminalPrivate::~TerminalPrivate() = default;

void TerminalPrivate::initializeGL()
{
    rectangle = painters->rectanglePainter.createObject();
    scrollObject = painters->textPainter.createScrollObject();
    clipObject = painters->textPainter.createClipObject();

    painters->rectanglePainter.setColor(rectangle, TERMINAL_BACKGROUND_COLOR);

    std::tuple<GlobalIPC::OutputLevel, std::string> data;
    while (messagesQueue.try_dequeue(data)) {
        const auto& [level, message] { data };
        dispatchMessage(level, message);
    }
}

void TerminalPrivate::destroyGL()
{
    for (auto object : texts) {
        painters->textPainter.destroyObject(object);
    }
    painters->rectanglePainter.destroyObject(rectangle);
    painters->textPainter.destroyScrollObject(scrollObject);
    painters->textPainter.destroyClipObject(clipObject);
}

void TerminalPrivate::dispatchMessage(GlobalIPC::OutputLevel level, std::string_view message)
{
    switch (level) {
    case GlobalIPC::OutputLevel::Title:
        addText(GlobalIPC::OutputLevel::Standard, message);
        addText(GlobalIPC::OutputLevel::Title, std::string(40, '-'));
        break;
    case GlobalIPC::OutputLevel::Loop:
        if (!texts.empty()) {
            painters->textPainter.setText(texts.back(), message);
        }
        break;
    case GlobalIPC::OutputLevel::Clear:
        for (auto object : texts) {
            painters->textPainter.destroyObject(object);
        }
        texts.clear();
        updateScrollingVector();
        break;
    case GlobalIPC::OutputLevel::End:
        finished.store(true);
        break;
    default:
        addText(level, message);
    }
}

void TerminalPrivate::addText(GlobalIPC::OutputLevel level, std::string_view message)
{
    const auto object { painters->textPainter.createObject() };
    painters->textPainter.setScaleFactor(object, FONT_SCALE);
    painters->textPainter.setClippingRectangle(object, clipObject);
    painters->textPainter.setScrollVector(object, scrollObject);
    updateScrollingVector();

    // affecte à l'objet la chaîne text
    painters->textPainter.setText(object, message);

    float red { 1.F };
    float green { 1.F };
    float blue { 1.F };

    if (level == GlobalIPC::OutputLevel::Error) {
        green = 0.F;
        blue = 0.F;
    } else if (level == GlobalIPC::OutputLevel::Title) {
        red = 0.F;
    }

    painters->textPainter.setColor(object, { red, green, blue, 1.F });

    // calcul de la position de la nouvelle chaîne : si c'est la première, on la met à l'origine,
    // sinon c'est la position de la dernière - l'interligne
    if (texts.empty()) {
        painters->textPainter.setPosition(object, textOrigin);
    } else {
        auto [originX, originY, originZ] { painters->textPainter.position(texts.back()) };
        painters->textPainter.setPosition(object, { originX, originY - painters->textPainter.interline() * FONT_SCALE, originZ });
    }

    // on ajoute l'objet à notre liste
    texts.push_back(object);
}

void TerminalPrivate::updateScrollingVector()
{
    if (!texts.empty()) {
        const auto [lastX, lastY, lastZ] { painters->textPainter.position(texts.back()) };
        const auto dY = (lastY < bottom) ? bottom - lastY + 2.F * painters->textPainter.interline() * FONT_SCALE : 0.F;
        painters->textPainter.updateScrollObjectVector(scrollObject, { 0.F, dY });
    }
}

void TerminalPrivate::resize(const Rect& rect)
{
    painters->rectanglePainter.setRectangle(rectangle, rect.tuple(), depth);

    textOrigin = { rect.left() + INTER_WIDGET_SPACE, rect.top() - painters->textPainter.interline() * FONT_SCALE, depth + INTER_LAYOUT_DEPTH };
    bottom = rect.bottom();
    auto [originX, originY, originZ] { textOrigin };

    for (const auto text : texts) {
        painters->textPainter.setPosition(text, { originX, originY, originZ });
        originY -= painters->textPainter.interline() * FONT_SCALE;
    }

    painters->textPainter.updateClipObjectRectangle(clipObject, { rect.left<int>(), rect.bottom<int>(), rect.width<int>(), rect.height<int>() });
    updateScrollingVector();
}

Terminal::Terminal(float backgroundDepth, GLPainters* const painters, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl(std::make_unique<TerminalPrivate>(backgroundDepth, painters, std::move(globalIpc)))
{
}

Terminal::~Terminal() = default;

bool Terminal::finished() const
{
    return _impl->finished;
}

void Terminal::initializeGL()
{
    _impl->initializeGL();
}

void Terminal::destroyGL()
{
    _impl->destroyGL();
}

OU::RectanglePainterObjectsVector Terminal::rectangles() const
{
    return { _impl->rectangle };
}

OU::TextPainterObjectVector Terminal::texts() const
{
    std::tuple<GlobalIPC::OutputLevel, std::string> data;
    if (_impl->messagesQueue.try_dequeue(data)) {
        const auto& [level, message] { data };
        _impl->dispatchMessage(level, message);
    }
    return _impl->texts;
}

void Terminal::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

} // namespace Jgv::GLWidget
