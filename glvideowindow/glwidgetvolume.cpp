/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetvolume.h"
#include "glwidgetvolume_p.h"

#include "glpagesmanager.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <filesystem>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <iostream>

namespace Jgv::GLWidget {

constexpr std::size_t TEXT_MAX_SIZE { 64U };
constexpr float TEXT_HEIGHT_FACTOR { 0.8F };

VolumePrivate::VolumePrivate(float backgroundDepth, GLPainters* p, std::string_view _uuid, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : depth(backgroundDepth)
    , painters(p)
    , uuid(_uuid)
    , globalIpc(std::move(_globalIpc))
{
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {

        backgroundFar = painters->rectanglePainter.createObject();
        backgroundNear = painters->rectanglePainter.createObject();
        iconBackground = painters->rectanglePainter.createObject();

        icon = painters->texturePainter.createObject();

        driveName = painters->textPainter.createObject();
        mountPoint = painters->textPainter.createObject();
        space = painters->textPainter.createObject();

        clippingText = painters->textPainter.createClipObject();

        const auto& [name, path, interface] { vtoConfig->volumeInfos(uuid) };
        const auto G { 1024.F * 1024.F * 1024.F };
        auto spaceInfos { std::filesystem::space(path) };
        std::ostringstream spaceString;
        spaceString << "libre: ";
        spaceString << std::fixed << std::setprecision(1) << spaceInfos.available / G << " sur ";
        spaceString << spaceInfos.capacity / G << " Go";

        if (interface == "usb") {
            logoTexture = painters->texturePainter.createTextureObject(GnuIntallDirs::assetsDir() + "/drive-removable.png");
        } else if (interface == "home") {
            logoTexture = painters->texturePainter.createTextureObject(GnuIntallDirs::assetsDir() + "/user-home.png");
        } else if (interface == "ata") {
            logoTexture = painters->texturePainter.createTextureObject(GnuIntallDirs::assetsDir() + "/harddrive.png");
        }
        painters->texturePainter.setTexture(icon, logoTexture);

        painters->rectanglePainter.setColor(backgroundFar, FOCUS_BACKGROUND_COLOR);
        painters->rectanglePainter.setColor(backgroundNear, MAIN_BACKGROUND_COLOR);
        painters->rectanglePainter.setColor(iconBackground, MAIN_TEXT_COLOR);

        painters->textPainter.setClippingRectangle(driveName, clippingText);
        painters->textPainter.setColor(driveName, MAIN_TEXT_COLOR);
        painters->textPainter.reserve(driveName, TEXT_MAX_SIZE);
        painters->textPainter.setText(driveName, name);

        painters->textPainter.setScaleFactor(mountPoint, TEXT_HEIGHT_FACTOR);
        painters->textPainter.setClippingRectangle(mountPoint, clippingText);
        painters->textPainter.setColor(mountPoint, MAIN_TEXT_COLOR);
        painters->textPainter.reserve(mountPoint, TEXT_MAX_SIZE);
        painters->textPainter.setText(mountPoint, path);

        painters->textPainter.setScaleFactor(space, TEXT_HEIGHT_FACTOR);
        painters->textPainter.setClippingRectangle(space, clippingText);
        painters->textPainter.setColor(space, MAIN_TEXT_COLOR);
        painters->textPainter.reserve(space, TEXT_MAX_SIZE);
        painters->textPainter.setText(space, spaceString.str());
    }
}

VolumePrivate::~VolumePrivate()
{
    painters->rectanglePainter.destroyObject(backgroundNear);
    painters->rectanglePainter.destroyObject(backgroundFar);
    painters->rectanglePainter.destroyObject(iconBackground);

    painters->texturePainter.destroyObject(icon);

    painters->textPainter.destroyObject(driveName);
    painters->textPainter.destroyObject(mountPoint);
    painters->textPainter.destroyObject(space);
    painters->textPainter.destroyClipObject(clippingText);

    if (logoTexture.index > 0U) {
        painters->texturePainter.destroyTextureObject(logoTexture);
    }
}

void VolumePrivate::resize(const Rect& rect)
{
    painters->rectanglePainter.setRectangle(backgroundFar, rect.tuple(), depth);
    painters->rectanglePainter.setRectangle(backgroundNear, rect.reduced(INTER_WIDGET_SPACE).tuple(), depth + INTER_LAYOUT_DEPTH);

    auto iconRect { rect.reduced(INTER_WIDGET_SPACE) };
    iconRect.setRight(iconRect.left() + iconRect.height());
    painters->rectanglePainter.setRectangle(iconBackground, iconRect.tuple(), depth + INTER_LAYOUT_DEPTH + INTER_LAYOUT_DEPTH);
    painters->texturePainter.setRectangle(icon, iconRect.tuple(), depth + INTER_LAYOUT_DEPTH + INTER_LAYOUT_DEPTH + INTER_LAYOUT_DEPTH);

    auto textRect { rect.reduced(INTER_WIDGET_SPACE) };
    textRect.setLeft(iconRect.right());

    auto [left, bottom, right, top] { painters->textPainter.boundingBox(driveName) };
    painters->textPainter.updateClipObjectRectangle(clippingText, textRect.tuple());
    // on centre le nom
    painters->textPainter.setPosition(driveName, { textRect.left() + HALF * (textRect.width() - (right - left)), textRect.top() - top, depth + INTER_LAYOUT_DEPTH + INTER_LAYOUT_DEPTH });
    painters->textPainter.setPosition(mountPoint, { textRect.left() + INTER_WIDGET_SPACE, textRect.top() - top - (top - bottom), depth + INTER_LAYOUT_DEPTH + INTER_LAYOUT_DEPTH });
    painters->textPainter.setPosition(space, { textRect.left() + INTER_WIDGET_SPACE, textRect.top() - top - (top - bottom) - (top - bottom), depth + INTER_LAYOUT_DEPTH + INTER_LAYOUT_DEPTH });
}

Volume::Volume(float backgroundDepth, GLPainters* const painters, std::string_view uuid, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl(std::make_unique<VolumePrivate>(backgroundDepth, painters, uuid, std::move(globalIpc)))
{
}

Volume::~Volume() = default;

auto Volume::uuid() const -> std::string
{
    return _impl->uuid;
}

void Volume::setClickFunction(std::function<void(void)> function)
{
    _impl->clicked = std::move(function);
}

auto Volume::rectangles() const -> OU::RectanglePainterObjectsVector
{
    return isActived() ? std::vector<OU::RectanglePainterObject> { _impl->backgroundFar, _impl->backgroundNear, _impl->iconBackground } : std::vector<OU::RectanglePainterObject> { _impl->backgroundNear, _impl->iconBackground };
}

auto Volume::texts() const -> OU::TextPainterObjectVector
{
    return { _impl->driveName, _impl->mountPoint, _impl->space };
}

auto Volume::textures() const -> OU::TexturePainterObjectsVector
{
    return { _impl->icon };
}

void Volume::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Volume::clicked()
{
    if (_impl->clicked) {
        _impl->clicked();
    }
    setActived(true);
}

} // namespace Jgv::GLWidget
