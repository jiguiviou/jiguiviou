/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetkeyspecial.h"

#include "glpagesmanager.h"
#include "theme.h"

namespace Jgv::GLWidget {

KeySpecial::KeySpecial(float backgroundDepth, const std::string& iconFile, GLPainters* const painters)
    : _depth(backgroundDepth)
    , _painters(painters)
{
    _backgroundObject = _painters->rectanglePainter.createObject();
    _iconObject = _painters->texturePainter.createObject();
    _iconTextureObject = _painters->texturePainter.createTextureObject(iconFile);

    _painters->rectanglePainter.setColor(_backgroundObject, KEY_BACKGROUND_COLOR);
    _painters->texturePainter.setTexture(_iconObject, _iconTextureObject);
}

KeySpecial::~KeySpecial()
{
    _painters->rectanglePainter.destroyObject(_backgroundObject);
    _painters->texturePainter.destroyObject(_iconObject);
    _painters->texturePainter.destroyTextureObject(_iconTextureObject);
}

void KeySpecial::setClickedFunction(std::function<void(void)> function)
{
    _clicked = std::move(function);
}

OU::TexturePainterObject KeySpecial::textureObject() const
{
    return _iconObject;
}

OU::RectanglePainterObject KeySpecial::rectangleObject() const
{
    return _backgroundObject;
}

void KeySpecial::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _painters->rectanglePainter.setRectangle(_backgroundObject, rect.tuple(), _depth);
    _painters->texturePainter.setRectangle(_iconObject, rect.square().tuple(), _depth + INTER_LAYOUT_DEPTH);
}

void KeySpecial::clicked()
{
    if (_clicked) {
        _clicked();
    }
}

} // namespace Jgv
