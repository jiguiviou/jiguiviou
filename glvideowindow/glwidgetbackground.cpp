/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetbackground.h"
#include "glwidgetbackground_p.h"

#include "glpainters.h"
#include "theme.h"

namespace Jgv::GLWidget {

BackgroundPrivate::BackgroundPrivate(float backgroundDepth, GLPainters* const p)
    : depth(backgroundDepth)
    , painters(p)
{
    backgroundObject = painters->rectanglePainter.createObject();
    painters->rectanglePainter.setColor(backgroundObject, GLWidget::MAIN_BACKGROUND_COLOR);
}

BackgroundPrivate::~BackgroundPrivate()
{
    painters->rectanglePainter.destroyObject(backgroundObject);
}

void BackgroundPrivate::resize(const Rect& rect)
{
    painters->rectanglePainter.setRectangle(backgroundObject, rect.tuple(), depth);
}

Background::Background(float backgroundDepth, GLPainters* const painters)
    : _impl(std::make_unique<BackgroundPrivate>(backgroundDepth, painters))
{
}

Background::~Background() = default;

void Background::setBackgroundColor(const Color& color)
{
    _impl->painters->rectanglePainter.setColor(_impl->backgroundObject, color);
}

std::vector<OU::RectanglePainterObject> Background::rectangles() const
{
    return { _impl->backgroundObject };
}

void Background::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

} // namespace Jgv::GLWidget
