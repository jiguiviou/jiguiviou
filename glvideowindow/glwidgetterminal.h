/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETTERMINAL_H
#define GLWIDGETTERMINAL_H

#include "glwidgetframe.h"

#include <memory>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

class TerminalPrivate;
class Terminal final : public Frame {

public:
    Terminal(float backgroundDepth, GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~Terminal() final;
    Terminal(const Terminal&) = delete;
    Terminal(Terminal&&) = delete;
    auto operator=(const Terminal&) -> Terminal& = delete;
    auto operator=(Terminal &&) -> Terminal& = delete;

    [[nodiscard]] auto finished() const -> bool;

    void initializeGL();
    void destroyGL();
    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsVector;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectVector;

    void resize(const Rect& rect) noexcept override;

private:
    const std::unique_ptr<TerminalPrivate> _impl;

}; // class Terminal

} // namespace Jgv::GLWidget

#endif // GLWIDGETTERMINAL_H
