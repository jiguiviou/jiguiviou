/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETBUTTON_H
#define GLWIDGETBUTTON_H

#include "glwidgetframe.h"

#include <functional>
#include <memory>

namespace Jgv::GLWidget {

class ButtonPrivate;
class Button : public Frame {
public:
    explicit Button(float backgroundDepth, GLPainters* painters);
    ~Button() override;
    Button(const Button&) = delete;
    Button(Button&&) = delete;
    auto operator=(const Button&) -> Button& = delete;
    auto operator=(Button &&) -> Button& = delete;

    void setClickedFunction(std::function<void(void)> clicked);

    void initialize(const std::string& logoFileName, const std::string& backgroundOffFileName, const std::string& backgroundOnFileName);

    [[nodiscard]] auto textures() const -> std::vector<OU::TexturePainterObject>;

    void setActived(bool actived) noexcept override;
    void setBlinking(bool blinking) noexcept;
    void resize(const Rect& rect) noexcept override;

private:
    void clicked() override;
    void pressed() override;
    void released() override;
    void moved() override;
    const std::unique_ptr<ButtonPrivate> _impl;

}; // class Button

} // namespace Jgv

#endif // GLWIDGETBUTTON_H
