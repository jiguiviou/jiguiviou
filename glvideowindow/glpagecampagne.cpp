/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glpagecampagne.h"
#include "glpagecampagne_p.h"

#include "glbuttonscolumn.h"
#include "glpagesmanager.h"
#include "glwidgetbackground.h"
#include "glwidgetbutton.h"
#include "glwidgetcampagne.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <chrono>
#include <configfile/configfile.h>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <iomanip>
#include <sstream>

#include <iostream>

namespace {
auto nameFromDate() -> std::string
{
    const auto now { std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()) };
    const auto tm { *(std::gmtime(&now)) };
    std::ostringstream stream;
    stream << std::put_time(&tm, "%Y-%m-%d_%H-%M-%S");
    return stream.str();
}
} // namespace

namespace Jgv::GLPage {

CampagnePrivate::CampagnePrivate(GLPainters* p, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : BasePrivate(p, std::move(_globalIpc))
    , leftColumn { std::make_unique<GLWidget::ButtonsColumn>() }
    , rightColumn { std::make_unique<GLWidget::ButtonsColumn>() }
{
}

void CampagnePrivate::show()
{
    if (!background) {
        background = std::make_unique<GLWidget::Background>(GLWidget::WIDGET_BACKGROUND_DEPTH, painters);
    }

    if (!campagneWidget) {
        campagneWidget = std::make_unique<GLWidget::Campagne>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters, globalIpc);
    }

    if (!prevButton) {
        prevButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        prevButton->initialize(GnuIntallDirs::assetsDir() + "/previous.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto abord = [this]() {
            if (campagneWidget && campagneWidget->abord()) {
                manager->showParent(managerId);
            }
        };
        prevButton->setClickedFunction(abord);
    }

    if (!nextButton) {
        nextButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        nextButton->initialize(GnuIntallDirs::assetsDir() + "/next.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto exit = [this]() {
            if (campagneWidget && campagneWidget->exit()) {
                if (auto vtoConfig { globalIpc->vtoConfig() }) {
                    vtoConfig->setCampagne({ campagneWidget->blockUUID(), campagneWidget->name(), campagneWidget->cri() });
                }
                manager->showParent(managerId);
            }
        };
        nextButton->setClickedFunction(exit);
    }
}

void CampagnePrivate::hide()
{
    destroyGL();
}

void CampagnePrivate::destroyGL() noexcept
{
    campagneWidget.reset(nullptr);
    prevButton.reset(nullptr);
    nextButton.reset(nullptr);
    background.reset(nullptr);
}

auto CampagnePrivate::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    if (background && campagneWidget) {
        auto rectangles { campagneWidget->rectangles() };
        rectangles.emplace_back(background->rectangles());
        return { rectangles };
    }
    return {};
}

auto CampagnePrivate::texts() const -> OU::TextPainterObjectProxy
{
    if (campagneWidget) {
        return campagneWidget->texts();
    }
    return {};
}

auto CampagnePrivate::textures() const -> OU::TexturePainterObjectsProxy
{
    if (campagneWidget && prevButton && nextButton) {
        auto proxy { campagneWidget->textures() };
        proxy.emplace_back(prevButton->textures());
        proxy.emplace_back(nextButton->textures());
        return proxy;
    }
    return {};
}

void CampagnePrivate::resize(float width, float height)
{
    // maintient le ratio 1:1 des colonnes de boutons
    leftColumn->resize(GLWidget::Rect { 0.F, 0.F, height / leftColumn->cellsCount(), height });
    rightColumn->resize(GLWidget::Rect { width - height / rightColumn->cellsCount(), 0.F, width, height });

    if (background) {
        background->resize(GLWidget::Rect { 0.F, 0.F, width, height });
    }
    if (campagneWidget) {
        const GLWidget::Rect centralRect(leftColumn->right(), 0.F, rightColumn->left(), height);
        campagneWidget->resize(centralRect);
    }
    if (prevButton) {
        prevButton->resize(leftColumn->rectOf(0U));
    }
    if (nextButton) {
        nextButton->resize(rightColumn->rectOf(0U));
    }
}

void CampagnePrivate::handlePress(float x, float y)
{
    if (campagneWidget) {
        campagneWidget->handlePress(x, y);
    }
    if (prevButton) {
        prevButton->handlePress(x, y);
    }
    if (nextButton) {
        nextButton->handlePress(x, y);
    }
}

void CampagnePrivate::handleRelease(float x, float y)
{
    if (campagneWidget) {
        campagneWidget->handleRelease(x, y);
    }
    if (prevButton) {
        prevButton->handleRelease(x, y);
    }
    if (nextButton) {
        nextButton->handleRelease(x, y);
    }
}

void CampagnePrivate::handleMove(float x, float y)
{
    if (campagneWidget) {
        campagneWidget->handleMove(x, y);
    }
    if (prevButton) {
        prevButton->handleMove(x, y);
    }
    if (nextButton) {
        nextButton->handleMove(x, y);
    }
}

Campagne::Campagne(GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : Base(std::make_unique<CampagnePrivate>(painters, globalIpc))
{
}

} // namespace Jgv::GLPage
