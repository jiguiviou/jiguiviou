get_filename_component(SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(${SELF_DIR}/GlVideoWindow.cmake)
get_filename_component(glvideowindow_INCLUDE_DIRS "${SELF_DIR}/../../include/glvideowindow" ABSOLUTE)

message(STATUS "GlVideoWindowConfig: include : ${glvideowindow_INCLUDE_DIRS}")
