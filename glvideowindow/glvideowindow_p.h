/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLVIDEOWINDOW_P_H
#define GLVIDEOWINDOW_P_H

#include "glpagesmanager.h"
#include "glpainters.h"

#include <atomic>
#include <memory>
#include <unordered_map>

namespace Jgv::GLWidget {
class Terminal;
class Label;
class Texture;
} // namespace Jgv::GLWidget

namespace Jgv::GlobalIPC {
class Object;
enum class ClockState;
enum class OutputLevel;
} // namespace Jgv::GlobalIPC

namespace Jgv {

class ExternalTexture {
    struct _ExternalTexture {
        GLuint id;
        float ratio;
    };

    std::atomic<_ExternalTexture> _reg {};
    _ExternalTexture _to {};

public:
    void set(GLuint id, float value) noexcept
    {
        _reg.store({ id, value });
    }
    auto changed() noexcept
    {
        return !_reg.compare_exchange_weak(_to, _reg.load(), std::memory_order_relaxed);
    }
    [[nodiscard]] auto value() const noexcept -> _ExternalTexture
    {
        return _to;
    }
};

struct GLVideoWindowPrivate {
    explicit GLVideoWindowPrivate(std::shared_ptr<GlobalIPC::Object> globalIpc);
    const std::shared_ptr<GlobalIPC::Object> globalIpc;

    std::shared_ptr<std::function<void(GlobalIPC::OutputLevel, std::string_view)>> logOuput;
    std::shared_ptr<std::function<void(std::tuple<uint32_t, uint32_t, uint32_t>)>> updateTextureFunction;
    std::shared_ptr<std::function<void(bool)>> syncFunction;
    std::shared_ptr<std::function<void(uint64_t)>> triggerDelayFunction;
    std::shared_ptr<std::function<void(GlobalIPC::ClockState, std::string_view)>> sourceClockFunction;
    std::shared_ptr<std::function<void(GlobalIPC::ClockState, std::string_view)>> clockFunction;

    OU::TexturePainterObject background { 0U };
    OU::TextPainterObject sourceClock { 0U };
    OU::TextPainterObject clock { 0U };
    OU::TextPainterObject ptp { 0U };
    OU::TextPainterObject triggerDelay { 0U };
    std::shared_ptr<GLWidget::Terminal> terminal;
    GLPainters painters;
    GLPage::Manager manager;

    ExternalTexture externalTexture {};

    bool haveVtoConfig { true };
    bool pressed = false;

    void initializeGL();
    void resizeVideo(float width, float height, float ratio);
    void resizeGL(float width, float height);
    void paintGL(float width, float height);
};

} // namespace Jgv

#endif // GLVIDEOWINDOW_P_H
