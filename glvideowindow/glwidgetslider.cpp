/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetslider.h"
#include "glwidgetslider_p.h"

#include "glpagesmanager.h"
#include "theme.h"

namespace Jgv::GLWidget {

SliderPrivate::SliderPrivate(float backgroundDepth, GLPainters* const p)
    : depth(backgroundDepth)
    , painters(p)
{
    farLine = painters->rectanglePainter.createObject();
    nearLine = painters->rectanglePainter.createObject();
    farCursor = painters->rectanglePainter.createObject();
    nearCursor = painters->rectanglePainter.createObject();
    scroll = painters->rectanglePainter.createScrollObject();

    painters->rectanglePainter.setColor(farLine, DECORATION_COLOR);
    painters->rectanglePainter.setColor(nearLine, SLIDER_BACKGROUND_COLOR);
    painters->rectanglePainter.setColor(farCursor, DECORATION_COLOR);
    painters->rectanglePainter.setColor(nearCursor, SLIDER_BACKGROUND_COLOR);

    painters->rectanglePainter.setScrollObject(farCursor, scroll);
    painters->rectanglePainter.setScrollObject(nearCursor, scroll);
}

SliderPrivate::~SliderPrivate()
{
    painters->rectanglePainter.destroyScrollObject(scroll);
    painters->rectanglePainter.destroyObject(nearCursor);
    painters->rectanglePainter.destroyObject(farCursor);
    painters->rectanglePainter.destroyObject(nearLine);
    painters->rectanglePainter.destroyObject(farLine);
}

void SliderPrivate::resize(const Rect& rect)
{
    auto cursorRect { rect };
    cursorRect.setRight(cursorRect.left() + cursorRect.height());

    const auto lineHeight { 3.F * DECORATION_WIDTH };
    auto lineRect { rect };
    lineRect.setLeft(rect.left() + 0.5F * cursorRect.width());
    lineRect.setRight(rect.right() - 0.5F * cursorRect.width());
    lineRect.setBottom(rect.bottom() + 0.5F * (rect.height() - lineHeight));
    lineRect.setTop(lineRect.bottom() + lineHeight);
    painters->rectanglePainter.setRectangle(farLine, lineRect.tuple(), depth);
    painters->rectanglePainter.setRectangle(nearLine, lineRect.reduced(DECORATION_WIDTH).tuple(), depth + INTER_LAYOUT_DEPTH);
    painters->rectanglePainter.setRectangle(farCursor, cursorRect.tuple(), depth + INTER_LAYOUT_DEPTH + INTER_LAYOUT_DEPTH);
    painters->rectanglePainter.setRectangle(nearCursor, cursorRect.reduced(DECORATION_WIDTH).tuple(), depth + INTER_LAYOUT_DEPTH + INTER_LAYOUT_DEPTH + INTER_LAYOUT_DEPTH);

    updateScroll(rect);
}

void SliderPrivate::updateScroll(const Rect& rect)
{
    const auto min { rect.left() + 0.5F * rect.height() };
    const auto max { rect.right() - 0.5F * rect.height() };
    painters->rectanglePainter.updateScrollVector(scroll, value * (max - min) / 100.F, 0.F);
}

void SliderPrivate::updateScroll(const Rect& rect, float x)
{
    const auto min { rect.left() + 0.5F * rect.height() };
    const auto max { rect.right() - 0.5F * rect.height() };

    painters->rectanglePainter.updateScrollVector(scroll, std::clamp(x, min, max) - rect.left() - 0.5F * rect.height(), 0.F);

    if (slideFunction) {
        slideFunction(std::clamp((x - min) / (max - min), 0.F, 1.F));
    }
}

Slider::Slider(float backgroundDepth, GLPainters* const painters)
    : _impl(std::make_unique<SliderPrivate>(backgroundDepth, painters))
{
}

Slider::~Slider() = default;

void Slider::setSlideFunction(std::function<void(float)> function)
{
    _impl->slideFunction = std::move(function);
}

void Slider::setValue(float value)
{
    _impl->value = std::clamp(value, 0.F, 1.F);
    _impl->updateScroll(*this);
}

auto Slider::rectangles() const -> OU::RectanglePainterObjectsVector
{
    return { _impl->nearCursor, _impl->farCursor, _impl->nearLine, _impl->farLine };
}

void Slider::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Slider::handlePress(float x, float y)
{
    Frame::handlePress(x, y);
    if (_enabled && _pressed) {
        _impl->updateScroll(*this, x);
    }
}

void Slider::handleMove(float x, float y)
{
    Frame::handleMove(x, y);
    if (_enabled && _pressed) {
        _impl->updateScroll(*this, x);
    }
}

} // namespace Jgv::GLWidget
