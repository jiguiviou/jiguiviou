/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETRADIO_P_H
#define GLWIDGETRADIO_P_H

#include "glpainters.h"

#include <functional>

namespace Jgv {
class Rect;
} // namespace Jgv

namespace Jgv::GLWidget {

struct RadioPrivate {
    RadioPrivate(float backgroundDepth, GLPainters* p);
    ~RadioPrivate();
    RadioPrivate(const RadioPrivate&) = delete;
    RadioPrivate(RadioPrivate&&) = delete;
    auto operator=(const RadioPrivate&) -> RadioPrivate& = delete;
    auto operator=(RadioPrivate&&) -> RadioPrivate& = delete;

    const float depth;
    GLPainters* const painters;

    OU::TextPainterObject textObject { 0U };
    OU::RectanglePainterObject backgroundFar { 0U };
    OU::RectanglePainterObject backgroundNear { 0U };
    OU::TextClipObject textClipObject { 0U };

    std::function<void(void)> clicked;
    std::function<void(void)> uncheckAll;

    void resize(const Rect& rect);

}; // struct RadioPrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETRADIO_P_H
