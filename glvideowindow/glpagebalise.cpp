/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glpagebalise.h"
#include "glpagebalise_p.h"

#include "glbuttonscolumn.h"
#include "glpagesmanager.h"
#include "glwidgetbackground.h"
#include "glwidgetbalise.h"
#include "glwidgetbutton.h"
#include "gnuinstalldir.h"

#include <globalipc/globalipc.h>

namespace Jgv::GLPage {

BalisePrivate::BalisePrivate(GLPainters* p, Channel::Type _channel, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : BasePrivate(p, std::move(_globalIpc))
    , channel(_channel)
    , leftColumn { std::make_unique<GLWidget::ButtonsColumn>() }
    , rightColumn { std::make_unique<GLWidget::ButtonsColumn>() }
{
}

void BalisePrivate::show()
{
    if (!background) {
        background = std::make_unique<GLWidget::Background>(GLWidget::WIDGET_BACKGROUND_DEPTH, painters);
    }

    if (!baliseWidget) {
        baliseWidget = std::make_unique<GLWidget::Balise>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters, channel, globalIpc);
    }

    if (!exitButton) {
        exitButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        exitButton->initialize(GnuIntallDirs::assetsDir() + "/valid.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto exit = [this]() {
            manager->showParent(managerId);
        };
        exitButton->setClickedFunction(exit);
    }
}

void BalisePrivate::hide()
{
    destroyGL();
}

void BalisePrivate::destroyGL() noexcept
{
    baliseWidget.reset(nullptr);
    exitButton.reset(nullptr);
    background.reset(nullptr);
}

auto BalisePrivate::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    if (background && baliseWidget) {
        auto proxy { baliseWidget->rectangles() };
        proxy.emplace_back(background->rectangles());
        return proxy;
    }
    return {};
}

auto BalisePrivate::texts() const -> OU::TextPainterObjectProxy
{
    if (baliseWidget) {
        return baliseWidget->texts();
    }
    return {};
}

auto BalisePrivate::textures() const -> OU::TexturePainterObjectsProxy
{
    if (baliseWidget && exitButton) {
        auto proxy { baliseWidget->textures() };
        proxy.emplace_back(exitButton->textures());
        return proxy;
    }
    return {};
}

void BalisePrivate::resize(float width, float height)
{
    leftColumn->resize(GLWidget::Rect { 0.F, 0.F, height / leftColumn->cellsCount(), height });
    rightColumn->resize(GLWidget::Rect { width - height / rightColumn->cellsCount(), 0.F, width, height });

    if (background) {
        background->resize(GLWidget::Rect { 0.F, 0.F, width, height });
    }

    if (baliseWidget) {
        baliseWidget->resize(GLWidget::Rect { leftColumn->right(), 0.F, rightColumn->left(), height });
    }

    if (exitButton) {
        exitButton->resize(rightColumn->rectOf(0U));
    }
}

void BalisePrivate::handlePress(float x, float y)
{
    if (baliseWidget) {
        baliseWidget->handlePress(x, y);
    }

    if (exitButton) {
        exitButton->handlePress(x, y);
    }
}

void BalisePrivate::handleRelease(float x, float y)
{
    if (baliseWidget) {
        baliseWidget->handleRelease(x, y);
    }

    if (exitButton) {
        exitButton->handleRelease(x, y);
    }
}

void BalisePrivate::handleMove(float x, float y)
{
    if (baliseWidget) {
        baliseWidget->handleMove(x, y);
    }

    if (exitButton) {
        exitButton->handleMove(x, y);
    }
}

Balise::Balise(GLPainters* painters, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : Base(std::make_unique<BalisePrivate>(painters, channel, globalIpc))
{
}

} // namespace Jgv
