/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETRETICLEALIGN_H
#define GLWIDGETRETICLEALIGN_H

#include "glwidgetframe.h"

#include <memory>
#include <vector>

namespace Jgv::Gvsp::QtBackend {
class ReceiverConfig;
} // namespace Jgv::Gvsp::QtBackend

namespace Jgv {
class StreamController;
} // namespace Jgv

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

using ConfigPtr = std::weak_ptr<Gvsp::QtBackend::ReceiverConfig>;
using StreamControllerPtr = std::weak_ptr<StreamController>;

struct ReticlePrivate;
class Reticle : public Frame {
public:
    explicit Reticle(float backgroundDepth, GLPainters* painters, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~Reticle() override;
    Reticle(const Reticle&) = delete;
    Reticle(Reticle&&) = delete;
    auto operator=(const Reticle&) -> Reticle& = delete;
    auto operator=(Reticle &&) -> Reticle& = delete;

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsProxy;
    [[nodiscard]] auto textures() const -> OU::TexturePainterObjectsProxy;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectProxy;

    void resize(const Rect& rect) noexcept override;

    void handlePress(float x, float y) override;
    void handleRelease(float x, float y) override;
    void handleMove(float x, float y) override;

private:
    const std::unique_ptr<ReticlePrivate> _impl;

}; // class central

} // namespace Jgv

#endif // GLWIDGETRETICLEALIGN_H
