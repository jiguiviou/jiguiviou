/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLPAGEBASE_P_H
#define GLPAGEBASE_P_H

#include "glpainters.h"
#include "glvideowindow.h"

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLPage {

class Manager;
struct BasePrivate {
public:
    explicit BasePrivate(GLPainters* p, std::shared_ptr<GlobalIPC::Object> _globalIpc);
    virtual ~BasePrivate() = default;
    BasePrivate(const BasePrivate&) = delete;
    BasePrivate(BasePrivate&&) = delete;
    auto operator=(const BasePrivate&) -> BasePrivate& = delete;
    auto operator=(BasePrivate &&) -> BasePrivate& = delete;

    GLPainters* const painters;
    std::shared_ptr<GlobalIPC::Object> globalIpc;
    std::function<void(bool)> exitFunction { nullptr };
    Manager* manager { nullptr };
    uint64_t managerId { 0 };
    bool isVisible { false };
    const std::array<char, 7> pad {};

    [[nodiscard]] virtual auto rectangles() const -> OU::RectanglePainterObjectsProxy = 0;
    [[nodiscard]] virtual auto texts() const -> OU::TextPainterObjectProxy = 0;
    [[nodiscard]] virtual auto textures() const -> OU::TexturePainterObjectsProxy = 0;

    virtual void show() = 0;
    virtual void hide() = 0;
    virtual void destroyGL() noexcept = 0;

    virtual void handlePress(float x, float y) = 0;
    virtual void handleRelease(float x, float y) = 0;
    virtual void handleMove(float x, float y) = 0;

    virtual void setManager(uint64_t id, Manager* m);
    virtual void resize(float width, float height) = 0;

}; // struct BasePrivate

} // namespace Jgv

#endif // GLPAGEBASE_P_H
