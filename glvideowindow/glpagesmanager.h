/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLPAGESMANAGER_H
#define GLPAGESMANAGER_H

#include "glpainters.h"
#include "glvideowindow.h"

#include <memory>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLPage {

class Base;

class ManagerPrivate;
class Manager {
public:
    explicit Manager(std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~Manager();
    Manager(const Manager&) = delete;
    Manager(Manager&&) = delete;
    auto operator=(const Manager&) -> Manager& = delete;
    auto operator=(Manager &&) -> Manager& = delete;

    void initialize(GLPainters* painters);
    auto registerPage(uint64_t from, std::unique_ptr<Base>&& page) -> uint64_t;

    void show(uint64_t id);
    void showParent(uint64_t id);
    void hideAll();
    void destroyGL() noexcept;

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsProxy;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectProxy;
    [[nodiscard]] auto textures() const -> OU::TexturePainterObjectsProxy;

    void resize(float width, float height);
    void mousePressEvent(float x, float y);
    void mouseReleaseEvent(float x, float y);
    void mouseMoveEvent(float x, float y);

private:
    const std::unique_ptr<ManagerPrivate> _impl;

}; // class ButtonsColumn

} // namespace Jgv::GLPage

#endif // GLPAGESMANAGER_H
