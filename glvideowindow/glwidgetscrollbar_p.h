/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETSCROLLBAR_P_H
#define GLWIDGETSCROLLBAR_P_H

#include "glpainters.h"

namespace Jgv {
class Rect;
} // namespace Jgv

namespace Jgv::GLWidget {

struct ScrollBarPrivate {
    ScrollBarPrivate(float backgroundDepth, GLPainters* p);
    ~ScrollBarPrivate();
    ScrollBarPrivate(const ScrollBarPrivate&) = delete;
    ScrollBarPrivate(ScrollBarPrivate&&) = delete;
    auto operator=(const ScrollBarPrivate&) -> ScrollBarPrivate& = delete;
    auto operator=(ScrollBarPrivate &&) -> ScrollBarPrivate& = delete;

    const float depth;
    GLPainters* const painters;

    OU::RectanglePainterObject cursorObject { 0U };
    OU::RectangleClippingObject clipObject { 0U };
    OU::RectangleScrollObject scrollObject { 0U };

    float scroll { 0.F };

}; // struct ScrollBarPrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETSCROLLBAR_P_H
