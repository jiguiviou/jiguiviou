﻿/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETFRAME_H
#define GLWIDGETFRAME_H

#include "glpainters.h"
#include "glwidgetrect.h"

using Color = std::tuple<float, float, float, float>;

namespace Jgv::GLWidget {

class Frame : public Rect {
protected:
    bool _enabled { true };
    bool _actived { false };
    bool _pressed { false };

public:
    [[nodiscard]] auto backgroundDepth() const noexcept -> float;

    void setEnabled(bool enabled) noexcept;
    [[nodiscard]] auto isEnabled() const noexcept -> bool;

    virtual void setActived(bool actived) noexcept;
    [[nodiscard]] auto isActived() const noexcept -> bool;

    virtual void handlePress(float x, float y);
    virtual void handleRelease(float x, float y);
    virtual void handleMove(float x, float y);

protected:
    virtual void clicked();
    virtual void pressed();
    virtual void released();
    virtual void moved();

}; // class Frame

} // namespace Jgv::GLWidget

#endif // GLWIDGETFRAME_H
