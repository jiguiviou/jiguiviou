/***************************************************************************
*   Copyright (C) 2014-2017 by Cyril BALETAUD *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetvolumes.h"
#include "glwidgetvolumes_p.h"

#include "glwidgetvolume.h"
#include "theme.h"

#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <iostream>

namespace Jgv::GLWidget {

constexpr float VOLUME_HEIGHT { 180.F };

VolumesPrivate::VolumesPrivate(float backgroundDepth, GLPainters* const painters, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : depth(backgroundDepth)
    , painters(painters)
    , globalIpc(std::move(_globalIpc))
{
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        auto uuids { vtoConfig->volumesUUID() };
        const auto [current, x, y] { vtoConfig->campagne() };

        for (auto& uuid : uuids) {
            auto volume { std::make_unique<Volume>(depth + INTER_WIDGET_DEPTH, painters, uuid, globalIpc) };
            if (current == uuid) {
                volume->setActived(true);
            }

            auto clicked = [this]() {
                for (auto& v : volumes) {
                    v->setActived(false);
                }
            };
            volume->setClickFunction(clicked);
            volumes.emplace_back(std::move(volume));
        }
    }
}

void VolumesPrivate::resize(const Rect& rect)
{
    auto volumeRect { rect };
    volumeRect.setBottom(volumeRect.top() - VOLUME_HEIGHT);
    volumeRect.translate(0.F, -INTER_WIDGET_SPACE);

    for (auto& volume : volumes) {
        volume->resize(volumeRect);
        volumeRect.translate(0.F, -VOLUME_HEIGHT);
        volumeRect.translate(0.F, -INTER_WIDGET_SPACE);
    }
}

auto VolumesPrivate::rectangles() -> OU::RectanglePainterObjectsProxy
{
    std::vector<std::vector<OU::RectanglePainterObject>> proxy;
    std::transform(volumes.cbegin(), volumes.cend(), std::back_inserter(proxy),
        [](const auto& value) -> std::vector<OU::RectanglePainterObject> { return value->rectangles(); });
    return proxy;
}

auto VolumesPrivate::texts() -> OU::TextPainterObjectProxy
{
    OU::TextPainterObjectProxy proxy;
    std::transform(volumes.cbegin(), volumes.cend(), std::back_inserter(proxy),
        [](const auto& value) -> std::vector<OU::TextPainterObject> { return value->texts(); });
    return proxy;
}

auto VolumesPrivate::textures() -> OU::TexturePainterObjectsProxy
{
    OU::TexturePainterObjectsProxy proxy;
    std::transform(volumes.cbegin(), volumes.cend(), std::back_inserter(proxy),
        [](const auto& value) -> std::vector<OU::TexturePainterObject> { return value->textures(); });
    return proxy;
}

Volumes::Volumes(float backgroundDepth, GLPainters* const painters, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl(std::make_unique<VolumesPrivate>(backgroundDepth, painters, std::move(globalIpc)))
{
}

Volumes::~Volumes() = default;

auto Volumes::blockUUID() const -> std::string
{
    const auto it { std::find_if(_impl->volumes.cbegin(), _impl->volumes.cend(), [](const auto& volume) {
        return volume->isActived();
    }) };
    return (it != _impl->volumes.cend()) ? (*it)->uuid() : "";
}

auto Volumes::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    return _impl->rectangles();
}

auto Volumes::texts() const -> OU::TextPainterObjectProxy
{
    return _impl->texts();
}

auto Volumes::textures() const -> OU::TexturePainterObjectsProxy
{
    return _impl->textures();
}

void Volumes::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Volumes::handlePress(float x, float y)
{
    if (_enabled) {
        for (const auto& volume : _impl->volumes) {
            volume->handlePress(x, y);
        }
    }
}

void Volumes::handleRelease(float x, float y)
{
    if (_enabled) {
        for (const auto& volume : _impl->volumes) {
            volume->handleRelease(x, y);
        }
    }
}

void Volumes::handleMove(float x, float y)
{
    if (_enabled) {
        for (const auto& volume : _impl->volumes) {
            volume->handleMove(x, y);
        }
    }
}

} // namespace Jgv::GLWidget
