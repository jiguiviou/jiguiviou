/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glpagereticle.h"
#include "glpagereticle_p.h"

#include "glbuttonscolumn.h"
#include "glpagesmanager.h"
#include "glwidgetbackground.h"
#include "glwidgetbutton.h"
#include "glwidgetreticle.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <iostream>

namespace Jgv::GLPage {

ReticlePrivate::ReticlePrivate(Channel::Type c, GLPainters* p, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : BasePrivate(p, std::move(_globalIpc))
    , channel(c)
    , leftColumn { std::make_unique<GLWidget::ButtonsColumn>() }
    , rightColumn { std::make_unique<GLWidget::ButtonsColumn>() }
{
}

void ReticlePrivate::show()
{
    if (!background) {
        background = std::make_unique<GLWidget::Background>(GLWidget::WIDGET_BACKGROUND_DEPTH, painters);
    }

    if (!reticleWidget) {
        reticleWidget = std::make_unique<GLWidget::Reticle>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters, channel, globalIpc);
    }

    if (!exitButton) {
        exitButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        exitButton->initialize(GnuIntallDirs::assetsDir() + "/valid.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        exitButton->setClickedFunction([this]() { manager->showParent(managerId); });
    }

    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        vtoConfig->loadVideoMode(channel, GlobalIPC::VideoMode::Reticle);
    }
}

void ReticlePrivate::hide()
{
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        vtoConfig->saveVideoMode(channel,GlobalIPC::VideoMode::Reticle);
        vtoConfig->loadVideoMode(channel, GlobalIPC::VideoMode::Main);
    }
    destroyGL();
}

void ReticlePrivate::destroyGL() noexcept
{
    reticleWidget.reset(nullptr);
    exitButton.reset(nullptr);
    background.reset(nullptr);
}

auto ReticlePrivate::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    if (reticleWidget && background) {
        auto proxy { reticleWidget->rectangles() };
        proxy.emplace_back(background->rectangles());
        return proxy;
    }
    return {};
}

auto ReticlePrivate::texts() const -> OU::TextPainterObjectProxy
{
    if (reticleWidget) {
        return reticleWidget->texts();
    }
    return {};
}

auto ReticlePrivate::textures() const -> OU::TexturePainterObjectsProxy
{
    if (reticleWidget && exitButton) {
        auto proxy { reticleWidget->textures() };
        proxy.emplace_back(exitButton->textures());
        return proxy;
    }
    return {};
}

void ReticlePrivate::resize(float width, float height)
{
    // maintient le ratio 1:1 des colonnes de boutons
    leftColumn->resize(GLWidget::Rect { 0.F, 0.F, height / leftColumn->cellsCount(), height });
    rightColumn->resize(GLWidget::Rect { width - height / rightColumn->cellsCount(), 0.F, width, height });

    if (background) {
        background->resize(GLWidget::Rect { 0.F, 0.F, width, height });
    }
    if (reticleWidget) {
        const GLWidget::Rect centralRect(leftColumn->right(), 0.F, rightColumn->left(), height);
        reticleWidget->resize(centralRect);
    }

    if (exitButton) {
        exitButton->resize(rightColumn->rectOf(0U));
    }
}

void ReticlePrivate::handlePress(float x, float y)
{
    if (reticleWidget) {
        reticleWidget->handlePress(x, y);
    }

    if (exitButton) {
        exitButton->handlePress(x, y);
    }
}

void ReticlePrivate::handleRelease(float x, float y)
{
    if (reticleWidget) {
        reticleWidget->handleRelease(x, y);
    }

    if (exitButton) {
        exitButton->handleRelease(x, y);
    }
}

void ReticlePrivate::handleMove(float x, float y)
{
    if (reticleWidget) {
        reticleWidget->handleMove(x, y);
    }

    if (exitButton) {
        exitButton->handleMove(x, y);
    }
}

Reticle::Reticle(Channel::Type channel, GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : Base(std::make_unique<ReticlePrivate>(channel, painters, globalIpc))
{
}

} // namespace Jgv
