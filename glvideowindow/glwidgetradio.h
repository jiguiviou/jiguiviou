/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETRADIO_H
#define GLWIDGETRADIO_H

#include "glwidgetframe.h"

#include <functional>
#include <memory>
#include <string_view>

namespace Jgv::GLWidget {

class RadioPrivate;
class Radio : public Frame {

public:
    Radio(float backgroundDepth, GLPainters* painters);
    ~Radio() override;
    Radio(const Radio&) = delete;
    Radio(Radio&&) = delete;
    auto operator=(const Radio&) -> Radio& = delete;
    auto operator=(Radio &&) -> Radio& = delete;

    void setText(std::string_view text);
    void setClickedFunction(std::function<void(void)> function);
    [[nodiscard]] auto clickedFunction() const -> std::function<void()>;
    void setUncheckAllFunction(std::function<void(void)> function);
    void setChecked(bool checked);
    [[nodiscard]] auto widthHint(float height) const -> float;

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsVector;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectVector;

    void resize(const Rect& rect) noexcept override;

private:
    const std::unique_ptr<RadioPrivate> _impl;

    void clicked() override;

}; // class Radio

} // namespace Jgv::GLWidget

#endif // GLWIDGETRADIO_H
