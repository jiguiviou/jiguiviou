/***************************************************************************
 *   Copyright (C)2019 by Cyril BALETAUD                                   *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glpageroot.h"
#include "glpageroot_p.h"

#include "glbuttonscolumn.h"
#include "glpagebalise.h"
#include "glpagebulles.h"
#include "glpagebullesview.h"
#include "glpagecalibration.h"
#include "glpagecampagne.h"
#include "glpagereticle.h"
#include "glpageshutdown.h"
#include "glpagesmanager.h"
#include "glwidgetbutton.h"
#include "glwidgetslider.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <chrono>
#include <filesystem>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>

#include <iostream>

namespace Jgv::GLPage {

RootPrivate::RootPrivate(GLPainters* p, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : BasePrivate(p, std::move(_globalIpc))
    , channel(Channel::Type::Primary)
    , leftColumn { std::make_unique<GLWidget::ButtonsColumn>() }
    , rightColumn { std::make_unique<GLWidget::ButtonsColumn>() }
{
}

void RootPrivate::switchCampagneMode()
{
    auto enable { false };
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        enable = vtoConfig->campagneReady();
    }
    if (reticleButton) {
        reticleButton->setEnabled(enable);
    }
    if (calibrationButton) {
        calibrationButton->setEnabled(enable);
    }
    if (bullesButton) {
        bullesButton->setEnabled(enable);
    }
    if (baliseButton) {
        baliseButton->setEnabled(enable);
    }
    if (bullesViewButton) {
        bullesViewButton->setEnabled(enable);
    }
}

void RootPrivate::enableMixer(bool enable)
{
    if (mixer) {
        mixer->setEnabled(enable);
        if (enable) {
            mixerOnTime = std::chrono::system_clock::now();
        }
    }
}

void RootPrivate::setManager(uint64_t id, Manager* m)
{
    BasePrivate::setManager(id, m);
    campagnePage = manager->registerPage(managerId, std::make_unique<Campagne>(painters, globalIpc));
    reticlePage = manager->registerPage(managerId, std::make_unique<Reticle>(Channel::Type::Primary, painters, globalIpc));
    calibrationPage = manager->registerPage(managerId, std::make_unique<Calibration>(Channel::Type::Primary, painters, globalIpc));
    bullesPage = manager->registerPage(managerId, std::make_unique<Bulles>(Channel::Type::Bulles, painters, globalIpc));
    balisePage = manager->registerPage(managerId, std::make_unique<Balise>(painters, Channel::Type::Primary, globalIpc));
    bullesViewPage = manager->registerPage(managerId, std::make_unique<BullesView>(Channel::Type::Bulles, painters, globalIpc));
    shutdownPage = manager->registerPage(managerId, std::make_unique<Shutdown>(Channel::Type::Primary, painters, globalIpc));
    switchCampagneMode();
}

void RootPrivate::show()
{
    if (!recordButton) {
        recordButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        recordButton->initialize(GnuIntallDirs::assetsDir() + "/record.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");

        // s'abonne au changement d'état
        if (auto config { globalIpc->vtoConfig() }) {
            auto setRecord = [this](bool record) {
                recordButton->setBlinking(record);
                recordButton->setActived(record);
            };
            // état initial
            setRecord(config->record(channel));
            // abonnement
            recordEvent = std::make_shared<std::function<void(bool)>>(setRecord);
            config->addRecordChangedListener(channel, recordEvent);
        }

        // la commande
        auto record = [this]() {
            if (auto config { globalIpc->vtoConfig() }) {
                config->toggleRecord(channel);
            }
        };
        recordButton->setClickedFunction(record);
    }

    if (!equalizeButton) {
        equalizeButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        equalizeButton->initialize(GnuIntallDirs::assetsDir() + "/equalization.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");

        // s'abonne au changement d'état
        if (auto config { globalIpc->vtoConfig() }) {
            auto setEqualize = [this](bool equalize) {
                equalizeButton->setActived(equalize);
            };
            // état initial
            setEqualize(config->equalize(channel));
            // abonnement
            equalizeEvent = std::make_shared<std::function<void(bool)>>(setEqualize);
            config->addEqualizeChangedListener(channel, equalizeEvent);
        }

        // la commande
        auto equalize = [this]() {
            if (auto config { globalIpc->vtoConfig() }) {
                config->toggleEqualize(channel);
            }
        };
        equalizeButton->setClickedFunction(equalize);
    }

    if (!shutdownButton) {
        shutdownButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        shutdownButton->initialize(GnuIntallDirs::assetsDir() + "/shutdown.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto show = [this]() {
            manager->show(shutdownPage);
        };
        shutdownButton->setClickedFunction(show);
    }

    if (!exitButton) {
        exitButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        exitButton->initialize(GnuIntallDirs::assetsDir() + "/exit.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto exit = [this]() {
            manager->hideAll();
        };
        exitButton->setClickedFunction(exit);
    }

    if (!campagneButton) {
        campagneButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        campagneButton->initialize(GnuIntallDirs::assetsDir() + "/newcampagne.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto show = [this]() {
            manager->show(campagnePage);
        };
        campagneButton->setClickedFunction(show);
    }

    if (!bullesViewButton) {
        bullesViewButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        bullesViewButton->initialize(GnuIntallDirs::assetsDir() + "/bubbleview.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto show = [this]() {
            manager->show(bullesViewPage);
        };
        bullesViewButton->setClickedFunction(show);
    }

    if (!reticleButton) {
        reticleButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        reticleButton->initialize(GnuIntallDirs::assetsDir() + "/reticle_align.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto show = [this]() {
            manager->show(reticlePage);
        };
        reticleButton->setClickedFunction(show);
    }

    if (!calibrationButton) {
        calibrationButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        calibrationButton->initialize(GnuIntallDirs::assetsDir() + "/calibration.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto show = [this]() {
            manager->show(calibrationPage);
        };
        calibrationButton->setClickedFunction(show);
    }

    if (!bullesButton) {
        bullesButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        bullesButton->initialize(GnuIntallDirs::assetsDir() + "/bubblesave.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto show = [this]() {
            manager->show(bullesPage);
        };
        bullesButton->setClickedFunction(show);
    }

    if (!baliseButton) {
        baliseButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        baliseButton->initialize(GnuIntallDirs::assetsDir() + "/balise.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto show = [this]() {
            manager->show(balisePage);
        };
        baliseButton->setClickedFunction(show);
    }

    if (!mixer) {
        mixer = std::make_unique<GLWidget::Slider>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        if (auto config { globalIpc->vtoConfig() }) {
            //            mixer->setEnabled(config->histogramEqIsActive());
            //            mixer->setValue(config->property(Gvsp::QtBackend::Config::Properties::Value::EqualizationBalance));
        }
        auto slide = [this](float value) {
            if (auto config { globalIpc->vtoConfig() }) {
                config->setEqualizeFactor(channel, value);
            }
        };
        mixer->setSlideFunction(slide);
    }

    switchCampagneMode();
}

void RootPrivate::hide()
{
    destroyGL();
}

void RootPrivate::destroyGL() noexcept
{
    recordButton.reset(nullptr);
    equalizeButton.reset(nullptr);
    exitButton.reset(nullptr);
    campagneButton.reset(nullptr);
    reticleButton.reset(nullptr);
    calibrationButton.reset(nullptr);
    bullesButton.reset(nullptr);
    baliseButton.reset(nullptr);
    bullesViewButton.reset(nullptr);
    shutdownButton.reset(nullptr);
    mixer.reset(nullptr);
}

void RootPrivate::resize(float width, float height)
{
    leftColumn->resize(GLWidget::Rect { 0.F, 0.F, height / leftColumn->cellsCount(), height });
    rightColumn->resize(GLWidget::Rect { width - height / rightColumn->cellsCount(), 0.F, width, height });

    if (recordButton) {
        recordButton->resize(leftColumn->rectOf(6U));
    }
    if (equalizeButton) {
        equalizeButton->resize(leftColumn->rectOf(4U));
    }
    if (shutdownButton) {
        shutdownButton->resize(leftColumn->rectOf(2U));
    }
    if (exitButton) {
        exitButton->resize(leftColumn->rectOf(1U));
    }
    if (campagneButton) {
        campagneButton->resize(rightColumn->rectOf(6U));
    }
    if (bullesViewButton) {
        bullesViewButton->resize(rightColumn->rectOf(5U));
    }
    if (reticleButton) {
        reticleButton->resize(rightColumn->rectOf(4U));
    }
    if (calibrationButton) {
        calibrationButton->resize(rightColumn->rectOf(3U));
    }
    if (baliseButton) {
        baliseButton->resize(rightColumn->rectOf(2U));
    }
    if (bullesButton) {
        bullesButton->resize(rightColumn->rectOf(1U));
    }
    if (mixer) {
        const auto mixerHeight { leftColumn->rectOf(0U).height() };
        mixer->resize(GLWidget::Rect { leftColumn->right(), 0.25F * (height - mixerHeight), rightColumn->left(), 0.25F * (height - mixerHeight) + mixerHeight });
    }
}

auto RootPrivate::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    if (mixer && mixer->isEnabled()) {
        const auto duration { std::chrono::system_clock::now() - mixerOnTime };
        if (duration < MIXER_TIME_ON) {
            return { mixer->rectangles() };
        }
        mixer->setEnabled(false);
    }
    return {};
}

auto RootPrivate::texts() const -> OU::TextPainterObjectProxy
{
    return {};
}

auto RootPrivate::textures() const -> OU::TexturePainterObjectsProxy
{
    if (recordButton && equalizeButton && exitButton && campagneButton && reticleButton && calibrationButton
        && bullesButton && baliseButton && bullesViewButton && shutdownButton) {
        return { recordButton->textures(), equalizeButton->textures(), exitButton->textures(),
            campagneButton->textures(), reticleButton->textures(), calibrationButton->textures(),
            bullesButton->textures(), baliseButton->textures(), bullesViewButton->textures(), shutdownButton->textures() };
    }
    return {};
}

void RootPrivate::handlePress(float x, float y)
{
    if (recordButton) {
        recordButton->handlePress(x, y);
    }
    if (equalizeButton) {
        equalizeButton->handlePress(x, y);
    }
    if (shutdownButton) {
        shutdownButton->handlePress(x, y);
    }
    if (exitButton) {
        exitButton->handlePress(x, y);
    }
    if (campagneButton) {
        campagneButton->handlePress(x, y);
    }
    if (reticleButton) {
        reticleButton->handlePress(x, y);
    }
    if (calibrationButton) {
        calibrationButton->handlePress(x, y);
    }
    if (bullesButton) {
        bullesButton->handlePress(x, y);
    }
    if (baliseButton) {
        baliseButton->handlePress(x, y);
    }
    if (bullesViewButton) {
        bullesViewButton->handlePress(x, y);
    }
    // on active le mixer dès le le touché
    if (x > leftColumn->right() && x < rightColumn->left()) {
        if (equalizeButton->isActived()) {
            enableMixer(true);
        }
    }

    if (mixer && mixer->isEnabled()) {
        mixer->handlePress(x, y);
    }
}

void RootPrivate::handleRelease(float x, float y)
{
    if (recordButton) {
        recordButton->handleRelease(x, y);
    }
    if (equalizeButton) {
        equalizeButton->handleRelease(x, y);
    }
    if (shutdownButton) {
        shutdownButton->handleRelease(x, y);
    }
    if (exitButton) {
        exitButton->handleRelease(x, y);
    }
    if (campagneButton) {
        campagneButton->handleRelease(x, y);
    }
    if (reticleButton) {
        reticleButton->handleRelease(x, y);
    }
    if (calibrationButton) {
        calibrationButton->handleRelease(x, y);
    }
    if (bullesButton) {
        bullesButton->handleRelease(x, y);
    }
    if (baliseButton) {
        baliseButton->handleRelease(x, y);
    }
    if (bullesViewButton) {
        bullesViewButton->handleRelease(x, y);
    }
    // on active le mixer dès le le touché
    if (x > leftColumn->right() && x < rightColumn->left()) {
        if (equalizeButton->isActived()) {
            enableMixer(true);
        }
    }
    if (mixer && mixer->isEnabled()) {
        mixer->handleRelease(x, y);
    }
}

void RootPrivate::handleMove(float x, float y)
{
    if (recordButton) {
        recordButton->handleMove(x, y);
    }
    if (equalizeButton) {
        equalizeButton->handleMove(x, y);
    }
    if (shutdownButton) {
        shutdownButton->handleMove(x, y);
    }
    if (exitButton) {
        exitButton->handleMove(x, y);
    }
    if (campagneButton) {
        campagneButton->handleMove(x, y);
    }
    if (reticleButton) {
        reticleButton->handleMove(x, y);
    }
    if (calibrationButton) {
        calibrationButton->handleMove(x, y);
    }
    if (bullesButton) {
        bullesButton->handleMove(x, y);
    }
    if (baliseButton) {
        baliseButton->handleMove(x, y);
    }
    if (bullesViewButton) {
        bullesViewButton->handleMove(x, y);
    }

    // on active le mixer dès le le touché
    if (x > leftColumn->right() && x < rightColumn->left()) {
        if (equalizeButton->isActived()) {
            enableMixer(true);
        }
    }
    if (mixer && mixer->isEnabled()) {
        mixer->handleMove(x, y);
    }
}

Root::Root(GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : Base(std::make_unique<RootPrivate>(painters, globalIpc))
{
}

} // namespace Jgv::GLPage
