/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glpagebullesview.h"
#include "glpagebullesview_p.h"

#include "glbuttonscolumn.h"
#include "glpagesmanager.h"
#include "glwidgetbackground.h"
#include "glwidgetbutton.h"
#include "glwidgetlabel.h"
#include "glwidgettexture.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <genicam2/genicam.h>
#include <genicam2/genicaminterface.h>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>

namespace Jgv::GLPage {

BullesViewPrivate::BullesViewPrivate(Channel::Type c, GLPainters* p, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : BasePrivate(p, std::move(_globalIpc))
    , channel(c)
    , leftColumn { std::make_unique<GLWidget::ButtonsColumn>() }
    , rightColumn { std::make_unique<GLWidget::ButtonsColumn>() }
{
}

void BullesViewPrivate::resizeVideo() const noexcept
{
    const GLWidget::Rect centralRect(leftColumn->right(), leftColumn->bottom() + GLWidget::INTER_WIDGET_SPACE,
        rightColumn->left(), title->bottom() - GLWidget::INTER_WIDGET_SPACE);
    auto videorect { centralRect };
    const auto ratio { centralRect.width() / centralRect.height() };
    const auto videoRatio { video->textureRatio() };
    if (videoRatio > ratio) {
        videorect.setTop(centralRect.bottom() + (centralRect.width() / videoRatio));

    } else {
        videorect.setRight(centralRect.left() + (centralRect.height() * videoRatio));
    }
    videorect.setCenter(centralRect.centerX(), centralRect.centerY());

    video->resize(videorect);
}

void BullesViewPrivate::show()
{
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        if (const auto genicam { vtoConfig->genicamInterface(channel) }) {
            genicam->setValue(Genicam::Dictionary::Feature::AcquisitionStart, {});
        }
    }

    if (!background) {
        background = std::make_unique<GLWidget::Background>(GLWidget::WIDGET_BACKGROUND_DEPTH, painters);
    }

    if (!title) {
        title = std::make_unique<GLWidget::Label>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        title->setBackgroundColor(GLWidget::TITLE_BACKGROUND_COLOR);
        title->setTextColor(GLWidget::TITLE_TEXT_COLOR);
        title->setText("Contrôle de la verticalité");
    }

    if (!video) {
        video = std::make_unique<GLWidget::Texture>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters, globalIpc);
        video->setTexture(channel);
    }

    if (!exitButton) {
        exitButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        exitButton->initialize(GnuIntallDirs::assetsDir() + "/valid.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto exit = [this]() {
            manager->showParent(managerId);
        };
        exitButton->setClickedFunction(exit);
    }
}

void BullesViewPrivate::hide()
{
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        if (const auto genicam { vtoConfig->genicamInterface(channel) }) {
            genicam->setValue(Genicam::Dictionary::Feature::AcquisitionStop, {});
        }
    }
    destroyGL();
}

void BullesViewPrivate::destroyGL() noexcept
{
    video.reset(nullptr);
    exitButton.reset(nullptr);
    background.reset(nullptr);
}

auto BullesViewPrivate::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    if (video && title && background) {
        if (video->textureRatioChanged()) {
            resizeVideo();
        }
        return { video->rectangles(), title->rectangles(), background->rectangles() };
    }
    return {};
}

auto BullesViewPrivate::texts() const -> OpenGLUtils::TextPainterObjectProxy
{
    if (title) {
        return { title->texts() };
    }
    return {};
}

auto BullesViewPrivate::textures() const -> OpenGLUtils::TexturePainterObjectsProxy
{
    if (video && exitButton) {
        return { video->textures(), exitButton->textures() };
    }
    return {};
}

void BullesViewPrivate::resize(float width, float height)
{
    // maintient le ratio 1:1 des colonnes de boutons
    leftColumn->resize(GLWidget::Rect { 0.F, 0.F, height / leftColumn->cellsCount(), height });
    rightColumn->resize(GLWidget::Rect { width - height / rightColumn->cellsCount(), 0.F, width, height });

    if (background && video && title && exitButton) {
        background->resize(GLWidget::Rect { 0.F, 0.F, width, height });
        const GLWidget::Rect titleRect { leftColumn->right(), leftColumn->top() - GLWidget::TITLE_HEIGHT, rightColumn->left(), rightColumn->top() };
        title->resize(titleRect);
        resizeVideo();

        exitButton->resize(rightColumn->rectOf(0U));
    }
}

void BullesViewPrivate::handlePress(float x, float y)
{

    if (exitButton) {
        exitButton->handlePress(x, y);
    }
}

void BullesViewPrivate::handleRelease(float x, float y)
{

    if (exitButton) {
        exitButton->handleRelease(x, y);
    }
}

void BullesViewPrivate::handleMove(float x, float y)
{

    if (exitButton) {
        exitButton->handleMove(x, y);
    }
}

BullesView::BullesView(Channel::Type channel, GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : Base(std::make_unique<BullesViewPrivate>(channel, painters, globalIpc))
{
}

} // namespace Jgv
