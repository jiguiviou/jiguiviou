/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLPAGEBASE_H
#define GLPAGEBASE_H

#include "glpainters.h"

#include <functional>
#include <memory>
#include <vector>

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GLPage {

class Manager;

struct BasePrivate;
class Base {

protected:
    explicit Base(std::unique_ptr<BasePrivate> dd);

public:
    virtual ~Base();
    Base(const Base&) = delete;
    Base(Base&&) = delete;
    auto operator=(const Base&) -> Base& = delete;
    auto operator=(Base &&) -> Base& = delete;

    void setExitFunction(std::function<void(bool)> function);
    void show(float width, float height);
    void hide();
    void destroyGL() noexcept;

    [[nodiscard]] auto isVisible() const noexcept -> bool;

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsProxy;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectProxy;
    [[nodiscard]] auto textures() const -> OU::TexturePainterObjectsProxy;

    void resizeGL(float width, float height);

    auto acceptPress(float x, float y) -> bool;
    auto acceptRelease(float x, float y) -> bool;
    auto acceptMove(float x, float y) -> bool;

private:
    const std::unique_ptr<BasePrivate> _impl;
    friend class Manager;
    void setManager(uint64_t id, Manager* manager);

}; // class Base

} // namespace Jgv::GLPage

#endif // GLPAGEBASE_H
