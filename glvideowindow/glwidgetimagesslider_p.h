/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETIMAGESSLIDER_P_H
#define GLWIDGETIMAGESSLIDER_P_H

#include "glpainters.h"
#include "glwidgetrect.h"

#include <filesystem>
#include <functional>
#include <memory>
#include <set>
#include <vector>

namespace Jgv {
class GLPainters;
} // namespace Jgv

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

class ScrollBar;
class Texture;

struct Cell {
    std::string filename;
    Rect rect;
    std::unique_ptr<Texture> widget;
    bool draw { true };
};
const auto DEFAULT_CELLS_COUNT { 7 };

struct ImagesSliderPrivate {
    ImagesSliderPrivate(float backgroundDepth, GLPainters* p, std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc);
    ~ImagesSliderPrivate();
    ImagesSliderPrivate(const ImagesSliderPrivate&) = delete;
    ImagesSliderPrivate(ImagesSliderPrivate&&) = delete;
    auto operator=(const ImagesSliderPrivate&) -> ImagesSliderPrivate& = delete;
    auto operator=(ImagesSliderPrivate &&) -> ImagesSliderPrivate& = delete;

    const float depth;
    GLPainters* const painters;
    std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc;

    std::unique_ptr<ScrollBar> scrollBar;

    int64_t cellsCount { DEFAULT_CELLS_COUNT };
    std::vector<Cell> cells;

    OU::RectanglePainterObject decorationObject { 0U };
    OU::RectanglePainterObject backgroundObject { 0U };
    OU::TextureScrollObject texturesScrollObject { 0U };
    OU::RectangleScrollObject rectanglesScrollObject { 0U };
    OU::TextureClipObject texturesClipObject { 0U };
    OU::RectangleClippingObject rectanglesClipObject { 0U };

    bool pressed { false };
    bool scrolling { false };

    float scrollX { 0.F }; // le vecteur de scrolling ( horizontal seulement )
    float pressedX { 0.F }; // la dernière position press
    float translateOrigin { 0.F }; // l'origine de la dernière translation

    int64_t lastSelected { 0 };
    std::function<void(const std::string&)> selectImage { nullptr };

    std::filesystem::path path;
    int notifyFd { 0 };
    int watchFd { 0 };

    void resize(const Rect& rect);

    auto readFromNotify() -> std::set<std::filesystem::path>;
    auto readFromPath() -> std::set<std::filesystem::path>;
    auto addImagePath(const std::filesystem::path& imagePath) -> std::vector<Cell>::iterator;
    void updateCellsFromPathSet(const std::set<std::filesystem::path>& set, const Rect& viewRect);
    void updateCellsFromInotify(const Rect& viewRect);
    void updateCellsFromPath(const Rect& viewRect);
    void resizeCell(std::vector<Cell>::iterator cell, const Rect& viewRect);
    void optimizeTextures(const Rect& rect);
    void scroll(float dx);
    void alignScroll(const Rect& rect);
    void setFocus(const std::filesystem::path& imagePath);

    void handleClick(float x, float y);

}; // struct ImagesSliderPrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETIMAGESSLIDER_P_H
