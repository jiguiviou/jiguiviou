/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETCAMPAGNE_P_H
#define GLWIDGETCAMPAGNE_P_H

#include "glpainters.h"

#include <functional>
#include <memory>
#include <openglutils/rectanglepainter.h>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

constexpr std::string_view MEDIUM { "Medium" };
constexpr std::string_view NAME { "Name" };
constexpr std::string_view CRI { "Cri" };
enum class State {
    medium,
    name,
    cri
};
constexpr auto toString(State state) -> std::string_view
{
    switch (state) {
    case State::medium:
        return MEDIUM;
    case State::name:
        return NAME;
    case State::cri:
        return CRI;
    }
}

class Checkbox;
class Keyboard;
class Keypad;
class Label;
class Rect;
class TextEdit;
class Volumes;
class Radio;

struct CampagnePrivate {
    CampagnePrivate(float backgroundDepth, GLPainters* p, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~CampagnePrivate() = default;
    CampagnePrivate(const CampagnePrivate&) = delete;
    CampagnePrivate(CampagnePrivate&&) = delete;
    auto operator=(const CampagnePrivate&) -> CampagnePrivate& = delete;
    auto operator=(CampagnePrivate &&) -> CampagnePrivate& = delete;

    const float depth;
    GLPainters* const painters;
    const std::shared_ptr<GlobalIPC::Object> globalIpc;

    std::unique_ptr<Label> title;
    std::unique_ptr<Volumes> volumes;
    std::unique_ptr<Keyboard> keyboard;
    std::unique_ptr<Keypad> keypad;
    std::unique_ptr<TextEdit> nameEditor;
    std::unique_ptr<TextEdit> criEditor;

    State currentState { State::medium };
    const int32_t _padding { 0 };

    void updateTitle();

    auto rectangles() -> OU::RectanglePainterObjectsProxy;
    auto textures() -> OU::TexturePainterObjectsProxy;
    auto texts() -> OU::TextPainterObjectProxy;

    void resize(const Rect& rect) noexcept;

    auto validateCurrentState() -> bool;

    auto previous(const Rect& rect) -> bool;
    auto next(const Rect& rect) -> bool;

    void handlePress(float x, float y);
    void handleRelease(float x, float y);
    void handleMove(float x, float y);

}; // struct CampagnePrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETCAMPAGNE_P_H
