/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLPAGECALIBRATION_H
#define GLPAGECALIBRATION_H

#include "glpagebase.h"

namespace Jgv {
struct GLPainters;
} // namespace Jgv

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLPage {

class Calibration : public Base {

public:
    Calibration(Channel::Type channel, GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~Calibration() override = default;
    Calibration(const Calibration&) = delete;
    Calibration(Calibration&&) = delete;
    auto operator=(const Calibration&) -> Calibration& = delete;
    auto operator=(Calibration &&) -> Calibration& = delete;

}; // class Calibration

} // namespace Jgv::GLPage

#endif // GLPAGECALIBRATION_H
