﻿/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETTEXTEDIT_H
#define GLWIDGETTEXTEDIT_H

#include "glwidgetframe.h"

#include <memory>
#include <string>
#include <vector>

namespace Jgv::GLWidget {

using Color = std::tuple<float, float, float, float>;

class TextEditPrivate;
class TextEdit : public Frame {

public:
    TextEdit(float backgroundDepth, GLPainters* painters);
    ~TextEdit() override;
    TextEdit(const TextEdit&) = delete;
    TextEdit(TextEdit&&) = delete;
    auto operator=(const TextEdit&) -> TextEdit& = delete;
    auto operator=(TextEdit &&) -> TextEdit& = delete;

    void disableCursor();

    void setPrefix(const std::string& prefix, const Color& color);

    void reserve(std::size_t textMaxSize);
    void setText(std::string_view text);
    void setTextColor(const Color& color);

    void setBackground(const Color& color);
    [[nodiscard]] auto text() const -> std::string;

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsVector;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectVector;

    void resize(const Rect& rect) noexcept override;

private:
    const std::unique_ptr<TextEditPrivate> _impl;

}; // class TextEdit

} // namespace Jgv::GLWidget

#endif // GLWIDGETTEXTEDIT_H
