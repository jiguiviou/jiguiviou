/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETSLIDER_P_H
#define GLWIDGETSLIDER_P_H

#include "glpainters.h"

#include <functional>

namespace Jgv {
class Rect;
} // namespace Jgv

namespace Jgv::GLWidget {

struct SliderPrivate {
    SliderPrivate(float backgroundDepth, GLPainters* p);
    ~SliderPrivate();
    SliderPrivate(const SliderPrivate&) = delete;
    SliderPrivate(SliderPrivate&&) = delete;
    auto operator=(const SliderPrivate&) -> SliderPrivate& = delete;
    auto operator=(SliderPrivate &&) -> SliderPrivate& = delete;

    const float depth;
    GLPainters* const painters;

    OU::RectanglePainterObject farLine { 0U };
    OU::RectanglePainterObject nearLine { 0U };
    OU::RectanglePainterObject farCursor { 0U };
    OU::RectanglePainterObject nearCursor { 0U };
    OU::RectangleScrollObject scroll { 0U };

    float value { 0.F };
    std::function<void(float)> slideFunction;

    void resize(const Rect& rect);
    void updateScroll(const Rect& rect);
    void updateScroll(const Rect& rect, float x);

}; // struct SliderPrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETSLIDER_P_H
