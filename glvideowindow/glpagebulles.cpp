/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glpagebulles.h"
#include "glpagebulles_p.h"

#include "glbuttonscolumn.h"
#include "glpagesmanager.h"
#include "glwidgetbackground.h"
#include "glwidgetbulles.h"
#include "glwidgetbutton.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <genicam2/genicam.h>
#include <genicam2/genicaminterface.h>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>

namespace Jgv::GLPage {

BullesPrivate::BullesPrivate(Channel::Type c, GLPainters* p, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : BasePrivate(p, std::move(_globalIpc))
    , channel(c)
    , leftColumn { std::make_unique<GLWidget::ButtonsColumn>() }
    , rightColumn { std::make_unique<GLWidget::ButtonsColumn>() }
{
}

void BullesPrivate::show()
{
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        if (const auto genicam { vtoConfig->genicamInterface(channel) }) {
            genicam->setValue(Genicam::Dictionary::Feature::AcquisitionStart, {});
        }
    }

    if (!background) {
        background = std::make_unique<GLWidget::Background>(GLWidget::WIDGET_BACKGROUND_DEPTH, painters);
    }

    if (!bullesWidget) {
        bullesWidget = std::make_unique<GLWidget::Bulles>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters, channel, globalIpc);
    }

    if (!exitButton) {
        exitButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        exitButton->initialize(GnuIntallDirs::assetsDir() + "/valid.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto exit = [this]() {
            manager->showParent(managerId);
        };
        exitButton->setClickedFunction(exit);
    }
}

void BullesPrivate::hide()
{
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        if (const auto genicam { vtoConfig->genicamInterface(channel) }) {
            genicam->setValue(Genicam::Dictionary::Feature::AcquisitionStop, {});
        }
    }
    destroyGL();
}

void BullesPrivate::destroyGL() noexcept
{
    bullesWidget.reset(nullptr);
    exitButton.reset(nullptr);
    background.reset(nullptr);
}

auto BullesPrivate::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    if (bullesWidget) {
        auto proxy { bullesWidget->rectangles() };
        proxy.emplace_back(background->rectangles());
        return proxy;
    }
    return {};
}

auto BullesPrivate::texts() const -> OU::TextPainterObjectProxy
{
    if (bullesWidget) {
        return bullesWidget->texts();
    }
    return {};
}

auto BullesPrivate::textures() const -> OpenGLUtils::TexturePainterObjectsProxy
{
    if (bullesWidget && exitButton) {
        auto proxy { bullesWidget->textures() };
        proxy.emplace_back(exitButton->textures());
        return proxy;
    }
    return {};
}

void BullesPrivate::resize(float width, float height)
{
    // maintient le ratio 1:1 des colonnes de boutons
    leftColumn->resize(GLWidget::Rect { 0.F, 0.F, height / leftColumn->cellsCount(), height });
    rightColumn->resize(GLWidget::Rect { width - height / rightColumn->cellsCount(), 0.F, width, height });

    if (background) {
        background->resize(GLWidget::Rect { 0.F, 0.F, width, height });
    }

    if (bullesWidget) {
        const GLWidget::Rect centralRect(leftColumn->right(), 0.F, rightColumn->left(), height);
        bullesWidget->resize(centralRect);
    }

    if (exitButton) {
        exitButton->resize(rightColumn->rectOf(0U));
    }
}

void BullesPrivate::handlePress(float x, float y)
{
    if (bullesWidget) {
        bullesWidget->handlePress(x, y);
    }

    if (exitButton) {
        exitButton->handlePress(x, y);
    }
}

void BullesPrivate::handleRelease(float x, float y)
{
    if (bullesWidget) {
        bullesWidget->handleRelease(x, y);
    }

    if (exitButton) {
        exitButton->handleRelease(x, y);
    }
}

void BullesPrivate::handleMove(float x, float y)
{
    if (bullesWidget) {
        bullesWidget->handleMove(x, y);
    }

    if (exitButton) {
        exitButton->handleMove(x, y);
    }
}

Bulles::Bulles(Channel::Type channel, GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : Base(std::make_unique<BullesPrivate>(channel, painters, globalIpc))
{
}

} // namespace Jgv
