/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetlabel.h"
#include "glwidgetlabel_p.h"

#include "glpagesmanager.h"
#include "theme.h"

namespace Jgv::GLWidget {

LabelPrivate::LabelPrivate(float backgroundDepth, GLPainters* const p)
    : depth(backgroundDepth)
    , painters(p)
{
    backgroundObject = painters->rectanglePainter.createObject();
    textObject = painters->textPainter.createObject();
    clippingObject = painters->textPainter.createClipObject();

    painters->textPainter.setClippingRectangle(textObject, clippingObject);
}

LabelPrivate::~LabelPrivate()
{
    painters->textPainter.destroyObject(textObject);
    painters->textPainter.destroyClipObject(clippingObject);
    painters->rectanglePainter.destroyObject(backgroundObject);
}

void LabelPrivate::resize(const Rect& rect)
{
    painters->rectanglePainter.setRectangle(backgroundObject, rect.tuple(), depth);

    const auto [left, bottom, right, top] { painters->textPainter.boundingBox(textObject) };
    Rect textRect { right, top };
    textRect.alignCenter(rect);
    painters->textPainter.setPosition(textObject, { textRect.left(), textRect.bottom(), depth + INTER_LAYOUT_DEPTH });
    painters->textPainter.updateClipObjectRectangle(clippingObject, { rect.left<int>(), rect.bottom<int>(), rect.width<int>(), rect.height<int>() });
}

Label::Label(float backgroundDepth, GLPainters* const painters)
    : _impl(std::make_unique<LabelPrivate>(backgroundDepth, painters))
{
}

Label::~Label() = default;

void Label::reserve(std::size_t textMaxSize)
{
    _impl->painters->textPainter.reserve(_impl->textObject, textMaxSize);
}

void Label::setText(std::string_view text)
{
    _impl->painters->textPainter.setText(_impl->textObject, text);
    _impl->resize(*this);
}

void Label::setTextColor(const Color& color)
{
    _impl->painters->textPainter.setColor(_impl->textObject, color);
}

void Label::setBackgroundColor(const Color& color)
{
    _impl->painters->rectanglePainter.setColor(_impl->backgroundObject, color);
}

OU::RectanglePainterObjectsVector Label::rectangles() const
{
    return { _impl->backgroundObject };
}

OU::TextPainterObjectVector Label::texts() const
{
    return { _impl->textObject };
}

void Label::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

} // namespace Jgv::GLWidget
