/**************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetkeyboard.h"
#include "glwidgetkeyboard_p.h"

#include <iostream>

namespace {
const uint COLUMNS_COUNT { 10 };
const uint LINES_COUNT { 5 };
const auto KEY_SCALE_FACTOR { 0.9F };
} // namespace

namespace Jgv::GLWidget {

KeyboardPrivate::KeyboardPrivate(float backgroundDepth, GLPainters* const painters)
    : depth(backgroundDepth)
    , painters(painters)
{
    keys.emplace_back(std::make_unique<Key>(depth, ' ', ' ', painters));

    keys.emplace_back(std::make_unique<Key>(depth, 'w', 'W', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'x', 'X', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'c', 'C', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'v', 'V', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'b', 'B', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'n', 'N', painters));

    keys.emplace_back(std::make_unique<Key>(depth, 'q', 'Q', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 's', 'S', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'd', 'D', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'f', 'F', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'g', 'G', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'h', 'H', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'j', 'J', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'k', 'K', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'l', 'L', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'm', 'M', painters));

    keys.emplace_back(std::make_unique<Key>(depth, 'a', 'A', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'z', 'Z', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'e', 'E', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'r', 'R', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 't', 'T', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'y', 'Y', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'u', 'U', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'i', 'I', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'o', 'O', painters));
    keys.emplace_back(std::make_unique<Key>(depth, 'p', 'P', painters));

    keys.emplace_back(std::make_unique<Key>(depth, '1', '1', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '2', '2', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '3', '3', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '4', '4', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '5', '5', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '6', '6', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '7', '7', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '8', '8', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '9', '9', painters));
    keys.emplace_back(std::make_unique<Key>(depth, '0', '0', painters));

    majKey = std::make_unique<KeySpecial>(depth, GnuIntallDirs::assetsDir() + "/input-caps-on.png", painters);
    auto majClicked = [this]() {
        for (auto& key : keys) {
            key->toggleMaj();
        }
    };
    majKey->setClickedFunction(majClicked);

    auto supprClicked = [this]() {
        if (text && setText) {
            auto t { text() };
            if (!t.empty()) {
                t.pop_back();
                setText(std::move(t));
            }
        }
    };
    supprKey = std::make_unique<KeySpecial>(depth, GnuIntallDirs::assetsDir() + "/cleanup.png", painters);
    supprKey->setClickedFunction(supprClicked);

    auto keyboardClicked = [this](char character) {
        if (text && setText) {
            setText(text() + character);
        }
    };
    for (auto& key : keys) {
        key->setClickedFunction(keyboardClicked);
    }
}

auto KeyboardPrivate::rectangles() const -> OU::RectanglePainterObjectsVector
{
    OU::RectanglePainterObjectsVector vector;
    std::transform(keys.cbegin(), keys.cend(), std::back_inserter(vector),
        [](const auto& value) { return value->rectangleObject(); });
    vector.push_back(majKey->rectangleObject());
    vector.push_back(supprKey->rectangleObject());
    return vector;
}

auto KeyboardPrivate::textures() const -> OU::TexturePainterObjectsVector
{
    return { majKey->textureObject(), supprKey->textureObject() };
}

auto KeyboardPrivate::texts() const -> OU::TextPainterObjectVector
{
    OU::TextPainterObjectVector vector;
    std::transform(keys.cbegin(), keys.cend(), std::back_inserter(vector),
        [](const auto& value) { return value->textObject(); });
    return vector;
}

void KeyboardPrivate::resize(const Rect& rect)
{
    const auto keySize { std::min(rect.width() / COLUMNS_COUNT, rect.height() / LINES_COUNT) };
    Rect keyboardRect { 0, 0, COLUMNS_COUNT * keySize, LINES_COUNT * keySize };
    keyboardRect.alignCenter(rect);

    auto resize = [&](char key, float c, float l) {
        auto it = std::find_if(keys.begin(), keys.end(), [&](const auto& value) {
            return value->normal() == key;
        });

        Rect keyRect { 0.f, 0.f, keySize, keySize };
        keyRect.translate(keyboardRect.left() + c * keySize, keyboardRect.bottom() + l * keySize);
        (*it)->resize(keyRect.scaled(KEY_SCALE_FACTOR));
    };

    auto find = [&](char key) {
        return std::find_if(keys.begin(), keys.end(), [&](const auto& value) {
            return value->normal() == key;
        });
    };

    Rect majRect { 0.F, 0.F, 1.5F * keySize, keySize };
    majRect.translate(keyboardRect.left(), keyboardRect.bottom() + keySize);
    majKey->resize(majRect.scaled(KEY_SCALE_FACTOR));

    Rect supprRect { 0.F, 0.F, 1.5F * keySize, keySize };
    supprRect.translate(keyboardRect.left() + 8.5F * keySize, keyboardRect.bottom() + keySize);
    supprKey->resize(supprRect.scaled(KEY_SCALE_FACTOR));

    Rect keyRect { 0.F, 0.F, 4.F * keySize, 0.8F * keySize };
    keyRect.translate(keyboardRect.left() + 3.F * keySize, keyboardRect.bottom() + 0.2F * keySize);
    auto space { find(' ') };
    (*space)->resize(keyRect.scaled(KEY_SCALE_FACTOR));

    resize('w', 2.F, 1.F);
    resize('x', 3.F, 1.F);
    resize('c', 4.F, 1.F);
    resize('v', 5.F, 1.F);
    resize('b', 6.F, 1.F);
    resize('n', 7.F, 1.F);

    resize('q', 0.F, 2.F);
    resize('s', 1.F, 2.F);
    resize('d', 2.F, 2.F);
    resize('f', 3.F, 2.F);
    resize('g', 4.F, 2.F);
    resize('h', 5.F, 2.F);
    resize('j', 6.F, 2.F);
    resize('k', 7.F, 2.F);
    resize('l', 8.F, 2.F);
    resize('m', 9.F, 2.F);

    resize('a', 0.F, 3.F);
    resize('z', 1.F, 3.F);
    resize('e', 2.F, 3.F);
    resize('r', 3.F, 3.F);
    resize('t', 4.F, 3.F);
    resize('y', 5.F, 3.F);
    resize('u', 6.F, 3.F);
    resize('i', 7.F, 3.F);
    resize('o', 8.F, 3.F);
    resize('p', 9.F, 3.F);

    resize('1', 0.F, 4.F);
    resize('2', 1.F, 4.F);
    resize('3', 2.F, 4.F);
    resize('4', 3.F, 4.F);
    resize('5', 4.F, 4.F);
    resize('6', 5.F, 4.F);
    resize('7', 6.F, 4.F);
    resize('8', 7.F, 4.F);
    resize('9', 8.F, 4.F);
    resize('0', 9.F, 4.F);
}

void KeyboardPrivate::handlePress(float x, float y)
{
    for (auto& key : keys) {
        key->handlePress(x, y);
    }
    majKey->handlePress(x, y);
    supprKey->handlePress(x, y);
}

void KeyboardPrivate::handleRelease(float x, float y)
{
    for (auto& key : keys) {
        key->handleRelease(x, y);
    }
    majKey->handleRelease(x, y);
    supprKey->handleRelease(x, y);
}

void KeyboardPrivate::handleMove(float x, float y)
{
    for (auto& key : keys) {
        key->handleMove(x, y);
    }
    majKey->handleMove(x, y);
    supprKey->handleMove(x, y);
}

Keyboard::Keyboard(float backgroundDepth, GLPainters* const painters)
    : _impl(std::make_unique<KeyboardPrivate>(backgroundDepth, painters))
{
}

Keyboard::~Keyboard() = default;

void Keyboard::setSetTextFunction(std::function<void(std::string_view)> setText)
{
    _impl->setText = std::move(setText);
}

void Keyboard::setGetTextFunction(std::function<std::string(void)> text)
{
    _impl->text = std::move(text);
}

auto Keyboard::rectangles() const -> OU::RectanglePainterObjectsVector
{
    return _impl->rectangles();
}

auto Keyboard::textures() const -> OU::TexturePainterObjectsVector
{
    return _impl->textures();
}

auto Keyboard::texts() const -> OU::TextPainterObjectVector
{
    return _impl->texts();
}

void Keyboard::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Keyboard::handlePress(float x, float y)
{
    _impl->handlePress(x, y);
}

void Keyboard::handleRelease(float x, float y)
{
    _impl->handleRelease(x, y);
}

void Keyboard::handleMove(float x, float y)
{
    _impl->handleMove(x, y);
}

} // namespace Jgv::GLWidget
