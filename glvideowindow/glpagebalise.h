/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLPAGEBALISE_H
#define GLPAGEBALISE_H

#include "glpagebase.h"

namespace Jgv {
struct GLPainters;
} // namespace Jgv

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLPage {

class Balise : public Base {

public:
    Balise(GLPainters* painters, Channel::Type channel, std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc);
    ~Balise() override = default;
    Balise(const Balise&) = delete;
    Balise(Balise&&) = delete;
    auto operator=(const Balise&) -> Balise& = delete;
    auto operator=(Balise &&) -> Balise& = delete;

}; // class Balise

} // namespace Jgv::GLPage

#endif // GLPAGEBALISE_H
