/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETTEXTURE_H
#define GLWIDGETTEXTURE_H

#include "glwidgetframe.h"

#include <functional>
#include <memory>
#include <string_view>
#include <vector>

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

using Color = std::tuple<float, float, float, float>;

class TexturePrivate;
class Texture : public Frame {

public:
    explicit Texture(float backgroundDepth, GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~Texture() override;
    Texture(const Texture&) = delete;
    Texture(Texture&&) = delete;
    auto operator=(const Texture&) -> Texture& = delete;
    auto operator=(Texture &&) -> Texture& = delete;

    void setClickedFunction(std::function<void(void)> clicked);

    void addClipping(OU::TextureClipObject texture, OU::RectangleClippingObject rectangle);
    void addScrollVector(OU::TextureScrollObject texture, OU::RectangleScrollObject rectangle);
    void setBackgroundColor(const Color& color);
    void scaleCenter(float factor);
    [[nodiscard]] auto scaleFactor() const -> float;
    [[nodiscard]] auto textureRect() const -> Rect;
    [[nodiscard]] auto textureRatio() const -> float;

    void setTexture(Channel::Type channel);
    auto textureRatioChanged() -> bool;
    void setTexture(std::string_view filename);

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsVector;
    [[nodiscard]] auto textures() const -> OU::TexturePainterObjectsVector;

    void resize(const Rect& rect) noexcept override;

private:
    const std::unique_ptr<TexturePrivate> _impl;
    void clicked() override;

}; // class Texture

} // namespace Jgv::GLWidget

#endif // GLWIDGETTEXTURE_H
