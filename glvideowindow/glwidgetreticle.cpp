﻿/**************************************************************************
*   Copyright (C) 2020 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetreticle.h"
#include "glwidgetreticle_p.h"

#include "glpagesmanager.h"
#include "glvideowindow.h"
#include "glwidgetimagesslider.h"
#include "glwidgetlabel.h"
#include "glwidgettexture.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <filesystem>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <iostream>

namespace Jgv::GLWidget {

constexpr std::string_view RETICLE_PATH { "Réticule" };

ReticlePrivate::ReticlePrivate(float backgroundDepth, GLPainters* const p, Channel::Type _channel, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : depth(backgroundDepth)
    , painters(p)
    , channel(_channel)
    , globalIpc(std::move(_globalIpc))
{
    title = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH, painters);
    title->setBackgroundColor(TITLE_BACKGROUND_COLOR);
    title->setText("Alignement du réticule");

    zoom = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters);
    zoom->setText("x00");
    zoom->setTextColor(FOCUS_BACKGROUND_COLOR);

    exposure = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH, painters);
    exposure->setText("Exp: 000000.000 ms");
    exposure->setBackgroundColor(LABEL_BACKGROUND_COLOR);

    gain = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH, painters);
    gain->setText("Gain: 00.0 dB");
    gain->setBackgroundColor(LABEL_BACKGROUND_COLOR);

    video = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    video->setBackgroundColor(TEXTURE_BACKGROUND_COLOR);
    video->setTexture(channel);

    capture = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    capture->setBackgroundColor(TEXTURE_BACKGROUND_COLOR);
    auto viewCapture = [this](const std::string& filename) {
        capture->setTexture(filename);
    };

    slider = std::make_unique<ImagesSlider>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    slider->setBackgroundColor(SLIDER_BACKGROUND_COLOR);
    slider->setSelectfunction(viewCapture);
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        const auto desiredPath { std::filesystem::path(globalIpc->vtoConfig()->pathGet()) / RETICLE_PATH };
        if (!std::filesystem::exists(desiredPath)) {
            std::error_code ec;
            if (!std::filesystem::create_directory(desiredPath, ec)) {
                std::cerr << "GLWidget::Reticle failed to create directory " << desiredPath << " error: " << ec.message() << std::endl;
                return;
            }
            std::filesystem::permissions(desiredPath, std::filesystem::perms::all, ec);
            if (ec != std::error_code()) {
                std::cout << "GLWidget::Reticle failed to change permissions on " << desiredPath << " error: " << ec.message() << std::endl;
            }
        }
        slider->setPath(desiredPath);
    }

    arrowUp = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    arrowUp->setTexture(GnuIntallDirs::assetsDir() + "/arrow-up.png");

    arrowLeft = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    arrowLeft->setTexture(GnuIntallDirs::assetsDir() + "/arrow-left.png");

    arrowDown = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    arrowDown->setTexture(GnuIntallDirs::assetsDir() + "/arrow-down.png");

    arrowRight = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    arrowRight->setTexture(GnuIntallDirs::assetsDir() + "/arrow-right.png");

    zoomPlus = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    zoomPlus->setTexture(GnuIntallDirs::assetsDir() + "/plus.png");

    zoomMoins = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    zoomMoins->setTexture(GnuIntallDirs::assetsDir() + "/moins.png");

    exposurePlus = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    exposurePlus->setTexture(GnuIntallDirs::assetsDir() + "/plus.png");

    exposureMoins = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    exposureMoins->setTexture(GnuIntallDirs::assetsDir() + "/moins.png");

    gainPlus = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    gainPlus->setTexture(GnuIntallDirs::assetsDir() + "/plus.png");

    gainMoins = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    gainMoins->setTexture(GnuIntallDirs::assetsDir() + "/moins.png");

    auto takeSnapshot = [this]() {
        if (const auto vtoConfig { globalIpc->vtoConfig() }) {
            if (std::filesystem::exists(std::filesystem::path(vtoConfig->pathGet()) / RETICLE_PATH)) {
                vtoConfig->snapshot(channel, 1, RETICLE_PATH);
            }
        }
    };
    snapshot = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    snapshot->setTexture(GnuIntallDirs::assetsDir() + "/touch-record.png");
    snapshot->setClickedFunction(takeSnapshot);

    if (const auto vtoConfig { globalIpc->vtoConfig() }) {

        arrowRight->setClickedFunction([this, vtoConfig]() { vtoConfig->moveVideo(channel, GlobalIPC::Move::Right); });
        arrowLeft->setClickedFunction([this, vtoConfig]() { vtoConfig->moveVideo(channel, GlobalIPC::Move::Left); });
        arrowDown->setClickedFunction([this, vtoConfig]() { vtoConfig->moveVideo(channel, GlobalIPC::Move::Down); });
        arrowUp->setClickedFunction([this, vtoConfig]() { vtoConfig->moveVideo(channel, GlobalIPC::Move::Up); });
        zoomPlus->setClickedFunction([this, vtoConfig]() { vtoConfig->changeZoomEvent(channel, true); });
        zoomMoins->setClickedFunction([this, vtoConfig]() { vtoConfig->changeZoomEvent(channel, false); });
        exposurePlus->setClickedFunction([this, vtoConfig]() { vtoConfig->changeExposureEvent(channel, true); });
        exposureMoins->setClickedFunction([this, vtoConfig]() { vtoConfig->changeExposureEvent(channel, false); });
        gainPlus->setClickedFunction([this, vtoConfig]() { vtoConfig->changeGainEvent(channel, true); });
        gainMoins->setClickedFunction([this, vtoConfig]() { vtoConfig->changeGainEvent(channel, false); });

        auto zoomFunction = [this](float value) {
            video->scaleCenter(value);
            std::ostringstream stream;
            stream << "x" << std::fixed << value;
            zoom->setText(stream.str());
        };
        zoomFactorEventListener = std::make_shared<std::function<void(float)>>(zoomFunction);
        vtoConfig->addZoomFactorListener(channel, zoomFactorEventListener);

        auto setExposureValue = [this](float value) {
            std::ostringstream stream;
            stream << "Exp: " << std::setprecision(3) << std::fixed << value * 0.001 << " ms";
            exposure->setText(stream.str());
        };
        exposureValueEventListener = std::make_shared<std::function<void(float)>>(setExposureValue);
        vtoConfig->addExposureValueListener(channel, exposureValueEventListener);

        auto setGainValue = [this](float value) {
            std::ostringstream stream;
            stream << "Gain: " << std::setprecision(1) << std::fixed << value << " dB";
            gain->setText(stream.str());
        };
        gainValueEventListener = std::make_shared<std::function<void(float)>>(setGainValue);
        vtoConfig->addGainValueListener(channel, gainValueEventListener);
    }
}

ReticlePrivate::~ReticlePrivate() = default;

auto ReticlePrivate::rectangles() -> OU::RectanglePainterObjectsProxy
{
    OU::RectanglePainterObjectsProxy proxy { slider->rectangles() };
    proxy.emplace_back(title->rectangles());
    proxy.emplace_back(video->rectangles());
    proxy.emplace_back(capture->rectangles());
    proxy.emplace_back(exposure->rectangles());
    proxy.emplace_back(gain->rectangles());
    return proxy;
}

auto ReticlePrivate::textures() -> OU::TexturePainterObjectsProxy
{
    auto proxy { slider->textures() };
    proxy.emplace_back(video->textures());
    proxy.emplace_back(capture->textures());
    proxy.emplace_back(arrowUp->textures());
    proxy.emplace_back(arrowLeft->textures());
    proxy.emplace_back(arrowDown->textures());
    proxy.emplace_back(arrowRight->textures());
    proxy.emplace_back(zoomPlus->textures());
    proxy.emplace_back(zoomMoins->textures());
    proxy.emplace_back(exposurePlus->textures());
    proxy.emplace_back(exposureMoins->textures());
    proxy.emplace_back(gainPlus->textures());
    proxy.emplace_back(gainMoins->textures());
    proxy.emplace_back(snapshot->textures());
    return proxy;
}

auto ReticlePrivate::texts() -> OU::TextPainterObjectProxy
{
    OU::TextPainterObjectProxy proxy { title->texts() };
    proxy.emplace_back(exposure->texts());
    proxy.emplace_back(gain->texts());
    proxy.emplace_back(zoom->texts());
    return proxy;
}

void ReticlePrivate::resize(const Rect& rect) noexcept
{
    auto titleRect { rect };
    titleRect.setBottom(rect.top() - TITLE_HEIGHT);
    title->resize(titleRect);

    auto sliderRect { rect };
    sliderRect.setBottom(sliderRect.bottom() + INTER_WIDGET_SPACE);
    sliderRect.setTop(sliderRect.bottom() + SLIDER_HEIGHT);
    slider->resize(sliderRect);

    auto videoRect { rect };
    videoRect.setTop(title->bottom() - INTER_WIDGET_SPACE);
    videoRect.setRight(rect.left() + 0.5F * rect.width()); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    videoRect.setBottom(videoRect.top() - (videoRect.width() / video->textureRatio()));
    // on réduit pour placer les touches
    video->resize(videoRect.reduced(TOUCH_SIZE * 0.5F)); // NOLINT(cppcoreguidelines-avoid-magic-numbers)

    Rect zoomRect { 2.F * LABEL_HEIGHT, LABEL_HEIGHT };

    zoomRect.setCenter(video->textureRect().centerX(), video->textureRect().centerY());
    zoomRect.moveLeft(video->textureRect().left());
    zoomRect.moveTop(video->textureRect().top());
    zoom->resize(zoomRect);

    auto widgetRect { videoRect.reduced(TOUCH_SIZE * 0.5F) }; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    widgetRect.setTop(videoRect.bottom() - INTER_WIDGET_SPACE);
    widgetRect.setBottom(widgetRect.top() - LABEL_HEIGHT);
    exposure->resize(widgetRect);

    widgetRect.moveTop(exposure->bottom() - INTER_WIDGET_SPACE);
    gain->resize(widgetRect);

    // la motié droite pour la vue capture
    auto captureRect { videoRect };
    captureRect.moveLeft(rect.left() + rect.width() * 0.5F); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    capture->resize(captureRect);

    // les touches
    Rect touchRect(TOUCH_SIZE, TOUCH_SIZE);
    touchRect.setCenter(video->textureRect().centerX(), video->textureRect().top());
    arrowUp->resize(touchRect);

    touchRect.setCenter(video->textureRect().left(), video->textureRect().centerY());
    arrowLeft->resize(touchRect);

    touchRect.setCenter(video->textureRect().centerX(), video->textureRect().bottom());
    arrowDown->resize(touchRect);

    touchRect.setCenter(video->textureRect().right(), video->textureRect().centerY());
    arrowRight->resize(touchRect);

    touchRect.setCenter(video->textureRect().right(), video->textureRect().top());
    zoomPlus->resize(touchRect);

    touchRect.setCenter(video->textureRect().left(), video->textureRect().bottom());
    zoomMoins->resize(touchRect);

    touchRect.setCenter(exposure->right(), exposure->centerY());
    exposurePlus->resize(touchRect);

    touchRect.setCenter(exposure->left(), exposure->centerY());
    exposureMoins->resize(touchRect);

    touchRect.setCenter(gain->right(), gain->centerY());
    gainPlus->resize(touchRect);

    touchRect.setCenter(gain->left(), gain->centerY());
    gainMoins->resize(touchRect);

    auto snapchotRect { touchRect.scaled(1.5F) }; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    snapchotRect.setCenter(capture->centerX(), capture->centerY());
    snapchotRect.moveTop(capture->bottom() - INTER_WIDGET_SPACE);
    snapshot->resize(snapchotRect);
}

void ReticlePrivate::handlePress(float x, float y)
{
    slider->handlePress(x, y);
    snapshot->handlePress(x, y);
    arrowRight->handlePress(x, y);
    arrowLeft->handlePress(x, y);
    arrowUp->handlePress(x, y);
    arrowDown->handlePress(x, y);
    zoomPlus->handlePress(x, y);
    zoomMoins->handlePress(x, y);
    exposurePlus->handlePress(x, y);
    exposureMoins->handlePress(x, y);
    gainPlus->handlePress(x, y);
    gainMoins->handlePress(x, y);
}

void ReticlePrivate::handleRelease(float x, float y)
{
    slider->handleRelease(x, y);
    snapshot->handleRelease(x, y);
    arrowRight->handleRelease(x, y);
    arrowLeft->handleRelease(x, y);
    arrowUp->handleRelease(x, y);
    arrowDown->handleRelease(x, y);
    zoomPlus->handleRelease(x, y);
    zoomMoins->handleRelease(x, y);
    exposurePlus->handleRelease(x, y);
    exposureMoins->handleRelease(x, y);
    gainPlus->handleRelease(x, y);
    gainMoins->handleRelease(x, y);
}

void ReticlePrivate::handleMove(float x, float y)
{
    slider->handleMove(x, y);
    snapshot->handleMove(x, y);
    arrowRight->handleMove(x, y);
    arrowLeft->handleMove(x, y);
    arrowUp->handleMove(x, y);
    arrowDown->handleMove(x, y);
    zoomPlus->handleMove(x, y);
    zoomMoins->handleMove(x, y);
    exposurePlus->handleMove(x, y);
    exposureMoins->handleMove(x, y);
    gainPlus->handleMove(x, y);
    gainMoins->handleMove(x, y);
}

Reticle::Reticle(float backgroundDepth, GLPainters* painters, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl { std::make_unique<ReticlePrivate>(backgroundDepth, painters, channel, std::move(globalIpc)) }
{
}

Reticle::~Reticle() = default;

auto Reticle::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    return _impl->rectangles();
}

auto Reticle::textures() const -> OU::TexturePainterObjectsProxy
{
    return _impl->textures();
}

auto Reticle::texts() const -> OU::TextPainterObjectProxy
{
    return _impl->texts();
}

void Reticle::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Reticle::handlePress(float x, float y)
{
    _impl->handlePress(x, y);
}

void Reticle::handleRelease(float x, float y)
{
    _impl->handleRelease(x, y);
}

void Reticle::handleMove(float x, float y)
{
    _impl->handleMove(x, y);
}

} // namespace Jgv::GLWidget
