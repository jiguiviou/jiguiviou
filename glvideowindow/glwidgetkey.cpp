/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetkey.h"

#include "glpagesmanager.h"
#include "glpainters.h"
#include "theme.h"

namespace Jgv::GLWidget {

Key::Key(float backgroundDepth, char normalChar, char majChar, GLPainters* const painters)
    : _depth(backgroundDepth)
    , _painters(painters)
    , _normal(normalChar)
    , _maj(majChar)
{
    _backgroundObject = _painters->rectanglePainter.createObject();
    _textObject = _painters->textPainter.createObject();

    _painters->rectanglePainter.setColor(_backgroundObject, KEY_BACKGROUND_COLOR);
    _painters->textPainter.setText(_textObject, { _normal });
    _painters->textPainter.setColor(_textObject, KEY_TEXT_COLOR);

    _current = _normal;
}

Key::~Key()
{
    _painters->rectanglePainter.destroyObject(_backgroundObject);
    _painters->textPainter.destroyObject(_textObject);
}

void Key::setClickedFunction(std::function<void(char)> function)
{
    _clicked = std::move(function);
}

void Key::toggleMaj()
{
    _current = _current == _normal ? _maj : _normal;

    _painters->textPainter.setText(_textObject, { _current });
    const auto [left, bottom, right, top] { _painters->textPainter.boundingBox(_textObject) };
    Rect textRect { right, top };
    textRect.alignCenter(*this);
    _painters->textPainter.setPosition(_textObject, { textRect.left(), textRect.bottom(), _depth + INTER_LAYOUT_DEPTH });
}

char Key::normal() const noexcept
{
    return _normal;
}

char Key::current() const noexcept
{
    return _current;
}

OU::TextPainterObject Key::textObject() const
{
    return _textObject;
}

OU::RectanglePainterObject Key::rectangleObject() const
{
    return _backgroundObject;
}

void Key::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _painters->rectanglePainter.setRectangle(_backgroundObject, rect.tuple(), _depth);

    const auto [left, bottom, right, top] { _painters->textPainter.boundingBox(_textObject) };
    Rect textRect { right, top };
    textRect.alignCenter(rect);

    _painters->textPainter.setPosition(_textObject, { textRect.left(), textRect.bottom(), _depth + INTER_LAYOUT_DEPTH });
}

void Key::clicked()
{
    if (_clicked) {
        _clicked(_current);
    }
}

} // namespace Jgv::GLWidget
