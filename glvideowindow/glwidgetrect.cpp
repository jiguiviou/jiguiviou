/**************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetrect.h"
#include "theme.h"

namespace Jgv::GLWidget {

Rect::Rect(float left, float bottom, float right, float top)
    : _left(left)
    , _bottom(bottom)
    , _right(right)
    , _top(top)
{
}

Rect::Rect(float width, float height)
    : _right(width)
    , _top(height)
{
}

void Rect::setLeft(float left) noexcept
{
    _left = left;
}

void Rect::moveLeft(float left) noexcept
{
    const auto W { width() };
    _right = left + W;
    _left = left;
}

float Rect::left() const noexcept
{
    return _left;
}

void Rect::setBottom(float bottom) noexcept
{
    _bottom = bottom;
}

void Rect::moveBottom(float bottom) noexcept
{
    const auto H(height());
    _bottom = bottom;
    _top = _bottom + H;
}

float Rect::bottom() const noexcept
{
    return _bottom;
}

void Rect::setRight(float right)
{
    _right = right;
}

float Rect::right() const noexcept
{
    return _right;
}

void Rect::setTop(float top) noexcept
{
    _top = top;
}

void Rect::moveTop(float top) noexcept
{
    const auto H(height());
    _top = top;
    _bottom = top - H;
}

float Rect::top() const noexcept
{
    return _top;
}

float Rect::width() const noexcept
{
    return _right - _left;
}

float Rect::height() const noexcept
{
    return _top - _bottom;
}

float Rect::centerX() const noexcept
{
    return _left + HALF * (width());
}

float Rect::centerY() const noexcept
{
    return _bottom + HALF * (height());
}

bool Rect::contains(float x, float y) const
{
    return x > _left && x < _right && y > _bottom && y < _top;
}

bool Rect::intersect(const Rect& other) const
{
    return other.right() > _left && other.left() < _right && other.bottom() < _top && other.top() > _bottom;
}

void Rect::alignLeft(const Rect& other)
{
    const auto w { width() };
    _left = other.left();
    _right = _left + w;
    // centrage verticale
    const auto h { height() };
    _bottom = { other.bottom() + HALF * (other.height() - height()) };
    _top = _bottom + h;
}

void Rect::alignCenter(const Rect& other)
{
    setCenter(other.centerX(), other.centerY());
}

void Rect::setCenter(float x, float y)
{
    const auto dw { 0.5F * width() };
    _left = x - dw;
    _right = x + dw;

    const auto dh { 0.5F * height() };
    _bottom = y - dh;
    _top = y + dh;
}

void Rect::translate(float x, float y)
{
    _left += x;
    _right += x;
    _bottom += y;
    _top += y;
}

void Rect::scaleWidth(float k)
{
    const auto d { 0.5F * width() * k };
    _left += d;
    _right -= d;
}

void Rect::scaleKeepAspectRatio(const Rect& other)
{
    const auto scaleX { other.width() / width() };
    const auto scaleY { other.height() / height() };
    setCenter(other.centerX(), other.centerY());

    if (scaleX < scaleY) {
        *this = this->scaled(scaleX);

    } else {
        *this = this->scaled(scaleY);
    }
}

void Rect::scaleKeepAspectRatio(Rect&& other)
{
    const auto scaleX { other.width() / width() };
    const auto scaleY { other.height() / height() };
    setCenter(other.centerX(), other.centerY());

    if (scaleX < scaleY) {
        *this = this->scaled(scaleX);

    } else {
        *this = this->scaled(scaleY);
    }
}

Rect Rect::square() const noexcept
{
    const auto d { 0.5F * (height() - width()) };
    return *this + ((d > 0.F) ? Rect { 0.F, d, 0.F, -d } : Rect { -d, 0.F, d, 0.F });
}

Rect Rect::scaled(float k) const noexcept
{
    const auto dx { (1.F - k) * width() * 0.5F };
    const auto dy { (1.F - k) * height() * 0.5F };
    return *this + Rect { dx, dy, -dx, -dy };
}

Rect Rect::reduced(float length) const noexcept
{
    return Rect { _left + length, _bottom + length, _right - length, _top - length };
}

void Rect::resize(const Rect& rect) noexcept
{
    *this = rect;
}

Rect Rect::operator+(const Rect& other) const noexcept
{
    return Rect(_left + other._left, _bottom + other._bottom, _right + other._right, _top + other._top);
}

auto Rect::rect() const noexcept -> Rect
{
    return (*this);
}

std::tuple<float, float, float, float> Rect::tuple() const noexcept
{
    return { _left, _bottom, _right, _top };
}

} // namespace Jgv::GLWidget
