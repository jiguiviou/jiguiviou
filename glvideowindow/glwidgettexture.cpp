/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgettexture.h"
#include "glwidgettexture_p.h"

#include "glpagesmanager.h"
#include "theme.h"

namespace Jgv::GLWidget {

TexturePrivate::TexturePrivate(float backgroundDepth, GLPainters* const p, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : depth(backgroundDepth)
    , painters(p)
    , globalIpc(std::move(_globalIpc))
{
    // rectangle pour la décoration
    backgroundObject = painters->rectanglePainter.createObject();
    // la texture à dessiner
    textureObject = painters->texturePainter.createObject();
    painters->rectanglePainter.setColor(backgroundObject, TEXTURE_BACKGROUND_COLOR);
}

TexturePrivate::~TexturePrivate()
{
    painters->texturePainter.destroyObject(textureObject);
    painters->rectanglePainter.destroyObject(backgroundObject);
}

void TexturePrivate::resize(const Rect& rect)
{
    painters->rectanglePainter.setRectangle(backgroundObject, rect.tuple(), depth);
    painters->texturePainter.setCenterScaleFactor(textureObject, scaleFactor);
    painters->texturePainter.setRectangle(textureObject, rect.reduced(DECORATION_WIDTH).tuple(), depth + INTER_LAYOUT_DEPTH);
}

Texture::Texture(float backgroundDepth, GLPainters* const painters, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl(std::make_unique<TexturePrivate>(backgroundDepth, painters, std::move(globalIpc)))
{
}

Texture::~Texture() = default;

void Texture::setClickedFunction(std::function<void(void)> clicked)
{
    _impl->clicked = std::move(clicked);
}

void Texture::addClipping(OU::TextureClipObject texture, OU::RectangleClippingObject rectangle)
{
    _impl->painters->texturePainter.setClipObject(_impl->textureObject, texture);
    _impl->painters->rectanglePainter.setClipObject(_impl->backgroundObject, rectangle);
}

void Texture::addScrollVector(OpenGLUtils::TextureScrollObject texture, OU::RectangleScrollObject rectangle)
{
    _impl->painters->texturePainter.setScrollObject(_impl->textureObject, texture);
    _impl->painters->rectanglePainter.setScrollObject(_impl->backgroundObject, rectangle);
}

void Texture::setBackgroundColor(const Color& color)
{
    _impl->painters->rectanglePainter.setColor(_impl->backgroundObject, color);
}

void Texture::scaleCenter(float factor)
{
    _impl->scaleFactor = factor;
    _impl->painters->texturePainter.setCenterScaleFactor(_impl->textureObject, _impl->scaleFactor);
}

auto Texture::scaleFactor() const -> float
{
    return _impl->scaleFactor;
}

auto Texture::textureRect() const -> Rect
{
    return this->reduced(DECORATION_WIDTH);
}

auto Texture::textureRatio() const -> float
{
    return _impl->textureRatio.to;
}

void Texture::setTexture(Channel::Type channel)
{
    auto update = [this](std::tuple<uint32_t, uint32_t, uint32_t> tuple) {
        const auto [texture, width, height] { tuple };
        if (height > 0) {
            _impl->painters->texturePainter.updateExternalTexture(_impl->textureObject, texture);
            _impl->textureRatio.reg.store(static_cast<const float>(width) / static_cast<const float>(height));
        }
    };
    _impl->updateTextureFunction = std::make_shared<std::function<void(std::tuple<uint32_t, uint32_t, uint32_t>)>>(update);
    _impl->globalIpc->addTextureChangedListener(channel, _impl->updateTextureFunction);

    // initialise
    const auto [texture, width, height] { _impl->globalIpc->texture(channel) };
    if (height > 0) {
        _impl->painters->texturePainter.setExternalTexture(_impl->textureObject, texture);
        _impl->textureRatio.reg.store(static_cast<const float>(width) / static_cast<const float>(height));
        _impl->textureRatio.to = static_cast<const float>(width) / static_cast<const float>(height);
    }
}

auto Texture::textureRatioChanged() -> bool
{
    return _impl->textureRatio.changed();
}

void Texture::setTexture(std::string_view filename)
{
    if (_impl->texture.index > 0U) {
        _impl->painters->texturePainter.destroyTextureObject(_impl->texture);
    }

    // on affiche seulement le rectangle dans le cas où le nom de fichier est vide
    if (filename.empty()) {
        _impl->painters->texturePainter.setTexture(_impl->textureObject, { 0U });
        _impl->texture = { 0U };

    } else {
        auto texture { _impl->painters->texturePainter.createTextureObject(std::string { filename }) };
        _impl->painters->texturePainter.setTexture(_impl->textureObject, texture);
        const auto [width, height] { _impl->painters->texturePainter.textureSize(texture) };
        if (height > 0) {
            const auto ratio { static_cast<float>(width) / static_cast<float>(height) };
            _impl->textureRatio.reg.store(ratio);
            _impl->textureRatio.to = ratio;
        }
        _impl->texture = texture;
    }
}

auto Texture::rectangles() const -> OU::RectanglePainterObjectsVector
{
    return { _impl->backgroundObject };
}

auto Texture::textures() const -> OU::TexturePainterObjectsVector
{
    return { _impl->textureObject };
}

void Texture::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Texture::clicked()
{
    if (_impl->clicked) {
        _impl->clicked();
    }
}

} // namespace Jgv
