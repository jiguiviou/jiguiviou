/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETKEYBOARD_H
#define GLWIDGETKEYBOARD_H

#include "glwidgetframe.h"

#include <functional>
#include <memory>

namespace Jgv::GLWidget {

class KeyboardPrivate;
class Keyboard : public Frame {
public:
    explicit Keyboard(float backgroundDepth, GLPainters* painters);
    ~Keyboard() override;
    Keyboard(const Keyboard&) = delete;
    Keyboard(Keyboard&&) = delete;
    auto operator=(const Keyboard&) -> Keyboard& = delete;
    auto operator=(Keyboard &&) -> Keyboard& = delete;

    void setSetTextFunction(std::function<void(std::string_view)> setText);
    void setGetTextFunction(std::function<std::string(void)> text);

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsVector;
    [[nodiscard]] auto textures() const -> OU::TexturePainterObjectsVector;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectVector;

    void resize(const Rect& rect) noexcept override;

    void handlePress(float x, float y) override;
    void handleRelease(float x, float y) override;
    void handleMove(float x, float y) override;

private:
    const std::unique_ptr<KeyboardPrivate> _impl;

}; // class Keyboard

} // namespace Jgv::GLWidget

#endif // GLWIDGETKEYBOARD_H
