/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef THEME_H
#define THEME_H

#include <tuple>

namespace Jgv::GLWidget {

constexpr auto HALF { 0.5F };

constexpr auto MAIN_BACKGROUND_COLOR { std::make_tuple(0.2F, 0.2F, 0.2F, 1.F) };
constexpr auto MAIN_TEXT_COLOR { std::make_tuple(0.9F, 0.9F, 0.9F, 1.F) };

constexpr auto FOCUS_BACKGROUND_COLOR { std::make_tuple(0.F, 1.F, 0.F, 1.F) };

constexpr auto TEXTURE_BACKGROUND_COLOR { std::make_tuple(1.F, 1.F, 1.F, 1.F) };

constexpr auto DECORATION_COLOR { std::make_tuple(1.F, 1.F, 1.F, 1.F) };
constexpr auto DECORATION_WIDTH { 5.F };

constexpr auto SLIDER_HEIGHT { 90.F };
constexpr auto SLIDER_BACKGROUND_COLOR { std::make_tuple(0.5F, 0.5F, 0.5F, 1.F) };
constexpr auto SLIDER_IMAGE_BACKGROUND_COLOR { std::make_tuple(0.8F, 0.8F, 0.8F, 1.F) };

constexpr auto SCROLLBAR_CURSOR_COLOR { std::make_tuple(0.1F, 0.1F, 0.1F, 1.F) };
constexpr auto SCROLLBAR_HEIGHT { 8.F };

constexpr auto RADIO_BACKGROUND_COLOR { std::make_tuple(0.F, 0.F, 0.F, 1.F) };
constexpr auto RADIO_TEXT_COLOR { std::make_tuple(1.F, 1.F, 1.F, 1.F) };
constexpr auto RADIO_HEIGHT { 40.F };

constexpr auto TITLE_TEXT_COLOR { std::make_tuple(0.F, 1.F, 1.F, 1.F) };
constexpr auto TITLE_BACKGROUND_COLOR { std::make_tuple(0.25F, 0.25F, 0.25F, 1.F) };

constexpr auto LABEL_HEIGHT { 45.F };
constexpr auto LABEL_BACKGROUND_COLOR { std::make_tuple(0.F, 0.F, 0.F, 1.F) };
constexpr auto LABEL_TEXT_COLOR { std::make_tuple(0.9F, 0.9F, 0.9F, 1.F) };

constexpr auto KEY_TEXT_COLOR { std::make_tuple(0.2F, 0.2F, 0.2F, 1.F) };
constexpr auto KEY_BACKGROUND_COLOR { std::make_tuple(0.9F, 0.9F, 0.9F, 1.F) };

constexpr auto TERMINAL_TEXT_COLOR { std::make_tuple(1.F, 1.F, 1.F, 1.F) };
constexpr auto TERMINAL_BACKGROUND_COLOR { std::make_tuple(0.F, 0.F, 0.F, 1.F) };

constexpr auto TOUCH_SIZE { 64.F };

constexpr auto BACKGROUND_DEPTH { -90.F };
constexpr auto INTER_WIDGET_DEPTH { 10.F };
constexpr auto WIDGET_BACKGROUND_DEPTH { BACKGROUND_DEPTH + INTER_WIDGET_DEPTH };
constexpr auto INTER_LAYOUT_DEPTH { 2.F };
constexpr auto INTER_WIDGET_SPACE { 10.F };
constexpr auto TITLE_HEIGHT { 50.F };

constexpr auto EDITOR_TEXT_MAX_SIZE { 32U };

} // namespace Jgv::GLWidget

#endif // THEME_H
