/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETVOLUME_P_H
#define GLWIDGETVOLUME_P_H

#include "glpainters.h"

#include <functional>

namespace Jgv {
class Rect;
} // namespace Jgv

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

struct VolumePrivate {
    VolumePrivate(float backgroundDepth, GLPainters* p, std::string_view _uuid, std::shared_ptr<GlobalIPC::Object> _globalIpc);
    ~VolumePrivate();
    VolumePrivate(const VolumePrivate&) = delete;
    VolumePrivate(VolumePrivate&&) = delete;
    auto operator=(const VolumePrivate&) -> VolumePrivate& = delete;
    auto operator=(VolumePrivate &&) -> VolumePrivate& = delete;

    const float depth;
    GLPainters* const painters;
    const std::string uuid;
    const std::shared_ptr<GlobalIPC::Object> globalIpc;

    void resize(const Rect& rect);

    OU::RectanglePainterObject backgroundFar { 0U };
    OU::RectanglePainterObject backgroundNear { 0U };
    OU::TexturePainterObject icon { 0U };
    OU::RectanglePainterObject iconBackground { 0U };
    OU::TextureObject logoTexture { 0U };
    OU::TextPainterObject driveName { 0U };
    OU::TextPainterObject mountPoint { 0U };
    OU::TextPainterObject space { 0U };
    OU::TextClipObject clippingText { 0U };

    std::function<void(void)> clicked;

}; // struct VolumePrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETVOLUME_P_H
