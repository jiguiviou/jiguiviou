/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLBUTTONSCOLUMN_H
#define GLBUTTONSCOLUMN_H

#include "glwidgetrect.h"

#include <memory>

namespace Jgv::GLWidget {

class ButtonsColumnPrivate;
class ButtonsColumn : public Rect {
public:
    ButtonsColumn();
    ~ButtonsColumn() override;
    ButtonsColumn(const ButtonsColumn&) = delete;
    ButtonsColumn(ButtonsColumn&&) = delete;
    auto operator=(const ButtonsColumn&) -> ButtonsColumn& = delete;
    auto operator=(ButtonsColumn &&) -> ButtonsColumn& = delete;

    void setCellsCount(std::size_t cellsCount);
    [[nodiscard]] auto cellsCount() const -> std::size_t;

    [[nodiscard]] auto rectOf(std::size_t index) const -> Rect;

    void resize(const Rect& rect) noexcept override;

private:
    const std::unique_ptr<ButtonsColumnPrivate> _impl;

}; // class ButtonsColumn

} // namespace Jgv::GLWidget

#endif // GLBUTTONSCOLUMN_H
