/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetimagesslider.h"
#include "glpagesmanager.h"
#include "glwidgetimagesslider_p.h"
#include "glwidgetscrollbar.h"
#include "glwidgettexture.h"
#include "theme.h"

#include <climits>
#include <cstring>
#include <iostream>
#include <poll.h>
#include <sys/inotify.h>
#include <unistd.h>

namespace Jgv::GLWidget {

constexpr std::size_t NOTIFY_BUFFER_SIZE { 4096 };

ImagesSliderPrivate::ImagesSliderPrivate(float backgroundDepth, GLPainters* const p, std::shared_ptr<Jgv::GlobalIPC::Object> _globalIpc)
    : depth(backgroundDepth)
    , painters(p)
    , globalIpc(std::move(_globalIpc))
{
    decorationObject = painters->rectanglePainter.createObject();
    backgroundObject = painters->rectanglePainter.createObject();
    texturesScrollObject = painters->texturePainter.createScrollObject();
    rectanglesScrollObject = painters->rectanglePainter.createScrollObject();
    texturesClipObject = painters->texturePainter.createClipObject();
    rectanglesClipObject = painters->rectanglePainter.createClipObject();

    painters->rectanglePainter.setColor(decorationObject, DECORATION_COLOR);
    painters->rectanglePainter.setColor(backgroundObject, SLIDER_BACKGROUND_COLOR);

    scrollBar = std::make_unique<ScrollBar>(depth + INTER_WIDGET_DEPTH, painters);

    notifyFd = inotify_init1(IN_NONBLOCK);
}

ImagesSliderPrivate::~ImagesSliderPrivate()
{
    painters->rectanglePainter.destroyObject(decorationObject);
    painters->rectanglePainter.destroyObject(backgroundObject);
    painters->texturePainter.destroyScrollObject(texturesScrollObject);
    painters->rectanglePainter.destroyScrollObject(rectanglesScrollObject);
    painters->texturePainter.destroyClipObject(texturesClipObject);
    painters->rectanglePainter.destroyClipObject(rectanglesClipObject);

    close(notifyFd);
}

void ImagesSliderPrivate::resize(const Rect& rect)
{
    painters->rectanglePainter.setRectangle(decorationObject, rect.tuple(), depth);
    auto backgroundRect { rect.reduced(DECORATION_WIDTH) };
    painters->rectanglePainter.setRectangle(backgroundObject, backgroundRect.tuple(), depth + INTER_LAYOUT_DEPTH);

    auto scrollBarRect { backgroundRect };
    scrollBarRect.setTop(backgroundRect.bottom() + SCROLLBAR_HEIGHT);
    scrollBar->resize(scrollBarRect);

    painters->texturePainter.setClippingRectangle(texturesClipObject, { backgroundRect.left<int>(), backgroundRect.bottom<int>(), backgroundRect.width<int>(), backgroundRect.height<int>() });
    painters->rectanglePainter.updateClippingRectangle(rectanglesClipObject, { backgroundRect.left<int>(), backgroundRect.bottom<int>(), backgroundRect.width<int>(), backgroundRect.height<int>() });

    auto cell { cells.begin() };
    while (cell != cells.end()) {
        resizeCell(cell, backgroundRect);
        ++cell;
    }

    // on scroll jusque la cellule qui a le focus
    auto cellFocus = cells.cbegin();
    std::advance(cellFocus, lastSelected);
    if (cellFocus != cells.cend()) {
        scrollX = rect.right() - cellFocus->rect.right();
        scroll(0);
    }

    alignScroll(backgroundRect);
    optimizeTextures(backgroundRect);
}

auto ImagesSliderPrivate::readFromNotify() -> std::set<std::filesystem::path>
{
    std::set<std::filesystem::path> paths;

    std::array<uint8_t, NOTIFY_BUFFER_SIZE> buffer {};
    auto p { buffer.data() };
    auto size { read(notifyFd, buffer.data(), buffer.size()) };

    const auto end { p + size };

    while (p < end) {
        const inotify_event* event { reinterpret_cast<const inotify_event*>(p) };
        if ((event->mask & IN_CLOSE_WRITE) > 0) {
            paths.insert(std::string { event->name });
        }
        p += sizeof(inotify_event) + event->len;
    }

    return paths;
}

auto ImagesSliderPrivate::readFromPath() -> std::set<std::filesystem::path>
{
    std::set<std::filesystem::path> images;
    for (auto& entry : std::filesystem::directory_iterator(path)) {
        if (entry.is_regular_file()) {
            images.insert(entry.path());
        }
    }
    return images;
}

auto ImagesSliderPrivate::addImagePath(const std::filesystem::path& imagePath) -> std::vector<Cell>::iterator
{
    auto it { cells.insert(cells.cend(), Cell { path / imagePath, Rect(), std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH, painters, globalIpc) }) };
    it->widget->setTexture(it->filename);
    it->widget->addClipping(texturesClipObject, rectanglesClipObject);
    it->widget->addScrollVector(texturesScrollObject, rectanglesScrollObject);
    it->widget->setBackgroundColor(std::make_tuple(1.F, 0.F, 0.F, 1.F));
    return it;
}

void ImagesSliderPrivate::updateCellsFromPathSet(const std::set<std::filesystem::path>& set, const Rect& viewRect)
{
    for (auto& path : set) {
        auto it { addImagePath(path) };
        resizeCell(it, viewRect);
    }
    // on met le focus sur la dernière cellule ajoutée
    setFocus(cells.back().filename);
    // on scroll à l'infini, puis on aligne auto ( donc sur le dernier )
    scrollX = -std::numeric_limits<float>::infinity();
    alignScroll(viewRect);
    // on détruit les tectures non visibles
    optimizeTextures(viewRect);
}

void ImagesSliderPrivate::updateCellsFromInotify(const Rect& viewRect)
{
    const auto files { readFromNotify() };
    if (!files.empty()) {
        updateCellsFromPathSet(files, viewRect);
    }
}

void ImagesSliderPrivate::updateCellsFromPath(const Rect& viewRect)
{
    const auto files { readFromPath() };
    if (!files.empty()) {
        updateCellsFromPathSet(files, viewRect);
    }
}

void ImagesSliderPrivate::resizeCell(std::vector<Cell>::iterator cell, const Rect& viewRect)
{
    // si c'est la première image, on calcule la taille de la cellule, sinon on utilise la taille de la précédente
    if (cell == cells.begin()) {
        // détermine la taille de la première case
        auto firstRect { viewRect };
        firstRect.setBottom(scrollBar->top());
        const auto firstRectWidth { viewRect.width() / static_cast<float>(cellsCount) };
        firstRect.setRight(firstRect.left() + firstRectWidth);
        cell->rect = firstRect;
        auto widgetRect { firstRect };
        widgetRect.setRight(firstRect.left() + firstRect.height() * cell->widget->textureRatio());
        widgetRect.setCenter(firstRect.centerX(), firstRect.centerY());
        cell->widget->resize(widgetRect);

    } else {
        auto rect { std::prev(cell)->rect };
        rect.translate(rect.width(), 0.F);
        cell->rect = rect;
        auto widgetRect { rect };
        widgetRect.setRight(rect.left() + rect.height() * cell->widget->textureRatio());
        widgetRect.setCenter(rect.centerX(), rect.centerY());
        cell->widget->resize(widgetRect);
    }
    // la taille du curseur de la scrollbar est la taille unitaire de la cellule
    // multipliée par le nombre max de cellules visibles
    scrollBar->setCursorLength(cellsCount * cell->rect.width(), cells.size() * cell->rect.width());
}

void ImagesSliderPrivate::optimizeTextures(const Rect& rect)
{
    // gère la visibilité
    for (auto& image : cells) {
        auto localRect { image.rect };
        // translation dans le repère courant
        localRect.translate(scrollX, 0.F);
        image.draw = localRect.intersect(rect);
        // on décharge les images non visibles
        if (image.draw) {
            image.widget->setTexture(image.filename);
        } else {
            image.widget->setTexture("");
        }
    }
}

void ImagesSliderPrivate::scroll(float dx)
{
    scrollX += dx;
    painters->texturePainter.setScrollVector(texturesScrollObject, scrollX, 0.F);
    painters->rectanglePainter.updateScrollVector(rectanglesScrollObject, scrollX, 0.F);
    scrollBar->scrollCursor(scrollX * static_cast<float>(cellsCount) / static_cast<float>(cells.size()));
}

void ImagesSliderPrivate::alignScroll(const Rect& rect)
{
    if (scrollX > 0.F) {
        scrollX = 0.F;
    }
    // si les cellules ne sont pas pleines, on aligne systèmatiquement à gauche
    else if (std::distance(cells.cbegin(), cells.cend()) <= cellsCount) {
        // on aligne par la gauche
        scrollX = 0.F;
    } else if (scrollX < rect.right() - cells.back().rect.right()) {
        scrollX = rect.right() - cells.back().rect.right();
    }

    painters->texturePainter.setScrollVector(texturesScrollObject, scrollX, 0.F);
    painters->rectanglePainter.updateScrollVector(rectanglesScrollObject, scrollX, 0.F);

    scrollBar->scrollCursor(scrollX * static_cast<float>(cellsCount) / static_cast<float>(cells.size()));
}

void ImagesSliderPrivate::setFocus(const std::filesystem::path& imagePath)
{
    // defocus tout le monde
    for (auto& cell : cells) {
        if (cell.widget) {
            cell.widget->setBackgroundColor(SLIDER_IMAGE_BACKGROUND_COLOR);
        }
    }

    // trouve la cellule
    auto it = std::find_if(cells.cbegin(), cells.cend(), [&imagePath](const auto& value) {
        return imagePath.compare(value.filename) == 0;
    });

    if (it != cells.cend() && it->widget) {
        it->widget->setBackgroundColor(FOCUS_BACKGROUND_COLOR);
        lastSelected = std::distance(cells.cbegin(), it);
        if (selectImage) {
            selectImage(it->filename);
        }
    }
}

void ImagesSliderPrivate::handleClick(float x, float y)
{
    // on cherche la première cellule visible
    auto it = std::find_if(cells.cbegin(), cells.cend(), [](const auto& value) {
        return value.draw;
    });

    while (it != cells.cend() && it->draw) {
        if (it->rect.contains(x - scrollX, y)) {
            it->widget->setBackgroundColor(FOCUS_BACKGROUND_COLOR);
            if (selectImage) {
                selectImage(it->filename);
            }

            // defocus de l'ancien
            auto last { cells.cbegin() };
            std::advance(last, lastSelected);
            last->widget->setBackgroundColor(TEXTURE_BACKGROUND_COLOR);

            lastSelected = std::distance(cells.cbegin(), it);

            break;
        }
        ++it;
    }
}

ImagesSlider::ImagesSlider(float backgroundDepth, GLPainters* const painters, std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc)
    : _impl(std::make_unique<ImagesSliderPrivate>(backgroundDepth, painters, globalIpc))
{
}

ImagesSlider::~ImagesSlider() = default;

void ImagesSlider::setBackgroundColor(const Color& color)
{
    _impl->painters->rectanglePainter.setColor(_impl->backgroundObject, color);
}

void ImagesSlider::setPath(std::filesystem::path path)
{
    // on coupe l'observation inotify
    if (_impl->watchFd > 0) {
        inotify_rm_watch(_impl->notifyFd, _impl->watchFd);
        _impl->watchFd = 0;
    }

    // supprime toutes les cellules
    _impl->cells.clear();

    // reset le scroll
    _impl->scrollBar->reset();

    // prévient les abonnés
    if (_impl->selectImage) {
        _impl->selectImage("");
    }

    // met à jour les cellules selon le contenu du path
    _impl->path = std::move(path);
    _impl->updateCellsFromPath((*this).reduced(DECORATION_WIDTH));

    // crée un observateur "création" sur le nouveau path
    _impl->watchFd = inotify_add_watch(_impl->notifyFd, _impl->path.c_str(), IN_CLOSE_WRITE);
}

void ImagesSlider::setSelectfunction(std::function<void(const std::string&)> selectImageFunction)
{
    _impl->selectImage = std::move(selectImageFunction);
}

auto ImagesSlider::rectangles() const -> std::vector<std::vector<OU::RectanglePainterObject>>
{
    _impl->updateCellsFromInotify((*this).reduced(DECORATION_WIDTH));

    std::vector<std::vector<OU::RectanglePainterObject>> proxy { { _impl->decorationObject, _impl->backgroundObject } };
    proxy.emplace_back(_impl->scrollBar->rectangles());
    for (const auto& image : _impl->cells) {
        if (image.draw) {
            proxy.emplace_back(image.widget->rectangles());
        }
    }
    return proxy;
}

auto ImagesSlider::textures() const -> std::vector<std::vector<OU::TexturePainterObject>>
{
    OU::TexturePainterObjectsProxy proxy;
    for (const auto& image : _impl->cells) {
        if (image.draw) {
            proxy.emplace_back(image.widget->textures());
        }
    }
    return proxy;
}

void ImagesSlider::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void ImagesSlider::handlePress(float x, float y)
{
    if (contains(x, y) && !_impl->pressed) {
        _impl->pressed = true;
        _impl->pressedX = x;
        _impl->translateOrigin = x;
    }
}

void ImagesSlider::handleRelease(float x, float y)
{
    if (contains(x, y) && _impl->pressed) {
        _impl->pressed = false;
        // sur une fin de scroll, on aligne la vue
        if (_impl->scrolling) {
            _impl->alignScroll(*this);
            _impl->optimizeTextures(*this);
        } else {
            // si on scroll pas, c'est un click
            _impl->handleClick(x, y);
        }
        _impl->scrolling = false;
    }
}

void ImagesSlider::handleMove(float x, float y)
{
    // un move en dehors de la suface annule le press et le scroll
    if (!contains(x, y)) {
        _impl->pressed = false;
        // sur une fin de scroll, on aligne la vue
        if (_impl->scrolling) {
            _impl->alignScroll(*this);
            _impl->optimizeTextures(*this);
        }
        _impl->scrolling = false;
    }
    // un move pressed provoque un scroll
    else if (_impl->pressed) {
        _impl->scrolling = true;
        auto oldPosition { std::exchange(_impl->translateOrigin, x) };
        _impl->scroll(x - oldPosition);
        _impl->optimizeTextures(*this);
    }
}

} // namespace Jgv::GLWidget
