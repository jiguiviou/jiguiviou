/***************************************************************************
 *   Copyright (C) 2014-2018 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETLABEL_H
#define GLWIDGETLABEL_H

#include "glwidgetframe.h"

#include <memory>
#include <string>

namespace Jgv::GLWidget {

using Color = std::tuple<float, float, float, float>;

class LabelPrivate;
class Label final : public Frame {

public:
    explicit Label(float backgroundDepth, GLPainters* painters);
    ~Label() override;
    Label(const Label&) = delete;
    Label(Label&&) = delete;
    auto operator=(const Label&) -> Label& = delete;
    auto operator=(Label &&) -> Label& = delete;

    void reserve(std::size_t textMaxSize);
    void setText(std::string_view text);
    void setTextColor(const Color& color);
    void setBackgroundColor(const Color& color);

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsVector;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectVector;

    void resize(const Rect& rect) noexcept override;

private:
    const std::unique_ptr<LabelPrivate> _impl;

}; // class Label

} // namespace Jgv::GLWidget

#endif // GLWIDGETLABEL_H
