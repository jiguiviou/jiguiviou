/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetscrollbar.h"
#include "glwidgetscrollbar_p.h"

#include "glpagesmanager.h"
#include "theme.h"

#include <iostream>

namespace Jgv::GLWidget {

ScrollBarPrivate::ScrollBarPrivate(float backgroundDepth, GLPainters* const p)
    : depth(backgroundDepth)
    , painters(p)
{
    cursorObject = painters->rectanglePainter.createObject();
    clipObject = painters->rectanglePainter.createClipObject();
    scrollObject = painters->rectanglePainter.createScrollObject();

    painters->rectanglePainter.setClipObject(cursorObject, clipObject);
    painters->rectanglePainter.setScrollObject(cursorObject, scrollObject);
    painters->rectanglePainter.setColor(cursorObject, SCROLLBAR_CURSOR_COLOR);
}

ScrollBarPrivate::~ScrollBarPrivate()
{
    painters->rectanglePainter.destroyObject(cursorObject);
    painters->rectanglePainter.destroyClipObject(clipObject);
    painters->rectanglePainter.destroyScrollObject(scrollObject);
}

ScrollBar::ScrollBar(float backgroundDepth, GLPainters* const painters)
    : _impl(std::make_unique<ScrollBarPrivate>(backgroundDepth, painters))
{
}

ScrollBar::~ScrollBar() = default;

void ScrollBar::setCursorLength(float length, float maximum)
{
    Rect cursorRect { *this };
    cursorRect.setRight(left() + length * width() / maximum);
    _impl->painters->rectanglePainter.setRectangle(_impl->cursorObject, cursorRect.tuple(), _impl->depth);
}

void ScrollBar::scrollCursor(float scroll)
{
    _impl->painters->rectanglePainter.updateScrollVector(_impl->scrollObject, -scroll, 0.F);
}

void ScrollBar::reset()
{
    _impl->painters->rectanglePainter.setRectangle(_impl->cursorObject, { 0.F, 0.F, 0.F, 0.F }, _impl->depth);
}

OU::RectanglePainterObjectsVector ScrollBar::rectangles() const
{
    return { _impl->cursorObject };
}

void ScrollBar::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->painters->rectanglePainter.updateClippingRectangle(_impl->clipObject, { rect.left<int>(), rect.bottom<int>(), rect.width<int>(), rect.height<int>() });
}

} // namespace Jgv::GLWidget
