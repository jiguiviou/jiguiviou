/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETTERMINAL_P_H
#define GLWIDGETTERMINAL_P_H

#include "glpainters.h"
#include "moodycamel/readerwriterqueue.h"

#include <functional>
#include <memory>
#include <vector>

namespace Jgv::GlobalIPC {
class Object;
enum class OutputLevel;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

class Rect;

struct TerminalPrivate {
    explicit TerminalPrivate(float backgroundDepth, GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~TerminalPrivate();
    TerminalPrivate(const TerminalPrivate&) = delete;
    TerminalPrivate(TerminalPrivate&&) = delete;
    auto operator=(const TerminalPrivate&) -> TerminalPrivate& = delete;
    auto operator=(TerminalPrivate &&) -> TerminalPrivate& = delete;

    const float depth;
    GLPainters* const painters;
    const std::shared_ptr<GlobalIPC::Object> globalIpc;
    std::shared_ptr<std::function<void(GlobalIPC::OutputLevel, std::string_view)>> logOuput;

    std::vector<OU::TextPainterObject> texts;
    OU::RectanglePainterObject rectangle { 0U };
    OU::TextClipObject clipObject { 0U };
    OU::TextScrollObject scrollObject { 0U };

    std::tuple<float, float, float> textOrigin { 0.F, 0.F, 0.F };
    float bottom { 0.F }; // pour le calcul du vecteur de scrolling
    OU::TextPainterObject referencedText { 0U };
    std::atomic<bool> finished { false };

    moodycamel::ReaderWriterQueue<std::tuple<GlobalIPC::OutputLevel, std::string>> messagesQueue;

    void initializeGL();
    void destroyGL();
    void dispatchMessage(GlobalIPC::OutputLevel level, std::string_view message);
    void addText(GlobalIPC::OutputLevel level, std::string_view message);
    void updateScrollingVector();
    void resize(const Rect& rect);

}; // struct TerminalPrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETTERMINAL_P_H
