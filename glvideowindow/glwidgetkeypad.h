/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETKEYPAD_H
#define GLWIDGETKEYPAD_H

#include "glwidgetframe.h"

#include <functional>
#include <memory>

namespace Jgv::GLWidget {

class KeypadPrivate;
class Keypad : public Frame {
public:
    explicit Keypad(float backgroundDepth, GLPainters* painters);
    ~Keypad() override;
    Keypad(const Keypad&) = delete;
    Keypad(Keypad&&) = delete;
    auto operator=(const Keypad&) -> Keypad& = delete;
    auto operator=(Keypad &&) -> Keypad& = delete;

    void setSetTextFunction(std::function<void(std::string_view)> setText);
    void setGetTextFunction(std::function<std::string_view(void)> text);

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsVector;
    [[nodiscard]] auto textures() const -> OU::TexturePainterObjectsVector;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectVector;

    void resize(const Rect& rect) noexcept override;

    void handlePress(float x, float y) override;
    void handleRelease(float x, float y) override;
    void handleMove(float x, float y) override;

private:
    const std::unique_ptr<KeypadPrivate> _impl;

}; // class Keypad

} // namespace Jgv::GLWidget

#endif // GLWIDGETKEYPAD_H
