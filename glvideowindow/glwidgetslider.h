/***************************************************************************
 *   Copyright (C) 2014-2018 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETSLIDER_H
#define GLWIDGETSLIDER_H

#include "glwidgetframe.h"

#include <functional>
#include <memory>

namespace Jgv::GLWidget {

class SliderPrivate;
class Slider : public Frame {

public:
    Slider(float backgroundDepth, GLPainters* painters);
    ~Slider() override;
    Slider(const Slider&) = delete;
    Slider(Slider&&) = delete;
    auto operator=(const Slider&) -> Slider& = delete;
    auto operator=(Slider &&) -> Slider& = delete;

    void setSlideFunction(std::function<void(float)> function);
    void setValue(float value);

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsVector;

    void resize(const Rect& rect) noexcept override;
    void handlePress(float x, float y) override;
    void handleMove(float x, float y) override;

private:
    const std::unique_ptr<SliderPrivate> _impl;

}; // class Slider

} // namespace Jgv::GLWidget

#endif // GLWIDGETSLIDER_H
