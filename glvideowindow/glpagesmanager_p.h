/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLPAGESMANAGER_P_H
#define GLPAGESMANAGER_P_H

#include "glvideowindow.h"

#include <memory>
#include <unordered_map>

namespace Jgv {
class GLPainters;
} // namespace Jgv

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLPage {

constexpr auto ID_BASE { UINT64_C(0xFFFF) };

class Base;
struct PageBranch {
    uint64_t parent { 0 };
    std::unique_ptr<Base> page { nullptr };
};

struct ManagerPrivate {
    explicit ManagerPrivate(std::shared_ptr<GlobalIPC::Object> globalIpc);
    std::shared_ptr<GlobalIPC::Object> globalIpc;

    std::unordered_map<uint64_t, PageBranch> branches;

    float pageWidth { 0.F };
    float pageHeight { 0.F };

    uint64_t rootId { 0 };
    uint64_t idCounter { ID_BASE };
    auto nextId() -> uint64_t;

}; // struct ManagerPrivate

} // namespace Jgv::GLPage

#endif // GLPAGESMANAGER_P_H
