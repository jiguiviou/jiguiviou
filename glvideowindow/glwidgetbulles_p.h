/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETBULLES_P_H
#define GLWIDGETBULLES_P_H

#include "glpainters.h"

#include <memory>
#include <vector>

namespace Jgv {
class StreamController;
} // namespace Jgv

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

class ImagesSlider;
class Label;
class Rect;
class Texture;

struct BullesPrivate {
    BullesPrivate(float backgroundDepth, GLPainters* p, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~BullesPrivate();
    BullesPrivate(const BullesPrivate&) = delete;
    BullesPrivate(BullesPrivate&&) = delete;
    auto operator=(const BullesPrivate&) -> BullesPrivate& = delete;
    auto operator=(BullesPrivate &&) -> BullesPrivate& = delete;

    const float depth;
    GLPainters* const painters;
    const Channel::Type channel;
    std::shared_ptr<GlobalIPC::Object> globalIpc;

    std::unique_ptr<Label> title;
    std::unique_ptr<Label> currentPositions;
    std::unique_ptr<Texture> video;
    std::unique_ptr<Texture> capture;
    std::unique_ptr<ImagesSlider> slider;
    std::unique_ptr<Texture> snapshot;

    int32_t siteValue { 0U };
    int32_t gisementValue { 0U };

    void updatePositions();

    auto rectangles() -> OU::RectanglePainterObjectsProxy;
    auto textures() -> OU::TexturePainterObjectsProxy;
    auto texts() -> OU::TextPainterObjectProxy;

    void resizeFloatingWidgets() noexcept;
    void resize(const Rect& rect) noexcept;

}; // struct BullesPrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETBULLES_P_H
