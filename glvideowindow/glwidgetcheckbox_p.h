/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETCHECKBOX_P_H
#define GLWIDGETCHECKBOX_P_H

#include "glpainters.h"

namespace Jgv {
class Rect;
} // namespace Jgv

namespace Jgv::GLWidget {

struct CheckboxPrivate {
    CheckboxPrivate(float backgroundDepth, GLPainters* p);
    ~CheckboxPrivate();
    CheckboxPrivate(const CheckboxPrivate&) = delete;
    CheckboxPrivate(CheckboxPrivate&&) = delete;
    auto operator=(const CheckboxPrivate&) -> CheckboxPrivate& = delete;
    auto operator=(CheckboxPrivate &&) -> CheckboxPrivate& = delete;

    OU::TextPainterObject textObject { 0U };
    OU::RectanglePainterObject backgroundFar { 0U };
    OU::RectanglePainterObject backgroundNear { 0U };
    OU::TextClipObject textClipObject { 0U };
    void resize(const Rect& rect);

    const float depth;
    GLPainters* const painters;
    bool checked { false };

}; // struct CheckboxPrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETCHECKBOX_P_H
