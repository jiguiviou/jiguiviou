﻿/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetcalibration.h"
#include "glwidgetcalibration_p.h"

#include "glpagesmanager.h"
#include "glvideowindow.h"
#include "glwidgetimagesslider.h"
#include "glwidgetlabel.h"
#include "glwidgettexture.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <configfile/configfile.h>
//#include <endat/endat.h>
#include <filesystem>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <iostream>
#include <libconfig.h++>

namespace Jgv::GLWidget {

constexpr float SCALE_FACTOR { 1.5F };
constexpr std::string_view CALIBRATION_PATH { "Calibration" };
constexpr std::string_view POSITION_TEXT { "+000.000 000.000" };

CalibrationPrivate::CalibrationPrivate(float backgroundDepth, GLPainters* const p, Channel::Type _channel, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : depth(backgroundDepth)
    , painters(p)
    , channel(_channel)
    , globalIpc(std::move(_globalIpc))
{
    title = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH, painters);
    title->setBackgroundColor(TITLE_BACKGROUND_COLOR);
    title->setTextColor(TITLE_TEXT_COLOR);
    title->setText("Calibration des codeurs sur balise");

    balisePositions = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH, painters);
    balisePositions->setBackgroundColor(LABEL_BACKGROUND_COLOR);
    balisePositions->setTextColor(LABEL_TEXT_COLOR);
    updateReferences(1, 2);

    currentPositions = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH, painters);
    currentPositions->setBackgroundColor(LABEL_BACKGROUND_COLOR);
    currentPositions->setTextColor(LABEL_TEXT_COLOR);
    currentPositions->reserve(POSITION_TEXT.size());
    currentPositions->setText(POSITION_TEXT);

    video = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    video->setBackgroundColor(TEXTURE_BACKGROUND_COLOR);
    video->setTexture(channel);

    capture = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    capture->setBackgroundColor(TEXTURE_BACKGROUND_COLOR);

    auto viewCapture = [this](const std::string& filename) {
        capture->setTexture(filename);
    };

    slider = std::make_unique<ImagesSlider>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    slider->setBackgroundColor(SLIDER_BACKGROUND_COLOR);
    slider->setSelectfunction(viewCapture);
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        const auto desiredPath { std::filesystem::path(globalIpc->vtoConfig()->pathGet()) / CALIBRATION_PATH };
        if (!std::filesystem::exists(desiredPath)) {
            std::error_code ec;
            if (!std::filesystem::create_directory(desiredPath, ec)) {
                std::cerr << "GLWidget::Calibration failed to create directory " << desiredPath << " error: " << ec.message() << std::endl;
                return;
            }
            std::filesystem::permissions(desiredPath, std::filesystem::perms::all, ec);
            if (ec != std::error_code()) {
                std::clog << "GLWidget::Calibration failed to change permissions on " << desiredPath << " error: " << ec.message() << std::endl;
            }
        }
        slider->setPath(desiredPath);
    }

    auto takeSnapshot = [&]() {
        if (const auto vtoConfig { globalIpc->vtoConfig() }) {
            if (std::filesystem::exists(std::filesystem::path(vtoConfig->pathGet()) / CALIBRATION_PATH)) {
                vtoConfig->snapshot(channel, 1, CALIBRATION_PATH);
            }
        }
    };
    snapshot = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    snapshot->setTexture(GnuIntallDirs::assetsDir() + "/touch-record.png");
    snapshot->setClickedFunction(takeSnapshot);

    zoomPlus = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    zoomPlus->setTexture((GnuIntallDirs::assetsDir() + "/plus.png"));
    auto increaseFactor = [this]() {
        video->scaleCenter(video->scaleFactor() * SCALE_FACTOR);
    };
    zoomPlus->setClickedFunction(increaseFactor);

    zoomMoins = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    zoomMoins->setTexture((GnuIntallDirs::assetsDir() + "/moins.png"));
    auto decreaseFactor = [this]() {
        const auto factor { video->scaleFactor() };
        if (factor > 1.F) {
            video->scaleCenter(factor / SCALE_FACTOR);
        }
    };
    zoomMoins->setClickedFunction(decreaseFactor);

    apply = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    apply->setTexture((GnuIntallDirs::assetsDir() + "/CalibrateTouch.png"));
    auto applyValue = [&]() {
        if (const auto vtoConfig { globalIpc->vtoConfig() }) {
            vtoConfig->calibrateWidthPosition(siteRef, gisementRef);
        }
    };
    apply->setClickedFunction(applyValue);
}

CalibrationPrivate::~CalibrationPrivate() = default;

void CalibrationPrivate::updatePositions()
{
    if (auto vtoConfig { globalIpc->vtoConfig() }) {
        auto [s, g] { vtoConfig->positions() };
        auto oldSite { std::exchange(siteValue, s) };
        auto oldGisement { std::exchange(gisementValue, g) };
        if (siteValue != oldSite || gisementValue != oldGisement) {
            auto absSite { std::abs(siteValue) };
            auto absGisement { std::abs(gisementValue) };
            std::ostringstream stream;
            stream << (siteValue < 0 ? '-' : '+')
                   << std::setfill('0') << std::setw(3) << std::setprecision(3) << absSite / 1000
                   << '.'
                   << std::setfill('0') << std::setw(3) << std::setprecision(3) << absSite % 1000
                   << " "
                   << std::setfill('0') << std::setw(3) << std::setprecision(3) << absGisement / 1000
                   << '.'
                   << std::setfill('0') << std::setw(3) << std::setprecision(3) << absGisement % 1000;
            currentPositions->setText(stream.str());
        }
    }
}

void CalibrationPrivate::updateReferences(int32_t site, int32_t gisement)
{
    siteRef = site;
    gisementRef = gisement;
    std::ostringstream stream;
    const auto absSite { std::abs(siteRef) };
    stream << "Site gisement balise: "
           << (siteRef < 0 ? '-' : '+')
           << std::setfill('0') << std::setw(3) << std::setprecision(3) << absSite / 1000
           << '.'
           << std::setfill('0') << std::setw(3) << std::setprecision(3) << absSite % 1000
           << " "
           << std::setfill('0') << std::setw(3) << std::setprecision(3) << gisementRef / 1000
           << '.'
           << std::setfill('0') << std::setw(3) << std::setprecision(3) << gisementRef % 1000;
    balisePositions->setText(stream.str());
}

auto CalibrationPrivate::rectangles() -> OU::RectanglePainterObjectsProxy
{
    OU::RectanglePainterObjectsProxy proxy { slider->rectangles() };
    proxy.emplace_back(title->rectangles());
    proxy.emplace_back(currentPositions->rectangles());
    proxy.emplace_back(balisePositions->rectangles());
    proxy.emplace_back(video->rectangles());
    proxy.emplace_back(capture->rectangles());
    return proxy;
}

auto CalibrationPrivate::textures() -> OU::TexturePainterObjectsProxy
{
    OU::TexturePainterObjectsProxy proxy { slider->textures() };
    proxy.emplace_back(video->textures());
    proxy.emplace_back(capture->textures());
    proxy.emplace_back(snapshot->textures());
    proxy.emplace_back(zoomPlus->textures());
    proxy.emplace_back(zoomMoins->textures());
    proxy.emplace_back(apply->textures());
    return proxy;
}

auto CalibrationPrivate::texts() -> OU::TextPainterObjectProxy
{
    updatePositions();
    OU::TextPainterObjectProxy proxy { title->texts() };
    proxy.emplace_back(currentPositions->texts());
    proxy.emplace_back(balisePositions->texts());
    return proxy;
}

void CalibrationPrivate::resize(const Rect& rect) noexcept
{
    auto titleRect { rect };
    titleRect.setBottom(rect.top() - TITLE_HEIGHT);
    title->resize(titleRect);

    auto refRect { rect };
    refRect.setTop(title->bottom() - INTER_WIDGET_SPACE);
    refRect.setBottom(refRect.top() - LABEL_HEIGHT);
    refRect.setLeft(refRect.left() + INTER_WIDGET_SPACE);
    refRect.setRight(refRect.right() - INTER_WIDGET_SPACE);
    balisePositions->resize(refRect);

    auto sliderRect { rect };
    sliderRect.setBottom(sliderRect.bottom() + INTER_WIDGET_SPACE);
    sliderRect.setTop(sliderRect.bottom() + SLIDER_HEIGHT);
    slider->resize(sliderRect);

    auto videoRect { rect };
    videoRect.setTop(balisePositions->bottom() - INTER_WIDGET_SPACE);
    videoRect.setRight(rect.left() + 0.5F * rect.width());
    videoRect.setBottom(videoRect.top() - (videoRect.width() / video->textureRatio()));
    // on réduit pour placer les touches

    video->resize(videoRect.reduced(TOUCH_SIZE * 0.5F));

    // les touches
    Rect touchRect { TOUCH_SIZE, TOUCH_SIZE };
    touchRect.setCenter(video->textureRect().right(), video->textureRect().top());
    zoomPlus->resize(touchRect);
    touchRect.setCenter(video->textureRect().left(), video->textureRect().bottom());
    zoomMoins->resize(touchRect);

    auto applyRect { touchRect.scaled(1.5F) };
    applyRect.setCenter(videoRect.centerX(), videoRect.centerY());
    applyRect.moveTop(zoomMoins->bottom() - INTER_WIDGET_SPACE);
    apply->resize(applyRect);

    auto positionsRect { video->rect() };
    positionsRect.setTop(positionsRect.bottom() + LABEL_HEIGHT);
    positionsRect.moveTop(apply->bottom() - INTER_WIDGET_SPACE);
    currentPositions->resize(positionsRect);

    // la motié droite pour la vue capture, aligné sur video
    auto captureRect { videoRect };
    captureRect.moveLeft(rect.left() + rect.width() * 0.5F);
    capture->resize(captureRect);

    auto snapchotRect { touchRect.scaled(1.5F) };
    snapchotRect.setCenter(capture->centerX(), apply->centerY());
    snapshot->resize(snapchotRect);
}

Calibration::Calibration(float backgroundDepth, GLPainters* painters, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl { std::make_unique<CalibrationPrivate>(backgroundDepth, painters, channel, globalIpc) }
{
}

Calibration::~Calibration() = default;

void Calibration::setZoomFactor(float factor)
{
    _impl->video->scaleCenter(factor);
}

void Calibration::setBaliseAngles(int32_t site, int32_t gisement)
{
    _impl->updateReferences(site, gisement);
}

auto Calibration::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    return _impl->rectangles();
}

auto Calibration::textures() const -> OU::TexturePainterObjectsProxy
{
    return _impl->textures();
}

auto Calibration::texts() const -> OU::TextPainterObjectProxy
{
    return _impl->texts();
}

void Calibration::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Calibration::handlePress(float x, float y)
{
    if (_enabled) {
        _impl->slider->handlePress(x, y);
        _impl->snapshot->handlePress(x, y);
        _impl->zoomPlus->handlePress(x, y);
        _impl->zoomMoins->handlePress(x, y);
        _impl->apply->handlePress(x, y);
    }
}

void Calibration::handleRelease(float x, float y)
{
    if (_enabled) {
        _impl->slider->handleRelease(x, y);
        _impl->snapshot->handleRelease(x, y);
        _impl->zoomPlus->handleRelease(x, y);
        _impl->zoomMoins->handleRelease(x, y);
        _impl->apply->handleRelease(x, y);
    }
}

void Calibration::handleMove(float x, float y)
{
    if (_enabled) {
        _impl->slider->handleMove(x, y);
        _impl->snapshot->handleMove(x, y);
        _impl->zoomPlus->handleMove(x, y);
        _impl->zoomMoins->handleMove(x, y);
        _impl->apply->handleMove(x, y);
    }
}

} // namespace Jgv::GLWidget
