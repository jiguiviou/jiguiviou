/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glpageshutdown.h"
#include "glpageshutdown_p.h"

#include "glbuttonscolumn.h"
#include "glpagesmanager.h"
#include "glwidgetbackground.h"
#include "glwidgetbutton.h"
#include "glwidgetlabel.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>

namespace Jgv::GLPage {

ShutdownPrivate::ShutdownPrivate(Channel::Type c, GLPainters* p, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : BasePrivate(p, _globalIpc)
    , channel(c)
{
    leftColumn = std::make_unique<GLWidget::ButtonsColumn>();
    rightColumn = std::make_unique<GLWidget::ButtonsColumn>();
}

void ShutdownPrivate::show()
{
    if (!background) {
        background = std::make_unique<GLWidget::Background>(GLWidget::WIDGET_BACKGROUND_DEPTH, painters);
        background->setBackgroundColor({ 0.F, 0.F, 0.F, 0.6F });
    }

    if (!label) {
        label = std::make_unique<GLWidget::Label>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        label->setTextColor(GLWidget::LABEL_TEXT_COLOR);
        label->setText("Éteindre le système !");
    }

    if (!shutdown) {
        shutdown = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        shutdown->initialize(GnuIntallDirs::assetsDir() + "/shutdown.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto quit = [this]() {
            if (const auto vtoConfig { globalIpc->vtoConfig() }) {
                vtoConfig->powerOff();
            }
        };
        shutdown->setClickedFunction(quit);
    }

    if (!exitButton) {
        exitButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        exitButton->initialize(GnuIntallDirs::assetsDir() + "/exit.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto exit = [this]() {
            manager->showParent(managerId);
        };
        exitButton->setClickedFunction(exit);
    }
}

void ShutdownPrivate::hide()
{
    destroyGL();
}

void ShutdownPrivate::destroyGL() noexcept
{
    shutdown.reset(nullptr);
    exitButton.reset(nullptr);
    background.reset(nullptr);
}

OU::RectanglePainterObjectsProxy ShutdownPrivate::rectangles() const
{
    if (label && background) {
        return { background->rectangles(), label->rectangles() };
    }
    return {};
}

OpenGLUtils::TextPainterObjectProxy ShutdownPrivate::texts() const
{
    if (label) {
        return { label->texts() };
    }
    return {};
}

OpenGLUtils::TexturePainterObjectsProxy ShutdownPrivate::textures() const
{
    if (shutdown && exitButton) {
        return { shutdown->textures(), exitButton->textures() };
    }
    return {};
}

void ShutdownPrivate::resize(float width, float height)
{
    // maintient le ratio 1:1 des colonnes de boutons
    leftColumn->resize(GLWidget::Rect { 0.F, 0.F, height / leftColumn->cellsCount(), height });
    rightColumn->resize(GLWidget::Rect { width - height / rightColumn->cellsCount(), 0.F, width, height });

    if (background) {
        background->resize(GLWidget::Rect { 0.F, 0.F, width, height });
    }

    if (shutdown && label) {
        GLWidget::Rect shutDownRect(0.F, 0.F, 128.F, 128.F);
        shutDownRect.setCenter(width / 2.F, height / 2.F);
        shutdown->resize(shutDownRect);

        GLWidget::Rect labelRect(0.F, 0.F, width, GLWidget::LABEL_HEIGHT);
        labelRect.moveBottom(shutdown->top() + GLWidget::INTER_WIDGET_SPACE);
        label->resize(labelRect);
    }

    if (exitButton) {
        exitButton->resize(leftColumn->rectOf(0U));
    }
}

void ShutdownPrivate::handlePress(float x, float y)
{
    if (shutdown) {
        shutdown->handlePress(x, y);
    }
    if (exitButton) {
        exitButton->handlePress(x, y);
    }
}

void ShutdownPrivate::handleRelease(float x, float y)
{
    if (shutdown) {
        shutdown->handleRelease(x, y);
    }
    if (exitButton) {
        exitButton->handleRelease(x, y);
    }
}

void ShutdownPrivate::handleMove(float x, float y)
{
    if (shutdown) {
        shutdown->handleMove(x, y);
    }
    if (exitButton) {
        exitButton->handleMove(x, y);
    }
}

Shutdown::Shutdown(Channel::Type channel, GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : Base(std::make_unique<ShutdownPrivate>(channel, painters, globalIpc))
{
}

} // namespace Jgv
