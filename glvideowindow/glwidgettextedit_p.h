/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETTEXTEDIT_P_H
#define GLWIDGETTEXTEDIT_P_H

#include "glpainters.h"

#include <openglutils/rectanglepainter.h>
#include <string>

namespace Jgv {
class Rect;
} // namespace Jgv

namespace Jgv::GLWidget {

using Color = std::tuple<float, float, float, float>;

struct TextEditPrivate {
    TextEditPrivate(float backgroundDepth, GLPainters* p);
    ~TextEditPrivate();
    TextEditPrivate(const TextEditPrivate&) = delete;
    TextEditPrivate(TextEditPrivate&&) = delete;
    auto operator=(const TextEditPrivate&) -> TextEditPrivate& = delete;
    auto operator=(TextEditPrivate &&) -> TextEditPrivate& = delete;

    const float depth;
    GLPainters* const painters;
    std::string text;
    OU::TextPainterObject prefixObject { 0U };
    OU::TextPainterObject textObject { 0U };
    OU::TextClipObject clippingRectangle { 0U };
    OU::RectanglePainterObject rectangleObject { 0U };

    void setPrefix(const std::string& prefix, const Color& color);
    void setText(std::string_view text);
    void setTextColor(const Color& color);

    void resize(const Rect& rect);

}; // struct TextEditPrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETTEXTEDIT_P_H
