/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLPAGEROOT_P_H
#define GLPAGEROOT_P_H

#include "glpagebase_p.h"

#include <chrono>
#include <memory>

namespace Jgv::GLWidget {
class ButtonsColumn;
class Button;
class Slider;
} // namespace Jgv::GLWidget

namespace Jgv::GLPage {

constexpr auto MIXER_TIME_ON { std::chrono::seconds(5) };

struct RootPrivate : public BasePrivate {
    RootPrivate(GLPainters* p, std::shared_ptr<GlobalIPC::Object> _globalIpc);

    std::unique_ptr<GLWidget::ButtonsColumn> leftColumn;
    std::unique_ptr<GLWidget::ButtonsColumn> rightColumn;
    std::unique_ptr<GLWidget::Button> recordButton;
    std::unique_ptr<GLWidget::Button> equalizeButton;
    std::unique_ptr<GLWidget::Button> shutdownButton;
    std::unique_ptr<GLWidget::Button> exitButton;
    std::unique_ptr<GLWidget::Button> campagneButton;
    std::unique_ptr<GLWidget::Button> reticleButton;
    std::unique_ptr<GLWidget::Button> calibrationButton;
    std::unique_ptr<GLWidget::Button> bullesButton;
    std::unique_ptr<GLWidget::Button> baliseButton;
    std::unique_ptr<GLWidget::Button> bullesViewButton;
    std::unique_ptr<GLWidget::Slider> mixer;

    uint64_t campagnePage { 0U };
    uint64_t reticlePage { 0U };
    uint64_t calibrationPage { 0U };
    uint64_t bullesPage { 0U };
    uint64_t balisePage { 0U };
    uint64_t bullesViewPage { 0U };
    uint64_t shutdownPage { 0U };

    std::shared_ptr<std::function<void(bool)>> recordEvent;
    std::shared_ptr<std::function<void(bool)>> equalizeEvent;

    std::chrono::system_clock::time_point mixerOnTime;

    Channel::Type channel;

    void switchCampagneMode();
    void enableMixer(bool enable);

    void setManager(uint64_t id, Manager* manager) override;
    void show() override;
    void hide() override;
    void destroyGL() noexcept override;

    void resize(float width, float height) override;

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsProxy override;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectProxy override;
    [[nodiscard]] auto textures() const -> OU::TexturePainterObjectsProxy override;

    void handlePress(float x, float y) override;
    void handleRelease(float x, float y) override;
    void handleMove(float x, float y) override;

}; // struct RootPrivate

} // namespace Jgv::GLPage

#endif // GLPAGEROOT_P_H
