﻿/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETVOLUME_H
#define GLWIDGETVOLUME_H

#include "glwidgetframe.h"

#include <functional>
#include <memory>
#include <vector>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

struct VolumePrivate;

class Volume : public Frame {

public:
    explicit Volume(float backgroundDepth, GLPainters* painters, std::string_view uuid, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~Volume() override;
    Volume(const Volume&) = delete;
    Volume(Volume&&) = delete;
    auto operator=(const Volume&) -> Volume& = delete;
    auto operator=(Volume &&) -> Volume& = delete;

    [[nodiscard]] auto uuid() const -> std::string;
    void setClickFunction(std::function<void(void)> function);

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsVector;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectVector;
    [[nodiscard]] auto textures() const -> OU::TexturePainterObjectsVector;

    void resize(const Rect& rect) noexcept override;

private:
    const std::unique_ptr<VolumePrivate> _impl;
    void clicked() override;

}; // class Volume

} // namespace Jgv::GLWidget

#endif // GLWIDGETVOLUME_H
