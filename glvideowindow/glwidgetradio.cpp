/**************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetradio.h"
#include "glwidgetradio_p.h"

#include "glpagesmanager.h"
#include "theme.h"

namespace Jgv::GLWidget {

RadioPrivate::RadioPrivate(float backgroundDepth, GLPainters* const p)
    : depth(backgroundDepth)
    , painters(p)
{
    backgroundFar = painters->rectanglePainter.createObject();
    backgroundNear = painters->rectanglePainter.createObject();
    textObject = painters->textPainter.createObject();
    textClipObject = painters->textPainter.createClipObject();

    painters->rectanglePainter.setColor(backgroundFar, DECORATION_COLOR);
    painters->rectanglePainter.setColor(backgroundNear, RADIO_BACKGROUND_COLOR);
    painters->textPainter.setColor(textObject, RADIO_TEXT_COLOR);
    painters->textPainter.setClippingRectangle(textObject, textClipObject);
}

RadioPrivate::~RadioPrivate()
{
    painters->textPainter.destroyObject(textObject);
    painters->textPainter.destroyClipObject(textClipObject);
    painters->rectanglePainter.destroyObject(backgroundNear);
    painters->rectanglePainter.destroyObject(backgroundFar);
}

void RadioPrivate::resize(const Rect& rect)
{
    // la case à cocher est carrée, le côté vaut une hauteur
    auto checkRect { rect };
    checkRect.setRight(rect.left() + rect.height());
    painters->rectanglePainter.setRectangle(backgroundFar, checkRect.tuple(), depth);
    painters->rectanglePainter.setRectangle(backgroundNear, checkRect.reduced(DECORATION_WIDTH).tuple(), depth + INTER_LAYOUT_DEPTH);

    // le texte commence à droite de la case + INTER_WIDGET

    const auto [left, bottom, right, top] { painters->textPainter.boundingBox(textObject) };
    Rect textRect { right, top };
    textRect.alignCenter(rect);
    painters->textPainter.setPosition(textObject, { checkRect.right() + INTER_WIDGET_SPACE, textRect.bottom(), depth + INTER_LAYOUT_DEPTH + INTER_LAYOUT_DEPTH });
    painters->textPainter.updateClipObjectRectangle(textClipObject, { rect.left<int>(), rect.bottom<int>(), rect.width<int>(), rect.height<int>() });
}

Radio::Radio(float backgroundDepth, GLPainters* const painters)
    : _impl(std::make_unique<RadioPrivate>(backgroundDepth, painters))
{
}

Radio::~Radio() = default;

void Radio::setText(std::string_view text)
{
    _impl->painters->textPainter.setText(_impl->textObject, std::string(text));
}

void Radio::setClickedFunction(std::function<void(void)> function)
{
    _impl->clicked = std::move(function);
}

auto Radio::clickedFunction() const -> std::function<void()>
{
    return _impl->clicked;
}

void Radio::setUncheckAllFunction(std::function<void()> function)
{
    _impl->uncheckAll = std::move(function);
}

void Radio::setChecked(bool checked)
{
    if (checked) {
        if (_impl->uncheckAll) {
            _impl->uncheckAll();
        }
        _impl->painters->rectanglePainter.setColor(_impl->backgroundNear, FOCUS_BACKGROUND_COLOR);
    } else {
        _impl->painters->rectanglePainter.setColor(_impl->backgroundNear, RADIO_BACKGROUND_COLOR);
    }
}

auto Radio::widthHint(float height) const -> float
{
    const auto [textLeft, textBottom, textRight, textTop] { _impl->painters->textPainter.boundingBox(_impl->textObject) };
    return height + INTER_WIDGET_SPACE + (textRight - textLeft);
}

auto Radio::rectangles() const -> OU::RectanglePainterObjectsVector
{
    return { _impl->backgroundFar, _impl->backgroundNear };
}

auto Radio::texts() const -> OU::TextPainterObjectVector
{
    return { _impl->textObject };
}

void Radio::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Radio::clicked()
{
    setChecked(true);
    if (_impl->clicked) {
        _impl->clicked();
    }
}

} // namespace Jgv::GLWidget
