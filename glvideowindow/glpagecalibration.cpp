#include <utility>

/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glpagecalibration.h"
#include "glpagecalibration_p.h"

#include "glbuttonscolumn.h"
#include "glpagesmanager.h"
#include "glwidgetbackground.h"
#include "glwidgetbutton.h"
#include "glwidgetcalibration.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>

namespace Jgv::GLPage {

CalibrationPrivate::CalibrationPrivate(Channel::Type c, GLPainters* p, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : BasePrivate(p, std::move(_globalIpc))
    , channel(c)
    , leftColumn { std::make_unique<GLWidget::ButtonsColumn>() }
    , rightColumn { std::make_unique<GLWidget::ButtonsColumn>() }
{
}

void CalibrationPrivate::show()
{
    if (!background) {
        background = std::make_unique<GLWidget::Background>(GLWidget::WIDGET_BACKGROUND_DEPTH, painters);
    }

    if (!calibrationWidget) {
        calibrationWidget = std::make_unique<GLWidget::Calibration>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters, channel, globalIpc);
        if (const auto vtoConfig { globalIpc->vtoConfig() }) {
            const auto angles { vtoConfig->baliseCoordinates() };
            calibrationWidget->setBaliseAngles(angles.first, angles.second);
        }
    }

    if (!exitButton) {
        exitButton = std::make_unique<GLWidget::Button>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, painters);
        exitButton->initialize(GnuIntallDirs::assetsDir() + "/valid.png", GnuIntallDirs::assetsDir() + "/background-off.png", GnuIntallDirs::assetsDir() + "/background-on.png");
        auto exit = [this]() {
            manager->showParent(managerId);
        };
        exitButton->setClickedFunction(exit);
    }
}

void CalibrationPrivate::hide()
{
    destroyGL();
}

void CalibrationPrivate::destroyGL() noexcept
{
    calibrationWidget.reset(nullptr);
    exitButton.reset(nullptr);
    background.reset(nullptr);
}

auto CalibrationPrivate::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    if (background && calibrationWidget) {
        auto proxy { calibrationWidget->rectangles() };
        proxy.emplace_back(background->rectangles());
        return proxy;
    }
    return {};
}

auto CalibrationPrivate::texts() const -> OU::TextPainterObjectProxy
{
    if (calibrationWidget) {
        return calibrationWidget->texts();
    }
    return {};
}

auto CalibrationPrivate::textures() const -> OU::TexturePainterObjectsProxy
{
    if (calibrationWidget && exitButton) {
        auto proxy { calibrationWidget->textures() };
        proxy.emplace_back(exitButton->textures());
        return proxy;
    }
    return {};
}

void CalibrationPrivate::resize(float width, float height)
{
    leftColumn->resize(GLWidget::Rect { 0.F, 0.F, height / leftColumn->cellsCount(), height });
    rightColumn->resize(GLWidget::Rect { width - height / rightColumn->cellsCount(), 0.F, width, height });

    if (background) {
        background->resize(GLWidget::Rect { 0.F, 0.F, width, height });
    }

    if (calibrationWidget) {
        const GLWidget::Rect centralRect(leftColumn->right(), 0.F, rightColumn->left(), height);
        calibrationWidget->resize(centralRect);
    }

    if (exitButton) {
        exitButton->resize(rightColumn->rectOf(0U));
    }
}

void CalibrationPrivate::handlePress(float x, float y)
{
    if (calibrationWidget) {
        calibrationWidget->handlePress(x, y);
    }

    if (exitButton) {
        exitButton->handlePress(x, y);
    }
}

void CalibrationPrivate::handleRelease(float x, float y)
{
    if (calibrationWidget) {
        calibrationWidget->handleRelease(x, y);
    }

    if (exitButton) {
        exitButton->handleRelease(x, y);
    }
}

void CalibrationPrivate::handleMove(float x, float y)
{
    if (calibrationWidget) {
        calibrationWidget->handleMove(x, y);
    }

    if (exitButton) {
        exitButton->handleMove(x, y);
    }
}

Calibration::Calibration(Channel::Type channel, GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : Base(std::make_unique<CalibrationPrivate>(channel, painters, globalIpc))
{
}

} // namespace Jgv
