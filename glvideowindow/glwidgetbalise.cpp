﻿/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetbalise.h"
#include "glwidgetbalise_p.h"

#include "glpagesmanager.h"
#include "glvideowindow.h"
#include "glwidgetimagesslider.h"
#include "glwidgetlabel.h"
#include "glwidgetradio.h"
#include "glwidgettexture.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <configfile/configfile.h>
#include <filesystem>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <iostream>
#include <libconfig.h++>

namespace Jgv::GLWidget {

constexpr std::string_view AVANT_INVERSE { "Avant inverse" };
constexpr std::string_view AVANT_DIRECTE { "Avant directe" };
constexpr std::string_view APRES_INVERSE { "Après inverse" };
constexpr std::string_view APRES_DIRECTE { "Après directe" };

constexpr int SNAPSHOT_IMAGES_COUNT { 10 };
constexpr auto SNAPSHOT_INTERVAL { std::chrono::minutes(4) };
constexpr auto SCALE_FACTOR { 2.F };

BalisePrivate::BalisePrivate(float backgroundDepth, GLPainters* const p, Channel::Type _channel, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : depth(backgroundDepth)
    , painters(p)
    , channel(_channel)
    , globalIpc(std::move(_globalIpc))
{
    title = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH, painters);
    title->setBackgroundColor(TITLE_BACKGROUND_COLOR);
    title->setTextColor(TITLE_TEXT_COLOR);
    title->setText("Visées sur balise");

    auto changePath = [this](std::string_view path) {
        if (const auto vtoConfig { globalIpc->vtoConfig() }) {
            const auto desiredPath { std::filesystem::path(globalIpc->vtoConfig()->pathGet()) / path };
            if (!std::filesystem::exists(desiredPath)) {
                std::error_code ec;
                if (!std::filesystem::create_directory(desiredPath, ec)) {
                    std::cerr << "GLWidget::Balise::changePath failed to create directory " << desiredPath << " error: " << ec.message() << std::endl;
                    return;
                }
                std::filesystem::permissions(desiredPath, std::filesystem::perms::all, ec);
                if (ec != std::error_code()) {
                    std::clog << "GLWidget::Balise::changePath failed to change permissions on " << desiredPath << " error: " << ec.message() << std::endl;
                }
            }
            currentPath = desiredPath;
            slider->setPath(currentPath);
        }
    };

    inverseBefore = std::make_unique<Radio>(depth, painters);
    inverseBefore->setText(AVANT_INVERSE);
    inverseBefore->setClickedFunction([changePath]() { changePath(AVANT_INVERSE); });

    directBefore = std::make_unique<Radio>(depth, painters);
    directBefore->setText(AVANT_DIRECTE);
    directBefore->setClickedFunction([changePath]() { changePath(AVANT_DIRECTE); });

    inverseAfter = std::make_unique<Radio>(depth, painters);
    inverseAfter->setText(APRES_INVERSE);
    inverseAfter->setClickedFunction([changePath]() { changePath(APRES_INVERSE); });

    directAfter = std::make_unique<Radio>(depth, painters);
    directAfter->setText(APRES_DIRECTE);
    directAfter->setClickedFunction([changePath]() { changePath(APRES_DIRECTE); });

    auto uncheckAll = [this]() {
        directBefore->setChecked(false);
        inverseBefore->setChecked(false);
        directAfter->setChecked(false);
        inverseAfter->setChecked(false);
    };
    inverseBefore->setUncheckAllFunction(uncheckAll);
    directBefore->setUncheckAllFunction(uncheckAll);
    inverseAfter->setUncheckAllFunction(uncheckAll);
    directAfter->setUncheckAllFunction(uncheckAll);

    video = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    video->setBackgroundColor(TEXTURE_BACKGROUND_COLOR);
    video->setTexture(channel);

    capture = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    capture->setBackgroundColor(TEXTURE_BACKGROUND_COLOR);

    auto viewCapture = [this](const std::string& filename) {
        capture->setTexture(filename);
    };

    slider = std::make_unique<ImagesSlider>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    slider->setBackgroundColor(SLIDER_BACKGROUND_COLOR);
    slider->setSelectfunction(viewCapture);

    snapshot = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    snapshot->setTexture(GnuIntallDirs::assetsDir() + "/touch-record.png");

    auto takeSnapshot = [this]() {
        if (auto vtoConfig { globalIpc->vtoConfig() }) {
            vtoConfig->snapshot(channel, SNAPSHOT_IMAGES_COUNT, currentPath.string());
            lastSnapshotTime = std::chrono::system_clock::now();
            elapsed = SNAPSHOT_INTERVAL;
        }
    };
    snapshot->setClickedFunction(takeSnapshot);

    zoomPlus = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    zoomPlus->setTexture((GnuIntallDirs::assetsDir() + "/plus.png"));
    auto increaseFactor = [this]() {
        video->scaleCenter(video->scaleFactor() * SCALE_FACTOR);
    };
    zoomPlus->setClickedFunction(increaseFactor);

    zoomMoins = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    zoomMoins->setTexture((GnuIntallDirs::assetsDir() + "/moins.png"));
    auto decreaseFactor = [this]() {
        const auto factor { video->scaleFactor() };
        if (factor > 1.F) {
            video->scaleCenter(factor / SCALE_FACTOR);
        }
    };
    zoomMoins->setClickedFunction(decreaseFactor);

    timer = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH, painters);
    timer->setBackgroundColor(LABEL_BACKGROUND_COLOR);
    timer->setTextColor(LABEL_TEXT_COLOR);
    timer->setText("00:00");

    auto checkPath = [this](std::string_view path) {
        if (auto vtoConfig { globalIpc->vtoConfig() }) {
            const auto p { std::filesystem::path(globalIpc->vtoConfig()->pathGet()) };
            if (!std::filesystem::exists(p / path)) {
                return true;
            }
            return std::filesystem::is_empty(p / path);
        }
        return false;
    };

    if (checkPath(AVANT_INVERSE)) {
        changePath(AVANT_INVERSE);
        inverseBefore->setChecked(true);
    } else if (checkPath(AVANT_DIRECTE)) {
        changePath(AVANT_DIRECTE);
        directBefore->setChecked(true);
    } else if (checkPath(APRES_INVERSE)) {
        changePath(APRES_INVERSE);
        inverseAfter->setChecked(true);
    } else {
        changePath(APRES_DIRECTE);
        directAfter->setChecked(true);
    }
}

BalisePrivate::~BalisePrivate() = default;

void BalisePrivate::updateChrono()
{
    elapsed = SNAPSHOT_INTERVAL - (std::chrono::system_clock::now() - lastSnapshotTime);
    const auto minutes { std::chrono::duration_cast<std::chrono::minutes>(elapsed) };
    const auto seconds { std::chrono::duration_cast<std::chrono::seconds>(elapsed) % 60 };

    std::ostringstream value;
    value << std::setw(2) << std::setfill('0') << std::to_string(minutes.count()) << ":" << std::setw(2) << std::setfill('0') << std::to_string(seconds.count());
    timer->setText(value.str());
}

auto BalisePrivate::rectangles() -> OU::RectanglePainterObjectsProxy
{
    std::vector<std::vector<OU::RectanglePainterObject>> proxy { slider->rectangles() };
    proxy.emplace_back(title->rectangles());
    proxy.emplace_back(timer->rectangles());
    proxy.emplace_back(directBefore->rectangles());
    proxy.emplace_back(inverseBefore->rectangles());
    proxy.emplace_back(directAfter->rectangles());
    proxy.emplace_back(inverseAfter->rectangles());
    proxy.emplace_back(video->rectangles());
    proxy.emplace_back(capture->rectangles());
    return proxy;
}

auto BalisePrivate::textures() -> OU::TexturePainterObjectsProxy
{
    auto proxy { slider->textures() };
    proxy.emplace_back(video->textures());
    proxy.emplace_back(capture->textures());
    proxy.emplace_back(snapshot->textures());
    proxy.emplace_back(zoomPlus->textures());
    proxy.emplace_back(zoomMoins->textures());
    return proxy;
}

auto BalisePrivate::texts() -> OpenGLUtils::TextPainterObjectProxy
{
    if (elapsed.count() > 0) {
        updateChrono();
    }
    OpenGLUtils::TextPainterObjectProxy proxy { title->texts() };
    proxy.emplace_back(timer->texts());
    proxy.emplace_back(directBefore->texts());
    proxy.emplace_back(inverseBefore->texts());
    proxy.emplace_back(directAfter->texts());
    proxy.emplace_back(inverseAfter->texts());
    return proxy;
}

void BalisePrivate::resize(const Rect& rect) noexcept
{
    auto titleRect { rect };
    titleRect.setBottom(rect.top() - TITLE_HEIGHT);
    title->resize(titleRect);

    auto sliderRect { rect };
    sliderRect.setBottom(sliderRect.bottom() + INTER_WIDGET_SPACE);
    sliderRect.setTop(sliderRect.bottom() + SLIDER_HEIGHT);
    slider->resize(sliderRect);

    auto videoRect { rect };
    videoRect.setTop(title->bottom() - INTER_WIDGET_SPACE);
    videoRect.setRight(rect.left() + 0.5F * rect.width());
    videoRect.setBottom(videoRect.top() - (videoRect.width() / video->textureRatio()));
    // on réduit pour placer les touches

    video->resize(videoRect.reduced(TOUCH_SIZE * 0.5F));

    // les touches
    Rect touchRect { TOUCH_SIZE, TOUCH_SIZE };
    touchRect.setCenter(video->textureRect().right(), video->textureRect().top());
    zoomPlus->resize(touchRect);
    touchRect.setCenter(video->textureRect().left(), video->textureRect().bottom());
    zoomMoins->resize(touchRect);

    // la place est serrée, on calcule pour entrer
    const auto space { std::min(RADIO_HEIGHT, ((zoomMoins->bottom() - slider->top() - 5.F * INTER_WIDGET_SPACE) * 0.25F)) };

    Rect radioRect { *video };
    radioRect.setTop(zoomMoins->bottom() - INTER_WIDGET_SPACE);
    radioRect.setBottom(radioRect.top() - space);
    inverseBefore->resize(radioRect);

    radioRect.moveTop(inverseBefore->bottom() - INTER_WIDGET_SPACE);
    directBefore->resize(radioRect);

    radioRect.moveTop(directBefore->bottom() - INTER_WIDGET_SPACE);
    inverseAfter->resize(radioRect);

    radioRect.moveTop(inverseAfter->bottom() - INTER_WIDGET_SPACE);
    directAfter->resize(radioRect);

    // la motié droite pour la vue capture, aligné sur video
    auto captureRect { videoRect };
    captureRect.moveLeft(rect.left() + rect.width() * 0.5F);
    capture->resize(captureRect);

    auto timerRect { captureRect };
    timerRect.setRight(timerRect.left() + 0.5F * capture->width());
    timerRect.setBottom(timerRect.top() - LABEL_HEIGHT);
    timerRect.setCenter(capture->centerX(), capture->centerY());
    timerRect.moveTop(capture->bottom() - INTER_WIDGET_SPACE);
    timer->resize(timerRect);

    auto snapshotRect { touchRect.scaled(1.5F) };
    snapshotRect.setCenter(timer->centerX(), timer->centerY());
    snapshotRect.moveTop(timer->bottom() - INTER_WIDGET_SPACE);
    snapshot->resize(snapshotRect);
}

void BalisePrivate::handlePress(float x, float y)
{
    slider->handlePress(x, y);
    snapshot->handlePress(x, y);
    zoomPlus->handlePress(x, y);
    zoomMoins->handlePress(x, y);
    directBefore->handlePress(x, y);
    inverseBefore->handlePress(x, y);
    directAfter->handlePress(x, y);
    inverseAfter->handlePress(x, y);
}

void BalisePrivate::handleRelease(float x, float y)
{
    slider->handleRelease(x, y);
    snapshot->handleRelease(x, y);
    zoomPlus->handleRelease(x, y);
    zoomMoins->handleRelease(x, y);
    directBefore->handleRelease(x, y);
    inverseBefore->handleRelease(x, y);
    directAfter->handleRelease(x, y);
    inverseAfter->handleRelease(x, y);
}

void BalisePrivate::handleMove(float x, float y)
{
    slider->handleMove(x, y);
    snapshot->handleMove(x, y);
    zoomPlus->handleMove(x, y);
    zoomMoins->handleMove(x, y);
    directBefore->handleMove(x, y);
    inverseBefore->handleMove(x, y);
    directAfter->handleMove(x, y);
    inverseAfter->handleMove(x, y);
}

Balise::Balise(float backgroundDepth, GLPainters* painters, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl { std::make_unique<BalisePrivate>(backgroundDepth, painters, channel, std::move(globalIpc)) }
{
}

Balise::~Balise() = default;

auto Balise::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    return _impl->rectangles();
}

auto Balise::textures() const -> OU::TexturePainterObjectsProxy
{
    return _impl->textures();
}

auto Balise::texts() const -> OpenGLUtils::TextPainterObjectProxy
{
    return _impl->texts();
}

void Balise::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Balise::handlePress(float x, float y)
{
    _impl->handlePress(x, y);
}

void Balise::handleRelease(float x, float y)
{
    _impl->handleRelease(x, y);
}

void Balise::handleMove(float x, float y)
{
    _impl->handleMove(x, y);
}

} // namespace Jgv::GLWidget
