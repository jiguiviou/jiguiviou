/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glbuttonscolumn.h"
#include "glbuttonscolumn_p.h"

#include "glpagesmanager.h"
#include "glwidgetbutton.h"

namespace Jgv::GLWidget {

const float BUTTON_SCALE { 0.8F };

void ButtonsColumnPrivate::resize(const Rect& rect)
{
    // le rectangle du premier bouton ( en bas )
    Rect cellRect { rect };
    cellRect.setTop(rect.height() / cells.size());

    for (auto& rect : cells) {
        rect = cellRect;
        cellRect.moveBottom(cellRect.top());
    }
}

ButtonsColumn::ButtonsColumn()
    : _impl(std::make_unique<ButtonsColumnPrivate>())
{
}

ButtonsColumn::~ButtonsColumn() = default;

void ButtonsColumn::setCellsCount(std::size_t cellsCount)
{
    _impl->cells.resize(cellsCount);
}

auto ButtonsColumn::cellsCount() const -> std::size_t
{
    return _impl->cells.size();
}

auto ButtonsColumn::rectOf(std::size_t index) const -> Rect
{
    if (index > _impl->cells.size()) {
        return Rect {};
    }
    return _impl->cells.at(index).scaled(BUTTON_SCALE);
}

void ButtonsColumn::resize(const Rect& rect) noexcept
{
    Rect::resize(rect);
    _impl->resize(rect);
}

} // namespace Jgv::GLWidget
