/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLPAGEBALISE_P_H
#define GLPAGEBALISE_P_H

#include "glpagebase_p.h"
#include "theme.h"

namespace Jgv::GLWidget {
class Background;
class Balise;
class ButtonsColumn;
class Button;
} // namespace Jgv::GLWidget

namespace Jgv::GLPage {

struct BalisePrivate : public BasePrivate {
    BalisePrivate(GLPainters* p, Channel::Type _channel, std::shared_ptr<GlobalIPC::Object> _globalIpc);
    const Channel::Type channel;

    std::unique_ptr<GLWidget::ButtonsColumn> leftColumn;
    std::unique_ptr<GLWidget::ButtonsColumn> rightColumn;
    std::unique_ptr<GLWidget::Button> exitButton;
    std::unique_ptr<GLWidget::Background> background;
    std::unique_ptr<GLWidget::Balise> baliseWidget;

    void show() override;
    void hide() override;
    void destroyGL() noexcept override;

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsProxy override;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectProxy override;
    [[nodiscard]] auto textures() const -> OU::TexturePainterObjectsProxy override;

    void resize(float width, float height) override;

    void handlePress(float x, float y) override;
    void handleRelease(float x, float y) override;
    void handleMove(float x, float y) override;

}; // struct CampagnePrivate

} // namespace Jgv::GLPage

#endif // GLPAGEBALISE_P_H
