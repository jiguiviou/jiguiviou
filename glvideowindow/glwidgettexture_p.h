/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETTEXTURE_P_H
#define GLWIDGETTEXTURE_P_H

#include "glpainters.h"
#include "glwidgetrect.h"

#include <atomic>
#include <functional>
#include <globalipc/globalipc.h>
#include <string>

namespace Jgv::GLWidget {

constexpr auto DEFAULT_TEXTURE_RATIO { 4.F / 3.F };
constexpr auto DEFAULT_SCALE_FACTOR { 1.F };

struct SyncRatio {
    std::atomic<float> reg;
    float to;
    auto changed() noexcept
    {
        auto tmp { reg.load() };
        return !reg.compare_exchange_weak(to, tmp);
    }
};

struct TexturePrivate {
    TexturePrivate(float backgroundDepth, GLPainters* p, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~TexturePrivate();
    TexturePrivate(const TexturePrivate&) = delete;
    TexturePrivate(TexturePrivate&&) = delete;
    auto operator=(const TexturePrivate&) -> TexturePrivate& = delete;
    auto operator=(TexturePrivate &&) -> TexturePrivate& = delete;

    const float depth;
    GLPainters* const painters;
    std::shared_ptr<GlobalIPC::Object> globalIpc;

    OU::TexturePainterObject textureObject { 0U };
    OU::RectanglePainterObject backgroundObject { 0U };
    OU::TextureObject texture { 0U };

    SyncRatio textureRatio { DEFAULT_TEXTURE_RATIO, DEFAULT_TEXTURE_RATIO };

    float scaleFactor { DEFAULT_SCALE_FACTOR };

    std::function<void(void)> clicked { nullptr };
    std::shared_ptr<std::function<void(std::tuple<uint32_t, uint32_t, uint32_t>)>> updateTextureFunction;

    void resize(const Rect& rect);

}; // struct TexturePrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETTEXTURE_P_H
