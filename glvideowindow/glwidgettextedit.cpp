/***************************************************************************
*   Copyright (C) 2014-2017 by Cyril BALETAUD *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgettextedit.h"
#include "glwidgettextedit_p.h"

#include "glpagesmanager.h"
#include "theme.h"

#include <iostream>

namespace Jgv::GLWidget {

TextEditPrivate::TextEditPrivate(float backgroundDepth, GLPainters* const p)
    : depth(backgroundDepth)
    , painters(p)
{
    rectangleObject = painters->rectanglePainter.createObject();
    prefixObject = painters->textPainter.createObject();
    textObject = painters->textPainter.createObject();
    clippingRectangle = painters->textPainter.createClipObject();

    painters->textPainter.enableCursor(textObject, true);
    painters->textPainter.setClippingRectangle(textObject, clippingRectangle);
    painters->textPainter.enableCursor(textObject, true);
}

TextEditPrivate::~TextEditPrivate()
{
    painters->textPainter.destroyObject(prefixObject);
    painters->textPainter.destroyObject(textObject);
    painters->textPainter.destroyClipObject(clippingRectangle);
    painters->rectanglePainter.destroyObject(rectangleObject);
}

void TextEditPrivate::setPrefix(const std::string& prefix, const Color& color)
{
    painters->textPainter.setText(prefixObject, prefix);
    painters->textPainter.setColor(prefixObject, color);
}

void TextEditPrivate::setText(std::string_view t)
{
    text = t;
    painters->textPainter.setText(textObject, text);
}

void TextEditPrivate::setTextColor(const Color& color)
{
    painters->textPainter.setColor(textObject, color);
}

void TextEditPrivate::resize(const Rect& rect)
{
    painters->rectanglePainter.setRectangle(rectangleObject, rect.tuple(), depth);

    const auto [left, bottom, right, top] { painters->textPainter.boundingBox(prefixObject) };
    Rect prefixRect { right, top };
    prefixRect.alignLeft(rect);
    painters->textPainter.setPosition(prefixObject, { prefixRect.left(), prefixRect.bottom(), depth + INTER_LAYOUT_DEPTH });
    painters->textPainter.setPosition(textObject, { prefixRect.right(), prefixRect.bottom(), depth + INTER_LAYOUT_DEPTH });
    painters->textPainter.updateClipObjectRectangle(clippingRectangle, { rect.left<int>(), rect.bottom<int>(), rect.width<int>(), rect.height<int>() });
}

TextEdit::TextEdit(float backgroundDepth, GLPainters* const painters)
    : _impl(std::make_unique<TextEditPrivate>(backgroundDepth, painters))
{
}

TextEdit::~TextEdit() = default;

void TextEdit::disableCursor()
{
    _impl->painters->textPainter.enableCursor(_impl->textObject, false);
}

void TextEdit::setPrefix(const std::string& prefix, const Color& color)
{
    _impl->setPrefix(prefix, color);
}

void TextEdit::reserve(std::size_t textMaxSize)
{
    _impl->painters->textPainter.reserve(_impl->textObject, textMaxSize);
}

void TextEdit::setTextColor(const Color& color)
{
    _impl->setTextColor(color);
}

void TextEdit::setText(std::string_view text)
{
    _impl->setText(text);
}

void TextEdit::setBackground(const Color& color)
{
    _impl->painters->rectanglePainter.setColor(_impl->rectangleObject, color);
}

std::string TextEdit::text() const
{
    return _impl->text;
}

OU::RectanglePainterObjectsVector TextEdit::rectangles() const
{
    return { _impl->rectangleObject };
}

OU::TextPainterObjectVector TextEdit::texts() const
{
    return { _impl->prefixObject, _impl->textObject };
}

void TextEdit::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

} // namespace Jgv::GLWidget
