/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glpagesmanager.h"
#include "glpagesmanager_p.h"

#include "glpageroot.h"

#include <algorithm>

namespace Jgv::GLPage {

ManagerPrivate::ManagerPrivate(std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : globalIpc(std::move(_globalIpc))
{
}

uint64_t ManagerPrivate::nextId()
{
    return ++idCounter;
}

Manager::Manager(std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl(std::make_unique<ManagerPrivate>(globalIpc))
{
}

Manager::~Manager() = default;

void Manager::initialize(GLPainters* painters)
{
    const auto id { _impl->nextId() };
    auto root { std::make_unique<GLPage::Root>(painters, _impl->globalIpc) };
    root->setManager(id, this);
    _impl->branches.emplace(std::make_pair(id, PageBranch { 0, std::move(root) }));
    _impl->rootId = id;
}

uint64_t Manager::registerPage(uint64_t from, std::unique_ptr<Base>&& page)
{
    const auto id { _impl->nextId() };
    page->setManager(id, this);
    _impl->branches.emplace(std::make_pair(id, PageBranch { from, std::move(page) }));
    return id;
}

void Manager::show(uint64_t id)
{
    auto found { _impl->branches.find(id) };
    if (found != _impl->branches.end()) {
        hideAll();
        auto& [id, branch] { *found };
        branch.page->show(_impl->pageWidth, _impl->pageHeight);
    }
}

void Manager::showParent(uint64_t id)
{
    auto found { _impl->branches.find(id) };
    if (found != _impl->branches.end()) {
        auto& [id, branch] { *found };
        show(branch.parent);
    }
}

void Manager::hideAll()
{
    for (auto& branch : _impl->branches) {
        branch.second.page->hide();
    }
}

void Manager::destroyGL() noexcept
{
    for (auto& branch : _impl->branches) {
        branch.second.page->destroyGL();
    }
}

OU::RectanglePainterObjectsProxy Manager::rectangles() const
{
    const auto it { std::find_if(_impl->branches.cbegin(), _impl->branches.cend(), [](const auto& value) {
        const auto& [id, branch] { value };
        return branch.page->isVisible();
    }) };
    if (it != _impl->branches.cend()) {
        const auto& [id, branch] { *it };
        return branch.page->rectangles();
    }
    return {};
}

OU::TextPainterObjectProxy Manager::texts() const
{
    const auto it { std::find_if(_impl->branches.cbegin(), _impl->branches.cend(), [](const auto& value) {
        const auto& [id, branch] { value };
        return branch.page->isVisible();
    }) };
    if (it != _impl->branches.cend()) {
        const auto& [id, branch] { *it };
        return branch.page->texts();
    }
    return {};
}

OU::TexturePainterObjectsProxy Manager::textures() const
{
    const auto it { std::find_if(_impl->branches.cbegin(), _impl->branches.cend(), [](const auto& value) {
        const auto& [id, branch] { value };
        return branch.page->isVisible();
    }) };
    if (it != _impl->branches.cend()) {
        const auto& [id, branch] { *it };
        return branch.page->textures();
    }
    return {};
}

void Manager::resize(float width, float height)
{
    _impl->pageWidth = width;
    _impl->pageHeight = height;
    for (auto& branch : _impl->branches) {
        branch.second.page->resizeGL(width, height);
    }
}

void Manager::mousePressEvent(float x, float y)
{
    std::any_of(_impl->branches.cbegin(), _impl->branches.cend(), [x, y](const auto& value) {
        return value.second.page->acceptPress(x, y);
    });

    //    for (auto& branch : _impl->branches) {
    //        if (branch.second.page->acceptPress(x, y)) {
    //            return;
    //        }
    //    }
}

void Manager::mouseReleaseEvent(float x, float y)
{
    if (!std::any_of(_impl->branches.cbegin(), _impl->branches.cend(), [x, y](const auto& value) {
            return value.second.page->acceptRelease(x, y);
        })) {
        auto root { _impl->branches.find(_impl->rootId) };
        if (root != _impl->branches.end()) {
            auto& [id, branch] { *root };
            branch.page->show(_impl->pageWidth, _impl->pageHeight);
        }
    }
}

void Manager::mouseMoveEvent(float x, float y)
{
    std::any_of(_impl->branches.cbegin(), _impl->branches.cend(), [x, y](const auto& value) {
        return value.second.page->acceptMove(x, y);
    });
}

} // namespace Jgv::GLPage
