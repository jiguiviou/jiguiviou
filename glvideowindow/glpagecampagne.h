/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLPAGECAMPAGNE_H
#define GLPAGECAMPAGNE_H

#include "glpagebase.h"

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLPage {

class Campagne : public Base {

public:
    Campagne(GLPainters* painters, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~Campagne() override = default;
    Campagne(const Campagne&) = delete;
    Campagne(Campagne&&) = delete;
    auto operator=(const Campagne&) -> Campagne& = delete;
    auto operator=(Campagne &&) -> Campagne& = delete;

}; // class Campagne

} // namespace Jgv::GLPage

#endif // GLPAGECAMPAGNE_H
