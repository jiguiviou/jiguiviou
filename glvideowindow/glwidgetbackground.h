/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETBACKGROUND_H
#define GLWIDGETBACKGROUND_H

#include "glwidgetframe.h"

#include <functional>
#include <memory>
#include <vector>

namespace Jgv::GLWidget {

using Color = std::tuple<float, float, float, float>;

class BackgroundPrivate;
class Background : public Frame {

public:
    explicit Background(float backgroundDepth, GLPainters* painters);
    ~Background() override;
    Background(const Background&) = delete;
    Background(Background&&) = delete;
    auto operator=(const Background&) -> Background& = delete;
    auto operator=(Background &&) -> Background& = delete;

    void setBackgroundColor(const Color& color);
    [[nodiscard]] auto rectangles() const -> std::vector<OU::RectanglePainterObject>;
    void resize(const Rect& rect) noexcept override;

private:
    const std::unique_ptr<BackgroundPrivate> _impl;

}; // class Background

} // namespace Jgv::GLWidget

#endif // GLWIDGETBACKGROUND_H
