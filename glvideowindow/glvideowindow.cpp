/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glvideowindow.h"
#include "glvideowindow_p.h"

#include "glpageroot.h"

#include "glwidgetterminal.h"
#include "theme.h"

#include <QMouseEvent>
#include <QMutexLocker>

#include <chrono>
#include <cmath>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <iostream>
#include <openglutils/openglprogram.h>
#include <openglutils/textpainter.h>

namespace Jgv {

const uint FONT_SIZE { 32 };
constexpr std::string_view PTP_TEXT { "CAMERA" };
constexpr std::string_view TRIGGER_DELAY_TEXT { "----- µs" };
constexpr std::string_view CLOCK_ERROR_TEXT { "ERROR" };
constexpr auto PTP_SYNC_COLOR { std::make_tuple(0.F, 1.F, 0.F, 1.F) };
constexpr auto PTP_UNSYNC_COLOR { std::make_tuple(1.F, 0.F, 0.F, 1.F) };
constexpr auto TRIGGER_DELAY_COLOR { std::make_tuple(1.F, 1.F, 1.F, 1.F) };
constexpr auto CLOCK_ERROR { std::make_tuple(1.F, 0.F, 0.F, 1.F) };
constexpr auto CLOCK_INPROGRESS { std::make_tuple(0.973F, 1.0F, 0.39F, 1.F) };
constexpr auto CLOCK_VALID { std::make_tuple(0.F, 1.0F, 0.F, 1.F) };

GLVideoWindowPrivate::GLVideoWindowPrivate(std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : globalIpc(_globalIpc)
    , manager(_globalIpc)
{
    if (auto vtoConfig { globalIpc->vtoConfig() }) {
        haveVtoConfig = true;
        //        auto output = [](GlobalIPC::OutputLevel, std::string_view text) {
        //            std::cout << "GLVideoWindow MESSAGE " << text << std::endl;
        //        };
        //        logOuput = std::make_shared<std::function<void(GlobalIPC::OutputLevel, std::string_view)>>(output);
        //vtoConfig->addOutputListener(logOuput);

        terminal = std::make_shared<GLWidget::Terminal>(GLWidget::WIDGET_BACKGROUND_DEPTH + GLWidget::INTER_WIDGET_DEPTH, &painters, globalIpc);
    }
}

void GLVideoWindowPrivate::initializeGL()
{
    auto updatePrimaryTexture = [this](std::tuple<uint32_t, uint32_t, uint32_t> tuple) {
        auto [texture, textureWidth, textureHeight] { tuple };
        externalTexture.set(texture, static_cast<float>(textureWidth) / static_cast<float>(textureHeight));
    };
    updateTextureFunction = std::make_shared<std::function<void(std::tuple<uint32_t, uint32_t, uint32_t>)>>(updatePrimaryTexture);
    globalIpc->addTextureChangedListener(Channel::Type::Primary, updateTextureFunction);

    painters.texturePainter.initializeGL();
    background = painters.texturePainter.createObject();

    updatePrimaryTexture(globalIpc->texture(Channel::Type::Primary));

    glEnable(GL_DEPTH_TEST);

    if (auto vtoConfig { globalIpc->vtoConfig() }) {
        painters.textPainter.initializeGL(FONT_SIZE, "monospace");
        painters.rectanglePainter.initializeGL();

        // transfert la gestion des pages au manager
        manager.initialize(&painters);

        if (terminal) {
            terminal->initializeGL();
        }

        const auto triggerDelayText { painters.textPainter.createObject() };
        painters.textPainter.setScaleFactor(triggerDelayText, 0.6F);
        painters.textPainter.reserve(triggerDelayText, 20);
        painters.textPainter.setText(triggerDelayText, std::string { TRIGGER_DELAY_TEXT });
        painters.textPainter.setColor(triggerDelayText, TRIGGER_DELAY_COLOR);
        painters.textPainter.setPosition(triggerDelayText, { GLWidget::INTER_WIDGET_SPACE, GLWidget::INTER_WIDGET_SPACE, GLWidget::BACKGROUND_DEPTH + GLWidget::INTER_LAYOUT_DEPTH });
        triggerDelay = triggerDelayText;

        const auto sourceClockText { painters.textPainter.createObject() };
        painters.textPainter.setScaleFactor(sourceClockText, 0.6F);
        painters.textPainter.reserve(sourceClockText, 20);
        painters.textPainter.setText(sourceClockText, CLOCK_ERROR_TEXT);
        painters.textPainter.setColor(sourceClockText, CLOCK_ERROR);
        sourceClock = sourceClockText;

        const auto clockText { painters.textPainter.createObject() };
        painters.textPainter.setScaleFactor(clockText, 0.6F);
        painters.textPainter.reserve(clockText, 20);
        painters.textPainter.setText(clockText, CLOCK_ERROR_TEXT);
        painters.textPainter.setColor(clockText, CLOCK_ERROR);
        clock = clockText;

        const auto ptpText { painters.textPainter.createObject() };
        painters.textPainter.setScaleFactor(ptpText, 0.6F);
        painters.textPainter.setText(ptpText, std::string { PTP_TEXT });
        painters.textPainter.setColor(ptpText, PTP_UNSYNC_COLOR);
        ptp = ptpText;

        auto sourceClockEvent = [this](GlobalIPC::ClockState state, std::string_view message) {
            switch (state) {
            case GlobalIPC::ClockState::valid:
                painters.textPainter.updateColor(sourceClock, CLOCK_VALID);
                break;
            case GlobalIPC::ClockState::inprogress:
                painters.textPainter.updateColor(sourceClock, CLOCK_INPROGRESS);
                break;
            case GlobalIPC::ClockState::error:
                painters.textPainter.updateColor(sourceClock, CLOCK_ERROR);
                break;
            }
            painters.textPainter.updateText(sourceClock, message);
        };
        sourceClockFunction = std::make_shared<std::function<void(GlobalIPC::ClockState, std::string_view)>>(sourceClockEvent);
        vtoConfig->addSourceClockStateListener(sourceClockFunction);
        {
            auto [state, message] { vtoConfig->sourceClockState() };
            sourceClockEvent(state, message);
        }

        auto clockEvent = [this](GlobalIPC::ClockState state, std::string_view message) {
            switch (state) {
            case GlobalIPC::ClockState::valid:
                painters.textPainter.updateColor(clock, CLOCK_VALID);
                break;
            case GlobalIPC::ClockState::inprogress:
                painters.textPainter.updateColor(clock, CLOCK_INPROGRESS);
                break;
            case GlobalIPC::ClockState::error:
                painters.textPainter.updateColor(clock, CLOCK_ERROR);
                break;
            }
            painters.textPainter.setText(clock, message);
        };
        clockFunction = std::make_shared<std::function<void(GlobalIPC::ClockState, std::string_view)>>(clockEvent);
        vtoConfig->addClockStateListener(clockFunction);
        {
            auto [state, message] { vtoConfig->clockState() };
            clockEvent(state, message);
        }

        auto sync = [this](bool locked) {
            painters.textPainter.updateColor(ptp, locked ? PTP_SYNC_COLOR : PTP_UNSYNC_COLOR);
        };
        syncFunction = std::make_shared<std::function<void(bool)>>(sync);
        vtoConfig->addPtpCameraLockedListener(syncFunction);
        sync(vtoConfig->ptpCameraLocked());

        auto triggerDelayEvent = [this](uint64_t delay) {
            painters.textPainter.updateText(triggerDelay, std::to_string(std::lround(delay / 1000.)) + " µs");
        };
        triggerDelayFunction = std::make_shared<std::function<void(uint64_t)>>(triggerDelayEvent);
        vtoConfig->addExposureDelayListener(Channel::Type::Primary, triggerDelayFunction);
    }
}

void GLVideoWindowPrivate::resizeVideo(float width, float height, float ratio)
{
    GLWidget::Rect videoRect(width, height);
    const auto windowRatio { static_cast<float>(width) / static_cast<float>(height) };
    if (windowRatio > ratio) {
        videoRect.setRight(height * ratio);
    } else {
        videoRect.setTop(width / ratio);
    }
    videoRect.setCenter(width / 2.F, height / 2.F);
    painters.texturePainter.setRectangle(background, videoRect.tuple(), GLWidget::BACKGROUND_DEPTH);
}

void GLVideoWindowPrivate::resizeGL(float width, float height)
{
    painters.texturePainter.resizeViewGL(width, height);
    resizeVideo(width, height, externalTexture.value().ratio);

    if (haveVtoConfig) {
        painters.textPainter.resizeViewGL(width, height);
        painters.rectanglePainter.resizeViewGL(width, height);

        manager.resize(width, height);
        if (terminal) {
            terminal->resize(GLWidget::Rect { 100.F, 100.F, width - 100.F, height - 100.F });
        }

        auto y { GLWidget::INTER_WIDGET_SPACE };

        // les infos timings sont en bas à gauche
        {
            const auto [left, bottom, right, top] { painters.textPainter.boundingBox(triggerDelay) };
            y += GLWidget::INTER_WIDGET_SPACE + top;
            painters.textPainter.setPosition(ptp, { GLWidget::INTER_WIDGET_SPACE, y, GLWidget::BACKGROUND_DEPTH + GLWidget::INTER_LAYOUT_DEPTH });
        }
        {
            const auto [left, bottom, right, top] { painters.textPainter.boundingBox(ptp) };
            y += GLWidget::INTER_WIDGET_SPACE + top;
            painters.textPainter.setPosition(clock, { GLWidget::INTER_WIDGET_SPACE, y, GLWidget::BACKGROUND_DEPTH + GLWidget::INTER_LAYOUT_DEPTH });
        }
        {
            const auto [left, bottom, right, top] { painters.textPainter.boundingBox(clock) };
            y += GLWidget::INTER_WIDGET_SPACE + top;
            painters.textPainter.setPosition(sourceClock, { GLWidget::INTER_WIDGET_SPACE, y, GLWidget::BACKGROUND_DEPTH + GLWidget::INTER_LAYOUT_DEPTH });
        }
    }
}

void GLVideoWindowPrivate::paintGL(float width, float height)
{
    // si la texture vidéo a changé
    if (externalTexture.changed()) {
        const auto val { externalTexture.value() };
        painters.texturePainter.setExternalTexture(background, val.id);
        resizeVideo(width, height, val.ratio);
    }

    glClearColor(0.2F, 0.2F, 0.2F, 1.F);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    painters.texturePainter.renderGL(background);
    if (haveVtoConfig) {
        if (terminal) {
            painters.textPainter.renderGL(terminal->texts());
            if (terminal->finished()) {
                terminal.reset();
            }
        } else {
            auto texts { manager.texts() };
            texts.push_back({ sourceClock, clock, ptp, triggerDelay });
            painters.rectanglePainter.renderGL(manager.rectangles());
            painters.texturePainter.renderGL(manager.textures());
            painters.textPainter.renderGL(texts);
        }
    }
}

GLVideoWindow::GLVideoWindow(std::shared_ptr<GlobalIPC::Object> globalIpc)
    : QOpenGLWindow(globalIpc->sharedContext())
    , _impl(std::make_unique<GLVideoWindowPrivate>(globalIpc))
{
    QSurfaceFormat fmt;
    fmt.setMajorVersion(4);
    fmt.setMinorVersion(2);
    fmt.setProfile(QSurfaceFormat::CoreProfile);
    fmt.setSwapInterval(0);
    fmt.setSwapBehavior(QSurfaceFormat::SingleBuffer);
    fmt.setRedBufferSize(8);
    fmt.setGreenBufferSize(8);
    fmt.setBlueBufferSize(8);
    fmt.setDepthBufferSize(24);
    setFormat(fmt);
}

GLVideoWindow::~GLVideoWindow()
{
    makeCurrent();
    // détruit les ressources opengl des pages
    _impl->manager.destroyGL();

    if (_impl->terminal) {
        _impl->terminal->destroyGL();
    }
    _impl->painters.textPainter.destroyGL();
    _impl->painters.texturePainter.destroyGL();
    _impl->painters.rectanglePainter.destroyGL();

    doneCurrent();
}

void GLVideoWindow::initializeGL()
{
    _impl->initializeGL();
}

void GLVideoWindow::resizeGL(int w, int h)
{
    _impl->resizeGL(static_cast<float>(w), static_cast<float>(h));
}

void GLVideoWindow::paintGL()
{
    _impl->paintGL(static_cast<float>(width()), static_cast<float>(height()));
    update();
}

void GLVideoWindow::mousePressEvent(QMouseEvent* event)
{

    if (_impl->haveVtoConfig) {
        _impl->manager.mousePressEvent(static_cast<float>(event->x()), static_cast<float>(height() - event->y()));
        event->accept();
    }
}

void GLVideoWindow::mouseReleaseEvent(QMouseEvent* event)
{
    if (_impl->haveVtoConfig) {
        _impl->manager.mouseReleaseEvent(static_cast<float>(event->x()), static_cast<float>(height() - event->y()));
        event->accept();
    }
}

void GLVideoWindow::mouseMoveEvent(QMouseEvent* event)
{
    if (_impl->haveVtoConfig) {
        _impl->manager.mouseMoveEvent(static_cast<float>(event->x()), static_cast<float>(height() - event->y()));
        event->accept();
    }
}

} // namespace Jgv
