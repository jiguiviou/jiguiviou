/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETKEYPAD_P_H
#define GLWIDGETKEYPAD_P_H

#include "glpainters.h"

#include <functional>
#include <glm/vec4.hpp>
#include <memory>

namespace Jgv::GLWidget {

class Key;
class KeySpecial;
class Rect;

struct KeypadPrivate {
    KeypadPrivate(float backgroundDepth, GLPainters* painters);
    const float depth;
    GLPainters* const painters;

    std::vector<std::unique_ptr<Key>> keys;
    std::function<void(std::string_view)> setText { nullptr };
    std::function<std::string_view(void)> text { nullptr };

    std::unique_ptr<KeySpecial> supprKey;

    [[nodiscard]] auto rectangles() const -> OU::RectanglePainterObjectsVector;
    [[nodiscard]] auto textures() const -> OU::TexturePainterObjectsVector;
    [[nodiscard]] auto texts() const -> OU::TextPainterObjectVector;

    void resize(const Rect& rect);

    void handlePress(float x, float y);
    void handleRelease(float x, float y);
    void handleMove(float x, float y);

}; // class KeyboardPrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETKEYPAD_P_H
