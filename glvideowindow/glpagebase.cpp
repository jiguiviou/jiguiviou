/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glpagebase.h"
#include "glpagebase_p.h"

namespace Jgv::GLPage {

BasePrivate::BasePrivate(GLPainters* p, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : painters(p)
    , globalIpc(std::move(_globalIpc))
{
}

void BasePrivate::setManager(uint64_t id, Manager* m)
{
    managerId = id;
    manager = m;
}

Base::Base(std::unique_ptr<BasePrivate> dd)
    : _impl(std::move(dd))
{
}

Base::~Base() = default;

void Base::setExitFunction(std::function<void(bool)> function)
{
    _impl->exitFunction = std::move(function);
}

void Base::show(float width, float height)
{
    if (!_impl->isVisible) {
        _impl->show();
        _impl->isVisible = true;
        _impl->resize(width, height);
    }
}

auto Base::isVisible() const noexcept -> bool
{
    return _impl->isVisible;
}

void Base::hide()
{
    if (_impl->isVisible) {
        _impl->isVisible = false;
        _impl->hide();
    }
}

void Base::destroyGL() noexcept
{
    _impl->destroyGL();
}

auto Base::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    return _impl->rectangles();
}

auto Base::texts() const -> OU::TextPainterObjectProxy
{
    return _impl->texts();
}

auto Base::textures() const -> OU::TexturePainterObjectsProxy
{
    return _impl->textures();
}

void Base::resizeGL(float width, float height)
{
    _impl->resize(width, height);
}

auto Base::acceptPress(float x, float y) -> bool
{
    if (!_impl->isVisible) {
        return false;
    }
    _impl->handlePress(x, y);

    return true;
}

auto Base::acceptRelease(float x, float y) -> bool
{
    if (!_impl->isVisible) {
        return false;
    }

    _impl->handleRelease(x, y);

    return true;
}

auto Base::acceptMove(float x, float y) -> bool
{
    if (!_impl->isVisible) {
        return false;
    }

    _impl->handleMove(x, y);

    return true;
}

void Base::setManager(uint64_t id, Manager* manager)
{
    _impl->setManager(id, manager);
}

} // namespace Jgv::GLPage
