/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETKEY_H
#define GLWIDGETKEY_H

#include "glpainters.h"
#include "glwidgetframe.h"

#include <functional>

namespace Jgv::GLWidget {

class Key : public Frame {
    const float _depth;
    GLPainters* const _painters;
    std::function<void(char character)> _clicked;
    OU::TextPainterObject _textObject { 0U };
    OU::RectanglePainterObject _backgroundObject { 0U };

    const char _normal;
    const char _maj;
    char _current { 0 };

public:
    explicit Key(float backgroundDepth, char normalChar, char majChar, GLPainters* painters);
    ~Key() override;
    Key(const Key&) = delete;
    Key(Key&&) = delete;
    auto operator=(const Key&) -> Key& = delete;
    auto operator=(Key &&) -> Key& = delete;

    void setClickedFunction(std::function<void(char)> function);
    void toggleMaj();
    [[nodiscard]] auto normal() const noexcept -> char;
    [[nodiscard]] auto current() const noexcept -> char;

    [[nodiscard]] auto textObject() const -> OU::TextPainterObject;
    [[nodiscard]] auto rectangleObject() const -> OU::RectanglePainterObject;

    void resize(const Rect& rect) noexcept override;

private:
    void clicked() override;

}; // class Key

} // namespace Jgv::GLWidget

#endif // GLWIDGETKEY_H
