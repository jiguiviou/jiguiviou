/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETBACKGROUND_P_H
#define GLWIDGETBACKGROUND_P_H

#include "glwidgetrect.h"

#include "glpainters.h"

namespace Jgv::GLWidget {

struct BackgroundPrivate {
    BackgroundPrivate(float backgroundDepth, GLPainters* p);
    ~BackgroundPrivate();
    BackgroundPrivate(const BackgroundPrivate&) = delete;
    BackgroundPrivate(BackgroundPrivate&&) = delete;
    auto operator=(const BackgroundPrivate&) -> BackgroundPrivate& = delete;
    auto operator=(BackgroundPrivate &&) -> BackgroundPrivate& = delete;

    const float depth;
    GLPainters* const painters;
    OU::RectanglePainterObject backgroundObject { 0U };
    void resize(const Rect& rect);

}; // struct BackgroundPrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETBACKGROUND_P_H
