/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETBALISE_P_H
#define GLWIDGETBALISE_P_H

#include "glpainters.h"

#include <chrono>
#include <filesystem>
#include <memory>
#include <vector>

namespace Jgv::Channel {
enum class Type ;
} // namespace Jgv::Channel

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::GLWidget {

class ImagesSlider;
class Label;
class Radio;
class Rect;
class Texture;

struct BalisePrivate {
    BalisePrivate(float backgroundDepth, GLPainters* p, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc);
    ~BalisePrivate();
    BalisePrivate(const BalisePrivate&) = delete;
    BalisePrivate(BalisePrivate&&) = delete;
    auto operator=(const BalisePrivate&) -> BalisePrivate& = delete;
    auto operator=(BalisePrivate &&) -> BalisePrivate& = delete;

    const float depth;
    GLPainters* const painters;
    const Channel::Type channel;
    std::shared_ptr<GlobalIPC::Object> globalIpc;

    std::filesystem::path currentPath;

    std::unique_ptr<Label> title;
    std::unique_ptr<Texture> video;
    std::unique_ptr<Radio> directBefore;
    std::unique_ptr<Radio> inverseBefore;
    std::unique_ptr<Radio> directAfter;
    std::unique_ptr<Radio> inverseAfter;
    std::unique_ptr<Label> timer;

    std::unique_ptr<ImagesSlider> slider;
    std::unique_ptr<Texture> capture;
    std::unique_ptr<Texture> zoomPlus;
    std::unique_ptr<Texture> zoomMoins;
    std::unique_ptr<Texture> snapshot;

    std::chrono::system_clock::time_point lastSnapshotTime;
    std::chrono::duration<double> elapsed { 0 };

    void updateChrono();

    auto rectangles() -> OU::RectanglePainterObjectsProxy;
    auto textures() -> OU::TexturePainterObjectsProxy;
    auto texts() -> OU::TextPainterObjectProxy;

    void resize(const Rect& rect) noexcept;

    void handlePress(float x, float y);
    void handleRelease(float x, float y);
    void handleMove(float x, float y);

}; // struct CampagnePrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETBALISE_P_H
