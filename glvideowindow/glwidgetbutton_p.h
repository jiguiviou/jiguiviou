/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLWIDGETBUTTON_P_H
#define GLWIDGETBUTTON_P_H

#include "glpainters.h"

#include <chrono>
#include <functional>

namespace Jgv {
class Rect;
} // namespace Jgv

namespace Jgv::GLWidget {

struct ButtonPrivate {
    ButtonPrivate(float backgroundDepth, GLPainters* p);
    ~ButtonPrivate();
    ButtonPrivate(const ButtonPrivate&) = delete;
    ButtonPrivate(ButtonPrivate&&) = delete;
    auto operator=(const ButtonPrivate&) -> ButtonPrivate& = delete;
    auto operator=(ButtonPrivate &&) -> ButtonPrivate& = delete;

    const float depth;
    GLPainters* const painters;

    bool blinking { false };
    std::chrono::system_clock::time_point blinkTime;

    std::function<void(void)> clicked { nullptr };

    OU::TexturePainterObject iconObject { 0U };
    OU::TexturePainterObject backgroundObject { 0U };

    OU::TextureObject iconTextureObject { 0U };
    OU::TextureObject backgroundOffTextureObject { 0U };
    OU::TextureObject backgroundOnTextureObject { 0U };

    void initialize(const std::string& logoFileName, const std::string& backgroundOffFileName, const std::string& backgroundOnFileName);
    void resize(const Rect& rect, bool pressed);

}; // struct ButtonPrivate

} // namespace Jgv::GLWidget

#endif // GLWIDGETBUTTON_P_H
