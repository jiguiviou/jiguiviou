﻿/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetbulles.h"
#include "glwidgetbulles_p.h"

#include "glpagesmanager.h"
#include "glvideowindow.h"
#include "glwidgetimagesslider.h"
#include "glwidgetlabel.h"
#include "glwidgettexture.h"
#include "gnuinstalldir.h"
#include "theme.h"

#include <configfile/configfile.h>
#include <filesystem>
#include <globalipc/globalipc.h>
#include <globalipc/vtoconfig.h>
#include <iostream>
#include <libconfig.h++>

namespace Jgv::GLWidget {

constexpr uint SNAPSHOT_COUNT { 1U };
constexpr std::string_view BULLES_PATH { "Bulles" };

BullesPrivate::BullesPrivate(float backgroundDepth, GLPainters* const p, Channel::Type _channel, std::shared_ptr<GlobalIPC::Object> _globalIpc)
    : depth(backgroundDepth)
    , painters(p)
    , channel(_channel)
    , globalIpc(std::move(_globalIpc))
{
    title = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH, painters);
    title->setBackgroundColor(TITLE_BACKGROUND_COLOR);
    title->setTextColor(TITLE_TEXT_COLOR);
    title->setText("Bulles de verticalité");

    currentPositions = std::make_unique<Label>(depth + INTER_WIDGET_DEPTH, painters);
    currentPositions->setBackgroundColor(LABEL_BACKGROUND_COLOR);
    currentPositions->setTextColor(LABEL_TEXT_COLOR);
    currentPositions->setText("+000.000 000.000");

    video = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    video->setTexture(channel);

    capture = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    capture->setBackgroundColor(TEXTURE_BACKGROUND_COLOR);

    auto viewCapture = [this](const std::string& filename) {
        capture->setTexture(filename);
    };

    slider = std::make_unique<ImagesSlider>(depth + INTER_WIDGET_DEPTH, painters, globalIpc);
    slider->setBackgroundColor(SLIDER_BACKGROUND_COLOR);
    slider->setSelectfunction(viewCapture);
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        const auto desiredPath { std::filesystem::path(globalIpc->vtoConfig()->pathGet()) / BULLES_PATH };
        if (!std::filesystem::exists(desiredPath)) {
            std::error_code ec;
            if (!std::filesystem::create_directory(desiredPath, ec)) {
                std::cerr << "GLWidget::Bulles failed to create directory " << desiredPath << " error: " << ec.message() << std::endl;
                return;
            }
            std::filesystem::permissions(desiredPath, std::filesystem::perms::all, ec);
            if (ec != std::error_code()) {
                std::clog << "GLWidget::Bulles failed to change permissions on " << desiredPath << " error: " << ec.message() << std::endl;
            }
        }
        slider->setPath(desiredPath);
    }

    snapshot = std::make_unique<Texture>(depth + INTER_WIDGET_DEPTH + INTER_WIDGET_DEPTH, painters, globalIpc);
    snapshot->setTexture(GnuIntallDirs::assetsDir() + "/touch-record.png");

    auto takeSnapshot = [this]() {
        if (const auto vtoConfig { globalIpc->vtoConfig() }) {
            if (std::filesystem::exists(std::filesystem::path(globalIpc->vtoConfig()->pathGet()) / BULLES_PATH)) {
                vtoConfig->snapshot(channel, SNAPSHOT_COUNT, BULLES_PATH);
            }
        }
    };
    snapshot->setClickedFunction(takeSnapshot);
}

BullesPrivate::~BullesPrivate() = default;

void BullesPrivate::updatePositions()
{
    if (const auto vtoConfig { globalIpc->vtoConfig() }) {
        constexpr auto M { 1000 };
        auto [s, g] { vtoConfig->positions() };
        auto oldSite { std::exchange(siteValue, s) };
        auto oldGisement { std::exchange(gisementValue, g) };
        if (siteValue != oldSite || gisementValue != oldGisement) {
            auto absSite { std::abs(siteValue) };
            auto absGisement { std::abs(gisementValue) };
            std::ostringstream stream;
            stream << (siteValue < 0 ? '-' : '+')
                   << std::setfill('0') << std::setw(3) << std::setprecision(3) << absSite / M
                   << '.'
                   << std::setfill('0') << std::setw(3) << std::setprecision(3) << absSite % M
                   << " "
                   << std::setfill('0') << std::setw(3) << std::setprecision(3) << absGisement / M
                   << '.'
                   << std::setfill('0') << std::setw(3) << std::setprecision(3) << absGisement % M;
            currentPositions->setText(stream.str());
        }
    }
}

auto BullesPrivate::rectangles() -> OU::RectanglePainterObjectsProxy
{
    auto proxy { slider->rectangles() };
    proxy.emplace_back(title->rectangles());
    proxy.emplace_back(currentPositions->rectangles());
    proxy.emplace_back(video->rectangles());
    proxy.emplace_back(capture->rectangles());
    return proxy;
}

auto BullesPrivate::textures() -> OU::TexturePainterObjectsProxy
{
    auto proxy { slider->textures() };
    proxy.emplace_back(video->textures());
    proxy.emplace_back(capture->textures());
    proxy.emplace_back(snapshot->textures());
    return proxy;
}

auto BullesPrivate::texts() -> OU::TextPainterObjectProxy
{
    updatePositions();
    OU::TextPainterObjectProxy proxy { title->texts() };
    proxy.emplace_back(currentPositions->texts());
    return proxy;
}

void BullesPrivate::resize(const Rect& rect) noexcept
{
    constexpr auto HALF { 0.5F };

    auto titleRect { rect };
    titleRect.setBottom(rect.top() - TITLE_HEIGHT);
    title->resize(titleRect);

    auto sliderRect { rect };
    sliderRect.setTop(rect.bottom() + SLIDER_HEIGHT);
    sliderRect.moveBottom(rect.bottom() + INTER_WIDGET_SPACE);
    slider->resize(sliderRect);

    auto videoRect { rect };
    videoRect.setRight(videoRect.left() + rect.width() * HALF);
    videoRect.setTop(videoRect.bottom() + videoRect.width() / video->textureRatio());
    videoRect.moveTop(title->bottom());
    video->resize(videoRect.reduced(INTER_WIDGET_SPACE));

    auto positionsRect { video->rect() };
    positionsRect.setTop(positionsRect.bottom() + LABEL_HEIGHT);
    positionsRect.moveTop(video->bottom() - INTER_WIDGET_SPACE);
    currentPositions->resize(positionsRect);

    // la moitié droite pour la vue capture
    auto captureRect { videoRect };
    captureRect.moveLeft(videoRect.right());
    capture->resize(captureRect.reduced(INTER_WIDGET_SPACE));

    // les touches
    Rect touchRect { TOUCH_SIZE * 1.5F, TOUCH_SIZE * 1.5F };
    touchRect.setCenter(capture->centerX(), capture->centerY());
    touchRect.moveTop(capture->bottom() - INTER_WIDGET_SPACE);
    snapshot->resize(touchRect);
}

Bulles::Bulles(float backgroundDepth, GLPainters* painters, Channel::Type channel, std::shared_ptr<GlobalIPC::Object> globalIpc)
    : _impl { std::make_unique<BullesPrivate>(backgroundDepth, painters, channel, globalIpc) }
{
}

Bulles::~Bulles() = default;

auto Bulles::rectangles() const -> OU::RectanglePainterObjectsProxy
{
    if (_impl->video->textureRatioChanged()) {
        _impl->resize(*this);
    }
    return _impl->rectangles();
}

auto Bulles::textures() const -> OU::TexturePainterObjectsProxy
{
    return _impl->textures();
}

auto Bulles::texts() const -> OU::TextPainterObjectProxy
{
    return _impl->texts();
}

void Bulles::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect);
}

void Bulles::handlePress(float x, float y)
{
    if (_enabled) {
        _impl->slider->handlePress(x, y);
        _impl->snapshot->handlePress(x, y);
    }
}

void Bulles::handleRelease(float x, float y)
{
    if (_enabled) {
        _impl->slider->handleRelease(x, y);
        _impl->snapshot->handleRelease(x, y);
    }
}

void Bulles::handleMove(float x, float y)
{
    if (_enabled) {
        _impl->slider->handleMove(x, y);
        _impl->snapshot->handleMove(x, y);
    }
}

} // namespace Jgv::GLWidget
