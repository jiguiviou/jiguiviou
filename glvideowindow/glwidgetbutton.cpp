/***************************************************************************
*   Copyright (C) 2019 by Cyril BALETAUD                                  *
*   cyril.baletaud@gmail.com                                              *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include "glwidgetbutton.h"
#include "glwidgetbutton_p.h"

#include "glpagesmanager.h"
#include "theme.h"

#include <iostream>

namespace Jgv::GLWidget {

const auto PRESSED_SCALE_FACTOR { 0.9F };
const auto ICON_SCALE_FACTOR { 0.6F };

ButtonPrivate::ButtonPrivate(float backgroundDepth, GLPainters* const p)
    : depth(backgroundDepth)
    , painters(p)
{
    iconObject = painters->texturePainter.createObject();
    backgroundObject = painters->texturePainter.createObject();
}

ButtonPrivate::~ButtonPrivate()
{
    if (iconObject.index > 0) {
        painters->texturePainter.destroyObject(iconObject);
    }
    if (backgroundObject.index > 0) {
        painters->texturePainter.destroyObject(backgroundObject);
    }
    if (iconTextureObject.index > 0) {
        painters->texturePainter.destroyTextureObject(iconTextureObject);
    }
    if (backgroundOffTextureObject.index > 0) {
        painters->texturePainter.destroyTextureObject(backgroundOffTextureObject);
    }
    if (backgroundOnTextureObject.index > 0) {
        painters->texturePainter.destroyTextureObject(backgroundOnTextureObject);
    }
}

void ButtonPrivate::initialize(const std::string& logoFileName, const std::string& backgroundOffFileName, const std::string& backgroundOnFileName)
{

    iconTextureObject = painters->texturePainter.createTextureObject(logoFileName);
    backgroundOffTextureObject = painters->texturePainter.createTextureObject(backgroundOffFileName);
    backgroundOnTextureObject = painters->texturePainter.createTextureObject(backgroundOnFileName);

    painters->texturePainter.setTexture(iconObject, iconTextureObject);
    painters->texturePainter.setTexture(backgroundObject, backgroundOffTextureObject);
}

void ButtonPrivate::resize(const Rect& rect, bool pressed)
{
    painters->texturePainter.setRectangle(backgroundObject, pressed ? rect.scaled(PRESSED_SCALE_FACTOR).tuple() : rect.tuple(), depth);
    painters->texturePainter.setRectangle(iconObject, pressed ? rect.scaled(PRESSED_SCALE_FACTOR * ICON_SCALE_FACTOR).tuple() : rect.scaled(ICON_SCALE_FACTOR).tuple(), depth + INTER_LAYOUT_DEPTH);
}

Button::Button(float backgroundDepth, GLPainters* const painters)
    : _impl(std::make_unique<ButtonPrivate>(backgroundDepth, painters))
{
}

Button::~Button() = default;

void Button::setClickedFunction(std::function<void()> clicked)
{
    _impl->clicked = std::move(clicked);
}

void Button::initialize(const std::string& logoFileName, const std::string& backgroundOffFileName, const std::string& backgroundOnFileName)
{
    _impl->initialize(logoFileName, backgroundOffFileName, backgroundOnFileName);
    _impl->painters->texturePainter.setTexture(_impl->backgroundObject, isActived() ? _impl->backgroundOnTextureObject : _impl->backgroundOffTextureObject);
}

auto Button::textures() const -> std::vector<OU::TexturePainterObject>
{
    if (_enabled) {
        if (_impl->blinking) {
            const auto now { std::chrono::system_clock::now() };
            auto elapsed { std::chrono::duration_cast<std::chrono::milliseconds>(now - _impl->blinkTime) };
            if (elapsed.count() > 1000) {
                _impl->blinkTime = now;
            }
            if (elapsed.count() > 500) {
                return { _impl->backgroundObject };
            }
        }
        // on dessine dans l'ordre
        return { _impl->backgroundObject, _impl->iconObject };
    }
    return {};
}

void Button::setActived(bool actived) noexcept
{
    Frame::setActived(actived);
    _impl->painters->texturePainter.setTexture(_impl->backgroundObject, actived ? _impl->backgroundOnTextureObject : _impl->backgroundOffTextureObject);
}

void Button::setBlinking(bool blinking) noexcept
{
    if (blinking) {
        _impl->blinkTime = std::chrono::system_clock::now();
    }
    _impl->blinking = blinking;
}

void Button::resize(const Rect& rect) noexcept
{
    Frame::resize(rect);
    _impl->resize(rect, _pressed);
}

void Button::clicked()
{
    if (_impl->clicked) {
        _impl->clicked();
    }
}

void Button::pressed()
{
    _impl->resize(*this, true);
}

void Button::released()
{
    _impl->resize(*this, false);
}

void Button::moved()
{
    if (_pressed) {
    }
}

} // namespace Jgv::GLWidget
