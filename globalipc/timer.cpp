/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "timer.h"
#include "timer_p.h"

#include <future>
#include <iostream>

namespace {

void loop(std::function<void(void)> task, std::future<void> barrierFuture, std::chrono::milliseconds timeout)
{
    std::future_status status;
    auto f { std::move(task) };
    do {
        status = barrierFuture.wait_for(timeout);
        if (status == std::future_status::timeout) {
            f();
        }

    } while (status != std::future_status::ready);
}

} // namespace

namespace Jgv {

Timer::Timer()
    : _impl(std::make_unique<TimerPrivate>())
{
}

Timer::~Timer()
{
    stop();
}

void Timer::setInterval(int millisecondes) noexcept
{
    _impl->interval = std::chrono::milliseconds { millisecondes };
}

void Timer::setTask(std::function<void()> task) noexcept
{
    _impl->task = std::move(task);
}

void Timer::start() noexcept
{
    if (_impl->thread.joinable()) {
        std::clog << "Timer::start: allready started !" << std::endl;
        return;
    }

    if (_impl->task) {
        std::future<void> barrierFuture { _impl->barrier.get_future() };
        _impl->thread = std::thread { &loop, _impl->task, std::move(barrierFuture), _impl->interval };
        pthread_setname_np(_impl->thread.native_handle(), "Timer");
    }
}

void Timer::stop() noexcept
{
    if (_impl->thread.joinable()) {
        _impl->barrier.set_value();
        _impl->thread.join();
    }
}

} // namespace Jgv
