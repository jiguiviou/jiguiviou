/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLOBALIPC_P_H
#define GLOBALIPC_P_H

#include <functional>
#include <list>
#include <map>
#include <memory>
#include <tuple>

class QOpenGLContext;

namespace Jgv::Channel {
enum class Type;
enum class Event;
} // namespace Jgv::Channel

namespace Jgv::Gvcp {
class Client;
} // namespace Jgv::Gvcp

namespace Jgv::GenICam {
class Model;
} // namespace Jgv::GenICam

namespace Jgv::Genicam2 {
class Interface;
} // namespace Jgv::Genicam2

namespace Jgv::GlobalIPC {

using ListenerList = std::list<std::function<void(uint64_t)>>;

template <typename T>
struct TypeAcces {
    std::weak_ptr<std::function<void(T)>> setter;
    std::weak_ptr<std::function<T(void)>> getter;
    std::list<std::weak_ptr<std::function<void(T)>>> listeners;
};

template <typename T>
struct StructAcces {
    std::weak_ptr<std::function<void(T)>> setter;
    std::weak_ptr<std::function<T(void)>> getter;
    std::list<std::weak_ptr<std::function<void(const T&)>>> listeners;
};

template <typename T, typename U>
struct TupleAccess {
    std::weak_ptr<std::function<void(std::tuple<T, U>)>> command;
    std::weak_ptr<std::function<std::tuple<T, U>(void)>> getter;
    std::list<std::weak_ptr<std::function<void(std::tuple<T, U>)>>> listeners;
};

template <typename T>
struct SharedObjectAccessor {
    std::weak_ptr<std::function<std::shared_ptr<T>(void)>> getter;
    std::list<std::weak_ptr<std::function<void(const std::shared_ptr<T>&)>>> setters;
};

struct NetworkInfos {
    uint32_t gvcpControllerIP;
    uint32_t gvcpTransmitterIP;

    uint32_t gvspTransmitterIP;
    uint32_t gvspReceiverIP;

    uint16_t gvcpControllerPort;
    uint16_t gvcpTransmitterPort;

    uint16_t gvspTransmitterPort;
    uint16_t gvspReceiverPort;
};

class VtoConfig;

struct ObjectPrivate {
    std::map<Channel::Type, NetworkInfos> networks;
    std::map<Channel::Type, std::weak_ptr<std::function<std::string(void)>>> emetterNameGetters;
    std::map<Channel::Type, std::weak_ptr<std::function<std::shared_ptr<Gvcp::Client>(void)>>> gvcpClients;
    std::map<Channel::Type, TypeAcces<std::tuple<uint, uint32_t, uint32_t>>> texture;
    std::map<Channel::Type, std::map<uint16_t, std::list<std::weak_ptr<std::function<void(uint64_t)>>>>> gvcpEvents;
    std::map<Channel::Type, std::function<int64_t(int64_t)>> timestampGetters;
    std::map<Channel::Type, std::list<std::weak_ptr<std::function<void(uint32_t, uint32_t, uint32_t)>>>> pixelFormatListeners;
    std::map<Channel::Type, std::list<std::weak_ptr<std::function<void(int64_t, int64_t)>>>> gvspPacketListeners;

    std::function<QOpenGLContext*(void)> contextGetter;
    std::function<std::shared_ptr<VtoConfig>(void)> vtoConfigGetter;
};

} // namespace Jgv::GlobalIPC

#endif // GLOBALIPC_P_H
