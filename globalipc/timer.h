/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef JGVTIMER_H
#define JGVTIMER_H

#include <functional>
#include <memory>

namespace Jgv {

struct TimerPrivate;

class Timer {
public:
    Timer();
    ~Timer();
    Timer(const Timer&) = delete;
    Timer(Timer&&) = delete;
    auto operator=(const Timer&) -> Timer& = delete;
    auto operator=(Timer &&) -> Timer& = delete;

    void setInterval(int millisecondes) noexcept;
    void setTask(std::function<void(void)> task) noexcept;
    void start() noexcept;
    void stop() noexcept;

private:
    const std::unique_ptr<TimerPrivate> _impl;
};

} // namespace Jgv

#endif // JGVTIMER_H
