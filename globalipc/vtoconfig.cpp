/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "vtoconfig.h"
#include "vtoconfig_p.h"

#include <iostream>

namespace Jgv::GlobalIPC {

VtoConfig::VtoConfig()
    : _impl(std::make_unique<VtoConfigPrivate>())
{
}

void VtoConfig::powerOff() noexcept
{
    if (const auto f { _impl->powerOffFunction.lock() }) {
        (*f)();
    }
}

void VtoConfig::setPowerOffFunction(std::weak_ptr<std::function<void(void)>> function) noexcept
{
    _impl->powerOffFunction = std::move(function);
}

VtoConfig::~VtoConfig() = default;

void VtoConfig::setCampagne(const Triplet<std::string_view>& campagne) noexcept
{
    if (auto setter { _impl->campagne.setter.lock() }) {
        (*setter)(campagne);
    }
}

void VtoConfig::setCampagneSetter(std::weak_ptr<std::function<void(const Triplet<std::string_view>&)>> setter) noexcept
{
    _impl->campagne.setter.swap(setter);
}

auto VtoConfig::campagne() const noexcept -> Triplet<std::string>
{
    if (const auto getter { _impl->campagne.getter.lock() }) {
        return (*getter)();
    }
    return {};
}

void VtoConfig::setCampagneGetter(std::weak_ptr<std::function<Triplet<std::string>()>> getter) noexcept
{
    _impl->campagne.getter.swap(getter);
}

auto VtoConfig::volumesUUID() const noexcept -> std::vector<std::string>
{
    if (const auto getter { _impl->uuidsGetter.lock() }) {
        return (*getter)();
    }
    return {};
}

void VtoConfig::setVolumesUUIDGetter(std::weak_ptr<std::function<std::vector<std::string>(void)>> getter) noexcept
{
    _impl->uuidsGetter.swap(getter);
}

auto VtoConfig::volumeInfos(std::string_view volumeUUID) const noexcept -> std::tuple<std::string, std::string, std::string>
{
    if (const auto f { _impl->volumesInfosGetter.lock() }) {
        return (*f)(volumeUUID);
    }
    return {};
}

void VtoConfig::setVolumeInfosGetter(std::weak_ptr<std::function<std::tuple<std::string, std::string, std::string>(std::string_view)>> getter)
{
    _impl->volumesInfosGetter = std::move(getter);
}

auto VtoConfig::pathGet() const noexcept -> std::string
{
    if (auto path { _impl->pathGetter.lock() }) {
        return (*path)();
    }
    return {};
}

void VtoConfig::setPathGetter(std::weak_ptr<std::function<std::string()>> getter) noexcept
{
    _impl->pathGetter.swap(getter);
}

void VtoConfig::output(OutputLevel level, std::string_view text) noexcept
{
    for (const auto& listener : _impl->outputListeners) {
        if (auto f { listener.lock() }) {
            (*f)(level, text);
        }
    }
}

void VtoConfig::addOutputListener(std::weak_ptr<std::function<void(OutputLevel, std::string_view)>> listener) noexcept
{
    _impl->outputListeners.emplace_back(std::move(listener));
}

auto VtoConfig::campagneReady() const noexcept -> bool
{
    if (const auto ready { _impl->readyGetter.lock() }) {
        return (*ready)();
    }
    return false;
}

void VtoConfig::setCampagneReadyGetter(std::weak_ptr<std::function<bool()>> getter) noexcept
{
    _impl->readyGetter.swap(getter);
}

void VtoConfig::toggleRecord(Channel::Type channel) noexcept
{
    if (auto command { _impl->record[channel].command.lock() }) {
        (*command)();
    }
}

void VtoConfig::setRecordToggler(Channel::Type channel, std::weak_ptr<std::function<void(void)>> toggler) noexcept
{
    _impl->record[channel].command.swap(toggler);
}

void VtoConfig::emitRecordChanged(Channel::Type channel, bool recording) noexcept
{
    // purge la liste
    _impl->record[channel].listeners.remove_if([](auto& value) {
        return value.expired();
    });

    for (auto& listener : _impl->record[channel].listeners) {
        if (auto f { listener.lock() }) {
            (*f)(recording);
        }
    }
}

void VtoConfig::addRecordChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(bool)>> listener) noexcept
{
    _impl->record[channel].listeners.emplace_back(std::move(listener));
}

auto VtoConfig::record(Channel::Type channel) const noexcept -> bool
{
    if (auto getter { _impl->record[channel].getter.lock() }) {
        return (*getter)();
    }
    return false;
}

void VtoConfig::setRecordGetter(Channel::Type channel, std::weak_ptr<std::function<bool(void)>> getter) noexcept
{
    _impl->record[channel].getter.swap(getter);
}

void VtoConfig::snapshot(Channel::Type channel, uint count, std::string_view directory) noexcept
{
    std::cout << "snapshot " << count << " in " << directory << std::endl;
    if (const auto command { _impl->snapshotCommands[channel].lock() }) {
        (*command)(count, directory);
    }
}

void VtoConfig::setSnapshotCommand(Channel::Type channel, std::weak_ptr<std::function<void(uint, std::string_view)>> command) noexcept
{
    _impl->snapshotCommands[channel].swap(command);
}

auto VtoConfig::genicamInterface(Channel::Type channel) noexcept -> std::shared_ptr<Genicam2::Interface>
{
    if (const auto getter { _impl->genicamInterface[channel].lock() }) {
        return (*getter)();
    }
    return {};
}

void VtoConfig::setGenicamInterfaceGetter(Channel::Type channel, std::weak_ptr<std::function<std::shared_ptr<Genicam2::Interface>()>> getter) noexcept
{
    _impl->genicamInterface[channel] = std::move(getter);
}

void VtoConfig::toggleEqualize(Channel::Type channel) noexcept
{
    if (auto command { _impl->equalize[channel].command.lock() }) {
        (*command)();
    }
}

void VtoConfig::setEqualizeToggler(Channel::Type channel, std::weak_ptr<std::function<void(void)>> function) noexcept
{
    _impl->equalize[channel].command.swap(function);
}

void VtoConfig::emitEqualizeChanged(Channel::Type channel, bool recording) noexcept
{
    // purge la liste
    _impl->equalize[channel].listeners.remove_if([](auto& value) {
        return value.expired();
    });

    for (auto& listener : _impl->equalize[channel].listeners) {
        if (auto f { listener.lock() }) {
            (*f)(recording);
        }
    }
}

void VtoConfig::addEqualizeChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(bool)>> listener) noexcept
{
    _impl->equalize[channel].listeners.emplace_back(std::move(listener));
}

auto VtoConfig::equalize(Channel::Type channel) const noexcept -> bool
{
    if (auto getter { _impl->equalize[channel].getter.lock() }) {
        return (*getter)();
    }
    return false;
}

void VtoConfig::setEqualizeGetter(Channel::Type channel, std::weak_ptr<std::function<bool(void)>> getter) noexcept
{
    _impl->equalize[channel].getter.swap(getter);
}

void VtoConfig::setEqualizeFactor(Channel::Type channel, float factor) noexcept
{
    if (auto command { _impl->equalizeFactor[channel].setter.lock() }) {
        (*command)(factor);
    }
}

void VtoConfig::setEqualizeFactorSetter(Channel::Type channel, std::weak_ptr<std::function<void(float)>> function) noexcept
{
    _impl->equalizeFactor[channel].setter.swap(function);
}

void VtoConfig::equalizeFactorChanged(Channel::Type channel, float factor) noexcept
{
    // purge la liste
    _impl->equalizeFactor[channel].listeners.remove_if([](auto& value) {
        return value.expired();
    });

    for (auto& listener : _impl->equalizeFactor[channel].listeners) {
        if (auto f { listener.lock() }) {
            (*f)(factor);
        }
    }
}

void VtoConfig::addEqualizeFactorChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(float)>> listener) noexcept
{
    _impl->equalizeFactor[channel].listeners.emplace_back(std::move(listener));
}

auto VtoConfig::equalizeFactor(Channel::Type channel) const noexcept -> float
{
    if (auto getter { _impl->equalizeFactor[channel].getter.lock() }) {
        return (*getter)();
    }
    return 0.F;
}

void VtoConfig::setEqualizeFactorGetter(Channel::Type channel, std::weak_ptr<std::function<float()>> getter) noexcept
{
    _impl->equalizeFactor[channel].getter.swap(getter);
}

void VtoConfig::setReticleOffsets(Channel::Type channel, const Pair<int32_t>& center) noexcept
{
    if (const auto setter { _impl->reticleOffsetAccess[channel].setter.lock() }) {
        (*setter)(center);
    }
}

void VtoConfig::setReticleOffsetsSetter(Channel::Type channel, std::weak_ptr<std::function<void(Pair<int32_t>)>> setter) noexcept
{
    _impl->reticleOffsetAccess[channel].setter.swap(setter);
}

auto VtoConfig::reticleOffsets(Channel::Type channel) const noexcept -> Pair<int32_t>
{
    if (const auto getter { _impl->reticleOffsetAccess[channel].getter.lock() }) {
        return (*getter)();
    }
    return {};
}

void VtoConfig::setReticleOffsetsGetter(Channel::Type channel, std::weak_ptr<std::function<Pair<int32_t>()>> getter) noexcept
{
    _impl->reticleOffsetAccess[channel].getter.swap(getter);
}

void VtoConfig::saveReticle(Channel::Type channel) noexcept
{
    if (const auto f { _impl->reticleSavers[channel].lock() }) {
        (*f)();
    }
}

void VtoConfig::setReticleSaver(Channel::Type channel, std::weak_ptr<std::function<void(void)>> saver) noexcept
{
    _impl->reticleSavers[channel] = std::move(saver);
}

auto VtoConfig::vtoXmp() const noexcept -> std::shared_ptr<Gvsp::QtBackend::RecorderMetas>
{
    if (const auto getter { _impl->vtoXmp.getter.lock() }) {
        return (*getter)();
    }
    return {};
}

void VtoConfig::setVtoXmpGetter(std::weak_ptr<std::function<std::shared_ptr<Gvsp::QtBackend::RecorderMetas>()>> getter) noexcept
{
    _impl->vtoXmp.getter.swap(getter);
}

void VtoConfig::setVtoXmp(std::shared_ptr<Gvsp::QtBackend::RecorderMetas> vtoXmp) const noexcept
{

    auto needPurge { false };
    auto tmp { std::move(vtoXmp) };
    for (auto& setter : _impl->vtoXmp.setters) {
        if (auto set { setter.lock() }) {
            (*set)(tmp);
        }
    }
    if (needPurge) {
        _impl->vtoXmp.setters.remove_if([](auto& value) {
            return value.expired();
        });
    }
}

void VtoConfig::setVtoXmpSetter(std::weak_ptr<std::function<void(const std::shared_ptr<Gvsp::QtBackend::RecorderMetas>&)>> vtoXmp) noexcept
{
    _impl->vtoXmp.setters.emplace_back(std::move(vtoXmp));
}

void VtoConfig::emitExposureDelay(Channel::Type channel, uint64_t delay)
{
    for (const auto& listener : _impl->exposureDelayListeners[channel]) {
        if (auto p = listener.lock()) {
            (*p)(delay);
        }
    }
}

void VtoConfig::addExposureDelayListener(Channel::Type channel, std::weak_ptr<std::function<void(uint64_t)>> listener)
{
    _impl->exposureDelayListeners[channel].emplace_back(std::move(listener));
}

void VtoConfig::emitSourceClockState(ClockState state, std::string_view message) noexcept
{
    for (const auto& listener : _impl->sourceClockStateListeners) {
        if (auto p = listener.lock()) {
            (*p)(state, message);
        }
    }
}

void VtoConfig::addSourceClockStateListener(std::weak_ptr<std::function<void(ClockState, std::string_view)>> listener) noexcept
{
    _impl->sourceClockStateListeners.emplace_back(std::move(listener));
}

void VtoConfig::setSourceClockStateGetter(std::weak_ptr<std::function<std::tuple<ClockState, std::string>(void)>> getter) noexcept
{
    _impl->sourceClockGetter = std::move(getter);
}

auto VtoConfig::sourceClockState() const noexcept -> std::tuple<ClockState, std::string>
{
    if (auto getter { _impl->sourceClockGetter.lock() }) {
        return (*getter)();
    }
    return { ClockState::error, "-------" };
}

void VtoConfig::emitClockState(ClockState state, std::string_view message) noexcept
{
    for (const auto& listener : _impl->clockStateListeners) {
        if (auto p = listener.lock()) {
            (*p)(state, message);
        }
    }
}

void VtoConfig::addClockStateListener(std::weak_ptr<std::function<void(ClockState, std::string_view)>> listener) noexcept
{
    _impl->clockStateListeners.emplace_back(std::move(listener));
}

void VtoConfig::setClockStateGetter(std::weak_ptr<std::function<std::tuple<ClockState, std::string>()>> getter) noexcept
{
    _impl->clockGetter = std::move(getter);
}

auto VtoConfig::clockState() const noexcept -> std::tuple<ClockState, std::string>
{
    if (auto getter { _impl->clockGetter.lock() }) {
        return (*getter)();
    }
    return { ClockState::error, "-------" };
}

void VtoConfig::emitPtpCameraLocked(bool locked) noexcept
{
    for (const auto& listener : _impl->ptpCameraLockedListeners) {
        if (auto p = listener.lock()) {
            (*p)(locked);
        }
    }
}

void VtoConfig::addPtpCameraLockedListener(std::weak_ptr<std::function<void(bool)>> listener) noexcept
{
    _impl->ptpCameraLockedListeners.emplace_back(std::move(listener));
}

void VtoConfig::setPtpCameraLockedGetter(std::weak_ptr<std::function<bool()>> getter) noexcept
{
    _impl->ptpCameraLockedGetter = std::move(getter);
}

auto VtoConfig::ptpCameraLocked() const noexcept -> bool
{
    if (auto getter { _impl->ptpCameraLockedGetter.lock() }) {
        return (*getter)();
    }
    return false;
}

void VtoConfig::setBaliseCoordinatesGetter(std::weak_ptr<std::function<Pair<int>(void)>>&& getter) noexcept
{
    _impl->endat.baliseGetter = std::move(getter);
}

auto VtoConfig::baliseCoordinates() const noexcept -> Pair<int>
{
    if (const auto f { _impl->endat.baliseGetter.lock() }) {
        return (*f)();
    }
    return {};
}

void VtoConfig::setPositionsGetter(std::weak_ptr<std::function<Pair<int32_t>()>>&& getter) noexcept
{
    _impl->endat.positionGetter = std::move(getter);
}

auto VtoConfig::positions() const noexcept -> Pair<int32_t>
{
    if (const auto getter { _impl->endat.positionGetter.lock() }) {
        return (*getter)();
    }
    return {};
}

void VtoConfig::calibrateWidthPosition(int32_t site, int32_t gisement) noexcept
{
    if (const auto setter { _impl->endat.calibrationSetter.lock() }) {
        (*setter)(site, gisement);
    }
}

void VtoConfig::setCalibrateWidthPositionSetter(std::weak_ptr<std::function<void(int32_t, int32_t)>>&& setter) noexcept
{
    _impl->endat.calibrationSetter = std::move(setter);
}

auto VtoConfig::histogramDownloadRatio(Channel::Type channel) const noexcept -> float
{
    if (const auto f { _impl->effects[channel].downloadRatioGetter.lock() }) {
        return (*f)();
    }
    return 1.F;
}

void VtoConfig::setHistogramDownloadRatioGetter(Channel::Type channel, std::weak_ptr<std::function<float(void)>>&& getter) noexcept
{
    _impl->effects[channel].downloadRatioGetter = std::move(getter);
}

auto VtoConfig::histogramEqualizationEnabled(Channel::Type channel) const noexcept -> bool
{
    if (const auto f { _impl->effects[channel].equalizationEnabledGetter.lock() }) {
        return (*f)();
    }
    return false;
}

void VtoConfig::setHistogramEqualizationEnabledGetter(Channel::Type channel, std::weak_ptr<std::function<bool()>>&& getter) noexcept
{
    _impl->effects[channel].equalizationEnabledGetter = std::move(getter);
}

auto VtoConfig::crosshairEnabled(Channel::Type channel) const noexcept -> bool
{
    if (const auto f { _impl->effects[channel].crosshairEnabledGetter.lock() }) {
        return (*f)();
    }
    return false;
}

void VtoConfig::setCrosshairEnabledGetter(Channel::Type channel, std::weak_ptr<std::function<bool()>>&& getter) noexcept
{
    _impl->effects[channel].crosshairEnabledGetter = std::move(getter);
}

auto VtoConfig::recorderInEnabled(Channel::Type channel) const noexcept -> bool
{
    if (const auto f { _impl->effects[channel].recorderInEnabledGetter.lock() }) {
        return (*f)();
    }
    return false;
}

void VtoConfig::setRecorderInEnabledGetter(Channel::Type channel, std::weak_ptr<std::function<bool()>>&& getter) noexcept
{
    _impl->effects[channel].recorderInEnabledGetter = std::move(getter);
}

auto VtoConfig::recorderOutEnabled(Channel::Type channel) const noexcept -> bool
{
    if (const auto f { _impl->effects[channel].recorderOutEnabledGetter.lock() }) {
        return (*f)();
    }
    return false;
}

void VtoConfig::setRecorderOutEnabledGetter(Channel::Type channel, std::weak_ptr<std::function<bool()>>&& getter) noexcept
{
    _impl->effects[channel].recorderOutEnabledGetter = std::move(getter);
}

void VtoConfig::moveVideo(Channel::Type channel, Move direction) noexcept
{
    if (const auto f { _impl->videoInterfaces[channel].moveFunction.lock() }) {
        (*f)(direction);
    }
}

void VtoConfig::setMoveVideoFunction(Channel::Type channel, std::weak_ptr<std::function<void(Move)>>&& function)
{
    _impl->videoInterfaces[channel].moveFunction = std::move(function);
}

void VtoConfig::changeExposureEvent(Channel::Type channel, bool more) noexcept
{
    for (const auto& listener : _impl->videoInterfaces[channel].changeExposureListeners) {
        if (const auto f { listener.lock() }) {
            (*f)(more);
        }
    }
}

void VtoConfig::addChangeExposureListener(Channel::Type channel, std::weak_ptr<std::function<void(bool)>> listener) noexcept
{
    _impl->videoInterfaces[channel].changeExposureListeners.emplace_back(std::move(listener));
    _impl->videoInterfaces[channel].changeExposureListeners.remove_if([](const auto& value) {
        return value.expired();
    });
}

void VtoConfig::exposureValueEvent(Channel::Type channel, float exposure) noexcept
{
    for (const auto& listener : _impl->videoInterfaces[channel].exposureValueListeners) {
        if (const auto f { listener.lock() }) {
            (*f)(exposure);
        }
    }
}

void VtoConfig::addExposureValueListener(Channel::Type channel, std::weak_ptr<std::function<void(float)>> listener) noexcept
{
    _impl->videoInterfaces[channel].exposureValueListeners.emplace_back(std::move(listener));
    _impl->videoInterfaces[channel].exposureValueListeners.remove_if([](const auto& value) {
        return value.expired();
    });
}

void VtoConfig::changeGainEvent(Channel::Type channel, bool more) noexcept
{
    for (const auto& listener : _impl->videoInterfaces[channel].changeGainListeners) {
        if (const auto f { listener.lock() }) {
            (*f)(more);
        }
    }
}

void VtoConfig::addChangeGainListener(Channel::Type channel, std::weak_ptr<std::function<void(bool)>> listener) noexcept
{
    _impl->videoInterfaces[channel].changeGainListeners.emplace_back(std::move(listener));
    _impl->videoInterfaces[channel].changeGainListeners.remove_if([](const auto& value) {
        return value.expired();
    });
}

void VtoConfig::gainValueEvent(Channel::Type channel, float gain) noexcept
{
    for (const auto& listener : _impl->videoInterfaces[channel].gainValueListeners) {
        if (const auto f { listener.lock() }) {
            (*f)(gain);
        }
    }
}

void VtoConfig::addGainValueListener(Channel::Type channel, std::weak_ptr<std::function<void(float)>> listener) noexcept
{
    _impl->videoInterfaces[channel].gainValueListeners.emplace_back(std::move(listener));
    _impl->videoInterfaces[channel].gainValueListeners.remove_if([](const auto& value) {
        return value.expired();
    });
}

void VtoConfig::changeZoomEvent(Channel::Type channel, bool more) noexcept
{
    for (const auto& listener : _impl->videoInterfaces[channel].changeZoomListeners) {
        if (const auto f { listener.lock() }) {
            (*f)(more);
        }
    }
}

void VtoConfig::addChangeZoomListener(Channel::Type channel, std::weak_ptr<std::function<void(bool)>> listener) noexcept
{
    _impl->videoInterfaces[channel].changeZoomListeners.emplace_back(std::move(listener));
    _impl->videoInterfaces[channel].changeZoomListeners.remove_if([](const auto& value) {
        return value.expired();
    });
}

void VtoConfig::zoomFactorEvent(Channel::Type channel, float factor) noexcept
{
    for (const auto& listener : _impl->videoInterfaces[channel].zoomFactorListeners) {
        if (const auto f { listener.lock() }) {
            (*f)(factor);
        }
    }
}

void VtoConfig::addZoomFactorListener(Channel::Type channel, std::weak_ptr<std::function<void(float)>> listener) noexcept
{
    _impl->videoInterfaces[channel].zoomFactorListeners.emplace_back(std::move(listener));
    _impl->videoInterfaces[channel].zoomFactorListeners.remove_if([](const auto& value) {
        return value.expired();
    });
}

void VtoConfig::loadVideoMode(Channel::Type channel, VideoMode mode) noexcept
{
    for (const auto& listener : _impl->videoModeInterfaces[channel].loadFunctions) {
        if (const auto f { listener.lock() }) {
            (*f)(mode);
        }
    }
}

void VtoConfig::addLoadVideoModeFunction(Channel::Type channel, std::weak_ptr<std::function<void(VideoMode)>>&& function)
{
    _impl->videoModeInterfaces[channel].loadFunctions.emplace_back(function);
    _impl->videoModeInterfaces[channel].loadFunctions.remove_if([](const auto& value) {
        return value.expired();
    });
}

void VtoConfig::saveVideoMode(Channel::Type channel, VideoMode mode) noexcept
{
    for (const auto& listener : _impl->videoModeInterfaces[channel].saveFunctions) {
        if (const auto f { listener.lock() }) {
            (*f)(mode);
        }
    }
}

void VtoConfig::addSaveVideoModeFunction(Channel::Type channel, std::weak_ptr<std::function<void(VideoMode)>>&& function)
{
    _impl->videoModeInterfaces[channel].saveFunctions.emplace_back(function);
    _impl->videoModeInterfaces[channel].loadFunctions.remove_if([](const auto& value) {
        return value.expired();
    });
}

} // namespace Jgv::GlobalIPC
