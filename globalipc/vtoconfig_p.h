/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef VTOCONFIG_P_H
#define VTOCONFIG_P_H

#include "globalipc_p.h"

#include <functional>
#include <list>
#include <map>
#include <memory>
#include <tuple>

namespace Jgv::Gvsp::QtBackend {
class RecorderMetas;
} // namespace Jgv::Gvsp::QtBackend

namespace Jgv::GlobalIPC {

enum class ClockState;
enum class OutputLevel;
enum class Move;
enum class VideoMode;

template <typename T>
struct Pair;
template <typename T>
struct Triplet;

using StringTriplet = Triplet<std::string>;
using StringViewTriplet = Triplet<std::string_view>;
template <>
struct StructAcces<StringTriplet> {
    std::weak_ptr<std::function<void(const StringViewTriplet&)>> setter;
    std::weak_ptr<std::function<StringTriplet(void)>> getter;
    std::list<std::weak_ptr<std::function<void(const StringTriplet&)>>> listeners;
};

struct BoolAcces {
    std::weak_ptr<std::function<void(void)>> command;
    std::weak_ptr<std::function<bool(void)>> getter;
    std::list<std::weak_ptr<std::function<void(bool)>>> listeners;
};

struct VideoInterface {
    std::weak_ptr<std::function<void(Move)>> moveFunction;
    std::list<std::weak_ptr<std::function<void(bool)>>> changeExposureListeners;
    std::list<std::weak_ptr<std::function<void(float)>>> exposureValueListeners;
    std::list<std::weak_ptr<std::function<void(bool)>>> changeGainListeners;
    std::list<std::weak_ptr<std::function<void(float)>>> gainValueListeners;
    std::list<std::weak_ptr<std::function<void(bool)>>> changeZoomListeners;
    std::list<std::weak_ptr<std::function<void(float)>>> zoomFactorListeners;
    std::weak_ptr<std::function<void(void)>> restore;
    std::weak_ptr<std::function<void(void)>> save;
};

struct VideoModeInterfaces {
    std::list<std::weak_ptr<std::function<void(VideoMode)>>> loadFunctions;
    std::list<std::weak_ptr<std::function<void(VideoMode)>>> saveFunctions;
};

struct EndatInterfaces {
    std::weak_ptr<std::function<Pair<int>(void)>> baliseGetter;
    std::weak_ptr<std::function<Pair<int32_t>(void)>> positionGetter;
    std::weak_ptr<std::function<void(int32_t, int32_t)>> calibrationSetter;
};

struct EffectInterfaces {
    std::weak_ptr<std::function<float(void)>> downloadRatioGetter;
    std::weak_ptr<std::function<bool(void)>> equalizationEnabledGetter;
    std::weak_ptr<std::function<bool(void)>> crosshairEnabledGetter;
    std::weak_ptr<std::function<bool(void)>> recorderInEnabledGetter;
    std::weak_ptr<std::function<bool(void)>> recorderOutEnabledGetter;
};

struct VtoConfigPrivate {
    StructAcces<Triplet<std::string>> campagne;
    std::weak_ptr<std::function<std::vector<std::string>(void)>> uuidsGetter;
    std::weak_ptr<std::function<std::tuple<std::string, std::string, std::string>(std::string_view)>> volumesInfosGetter;

    EndatInterfaces endat;

    std::weak_ptr<std::function<std::string(void)>> pathGetter;
    std::weak_ptr<std::function<bool(void)>> readyGetter;
    std::weak_ptr<std::function<void(void)>> powerOffFunction;

    std::list<std::weak_ptr<std::function<void(OutputLevel, std::string_view)>>> outputListeners;

    std::map<Channel::Type, BoolAcces> record;
    std::map<Channel::Type, std::weak_ptr<std::function<void(uint, std::string_view directory)>>> snapshotCommands;
    std::map<Channel::Type, BoolAcces> equalize;
    std::map<Channel::Type, TypeAcces<float>> equalizeFactor;
    std::map<Channel::Type, StructAcces<Pair<int32_t>>> reticleOffsetAccess;
    std::map<Channel::Type, std::weak_ptr<std::function<void(void)>>> reticleSavers;
    std::map<Channel::Type, std::list<std::weak_ptr<std::function<void(uint64_t)>>>> exposureDelayListeners;
    std::map<Channel::Type, VideoInterface> videoInterfaces;
    std::map<Channel::Type, VideoModeInterfaces> videoModeInterfaces;
    std::map<Channel::Type, EffectInterfaces> effects;
    std::map<Channel::Type, std::weak_ptr<std::function<std::shared_ptr<Genicam2::Interface>(void)>>> genicamInterface;

    std::list<std::weak_ptr<std::function<void(ClockState, std::string_view)>>> sourceClockStateListeners;
    std::weak_ptr<std::function<std::tuple<ClockState, std::string>(void)>> sourceClockGetter;
    std::list<std::weak_ptr<std::function<void(ClockState, std::string_view)>>> clockStateListeners;
    std::weak_ptr<std::function<std::tuple<ClockState, std::string>(void)>> clockGetter;
    std::list<std::weak_ptr<std::function<void(bool)>>> ptpCameraLockedListeners;
    std::weak_ptr<std::function<bool(void)>> ptpCameraLockedGetter;

    SharedObjectAccessor<Jgv::Gvsp::QtBackend::RecorderMetas> vtoXmp;
};

} // namespace Jgv::GlobalIPC

#endif // VTOCONFIG_P_H
