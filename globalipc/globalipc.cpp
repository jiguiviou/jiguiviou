/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "globalipc.h"
#include "globalipc_p.h"

#include <charconv>
#include <cmath>
#include <iostream>

namespace {
constexpr auto SHIFT_24 { 24U };
constexpr auto SHIFT_16 { 16U };
constexpr auto SHIFT_8 { 8U };
} // namespace

namespace Jgv {

auto Utils::ipFromString(std::string_view ip) noexcept -> uint32_t
{
    constexpr auto IP_FIELDS_COUNT { 4 };
    std::vector<std::string_view> fields {};
    std::string_view::size_type current { 0 };
    std::string_view::size_type previous { 0 };
    current = ip.find_first_of('.');
    while (current != std::string::npos) {
        fields.push_back(ip.substr(previous, current - previous));
        previous = current + 1;
        current = ip.find_first_of('.', previous);
    }
    fields.push_back(ip.substr(previous, current - previous));

    // on doit avoir 4 champs
    if (fields.size() != IP_FIELDS_COUNT) {
        std::cerr << "Jgv::ipFromString: failed, mal formatted ip " << ip << std::endl;
        return 0;
    }

    std::array<uint32_t, IP_FIELDS_COUNT> values {};

    std::transform(fields.cbegin(), fields.cend(), values.begin(),
        [](auto field) {
            uint32_t val { 0 };
            auto [p, ec] { std::from_chars(field.data(), field.data() + field.size(), val) };
            if (ec != std::errc()) {
                std::cerr << "Jgv::ipFromString: failed " << field << std::endl;
            }
            return val;
        });

    return (values[0] << SHIFT_24) | (values[1] << SHIFT_16) | (values[2] << SHIFT_8) | values[3];
}

auto Utils::ipToString(uint32_t ip) noexcept -> std::string
{
    // sous la forme a.b.c.d
    constexpr uint32_t MASK { 0xFF };
    constexpr std::size_t IP_MAX_SIZE { 4 * 3 + 3 };
    constexpr std::string_view SEPARATOR { "." };

    std::array<uint8_t, 4> fields {};
    for (auto& field : fields) {
        field = MASK & ip;
        ip >>= SHIFT_8;
    }

    std::array<char, 3> buffer {};
    std::string out;
    out.reserve(IP_MAX_SIZE);

    if (auto [p, ec] = std::to_chars(buffer.data(), buffer.data() + buffer.size(), fields[3]); ec == std::errc()) {
        out.append(buffer.data(), p - buffer.data());
    }
    out.append(SEPARATOR);
    if (auto [p, ec] = std::to_chars(buffer.data(), buffer.data() + buffer.size(), fields[2]); ec == std::errc()) {
        out.append(buffer.data(), p - buffer.data());
    }
    out.append(SEPARATOR);
    if (auto [p, ec] = std::to_chars(buffer.data(), buffer.data() + buffer.size(), fields[1]); ec == std::errc()) {
        out.append(buffer.data(), p - buffer.data());
    }
    out.append(SEPARATOR);
    if (auto [p, ec] = std::to_chars(buffer.data(), buffer.data() + buffer.size(), fields[0]); ec == std::errc()) {
        out.append(buffer.data(), p - buffer.data());
    }
    return out;
}

auto Utils::ipIsMusticast(uint32_t ip) noexcept -> bool
{
    return (ip & 0xF0000000U) == 0xE0000000U; // NOLINT(cppcoreguidelines-avoid-magic-numbers)
}

auto spanToString(std::span<const uint8_t> view) -> std::string
{
    const auto first { std::find(view.begin(), view.end(), 0) };
    const auto distance { std::distance(view.begin(), first) };
    std::string out;
    std::copy_n(view.begin(), distance, std::back_inserter(out));
    return out;
}

} // namespace Jgv

namespace Jgv::GlobalIPC {

void Utils::timestampToString(int64_t timestamp, std::span<char> string) noexcept
{
    // on résout la milliseconde
    const auto timestampMilli { std::div(timestamp, INT64_C(1000000)) };
    const int64_t ts { timestampMilli.rem > 500000LL ? timestampMilli.quot + 1 : timestampMilli.quot };
    // on traite les secondes puis les millisecondes
    const auto secondesMillisecondes { std::div(ts, INT64_C(1000)) };

    // les secondes
    const auto dayDiv { std::div(secondesMillisecondes.quot, INT64_C(24 * 60 * 60)) };
    const auto hourDiv { std::div(dayDiv.rem, INT64_C(60 * 60)) };
    const auto minDiv { std::div(hourDiv.rem, INT64_C(60)) };
    auto div { std::div(hourDiv.quot, INT64_C(10)) };
    string[0] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    string[1] = '0' + static_cast<char>(div.rem); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    div = std::div(minDiv.quot, INT64_C(10));
    string[3] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    string[4] = '0' + static_cast<char>(div.rem); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    div = std::div(minDiv.rem, INT64_C(10));
    string[6] = '0' + static_cast<char>(div.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    string[7] = '0' + static_cast<char>(div.rem); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    // les millisecondes
    auto mdiv { std::div(secondesMillisecondes.rem, INT64_C(100)) };
    string[9] = '0' + static_cast<char>(mdiv.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    mdiv = std::div(mdiv.rem, INT64_C(10));
    string[10] = '0' + static_cast<char>(mdiv.quot); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
    string[11] = '0' + static_cast<char>(mdiv.rem); // NOLINT(cppcoreguidelines-avoid-magic-numbers)
}

auto Utils::timestampToString(int64_t timestamp) noexcept -> std::string
{
    std::string tc { "00:00:00.000" };
    timestampToString(timestamp, tc);
    return tc;
}

Object::Object()
    : _impl(std::make_unique<ObjectPrivate>())
{
}

Object::~Object() = default;

void Object::setGvcpControllerIP(Channel::Type channel, uint32_t ip) noexcept
{
    _impl->networks[channel].gvcpControllerIP = ip;
}

auto Object::gvcpControllerIP(Channel::Type channel) const noexcept -> uint32_t
{
    return _impl->networks[channel].gvcpControllerIP;
}

void Object::setGvcpTransmitterIP(Channel::Type channel, uint32_t ip) noexcept
{
    _impl->networks[channel].gvcpTransmitterIP = ip;
}

auto Object::gvcpTransmitterIP(Channel::Type channel) const noexcept -> uint32_t
{
    return _impl->networks[channel].gvcpTransmitterIP;
}

void Object::setGvspReceiverIP(Channel::Type channel, uint32_t ip) noexcept
{
    _impl->networks[channel].gvspReceiverIP = ip;
}

auto Object::gvspReceiverIP(Channel::Type channel) const noexcept -> uint32_t
{
    return _impl->networks[channel].gvspReceiverIP;
}

void Object::setGvspTransmitterIP(Channel::Type channel, uint32_t ip) noexcept
{
    _impl->networks[channel].gvspTransmitterIP = ip;
}

auto Object::gvspTransmitterIP(Channel::Type channel) const noexcept -> uint32_t
{
    return _impl->networks[channel].gvspTransmitterIP;
}

void Object::setGvcpControllerPort(Channel::Type channel, uint16_t port) noexcept
{
    _impl->networks[channel].gvcpControllerPort = port;
}

auto Object::gvcpControllerPort(Channel::Type channel) const noexcept -> uint16_t
{
    return _impl->networks[channel].gvcpControllerPort;
}

void Object::setGvcpTransmitterPort(Channel::Type channel, uint16_t port) noexcept
{
    _impl->networks[channel].gvcpTransmitterPort = port;
}

auto Object::gvcpTransmitterPort(Channel::Type channel) const noexcept -> uint16_t
{
    return _impl->networks[channel].gvcpTransmitterPort;
}

void Object::setGvspReceiverPort(Channel::Type channel, uint16_t port) noexcept
{
    _impl->networks[channel].gvspReceiverPort = port;
}

auto Object::gvspReceiverPort(Channel::Type channel) const noexcept -> uint16_t
{
    return _impl->networks[channel].gvspReceiverPort;
}

void Object::setGvspTransmitterPort(Channel::Type channel, uint16_t port) noexcept
{
    _impl->networks[channel].gvspTransmitterPort = port;
}

auto Object::gvspTransmitterPort(Channel::Type channel) const noexcept -> uint16_t
{
    return _impl->networks[channel].gvspTransmitterPort;
}

auto Object::emetterName(Channel::Type channel) const -> std::string
{
    if (const auto getter { _impl->emetterNameGetters[channel].lock() }) {
        return (*getter)();
    }
    return {};
}

void Object::setEmetterNameGetter(Channel::Type channel, std::weak_ptr<std::function<std::string()>> getter) noexcept
{
    _impl->emetterNameGetters[channel].swap(getter);
}

void Object::emitPixelFormatChanged(Channel::Type channel, uint32_t width, uint32_t height, uint32_t pixelFormat) noexcept
{
    for (const auto& listener : _impl->pixelFormatListeners[channel]) {
        if (const auto p { listener.lock() }) {
            (*p)(width, height, pixelFormat);
        }
    }
}

void Object::addPixelFormatChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(uint32_t, uint32_t, uint32_t)>> listener) noexcept
{
    _impl->pixelFormatListeners[channel].emplace_back(std::move(listener));
}

void Object::emitGvspPacketChanged(Channel::Type channel, int64_t segmentSize, int64_t payload) noexcept
{
    for (const auto& listener : _impl->gvspPacketListeners[channel]) {
        if (const auto p { listener.lock() }) {
            (*p)(segmentSize, payload);
        }
    }
}

void Object::addGvspPacketChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(int64_t, int64_t)>> listener) noexcept
{
    _impl->gvspPacketListeners[channel].emplace_back(std::move(listener));
}

void Object::emitGvcpEvent(Channel::Type channel, uint16_t event, uint64_t timestamp) noexcept
{
    auto needPurge { false };
    for (auto& listener : _impl->gvcpEvents[channel][event]) {
        if (auto f { listener.lock() }) {
            (*f)(timestamp);
        } else {
            needPurge |= true;
        }
    }

    if (needPurge) {
        // purge la liste
        _impl->gvcpEvents[channel][event].remove_if([](auto& value) {
            return value.expired();
        });
    }
}

void Object::addGvcpEventListener(Channel::Type channel, uint16_t event, std::weak_ptr<std::function<void(uint64_t)>> listener) noexcept
{
    _impl->gvcpEvents[channel][event].emplace_back(std::move(listener));
}

auto Object::gvcpClient(Channel::Type channel) const noexcept -> std::shared_ptr<Gvcp::Client>
{
    if (const auto f { _impl->gvcpClients[channel].lock() }) {
        return (*f)();
    }
    return {};
}

void Object::setGvcpClientGetter(Channel::Type channel, std::weak_ptr<std::function<std::shared_ptr<Gvcp::Client>(void)>> client) noexcept
{
    _impl->gvcpClients[channel] = std::move(client);
}

//auto Object::genicamModel(Channel::Type channel) noexcept -> std::shared_ptr<GenICam::Model>
//{
//    const auto& getter { _impl->genicamModels[channel] };
//    if (!getter) {
//        std::clog << "GlobalIPC::Object: Genicam model does not exist for channel " << Channel::toString(channel) << std::endl;
//        return {};
//    }
//    return getter();
//}

//void Object::setGenicamModelGetter(Channel::Type channel, std::function<std::shared_ptr<GenICam::Model>()> getter) noexcept
//{
//    _impl->genicamModels[channel] = std::move(getter);
//}

//auto Object::genicamInterface(Channel::Type channel) noexcept -> std::shared_ptr<Genicam2::Interface>
//{
//    if (const auto getter { _impl->genicamInterface[channel].lock() }) {
//        return (*getter)();
//    }
//    return {};
//}

//void Object::setGenicamInterfaceGetter(Channel::Type channel, std::weak_ptr<std::function<std::shared_ptr<Genicam2::Interface>(void)>> getter) noexcept
//{
//    _impl->genicamInterface[channel] = std::move(getter);
//}

auto Object::sharedContext() noexcept -> QOpenGLContext*
{
    if (!_impl->contextGetter) {
        return nullptr;
    }
    return _impl->contextGetter();
}

void Object::setSharedContextGetter(std::function<QOpenGLContext*(void)> getter) noexcept
{
    _impl->contextGetter = std::move(getter);
}

void Object::textureChanged(Channel::Type channel, std::tuple<uint, uint32_t, uint32_t> twhInfos)
{
    // purge la liste
    _impl->texture[channel].listeners.remove_if([](auto& value) {
        return value.expired();
    });

    for (auto& listener : _impl->texture[channel].listeners) {
        if (auto f { listener.lock() }) {
            (*f)(twhInfos);
        }
    }
}

void Object::addTextureChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(std::tuple<uint, uint, uint>)>> listener)
{
    _impl->texture[channel].listeners.emplace_back(std::move(listener));
}

auto Object::texture(Channel::Type channel) const noexcept -> std::tuple<uint, uint32_t, uint32_t>
{
    if (auto getter { _impl->texture[channel].getter.lock() }) {
        return (*getter)();
    }
    return {};
}

void Object::setTextureGetter(Channel::Type channel, std::weak_ptr<std::function<std::tuple<uint, uint32_t, uint32_t>()>> getter) noexcept
{
    _impl->texture[channel].getter = std::move(getter);
}

auto Object::timestamp(Channel::Type channel, int64_t timestamp) const noexcept -> int64_t
{
    const auto& getter { _impl->timestampGetters[channel] };
    if (getter) {
        return getter(timestamp);
    }
    return timestamp;
}

void Object::setTimestampGetter(Channel::Type channel, std::function<int64_t(int64_t)> getter)
{
    _impl->timestampGetters[channel] = std::move(getter);
}

auto Object::vtoConfig() -> std::shared_ptr<VtoConfig>
{
    if (_impl->vtoConfigGetter) {
        return _impl->vtoConfigGetter();
    }
    return {};
}

void Object::setVtoConfigGetter(std::function<std::shared_ptr<VtoConfig>()> getter)
{
    _impl->vtoConfigGetter.swap(getter);
}

} // namespace Jgv::GlobalIPC
