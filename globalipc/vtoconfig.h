/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef VTOCONFIG_H
#define VTOCONFIG_H

#include <functional>
#include <memory>
#include <string>
#include <string_view>

namespace Jgv::Channel {
enum class Type;
} // namespace Jgv::Channel

namespace Jgv::Gvsp::QtBackend {
class RecorderMetas;
} // namespace Jgv::Gvsp::QtBackend

namespace Jgv::Endat {
class Server;
} // namespace Jgv::Endat

namespace Jgv::Genicam2 {
class Interface;
} // namespace Jgv::Genicam2

namespace Jgv::GlobalIPC {

template <typename T>
struct Pair {
    T first;
    T second;
};

template <typename T>
struct Triplet {
    T first;
    T second;
    T third;
};

enum class ClockState {
    error,
    inprogress,
    valid
};

enum class OutputLevel {
    Standard,
    Error,
    Title,
    Clear,
    Loop,
    End
};

enum class VideoMode {
    Main,
    Reticle
};

enum class Move {
    Left,
    Right,
    Up,
    Down
};

class VtoConfigPrivate;
class VtoConfig final {
public:
    VtoConfig();
    ~VtoConfig();
    VtoConfig(const VtoConfig&) = delete;
    VtoConfig(VtoConfig&&) = delete;
    auto operator=(const VtoConfig&) -> VtoConfig& = delete;
    auto operator=(VtoConfig &&) -> VtoConfig& = delete;

    // system
    void powerOff() noexcept;
    void setPowerOffFunction(std::weak_ptr<std::function<void(void)>> function) noexcept;

    // triplet uuid, nom, cri de la Campagne
    void setCampagne(const Triplet<std::string_view>& campagne) noexcept;
    void setCampagneSetter(std::weak_ptr<std::function<void(const Triplet<std::string_view>&)>> setter) noexcept;
    [[nodiscard]] auto campagne() const noexcept -> Triplet<std::string>;
    void setCampagneGetter(std::weak_ptr<std::function<Triplet<std::string>(void)>> getter) noexcept;

    // les uuids volume
    [[nodiscard]] auto volumesUUID() const noexcept -> std::vector<std::string>;
    void setVolumesUUIDGetter(std::weak_ptr<std::function<std::vector<std::string>(void)>> getter) noexcept;

    [[nodiscard]] auto volumeInfos(std::string_view volumeUUID) const noexcept -> std::tuple<std::string, std::string, std::string>;
    void setVolumeInfosGetter(std::weak_ptr<std::function<std::tuple<std::string, std::string, std::string>(std::string_view)>> getter);

    // le path du volume
    [[nodiscard]] auto pathGet() const noexcept -> std::string;
    void setPathGetter(std::weak_ptr<std::function<std::string(void)>> getter) noexcept;

    [[nodiscard]] auto campagneReady() const noexcept -> bool;
    void setCampagneReadyGetter(std::weak_ptr<std::function<bool()>> getter) noexcept;

    void output(OutputLevel level, std::string_view text) noexcept;
    void addOutputListener(std::weak_ptr<std::function<void(GlobalIPC::OutputLevel, std::string_view)>> listener) noexcept;

    // bool record
    void toggleRecord(Channel::Type channel) noexcept;
    void setRecordToggler(Channel::Type channel, std::weak_ptr<std::function<void(void)>> toggler) noexcept;
    void emitRecordChanged(Channel::Type channel, bool recording) noexcept;
    void addRecordChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(bool)>> listener) noexcept;
    [[nodiscard]] auto record(Channel::Type channel) const noexcept -> bool;
    void setRecordGetter(Channel::Type channel, std::weak_ptr<std::function<bool(void)>> getter) noexcept;

    // [uint, directory] snapshot
    void snapshot(Channel::Type channel, uint count, std::string_view directory) noexcept;
    void setSnapshotCommand(Channel::Type channel, std::weak_ptr<std::function<void(uint, std::string_view directory)>> command) noexcept;

    // genicam
    auto genicamInterface(Channel::Type channel) noexcept -> std::shared_ptr<Genicam2::Interface>;
    void setGenicamInterfaceGetter(Channel::Type channel, std::weak_ptr<std::function<std::shared_ptr<Genicam2::Interface>(void)>> getter) noexcept;

    // bool Equalize
    void toggleEqualize(Channel::Type channel) noexcept;
    void setEqualizeToggler(Channel::Type channel, std::weak_ptr<std::function<void(void)>> function) noexcept;
    void emitEqualizeChanged(Channel::Type channel, bool recording) noexcept;
    void addEqualizeChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(bool)>> listener) noexcept;
    [[nodiscard]] auto equalize(Channel::Type channel) const noexcept -> bool;
    void setEqualizeGetter(Channel::Type channel, std::weak_ptr<std::function<bool(void)>> getter) noexcept;

    // float EqualizeFactor
    void setEqualizeFactor(Channel::Type channel, float factor) noexcept;
    void setEqualizeFactorSetter(Channel::Type channel, std::weak_ptr<std::function<void(float)>> function) noexcept;
    void equalizeFactorChanged(Channel::Type channel, float factor) noexcept;
    void addEqualizeFactorChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(float)>> listener) noexcept;
    [[nodiscard]] auto equalizeFactor(Channel::Type channel) const noexcept -> float;
    void setEqualizeFactorGetter(Channel::Type channel, std::weak_ptr<std::function<float(void)>> getter) noexcept;

    // ReticleOffset
    void setReticleOffsets(Channel::Type channel, const Pair<int32_t>& center) noexcept;
    void setReticleOffsetsSetter(Channel::Type channel, std::weak_ptr<std::function<void(Pair<int32_t>)>> setter) noexcept;
    [[nodiscard]] auto reticleOffsets(Channel::Type channel) const noexcept -> Pair<int32_t>;
    void setReticleOffsetsGetter(Channel::Type channel, std::weak_ptr<std::function<Pair<int32_t>(void)>> getter) noexcept;
    void saveReticle(Channel::Type channel) noexcept;
    void setReticleSaver(Channel::Type channel, std::weak_ptr<std::function<void(void)>> saver) noexcept;

    // VtoXmp
    [[nodiscard]] auto vtoXmp() const noexcept -> std::shared_ptr<Jgv::Gvsp::QtBackend::RecorderMetas>;
    void setVtoXmpGetter(std::weak_ptr<std::function<std::shared_ptr<Jgv::Gvsp::QtBackend::RecorderMetas>(void)>> getter) noexcept;
    void setVtoXmp(std::shared_ptr<Jgv::Gvsp::QtBackend::RecorderMetas> vtoXmp) const noexcept;
    void setVtoXmpSetter(std::weak_ptr<std::function<void(const std::shared_ptr<Gvsp::QtBackend::RecorderMetas>&)>> vtoXmp) noexcept;

    // les évènements
    void emitExposureDelay(Channel::Type channel, uint64_t delay);
    void addExposureDelayListener(Channel::Type channel, std::weak_ptr<std::function<void(uint64_t)>> listener);

    void emitSourceClockState(ClockState state, std::string_view message) noexcept;
    void addSourceClockStateListener(std::weak_ptr<std::function<void(ClockState, std::string_view)>> listener) noexcept;
    void setSourceClockStateGetter(std::weak_ptr<std::function<std::tuple<ClockState, std::string>(void)>> getter) noexcept;
    [[nodiscard]] auto sourceClockState() const noexcept -> std::tuple<ClockState, std::string>;

    void emitClockState(ClockState state, std::string_view message) noexcept;
    void addClockStateListener(std::weak_ptr<std::function<void(ClockState, std::string_view)>> listener) noexcept;
    void setClockStateGetter(std::weak_ptr<std::function<std::tuple<ClockState, std::string>(void)>> getter) noexcept;
    [[nodiscard]] auto clockState() const noexcept -> std::tuple<ClockState, std::string>;

    void emitPtpCameraLocked(bool locked) noexcept;
    void addPtpCameraLockedListener(std::weak_ptr<std::function<void(bool)>> listener) noexcept;
    void setPtpCameraLockedGetter(std::weak_ptr<std::function<bool(void)>> getter) noexcept;
    [[nodiscard]] auto ptpCameraLocked() const noexcept -> bool;

    // Interface Endat
    void setBaliseCoordinatesGetter(std::weak_ptr<std::function<Pair<int>(void)>>&& getter) noexcept;
    [[nodiscard]] auto baliseCoordinates() const noexcept -> Pair<int>;
    void setPositionsGetter(std::weak_ptr<std::function<Pair<int32_t>(void)>>&& getter) noexcept;
    [[nodiscard]] auto positions() const noexcept -> Pair<int32_t>;
    void calibrateWidthPosition(int32_t site, int32_t gisement) noexcept;
    void setCalibrateWidthPositionSetter(std::weak_ptr<std::function<void(int32_t, int32_t)>>&& setter) noexcept;

    // Interface Config Video
    [[nodiscard]] auto histogramDownloadRatio(Channel::Type channel) const noexcept -> float;
    void setHistogramDownloadRatioGetter(Channel::Type channel, std::weak_ptr<std::function<float(void)>>&& getter) noexcept;
    [[nodiscard]] auto histogramEqualizationEnabled(Channel::Type channel) const noexcept -> bool;
    void setHistogramEqualizationEnabledGetter(Channel::Type channel, std::weak_ptr<std::function<bool(void)>>&& getter) noexcept;
    [[nodiscard]] auto crosshairEnabled(Channel::Type channel) const noexcept -> bool;
    void setCrosshairEnabledGetter(Channel::Type channel, std::weak_ptr<std::function<bool(void)>>&& getter) noexcept;
    [[nodiscard]] auto recorderInEnabled(Channel::Type channel) const noexcept -> bool;
    void setRecorderInEnabledGetter(Channel::Type channel, std::weak_ptr<std::function<bool(void)>>&& getter) noexcept;
    [[nodiscard]] auto recorderOutEnabled(Channel::Type channel) const noexcept -> bool;
    void setRecorderOutEnabledGetter(Channel::Type channel, std::weak_ptr<std::function<bool(void)>>&& getter) noexcept;

    // réglage vidéo
    void moveVideo(Channel::Type channel, Move direction) noexcept;
    void setMoveVideoFunction(Channel::Type channel, std::weak_ptr<std::function<void(Move)>>&& function);
    void changeExposureEvent(Channel::Type channel, bool more) noexcept;
    void addChangeExposureListener(Channel::Type channel, std::weak_ptr<std::function<void(bool)>> listener) noexcept;
    void exposureValueEvent(Channel::Type channel, float exposure) noexcept;
    void addExposureValueListener(Channel::Type channel, std::weak_ptr<std::function<void(float)>> listener) noexcept;
    void changeGainEvent(Channel::Type channel, bool more) noexcept;
    void addChangeGainListener(Channel::Type channel, std::weak_ptr<std::function<void(bool)>> listener) noexcept;
    void gainValueEvent(Channel::Type channel, float gain) noexcept;
    void addGainValueListener(Channel::Type channel, std::weak_ptr<std::function<void(float)>> listener) noexcept;
    void changeZoomEvent(Channel::Type channel, bool more) noexcept;
    void addChangeZoomListener(Channel::Type channel, std::weak_ptr<std::function<void(bool)>> listener) noexcept;
    void zoomFactorEvent(Channel::Type channel, float factor) noexcept;
    void addZoomFactorListener(Channel::Type channel, std::weak_ptr<std::function<void(float)>> listener) noexcept;

    // Changement mode vidéo
    void loadVideoMode(Channel::Type channel, VideoMode mode) noexcept;
    void addLoadVideoModeFunction(Channel::Type channel, std::weak_ptr<std::function<void(VideoMode)>>&& function);
    void saveVideoMode(Channel::Type channel, VideoMode mode) noexcept;
    void addSaveVideoModeFunction(Channel::Type channel, std::weak_ptr<std::function<void(VideoMode mode)>>&& function);

    [[nodiscard]] auto zoomFactor(Channel::Type channel) const noexcept -> float;
    void setZoomFactorGetter(Channel::Type channel, std::weak_ptr<std::function<float(void)>>&& getter) noexcept;

    //    void restoreReticleMode(Channel::Type channel) noexcept;
    //    void setRestoreReticleModeFunction(Channel::Type channel, std::weak_ptr<std::function<void(void)>>&& function) noexcept;
    //    void saveReticleMode(Channel::Type channel) noexcept;
    //    void setSaveReticleModeFunction(Channel::Type channel, std::weak_ptr<std::function<void(void)>>&& function) noexcept;

private:
    const std::unique_ptr<VtoConfigPrivate> _impl;
};

} // namespace Jgv::GlobalIPC

#endif // VTOCONFIG_H
