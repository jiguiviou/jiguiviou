﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLOBALIPC_H
#define GLOBALIPC_H

#include <functional>
#include <iomanip>
#include <list>
#include <memory>
#include <span>
#include <sstream>
#include <tuple>

class QOpenGLContext;

namespace Jgv::Endat {
class Server;
} // namespace Jgv::Endat

namespace Jgv::Channel {

constexpr std::string_view UNKNOW { "Unknow" };

constexpr std::string_view PRIMARY { "Primary" };
constexpr std::string_view SECONDARY { "Secondary" };
constexpr std::string_view BULLES { "Bulles" };
constexpr std::string_view TEXT { "Text" };
enum class Type {
    Primary,
    Secondary,
    Bulles,
    Text
};
constexpr auto toString(Type type) -> std::string_view
{

    switch (type) {
    case Type::Primary:
        return PRIMARY;
    case Type::Secondary:
        return SECONDARY;
    case Type::Bulles:
        return BULLES;
    case Type::Text:
        return TEXT;
    default:
        return UNKNOW;
    }
}

} // namespace Jgv::Channel

namespace Jgv::Gvcp {
class Client;
} // namespace Jgv::Gvcp

namespace Jgv::Genicam2 {
class Interface;
} // namespace Jgv::Genicam2

namespace Jgv {
class Utils {
public:
    static auto ipFromString(std::string_view ip) noexcept -> uint32_t;
    static auto ipToString(uint32_t ip) noexcept -> std::string;
    static auto ipIsMusticast(uint32_t ip) noexcept -> bool;
};

template <typename T>
static auto toValue(std::string_view text) noexcept -> T
{
    T out { 0 };
    std::stringstream ss;
    ss << text;
    ss >> std::setbase(0) >> out;
    return out;
}

template <typename T>
static auto toString(T value) noexcept -> std::string
{
    std::ostringstream sstream;
    sstream << std::setbase(0) << std::fixed << std::setprecision(3) << value;
    return sstream.str();
}

template <typename T>
constexpr auto enumType_cast(T enumerator) noexcept -> typename std::underlying_type<T>::type
{
    return static_cast<typename std::underlying_type<T>::type>(enumerator);
}

auto spanToString(std::span<const uint8_t> view) -> std::string;

} // namespace Jgv

namespace Jgv::GlobalIPC {

class VtoConfig;

class Utils {
public:
    static void timestampToString(int64_t timestamp, std::span<char> string) noexcept;
    static auto timestampToString(int64_t timestamp) noexcept -> std::string;
};

struct ObjectPrivate;
class Object {
public:
    Object();
    ~Object();
    Object(const Object&) = delete;
    Object(Object&&) = delete;
    auto operator=(const Object&) -> Object& = delete;
    auto operator=(Object &&) -> Object& = delete;

    void setGvcpControllerIP(Channel::Type channel, uint32_t ip) noexcept;
    [[nodiscard]] auto gvcpControllerIP(Channel::Type channel) const noexcept -> uint32_t;
    void setGvcpTransmitterIP(Channel::Type channel, uint32_t ip) noexcept;
    [[nodiscard]] auto gvcpTransmitterIP(Channel::Type channel) const noexcept -> uint32_t;
    void setGvspReceiverIP(Channel::Type channel, uint32_t ip) noexcept;
    [[nodiscard]] auto gvspReceiverIP(Channel::Type channel) const noexcept -> uint32_t;
    void setGvspTransmitterIP(Channel::Type channel, uint32_t ip) noexcept;
    [[nodiscard]] auto gvspTransmitterIP(Channel::Type channel) const noexcept -> uint32_t;

    void setGvcpControllerPort(Channel::Type channel, uint16_t port) noexcept;
    [[nodiscard]] auto gvcpControllerPort(Channel::Type channel) const noexcept -> uint16_t;
    void setGvcpTransmitterPort(Channel::Type channel, uint16_t port) noexcept;
    [[nodiscard]] auto gvcpTransmitterPort(Channel::Type channel) const noexcept -> uint16_t;
    void setGvspReceiverPort(Channel::Type channel, uint16_t port) noexcept;
    [[nodiscard]] auto gvspReceiverPort(Channel::Type channel) const noexcept -> uint16_t;
    void setGvspTransmitterPort(Channel::Type channel, uint16_t port) noexcept;
    [[nodiscard]] auto gvspTransmitterPort(Channel::Type channel) const noexcept -> uint16_t;

    [[nodiscard]] auto emetterName(Channel::Type channel) const -> std::string;
    void setEmetterNameGetter(Channel::Type channel, std::weak_ptr<std::function<std::string(void)>> getter) noexcept;

    void emitPixelFormatChanged(Channel::Type channel, uint32_t width, uint32_t height, uint32_t pixelFormat) noexcept;
    void addPixelFormatChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(uint32_t, uint32_t, uint32_t)>> listener) noexcept;

    void emitGvspPacketChanged(Channel::Type channel, int64_t segmentSize, int64_t payload) noexcept;
    void addGvspPacketChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(int64_t, int64_t)>> listener) noexcept;

    void emitGvcpEvent(Channel::Type channel, uint16_t event, uint64_t timestamp) noexcept;
    void addGvcpEventListener(Channel::Type channel, uint16_t event, std::weak_ptr<std::function<void(uint64_t)>> listener) noexcept;

    [[nodiscard]] auto gvcpClient(Channel::Type channel) const noexcept -> std::shared_ptr<Gvcp::Client>;
    void setGvcpClientGetter(Channel::Type channel, std::weak_ptr<std::function<std::shared_ptr<Gvcp::Client>(void)>> client) noexcept;

    //    auto genicamModel(Channel::Type channel) noexcept -> std::shared_ptr<Jgv::GenICam::Model>;
    //    void setGenicamModelGetter(Channel::Type channel, std::function<std::shared_ptr<Jgv::GenICam::Model>(void)> getter) noexcept;

    //    auto genicamInterface(Channel::Type channel) noexcept -> std::shared_ptr<Genicam2::Interface>;
    //    void setGenicamInterfaceGetter(Channel::Type channel, std::weak_ptr<std::function<std::shared_ptr<Genicam2::Interface>(void)>> getter) noexcept;

    auto sharedContext() noexcept -> QOpenGLContext*;
    void setSharedContextGetter(std::function<QOpenGLContext*(void)> getter) noexcept;

    void textureChanged(Channel::Type channel, std::tuple<uint, uint32_t, uint32_t> twhInfos);
    void addTextureChangedListener(Channel::Type channel, std::weak_ptr<std::function<void(std::tuple<uint, uint32_t, uint32_t>)>> listener);
    [[nodiscard]] auto texture(Channel::Type channel) const noexcept -> std::tuple<uint, uint32_t, uint32_t>;
    void setTextureGetter(Channel::Type channel, std::weak_ptr<std::function<std::tuple<uint, uint32_t, uint32_t>(void)>> getter) noexcept;

    [[nodiscard]] auto timestamp(Channel::Type channel, int64_t timestamp) const noexcept -> int64_t;
    void setTimestampGetter(Channel::Type channel, std::function<int64_t(int64_t)>);

    auto vtoConfig() -> std::shared_ptr<VtoConfig>;
    void setVtoConfigGetter(std::function<std::shared_ptr<VtoConfig>(void)> getter);

private:
    const std::unique_ptr<ObjectPrivate> _impl;
};

} // namespace Jgv::GlobalIPC

#endif // GLOBALIPC_H
