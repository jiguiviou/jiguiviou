/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMIPV4EDITOR_H
#define GENICAMIPV4EDITOR_H

#include "genicameditor.h"

class QLineEdit;

namespace Jiguiviou {

class Ipv4Editor : public Editor {
    QLineEdit* _lineEdit;

public:
    Ipv4Editor(std::string_view name, QWidget* parent = nullptr);
    virtual ~Ipv4Editor() override = default;

    void setData(Jgv::Genicam2::IInode* inode) override;
    auto value() -> QVariant override;
};

} // namespace Jiguiviou

#endif // GENICAMIPV4EDITOR_H
