/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "forceippage.h"

#include "discovererwrapper.h"
#include "jiguiviouutils.h"
#include "lineseparator.h"

#include <QCheckBox>
#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QNetworkAddressEntry>
#include <QPushButton>
#include <QRegExpValidator>
#include <globalipc/globalipc.h>
#include <gvcpdevices/gvcpdiscoverer.h>

namespace Jiguiviou {

ForceIPPage::ForceIPPage(std::shared_ptr<DiscovererWrapper> discoverer, std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc, QWidget* parent)
    : QWizardPage { parent }
    , _discoverer { std::move(discoverer) }
    , _globalIpc { std::move(globalIpc) }
{
    setTitle("FORCEIP");
    setSubTitle("FORCEIP permet la configuration à la volée de l'IP caméra (si nécessaire).");

    const QString ipRange { "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])" };
    const QRegExp ipRegex("^" + ipRange + "\\." + ipRange + "\\." + ipRange + "\\." + ipRange + "$");

    auto controllerIp { new QLabel };
    controllerIp->setObjectName("controllerIp");
    controllerIp->setAlignment(Qt::AlignCenter);
    controllerIp->setStyleSheet("background-color: white");

    auto transmeterInfos { new QLabel };
    transmeterInfos->setObjectName("transmeterInfos");
    transmeterInfos->setStyleSheet("background-color: white");

    auto useForceIP { new QCheckBox };
    useForceIP->setChecked(false);

    auto newIP { new QLineEdit };
    newIP->setAlignment(Qt::AlignCenter);
    newIP->setValidator(new QRegExpValidator(ipRegex, this)); // NOLINT(cppcoreguidelines-owning-memory)
    newIP->setEnabled(false);

    auto newMask { new QLineEdit };
    newMask->setAlignment(Qt::AlignCenter);
    newMask->setValidator(new QRegExpValidator(ipRegex, this)); // NOLINT(cppcoreguidelines-owning-memory)
    newMask->setEnabled(false);

    auto newGateway { new QLineEdit };
    newGateway->setAlignment(Qt::AlignCenter);
    newGateway->setValidator(new QRegExpValidator(ipRegex, this)); // NOLINT(cppcoreguidelines-owning-memory)
    newGateway->setEnabled(false);

    auto apply { new QPushButton("Appliquer") };

    auto formLayout { new QFormLayout };
    formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    formLayout->addRow("Contrôleur", controllerIp);
    formLayout->addRow(new LineSeparator); // NOLINT(cppcoreguidelines-owning-memory)
    formLayout->addRow("Transmetteur (caméra)", transmeterInfos);
    formLayout->addRow(new LineSeparator); // NOLINT(cppcoreguidelines-owning-memory)
    formLayout->addRow("Utiliser FORCEIP", useForceIP);
    formLayout->addRow("Nouvelle IP caméra", newIP);
    formLayout->addRow("Nouveau masque caméra", newMask);
    formLayout->addRow("Nouvelle passerelle caméra", newGateway);
    formLayout->addRow("Appliquer les changements", apply);

    setLayout(formLayout);

    connect(
        _discoverer.get(), &DiscovererWrapper::newDevice, this, [this, transmeterInfos](const Jgv::Gvcp::DeviceInfos& infos) {
            transmeterInfos->setText(DiscovererWrapper::bufferTohtml(infos));
            _ip = infos.ip;
            _netmask = infos.netmask;
            _mac = infos.mac;
        },
        Qt::QueuedConnection);

    connect(
        _discoverer.get(), &DiscovererWrapper::forceIpSuccess, this, [this, newIP]() {
            QHostAddress transmitterIp { QHostAddress(newIP->text()) };
            _globalIpc->setGvcpTransmitterIP(Jgv::Channel::Type::Primary, transmitterIp.toIPv4Address());
            _discoverer->discover(transmitterIp.toIPv4Address());
        },
        Qt::QueuedConnection);

    connect(apply, &QPushButton::clicked, [this, newIP, newMask, newGateway]() {
        const QHostAddress ip { newIP->text() };
        const QHostAddress netmask { newMask->text() };
        const QHostAddress gateway { newGateway->text() };
        _discoverer->forceIp(_mac, ip.toIPv4Address(), netmask.toIPv4Address(), gateway.toIPv4Address());
    });

    connect(useForceIP, &QCheckBox::toggled, [this, newIP, newMask, newGateway, apply](bool checked) {
        if (checked) {
            auto cameraNetwork { Network::addressEntryFromIp(_globalIpc->gvcpControllerIP(Jgv::Channel::Type::Primary)) };
            newIP->setText(cameraNetwork.broadcast().toString());
            newMask->setText(cameraNetwork.netmask().toString());
            newGateway->setText("0.0.0.0");
        } else {
            newIP->clear();
            newMask->clear();
            newGateway->clear();
        }
        newIP->setEnabled(checked);
        newMask->setEnabled(checked);
        newGateway->setEnabled(checked);
        apply->setEnabled(checked);
    });
}

ForceIPPage::~ForceIPPage() = default;

void ForceIPPage::initializePage()
{
    const auto transmitterAddress { _globalIpc->gvcpTransmitterIP(Jgv::Channel::Type::Primary) };
    _discoverer->discover(transmitterAddress);

    if (const auto controllerIp { findChild<QLabel*>("controllerIp") }; controllerIp != nullptr) {
        const auto controllerAddress { _globalIpc->gvcpControllerIP(Jgv::Channel::Type::Primary) };
        const auto entry { Network::addressEntryFromIp(controllerAddress) };
        controllerIp->setText(entry.ip().toString() + " / " + entry.netmask().toString());
    }
}

auto ForceIPPage::validatePage() -> bool
{
    const QHostAddress deviceIP { _ip };
    const auto controllerIP { _globalIpc->gvcpControllerIP(Jgv::Channel::Type::Primary) };
    const auto controllerAddress { Network::addressEntryFromIp(controllerIP) };
    return deviceIP.isInSubnet(controllerAddress.ip(), controllerAddress.prefixLength());
}

} // namespace Jiguiviou
