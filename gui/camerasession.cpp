/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "camerasession.h"
#include "camerasession_p.h"

#include "genicamtreeview.h"

#include <QFile>
#include <genicam2/genicaminterface.h>
#include <globalipc/globalipc.h>
#include <gvcpdevices/bootstrapregisters.h>
#include <gvcpdevices/gvcpclient.h>
#include <iostream>

#ifdef QTRECEIVER
#include <glvideowindow/glvideowindow.h>
#include <gvspqtreceiver/gvspqtreceiver.h>
#else
#include <gvspdevices/gvspdevices.h>
#endif

namespace Jiguiviou {

CameraSessionPrivate::CameraSessionPrivate(std::shared_ptr<Jgv::GlobalIPC::Object>&& ipc)
    : globalIpc(std::move(ipc))
{
}

CameraSession::CameraSession(std::shared_ptr<Jgv::GlobalIPC::Object>&& globalIpc)
    : _impl { std::make_unique<CameraSessionPrivate>(std::move(globalIpc)) }
{
}

CameraSession::~CameraSession() = default;

auto CameraSession::startControllerSession() -> std::tuple<QWidget*, QWidget*>
{
    auto Primary { Jgv::Channel::Type::Primary };

    auto gvcp { std::make_shared<Jgv::Gvcp::Client>(Primary, _impl->globalIpc) };
    if (!gvcp->controleDevice()) {
        std::clog << "CameraSession failed to control device" << std::endl;
        return {};
    }

    // hearthbeat
    QObject::connect(&_impl->hearthbeatTimer, &QTimer::timeout, [gvcp]() {
        gvcp->readRegister(Jgv::Gvcp::Bootstrap::Address::ControlChannelPrivilege);
    });
    const int timeout { 5000 };
    const uint32_t deviceTimeout { timeout * 120U / 100U };
    gvcp->writeRegister(Jgv::enumType_cast(Jgv::Gvcp::Bootstrap::Address::HeartbeatTimeout), deviceTimeout);
    _impl->hearthbeatTimer.start(timeout);

    auto gvcpGetter = [gvcp]() {
        return gvcp;
    };
    _impl->gvcpGetterFunction = std::make_shared<std::function<std::shared_ptr<Jgv::Gvcp::Client>(void)>>(gvcpGetter);
    _impl->globalIpc->setGvcpClientGetter(Primary, _impl->gvcpGetterFunction);

    _impl->genicam = std::make_shared<Jgv::Genicam2::Interface>(Jgv::Channel::Type::Primary, _impl->globalIpc);
    _impl->genicam->setXMLContent(gvcp->xmlFile());
#ifdef QTRECEIVER

    QSurfaceFormat fmt;
    fmt.setMajorVersion(4);
    fmt.setMinorVersion(2);
    fmt.setProfile(QSurfaceFormat::CoreProfile);
    _impl->context.setFormat(fmt);
    _impl->context.create();
    auto getContext = [this]() {
        return &_impl->context;
    };
    _impl->globalIpc->setSharedContextGetter(getContext);

    _impl->gvsp = std::make_shared<Jgv::Gvsp::QtBackend::Receiver>(Primary, _impl->globalIpc);
    auto widget { QWidget::createWindowContainer(new Jgv::GLVideoWindow(_impl->globalIpc)) }; // NOLINT(cppcoreguidelines-owning-memory)

#else
    _impl->gvsp = std::make_shared<Jgv::Gvsp::Receiver>(Primary, _impl->globalIpc);
    auto widget { new GvspWidget };
    _impl->gvsp->setAllocatorEventsListener(widget->events());
#endif
    _impl->gvsp->listenUnicast();

    const auto address { _impl->globalIpc->gvspReceiverIP(Primary) };
    const auto port { _impl->globalIpc->gvspReceiverPort(Primary) };
    gvcp->writeRegister(static_cast<uint32_t>(Jgv::Gvcp::Bootstrap::Address::StreamChannelDestinationAddress), address);
    gvcp->writeRegister(static_cast<uint32_t>(Jgv::Gvcp::Bootstrap::Address::StreamChannelPort), port);

    return std::make_tuple(widget, new GenICamTreeView(_impl->genicam, true)); // NOLINT(cppcoreguidelines-owning-memory)
}

auto CameraSession::startMonitorSession() -> QWidget*
{
    const auto Primary { Jgv::Channel::Type::Primary };

    auto gvcp { std::make_shared<Jgv::Gvcp::Client>(Primary, _impl->globalIpc) };
    if (!gvcp->monitorDevice()) {
        std::cerr << "CameraSession failed to control device" << std::endl;
        return nullptr;
    }
    auto gvcpGetter = [gvcp]() {
        return gvcp;
    };
    _impl->gvcpGetterFunction = std::make_shared<std::function<std::shared_ptr<Jgv::Gvcp::Client>(void)>>(gvcpGetter);
    _impl->globalIpc->setGvcpClientGetter(Primary, _impl->gvcpGetterFunction);

    auto genicam { std::make_shared<Jgv::Genicam2::Interface>(Jgv::Channel::Type::Primary, _impl->globalIpc) };
    genicam->setXMLContent(gvcp->xmlFile());

    return new GenICamTreeView(genicam, true);
}

auto CameraSession::bootstrapWidget() -> QWidget*
{
    auto genicam { std::make_shared<Jgv::Genicam2::Interface>(Jgv::Channel::Type::Primary, _impl->globalIpc) };

    static QFile file("://bootstrap.xml");
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        static auto kk { file.readAll() };
        genicam->setXMLContent(std::string_view { kk });
        file.close();
    }

    return new GenICamTreeView(genicam, false);
}

void CameraSession::startStream()
{
    if (_impl->genicam) {
        const auto Primary { Jgv::Channel::Type::Primary };

        uint32_t width { 0 };
        auto variant { _impl->genicam->getValue("Width") };
        if (auto pVal = std::get_if<int64_t>(&variant)) {
            width = static_cast<uint32_t>(*pVal);
        }

        uint32_t height { 0 };
        variant = _impl->genicam->getValue("Height");
        if (auto pVal = std::get_if<int64_t>(&variant)) {
            height = static_cast<uint32_t>(*pVal);
        }

        uint32_t pixelFormat { 0 };
        variant = _impl->genicam->getValue("PixelFormat");
        if (auto pVal = std::get_if<Jgv::Genicam2::EnumerationEntry>(&variant)) {
            const auto [value, name] { *pVal };
            pixelFormat = static_cast<uint32_t>(value);
        }

        _impl->globalIpc->emitPixelFormatChanged(Primary, width, height, pixelFormat);

        if (auto gvcp { _impl->globalIpc->gvcpClient(Primary) }) {
            const auto scps0 { gvcp->readRegister(Jgv::Gvcp::Bootstrap::Address::StreamChannelPacketSize) };

            // lecture de l'image size
            variant = _impl->genicam->getValue("PayloadSize");
            int64_t payloadSize { 0 };
            if (auto pVal = std::get_if<int64_t>(&variant)) {
                payloadSize = *pVal;
            }
            _impl->globalIpc->emitGvspPacketChanged(Jgv::Channel::Type::Primary, Jgv::Gvcp::Bootstrap::SCPSx::packetSize(scps0), payloadSize);

            _impl->genicam->setValue("AcquisitionStart", {});
        }
    }
}

void CameraSession::stopStream()
{
    _impl->genicam->setValue("AcquisitionStop", {});
}
} // namespace Jiguiviou
