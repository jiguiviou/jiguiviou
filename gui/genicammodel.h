/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMMODEL_H
#define GENICAMMODEL_H

#include <QAbstractItemModel>
#include <memory>

namespace Jgv::Genicam2 {
class Interface;
} // namespace Jgv::Genicam2

namespace Jiguiviou {

class GenicamModelPrivate;
class GenicamModel : public QAbstractItemModel {
    const std::shared_ptr<Jgv::Genicam2::Interface> _genicam;

public:
    explicit GenicamModel(std::shared_ptr<Jgv::Genicam2::Interface> genicam, QObject* parent = nullptr);
    ~GenicamModel() override;
    GenicamModel(const GenicamModel&) = delete;
    GenicamModel(GenicamModel&&) = delete;
    auto operator=(const GenicamModel&) -> GenicamModel& = delete;
    auto operator=(GenicamModel &&) -> GenicamModel& = delete;

    [[nodiscard]] auto index(int row, int column, const QModelIndex& parent) const -> QModelIndex override;
    [[nodiscard]] auto parent(const QModelIndex& child) const -> QModelIndex override;
    [[nodiscard]] auto rowCount(const QModelIndex& parent) const -> int override;
    [[nodiscard]] auto columnCount(const QModelIndex& parent) const -> int override;
    [[nodiscard]] auto data(const QModelIndex& index, int role) const -> QVariant override;
    [[nodiscard]] auto headerData(int section, Qt::Orientation orientation, int role) const -> QVariant override;
    [[nodiscard]] auto flags(const QModelIndex& index) const -> Qt::ItemFlags override;
    auto setData(const QModelIndex& index, const QVariant& value, int role) -> bool override;
};

} // namespace Jiguiviou

#endif // GENICAMMODEL_H
