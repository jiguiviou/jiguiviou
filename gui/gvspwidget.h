/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPWIDGET_H
#define GVSPWIDGET_H

#include <QWidget>
#include <gvspdevices/gvspreceiver.h>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Gvsp {
class Receiver;
} // namespace Jgv::Gvsp

namespace Jiguiviou {

class EventsWrapper : public QObject, public Jgv::Gvsp::AllocatorEvents {
    Q_OBJECT
public:
    void imageFormat(uint32_t width, uint32_t height, uint32_t format) override;
    void allocate(uint32_t number, std::size_t size) override;
signals:
    void imageFormatSignal(uint32_t width, uint32_t height, uint32_t format);
    void allocateSignal(uint32_t number, std::size_t size);
};

class GvspWidget : public QWidget {
    std::shared_ptr<EventsWrapper> _events;
    std::size_t _allocatedCummuled { 0 };

public:
    explicit GvspWidget(QWidget* parent = nullptr);

    auto events() -> std::weak_ptr<Jgv::Gvsp::AllocatorEvents>;
};

} // namespace Jiguiviou

#endif // GVSPWIDGET_H
