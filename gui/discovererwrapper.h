/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DISCOVERERWRAPPER_H
#define DISCOVERERWRAPPER_H

#include <QObject>
#include <memory>
#include <span>

namespace Jgv::Gvcp {
class Discoverer;
struct DeviceInfos;
} // namespace Jgv::Gvcp

namespace Jiguiviou {

class DiscovererWrapper : public QObject {

    Q_OBJECT

    const std::unique_ptr<Jgv::Gvcp::Discoverer> _discoverer;
    std::shared_ptr<std::function<void(const Jgv::Gvcp::DeviceInfos&)>> _addDeviceFunction;
    std::shared_ptr<std::function<void(void)>> _forceIpSuccessFunction;

public:
    DiscovererWrapper();
    ~DiscovererWrapper() override;
    void listen(uint32_t ip);
    void discover(uint32_t peer);
    void forceIp(uint64_t mac, uint32_t newIP, uint32_t newNetmask, uint32_t newGateway);
    void stop();

    static auto bufferTohtml(const Jgv::Gvcp::DeviceInfos& infos) noexcept -> QString;

signals:
    void newDevice(const Jgv::Gvcp::DeviceInfos& infos);
    void forceIpSuccess();
};

} // namespace Jiguiviou

#endif // DISCOVERERWRAPPER_H
