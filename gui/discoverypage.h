/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DISCOVERYPAGE_H
#define DISCOVERYPAGE_H

#include <QWizardPage>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jiguiviou {

class DiscovererWrapper;

class DiscoveryPage : public QWizardPage {
    const std::shared_ptr<DiscovererWrapper> _discoverer;
    const std::shared_ptr<Jgv::GlobalIPC::Object> _globalIpc;

public:
    explicit DiscoveryPage(std::shared_ptr<DiscovererWrapper> discoverer, std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc, QWidget* parent = nullptr);
    ~DiscoveryPage();
    DiscoveryPage(const DiscoveryPage&) = delete;
    DiscoveryPage(DiscoveryPage&&) = delete;
    auto operator=(const DiscoveryPage&) -> DiscoveryPage& = delete;
    auto operator=(DiscoveryPage &&) -> DiscoveryPage& = delete;

private:
    void initializePage() override;
    bool validatePage() override;
    void cleanupPage() override;
};

} // namespace Jiguiviou

#endif // DISCOVERYPAGE_H
