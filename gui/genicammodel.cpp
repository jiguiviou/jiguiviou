/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicammodel.h"

#include "jiguiviouutils.h"

#include <QColor>
#include <QFont>
#include <genicam2/genicaminterface.h>
#include <globalipc/globalipc.h>
#include <gvcpdevices/gvcpclient.h>
#include <iostream>

namespace {

template <typename T>
auto toQVariant(Jgv::Genicam2::Representation representation, T value) -> QVariant;
template <>
auto toQVariant<int64_t>(Jgv::Genicam2::Representation representation, int64_t value) -> QVariant
{
    if (representation == Jgv::Genicam2::Representation::MACAddress) {
        return Jiguiviou::Network::macToString(value);
    }
    if (representation == Jgv::Genicam2::Representation::IPV4Address) {
        return Jiguiviou::Network::ipToString(value);
    }
    return static_cast<qint64>(value);
}

} // namespace

namespace Jiguiviou {

GenicamModel::GenicamModel(std::shared_ptr<Jgv::Genicam2::Interface> genicam, QObject* parent)
    : QAbstractItemModel(parent)
    , _genicam { std::move(genicam) }
{
}

GenicamModel::~GenicamModel() = default;

auto GenicamModel::index(int row, int column, const QModelIndex& parent) const -> QModelIndex
{
    // teste l'index (row >= 0, column >= 0, row < rowCount(parent), column < columnCount(parent))
    if (!hasIndex(row, column, parent)) {
        return {};
    }

    // pas de parent, on renvoie root
    if (!parent.isValid()) {
        if (auto inode { _genicam->inode("Root") }; inode != nullptr) {
            if (auto child { inode->child(row) }; child != nullptr) {
                return createIndex(row, column, child);
            }
        }
    } else if (auto inode { static_cast<const Jgv::Genicam2::IInode*>(parent.internalPointer()) }; inode != nullptr) {
        if (auto child { inode->child(row) }; child != nullptr) {
            return createIndex(row, column, child);
        }
    }

    return {};
}

auto GenicamModel::parent(const QModelIndex& child) const -> QModelIndex
{
    if (!child.isValid()) {
        return {};
    }

    if (const auto inode { static_cast<Jgv::Genicam2::IInode*>(child.internalPointer()) }; inode != nullptr) {
        return createIndex(inode->parentRow(), 0, inode->parent());
    }

    return {};
}

auto GenicamModel::rowCount(const QModelIndex& parent) const -> int
{
    if (parent.isValid()) {
        if (auto inode { static_cast<Jgv::Genicam2::IInode*>(parent.internalPointer()) }; inode != nullptr) {
            return inode->rowCount();
        }
    } else if (auto inode { _genicam->inode("Root") }; inode != nullptr) {
        return inode->rowCount();
    }
    return 0;
}

auto GenicamModel::columnCount(const QModelIndex& parent) const -> int
{
    Q_UNUSED(parent)
    return 3;
}

auto GenicamModel::data(const QModelIndex& index, int role) const -> QVariant
{
    if (!index.isValid()) {
        return {};
    }

    auto inode { static_cast<Jgv::Genicam2::IInode*>(index.internalPointer()) };
    if (inode == nullptr) {
        return {};
    }

    switch (role) {

    case Qt::DisplayRole:
        if (index.column() == 0) {
            return inode->displayName().data();
        }
        if (index.column() == 1) {
            if (auto interface { inode->CategoryInterface() }; interface != nullptr) {
                return "";
            }
            if (!inode->isAvailable()) {
                return "--------";
            }
            if (auto interface { inode->IntegerInterface() }) {
                if (inode->representation() != Jgv::Genicam2::Representation::ByInterface) {
                    return toQVariant(inode->representation(), interface->value());
                }
                return static_cast<qint64>(interface->value());
            }
            if (auto interface { inode->EnumerationInterface() }) {
                return interface->currentEntry()->name().data();
            }
            if (auto interface { inode->StringInterface() }) {
                return interface->value().data();
            }
            if (auto interface { inode->FloatInterface() }) {
                return interface->value();
            }
            if (auto interface { inode->CommandInterface() }) {
                return "<Command>";
            }
            if (auto interface { inode->BooleanInterface() }) {
                return interface->value();
            }
            return "TODO";
        }
        if (index.column() == 2) {
            return inode->visibility().data();
        }
        break;

    case Qt::EditRole:
        return index;

    case Qt::FontRole:
        // première colonne avec enfant, en gras
        if (index.column() == 0 && inode->CategoryInterface() != nullptr) {
            QFont font;
            font.setBold(true);
            return font;
        }
        if (!inode->isWritable() || !inode->isAvailable()) {
            QFont font;
            font.setItalic(true);
            return font;
        }
        break;

    case Qt::ForegroundRole:
        // sans enfant (seule ICategory a des enfants)
        if (inode->CategoryInterface() == nullptr) {
            // première colonne bleu foncé
            if (index.column() == 0) {
                return QVariant(QColor(Qt::darkBlue));
            }
            // deuxième colonne en gris si pas modifiable, en rouge si verrouillé
            if (index.column() == 1) {
                //                if (!inode->isWritable()) {
                //                    return QColor(Qt::darkGray);
                //                }
                if (inode->isLocked()) {
                    return QColor(Qt::darkRed);
                }
            }
        } else if (index.column() == 0) {
            // première colonne category en noir
            return QColor(Qt::black);
        }

        break;
    }
    return {};
}

auto GenicamModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole) {
        return (section == 0) ? "Feature" : (section == 1) ? "Value" : "Visibility";
    }
    return QVariant();
}

auto GenicamModel::flags(const QModelIndex& index) const -> Qt::ItemFlags
{
    // si l'index est invalide, ou concerne une colonne non visible
    if (!index.isValid() || index.column() > 1) {
        return Qt::NoItemFlags;
    }

    if ((index.column() == 1)) {
        if (const auto inode { static_cast<Jgv::Genicam2::IInode*>(index.internalPointer()) }; inode != nullptr) {
            return inode->isWritable() ? Qt::ItemIsEnabled | Qt::ItemIsEditable : Qt::ItemIsEnabled;
        }
    }

    // si l'inode est writable et pas verrouillé, il devient éditable
    return Qt::NoItemFlags;
}

auto GenicamModel::setData(const QModelIndex& index, const QVariant& value, int role) -> bool
{

    if (role != Qt::EditRole) {
        return false;
    }

    auto inode { static_cast<Jgv::Genicam2::IInode*>(index.internalPointer()) };
    if (inode == nullptr) {
        return false;
    }

    if (index.column() == 1) {
        if (auto interface { inode->IntegerInterface() }; interface != nullptr) {
            interface->setValue(value.toLongLong());
            emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });
            return true;
        }
        if (auto interface { inode->EnumerationInterface() }; interface != nullptr) {
            interface->setValue(value.toLongLong());
            emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });
            return true;
        }
        if (auto interface { inode->StringInterface() }; interface != nullptr) {
            interface->setValue(value.toString().toStdString());
            emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });
            return true;
        }
        if (auto interface { inode->FloatInterface() }; interface != nullptr) {
            interface->setValue(value.toDouble());
            emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });
            return true;
        }
        if (auto interface { inode->CommandInterface() }; interface != nullptr) {
            interface->execute();
            emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });
            return true;
        }
        if (auto interface { inode->BooleanInterface() }; interface != nullptr) {
            interface->setValue(value.toBool());
            emit dataChanged(index, index, { Qt::DisplayRole, Qt::EditRole });
            return true;
        }
    }

    return false;
}

} // namespace Jiguiviou
