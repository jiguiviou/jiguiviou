/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMEDITOR_H
#define GENICAMEDITOR_H

#include <QFrame>

namespace Jgv::Genicam2 {
class IInode;
} // namespace Jgv::Genicam2

namespace Jiguiviou {

class Editor : public QFrame {
    Q_OBJECT
public:
    Editor(std::string_view name, QWidget* parent = nullptr);
    virtual ~Editor() = default;

    virtual void setData(Jgv::Genicam2::IInode* inode);
    virtual auto value() -> QVariant;

    static auto createEditor(Jgv::Genicam2::IInode* inode, QWidget* parent) -> Editor*;

signals:
    void commitAndClose();
    void close();
};

} // namespace Jiguiviou

#endif // GENICAMEDITOR_H
