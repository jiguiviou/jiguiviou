/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "networkselectionpage.h"
#include "lineseparator.h"

#include <QFormLayout>
#include <QNetworkInterface>
#include <QPushButton>
#include <QRadioButton>
#include <globalipc/globalipc.h>

namespace Jiguiviou {

NetworkSelectionPage::NetworkSelectionPage(std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc, QWidget* parent)
    : QWizardPage(parent)
    , _globalIpc { std::move(globalIpc) }
{
    setTitle("RÉSEAU");
    setSubTitle("Choisir ici sur quelle interface réseau jiguiviou communiquera avec la caméra GiGE Vision.");

    auto populate { new QPushButton("Rafraichir") };

    auto layout { new QFormLayout };
    layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    layout->addRow("Mettre à jour les interfaces", populate);
    layout->addRow(new LineSeparator); // NOLINT(cppcoreguidelines-owning-memory)

    setLayout(layout);

    connect(populate, &QPushButton::clicked, this, &NetworkSelectionPage::initializePage);
}

NetworkSelectionPage::~NetworkSelectionPage() = default;

void NetworkSelectionPage::initializePage()
{
    if (auto formLayout { qobject_cast<QFormLayout*>(layout()) }) {
        _globalIpc->setGvcpControllerIP(Jgv::Channel::Type::Primary, 0);
        auto radios { findChildren<QRadioButton*>() };
        for (auto radio : radios) {
            formLayout->removeRow(radio);
        }

        auto index { 0 };
        for (const auto& ni : QNetworkInterface::allInterfaces()) {
            for (const auto& ae : ni.addressEntries()) {
                if ((ae.ip().protocol() == QAbstractSocket::IPv4Protocol) && (ae.ip() != QHostAddress::LocalHost)) {
                    auto radio { new QRadioButton };
                    radio->setProperty("ip", ae.ip().toString());
                    radio->setText(ae.ip().toString() + " " + ni.name());

                    connect(radio, &QRadioButton::toggled, [this, radio](bool checked) {
                        if (checked) {
                            const QHostAddress address { radio->property("ip").toString() };
                            _globalIpc->setGvcpControllerIP(Jgv::Channel::Type::Primary, address.toIPv4Address());
                        }
                    });

                    if (index++ == 0) {
                        formLayout->addRow("Interfaces disponibles", radio);
                        radio->setChecked(true);
                    } else {
                        formLayout->addWidget(radio);
                    }
                }
            }
        }
    }
}

auto NetworkSelectionPage::validatePage() -> bool
{
    return _globalIpc->gvcpControllerIP(Jgv::Channel::Type::Primary) != 0;
}

} // namespace Jiguiviou
