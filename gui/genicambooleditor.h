/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMBOOLEDITOR_H
#define GENICAMBOOLEDITOR_H

#include "genicameditor.h"

class QCheckBox;

namespace Jiguiviou {

class BoolEditor : public Editor {
    QCheckBox* _checkbox;

public:
    BoolEditor(std::string_view name, QWidget* parent = nullptr);
    virtual ~BoolEditor() override = default;

    void setData(Jgv::Genicam2::IInode* inode) override;
    auto value() -> QVariant override;
};

} // namespace Jiguiviou

#endif // GENICAMBOOLEDITOR_H
