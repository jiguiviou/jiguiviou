/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamenumerationeditor.h"

#include <QComboBox>
#include <QLayout>
#include <genicam2/genicaminterface.h>

namespace Jiguiviou {

EnumerationEditor::EnumerationEditor(std::string_view name, QWidget* parent)
    : Editor { name, parent }
    , _combobox { new QComboBox }
{
    layout()->addWidget(_combobox);
}

void EnumerationEditor::setData(Jgv::Genicam2::IInode* inode)
{
    if (auto interface { inode->EnumerationInterface() }; interface != nullptr) {
        const auto entries { interface->entries() };
        for (const auto entry : entries) {
            _combobox->addItem(entry->name().data(), static_cast<qint64>(entry->index()));
        }
        const auto current { interface->currentEntry() };
        _combobox->setCurrentText(current->name().data());
    }
}

auto EnumerationEditor::value() -> QVariant
{
    return _combobox->currentData();
}

} // namespace Jiguiviou
