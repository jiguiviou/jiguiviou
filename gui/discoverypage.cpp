/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "discoverypage.h"

#include "discovererwrapper.h"
#include "jiguiviouutils.h"
#include "lineseparator.h"

#include <QCheckBox>
#include <QComboBox>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QNetworkAddressEntry>
#include <QPushButton>
#include <globalipc/globalipc.h>
#include <gvcpdevices/gvcpdiscoverer.h>

Q_DECLARE_METATYPE(Jgv::Gvcp::DeviceInfos)

namespace Jiguiviou {

DiscoveryPage::DiscoveryPage(std::shared_ptr<DiscovererWrapper> discoverer, std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc, QWidget* parent)
    : QWizardPage(parent)
    , _discoverer { std::move(discoverer) }
    , _globalIpc { std::move(globalIpc) }
{
    setTitle("DISCOVERY");
    setSubTitle("DISCOVERY permet la détection automatique de tous les périphériques GiGE respectant le chapitre <b>Device discovery</b>");

    auto controllerIp { new QLabel };
    controllerIp->setObjectName("controllerIp");
    controllerIp->setAlignment(Qt::AlignCenter);
    controllerIp->setStyleSheet("background-color: white");

    auto transmitterInfos { new QLabel };
    transmitterInfos->setObjectName("transmitterInfos");
    transmitterInfos->setStyleSheet("background-color: white");

    auto discover { new QPushButton("Start") };

    auto allNetworks { new QCheckBox("Tous les réseaux") };

    auto infos { new QLabel("Si tous les réseaux : \n # echo 2 > /proc/sys/net/ipv4/conf/all/rp_filter") };

    auto devicesList { new QComboBox };
    devicesList->setObjectName("devicesList");
    devicesList->setEnabled(false);

    auto dl { new QHBoxLayout };
    dl->addWidget(discover, 1);
    dl->addWidget(allNetworks);

    auto formLayout { new QFormLayout };
    formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    formLayout->addRow("Contrôleur", controllerIp);
    formLayout->addRow(new LineSeparator); // NOLINT(cppcoreguidelines-owning-memory)
    formLayout->addRow("Transmetteur (caméra)", transmitterInfos);
    formLayout->addRow(new LineSeparator); // NOLINT(cppcoreguidelines-owning-memory)
    formLayout->addRow(("Liste des découverts"), devicesList);
    formLayout->addRow(("Découvrir"), dl);
    formLayout->addWidget(infos);

    setLayout(formLayout);

    connect(
        _discoverer.get(), &DiscovererWrapper::newDevice, this, [devicesList, transmitterInfos](const Jgv::Gvcp::DeviceInfos& infos) {
            // on identifie par l'adresse mac pour éviter les doublons
            auto index { devicesList->findData(static_cast<quint64>(infos.mac), Qt::UserRole + 2) };
            if (index < 0) {
                devicesList->setEnabled(true);
                devicesList->addItem(QString::fromStdString(infos.model), DiscovererWrapper::bufferTohtml(infos));
                index = devicesList->count() - 1;
            } else {
                transmitterInfos->setText(DiscovererWrapper::bufferTohtml(infos));
            }
            devicesList->setItemData(index, infos.ip, Qt::UserRole + 1);
            devicesList->setItemData(index, static_cast<quint64>(infos.mac), Qt::UserRole + 2);
        },
        Qt::QueuedConnection);

    connect(discover, &QPushButton::clicked, [this, devicesList, allNetworks]() {
        devicesList->setEnabled(false);
        devicesList->clear();
        const auto IP { _globalIpc->gvcpControllerIP(Jgv::Channel::Type::Primary) };
        _discoverer->discover(allNetworks->isChecked() ? QHostAddress(QHostAddress::Broadcast).toIPv4Address() : Network::addressEntryFromIp(IP).broadcast().toIPv4Address());
    });

    connect(devicesList, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [transmitterInfos, devicesList](int index) {
        if (index >= 0) {
            transmitterInfos->setText(devicesList->itemData(index, Qt::UserRole).toString());
        }
    });
}

DiscoveryPage::~DiscoveryPage() = default;

void DiscoveryPage::initializePage()
{
    const auto controllerAddress { _globalIpc->gvcpControllerIP(Jgv::Channel::Type::Primary) };
    _discoverer->listen(controllerAddress);

    if (const auto controllerIp { findChild<QLabel*>("controllerIp") }; controllerIp != nullptr) {
        const auto entry { Network::addressEntryFromIp(controllerAddress) };
        controllerIp->setText(entry.ip().toString() + " / " + entry.netmask().toString());
    }

    if (const auto transmitterInfos { findChild<QLabel*>("transmitterInfos") }; transmitterInfos != nullptr) {
        transmitterInfos->setText(DiscovererWrapper::bufferTohtml({}));
    }
}

auto DiscoveryPage::validatePage() -> bool
{
    if (const auto devicesList { findChild<QComboBox*>("devicesList") }; devicesList != nullptr) {
        if (const auto index { devicesList->currentIndex() }; index >= 0) {
            uint32_t ip = devicesList->itemData(index, Qt::UserRole + 1).toUInt();
            _globalIpc->setGvcpTransmitterIP(Jgv::Channel::Type::Primary, ip);
            return true;
        }
    }
    return false;
}

void DiscoveryPage::cleanupPage()
{
    _discoverer->stop();

    if (const auto transmitterInfos { findChild<QLabel*>("transmitterInfos") }; transmitterInfos != nullptr) {
        transmitterInfos->setText("");
    }
    if (const auto devicesList { findChild<QComboBox*>("devicesList") }; devicesList != nullptr) {
        devicesList->clear();
        devicesList->setEnabled(false);
    }
    _globalIpc->setGvcpTransmitterIP(Jgv::Channel::Type::Primary, 0);
}

} // namespace Jiguiviou
