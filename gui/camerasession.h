/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CAMERASESSION_H
#define CAMERASESSION_H

#include <memory>

class QWidget;

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jiguiviou {

class CameraSessionPrivate;
class CameraSession final {

public:
    explicit CameraSession(std::shared_ptr<Jgv::GlobalIPC::Object>&& globalIpc);
    ~CameraSession();

    auto startControllerSession() -> std::tuple<QWidget*, QWidget*>;
    auto startMonitorSession() -> QWidget*;
    auto bootstrapWidget() -> QWidget*;

    void startStream();
    void stopStream();

private:
    const std::unique_ptr<CameraSessionPrivate> _impl;
};

} // namespace Jiguiviou

#endif // CAMERASESSION_H
