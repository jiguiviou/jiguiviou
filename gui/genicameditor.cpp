/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicameditor.h"

#include "genicambooleditor.h"
#include "genicamcommandeditor.h"
#include "genicamenumerationeditor.h"
#include "genicamfloateditor.h"
#include "genicamintegereditor.h"
#include "genicamipv4editor.h"
#include "genicamtexteditor.h"

#include <QFont>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QVariant>
#include <genicam2/genicaminterface.h>

namespace Jiguiviou {

Editor::Editor(std::string_view name, QWidget* parent)
    : QFrame(parent)
{
    setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
    setAutoFillBackground(true);
    setBackgroundRole(QPalette::AlternateBase);
    setAttribute(Qt::WA_NoMousePropagation);

    auto font { QFrame::font() };
    font.setBold(true);

    auto label { new QLabel(name.data()) };
    label->setAlignment(Qt::AlignCenter);
    label->setFont(font);

    auto abord { new QPushButton };
    abord->setFixedSize(label->sizeHint().height(), label->sizeHint().height());
    abord->setStyleSheet("background-color: red");

    auto accept { new QPushButton };
    accept->setFixedSize(label->sizeHint().height(), label->sizeHint().height());
    accept->setStyleSheet("background-color: green");
    accept->setDefault(true);

    auto layout { new QHBoxLayout };
    layout->addWidget(abord);
    layout->addWidget(label, 1);
    layout->addWidget(accept);

    auto mainLayout { new QVBoxLayout };
    mainLayout->addLayout(layout);

    setLayout(mainLayout);
    setToolTip("Entrée valide, Échap ignore");

    connect(abord, &QPushButton::clicked, this, &Editor::close);
    connect(accept, &QPushButton::clicked, this, &Editor::commitAndClose);
}

void Editor::setData(Jgv::Genicam2::IInode* inode)
{
    Q_UNUSED(inode)
}

auto Editor::value() -> QVariant
{
    return {};
}

auto Editor::createEditor(Jgv::Genicam2::IInode* inode, QWidget* parent) -> Editor*
{
    auto create = [](Jgv::Genicam2::IInode* inode, QWidget* parent) -> Editor* {
        if (const auto interface { inode->EnumerationInterface() }; interface != nullptr) {
            return new EnumerationEditor(inode->displayName(), parent);
        }
        if (const auto interface { inode->CommandInterface() }; interface != nullptr) {
            return new CommandEditor(inode->displayName(), parent);
        }
        if (const auto interface { inode->FloatInterface() }; interface != nullptr) {
            return new FloatEditor(inode->displayName(), parent);
        }
        if (const auto interface { inode->IntegerInterface() }; interface != nullptr) {
            if (inode->representation() == Jgv::Genicam2::Representation::IPV4Address) {
                return new Ipv4Editor(inode->displayName(), parent);
            }
            return new IntegerEditor(inode->displayName(), parent);
        }
        if (const auto interface { inode->BooleanInterface() }; interface != nullptr) {
            return new BoolEditor(inode->displayName(), parent);
        }
        if (const auto interface { inode->StringInterface() }; interface != nullptr) {
            return new TextEditor(inode->displayName(), parent);
        }
        return new Editor(inode->displayName(), parent); // NOLINT(cppcoreguidelines-owning-memory)
    };

    auto editor { create(inode, parent) };
    editor->setMinimumHeight(editor->sizeHint().height());
    editor->setFocusPolicy(Qt::StrongFocus);
    return editor;
}

//IntegerEditor* EditorFactory::createIntegerEditor(const QString& featureName, const QString& displayName, QWidget* parent)
//{
//    if (featureName == "GevCurrentIPAddress"
//        || featureName == "GevCurrentSubnetMask"
//        || featureName == "GevCurrentDefaultGateway"
//        || featureName == "GevPersistentIPAddress"
//        || featureName == "GevPersistentSubnetMask"
//        || featureName == "GevPersistentDefaultGateway"
//        || featureName == "GevPrimaryApplicationIPAddress"
//        || featureName == "GevMCDA"
//        || featureName == "GevSCDA") {
//        return new GenICam::Ipv4Editor(displayName, parent);
//    }
//    return new GenICam::Int64Editor(displayName, parent);
//}

//FloatEditor* EditorFactory::createDoubleEditor(const QString& featureName, const QString& displayName, QWidget* parent)
//{
//    Q_UNUSED(featureName)
//    return new GenICam::DoubleEditor(displayName, parent);
//}

//CommandEditor* EditorFactory::createCommandEditor(const QString& featureName, const QString& displayName, QWidget* parent)
//{
//    Q_UNUSED(featureName)
//    return new GenICam::ButtonEditor(displayName, parent);
//}

//EnumerationEditor* EditorFactory::createEnumerationEditor(const QString& featureName, const QString& displayName, QWidget* parent)
//{
//    Q_UNUSED(featureName)
//    return new GenICam::ComboboxEditor(displayName, parent);
//}

//BooleanEditor* EditorFactory::createBooleanEditor(const QString& featureName, const QString& displayName, QWidget* parent)
//{
//    Q_UNUSED(featureName)
//    return new GenICam::BoolEditor(displayName, parent);
//}

//StringEditor* EditorFactory::createStringEditor(const QString& featureName, const QString& displayName, QWidget* parent)
//{
//    Q_UNUSED(featureName)
//    return new GenICam::LineEditor(displayName, parent);
//}

} // namespace Jiguiviou
