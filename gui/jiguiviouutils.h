/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                             *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef JIGUIVIOUUTILS_H
#define JIGUIVIOUUTILS_H

#include <QNetworkInterface>
#include <QString>
#include <cstdlib>

namespace Jiguiviou {

namespace Network {

    [[nodiscard]] static auto macToString(int64_t mac)
    {
        const auto [a, remA] { std::div(mac, INT64_C(0x10000000000)) };
        const auto [b, remB] { std::div(remA, INT64_C(0x100000000)) };
        const auto [c, remC] { std::div(remB, INT64_C(0x1000000)) };
        const auto [d, remD] { std::div(remC, INT64_C(0x10000)) };
        const auto [e, remE] { std::div(remD, INT64_C(0x100)) };
        return QString("%0:%1:%2:%3:%4:%5")
            .arg(a, 2, 16, QLatin1Char('0'))
            .arg(b, 2, 16, QLatin1Char('0'))
            .arg(c, 2, 16, QLatin1Char('0'))
            .arg(d, 2, 16, QLatin1Char('0'))
            .arg(e, 2, 16, QLatin1Char('0'))
            .arg(remE, 2, 16, QLatin1Char('0'))
            .toUpper();
    }

    [[nodiscard]] static auto macToString(uint16_t highPart, uint32_t lowPart)
    {
        const auto [a, remA] { std::div(highPart, 0x100) };
        const auto [c, remC] { std::div(lowPart, 0x1000000) };
        const auto [d, remD] { std::div(remC, 0x10000) };
        const auto [e, remE] { std::div(remD, 0x100) };
        return QString("%0:%1:%2:%3:%4:%5")
            .arg(a, 2, 16, QLatin1Char('0'))
            .arg(remA, 2, 16, QLatin1Char('0'))
            .arg(c, 2, 16, QLatin1Char('0'))
            .arg(d, 2, 16, QLatin1Char('0'))
            .arg(e, 2, 16, QLatin1Char('0'))
            .arg(remE, 2, 16, QLatin1Char('0'))
            .toUpper();
    }

    [[nodiscard]] static auto ipToString(int64_t ip)
    {
        const auto [a, remA] { std::div(ip, INT64_C(0x1000000)) };
        const auto [b, remB] { std::div(remA, INT64_C(0x10000)) };
        const auto [c, remC] { std::div(remB, INT64_C(0x100)) };
        return QString("%0.%1.%2.%3").arg(a).arg(b).arg(c).arg(remC);
    }

    static auto addressEntryFromIp(uint32_t ipv4) -> QNetworkAddressEntry
    {
        const auto interfaces { QNetworkInterface::allInterfaces() };
        for (const auto& interface : interfaces) {
            const auto entries { interface.addressEntries() };
            const auto entry = std::find_if(entries.cbegin(), entries.cend(), [ipv4](const auto& entry) {
                return entry.ip().toIPv4Address() == ipv4;
            });
            if (entry != entries.cend()) {
                return *entry;
            }
        }

        return {};
    }

}

} // namespace Jiguiviou

#endif // JIGUIVIOUUTILS_H
