/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GENICAMFLOATEDITOR_H
#define GENICAMFLOATEDITOR_H

#include "genicameditor.h"

class QDoubleSpinBox;

namespace Jiguiviou {

class FloatEditor : public Editor {
    QDoubleSpinBox* _spinbox;

public:
    FloatEditor(std::string_view name, QWidget* parent = nullptr);
    virtual ~FloatEditor() override = default;

    void setData(Jgv::Genicam2::IInode* inode) override;
    auto value() -> QVariant override;
};

} // namespace Jiguiviou

#endif // GENICAMFLOATEDITOR_H
