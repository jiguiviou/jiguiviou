/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamdelegate.h"

#include "genicameditor.h"

#include <genicam2/genicaminterface.h>
#include <iostream>

namespace Jiguiviou {

GenicamDelegate::GenicamDelegate(QObject* parent)
    : QStyledItemDelegate(parent)
{
}

GenicamDelegate::~GenicamDelegate() = default;

auto GenicamDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& index) const -> QWidget*
{
    if (const auto data { index.data(Qt::EditRole) }; data.canConvert<QModelIndex>()) {
        if (const auto inode { static_cast<Jgv::Genicam2::IInode*>(data.toModelIndex().internalPointer()) }; inode != nullptr) {
            auto editor { Editor::createEditor(inode, parent) };
            connect(editor, &Editor::close, this, &GenicamDelegate::closeMeFromEditor);
            connect(editor, &Editor::commitAndClose, this, &GenicamDelegate::commitAndCloseMeFromEditor);
            return editor;
        }
    }
    return new QWidget(parent); // NOLINT(cppcoreguidelines-owning-memory)
}

void GenicamDelegate::setEditorData(QWidget* widget, const QModelIndex& index) const
{
    if (auto editor { qobject_cast<Editor*>(widget) }; editor != nullptr) {
        if (const auto data { index.data(Qt::EditRole) }; data.canConvert<QModelIndex>()) {
            if (const auto inode { static_cast<Jgv::Genicam2::IInode*>(data.toModelIndex().internalPointer()) }; inode != nullptr) {
                editor->setData(inode);
            }
        }
    }
}

void GenicamDelegate::setModelData(QWidget* widget, QAbstractItemModel* model, const QModelIndex& index) const
{
    if (auto editor { qobject_cast<Editor*>(widget) }; editor != nullptr) {
        model->setData(index, editor->value());
    }
}

void GenicamDelegate::closeMeFromEditor()
{
    emit closeEditor(qobject_cast<Editor*>(sender()));
}

void GenicamDelegate::commitAndCloseMeFromEditor()
{
    auto editor { qobject_cast<Editor*>(sender()) };
    emit commitData(editor);
    emit closeEditor(editor);
}

} // namespace Jiguiviou
