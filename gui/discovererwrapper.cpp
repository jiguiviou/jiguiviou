/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "discovererwrapper.h"

#include "htmltable.h"
#include "jiguiviouutils.h"

#include <QHostAddress>
#include <gvcpdevices/bootstrapregisters.h>
#include <gvcpdevices/gvcpdiscoverer.h>

namespace Jiguiviou {

DiscovererWrapper::DiscovererWrapper()
    : _discoverer { std::make_unique<Jgv::Gvcp::Discoverer>() }
{

    auto addDevice = [this](const Jgv::Gvcp::DeviceInfos& infos) {
        if (infos.deviceClass == static_cast<uint32_t>(Jgv::Gvcp::Bootstrap::DeviceClass::TRANSMITTER)) {
            emit newDevice(infos);
        }
    };
    _addDeviceFunction = std::make_shared<std::function<void(const Jgv::Gvcp::DeviceInfos& infos)>>(addDevice);
    _discoverer->addDeviceFoundedListener(_addDeviceFunction);

    auto forceIp = [this]() {
        emit forceIpSuccess();
    };
    _forceIpSuccessFunction = std::make_shared<std::function<void(void)>>(forceIp);
    _discoverer->addForceIpSuccesListener(_forceIpSuccessFunction);
}

void DiscovererWrapper::listen(uint32_t ip)
{
    _discoverer->listen(ip);
}

void DiscovererWrapper::discover(uint32_t peer)
{
    _discoverer->discover(peer);
}

void DiscovererWrapper::forceIp(uint64_t mac, uint32_t newIP, uint32_t newNetmask, uint32_t newGateway)
{
    _discoverer->forceIP(mac, newIP, newNetmask, newGateway);
}

void DiscovererWrapper::stop()
{
    _discoverer->stop();
}

auto DiscovererWrapper::bufferTohtml(const Jgv::Gvcp::DeviceInfos& infos) noexcept -> QString
{

    Jiguiviou::HtmlTable table;
    table.addRow("modèle", infos.model);
    table.addRow("fabricant", infos.manufacturer);
    table.addRow("version", infos.version);
    table.addRow("n° série", infos.serial);
    table.addRow("nom", infos.userName);
    table.addAddress("ip", infos.ip);
    table.addAddress("netmask", infos.netmask);
    table.addAddress("gateway", infos.gateway);
    table.addRow("mac", Network::macToString(infos.mac).toStdString());
    return std::move(table);
}

DiscovererWrapper::~DiscovererWrapper() = default;

} // namespace Jiguiviou
