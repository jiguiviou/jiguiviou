/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CAMERAVALIDATIONPAGE_H
#define CAMERAVALIDATIONPAGE_H

#include <QWizardPage>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Gvcp {
class Client;
} // namespace Jgv::Gvcp

namespace Jiguiviou {

class CameraValidationPagePrivate;
class CameraValidationPage : public QWizardPage {
    const std::shared_ptr<Jgv::GlobalIPC::Object> _globalIpc;
    std::unique_ptr<Jgv::Gvcp::Client> _gvcp;

public:
    explicit CameraValidationPage(std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc, QWidget* parent = nullptr);
    ~CameraValidationPage();
    CameraValidationPage(const CameraValidationPage&) = delete;
    CameraValidationPage(CameraValidationPage&&) = delete;
    auto operator=(const CameraValidationPage&) -> CameraValidationPage& = delete;
    auto operator=(CameraValidationPage &&) -> CameraValidationPage& = delete;

    auto wantControl() const -> bool;

private:
    void initializePage() override;
    bool validatePage() override;
    void cleanupPage() override;
};

} // namespace Jiguiviou

#endif // CAMERAVALIDATIONPAGE_H
