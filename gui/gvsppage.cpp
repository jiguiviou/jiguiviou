/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvsppage.h"

#include "lineseparator.h"

#include <QFormLayout>
#include <QLineEdit>
#include <QRadioButton>
#include <QRegExpValidator>
#include <globalipc/globalipc.h>

namespace Jiguiviou {

GvspPage::GvspPage(std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc, QWidget* parent)
    : QWizardPage(parent)
    , _globalIpc { std::move(globalIpc) }
{
    setTitle("GiGE Vision STREAM");
    setSubTitle("Le récepteur du flux vidéo peut-être local (sur cette machine) ou bien distant (une autre machine)");
    const QString ipRange { "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])" };
    const QRegExp ipRegex("^" + ipRange + "\\." + ipRange + "\\." + ipRange + "\\." + ipRange + "$");

    auto localRadio { new QRadioButton("Cette machine") };
    localRadio->setObjectName("localRadio");
    localRadio->setChecked(true);

    auto localeMulicastRadio { new QRadioButton("Cette machine, multicast") };
    localeMulicastRadio->setObjectName("localeMulicastRadio");

    auto multicastAddress { new QLineEdit };
    multicastAddress->setAlignment(Qt::AlignCenter);
    multicastAddress->setValidator(new QRegExpValidator(ipRegex, this)); // NOLINT(cppcoreguidelines-owning-memory)
    multicastAddress->setEnabled(false);

    auto distRadio { new QRadioButton("Machine distante") };
    distRadio->setEnabled(false);

    auto mainLayout { new QFormLayout };
    mainLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    mainLayout->addRow("Sélection du récepteur", localRadio);
    mainLayout->addWidget(localeMulicastRadio);
    mainLayout->addRow("Groupe multicast", multicastAddress);
    mainLayout->addWidget(distRadio);
    setLayout(mainLayout);

    connect(localeMulicastRadio, &QRadioButton::toggled, [multicastAddress](bool checked) {
        multicastAddress->setEnabled(checked);
    });
}

GvspPage::~GvspPage() = default;

auto GvspPage::validatePage() -> bool
{
    if (const auto localRadio { findChild<QRadioButton*>("localRadio") }; localRadio != nullptr) {
        if (localRadio->isChecked()) {
            _globalIpc->setGvspTransmitterIP(Jgv::Channel::Type::Primary, _globalIpc->gvcpTransmitterIP(Jgv::Channel::Type::Primary));
            _globalIpc->setGvspReceiverIP(Jgv::Channel::Type::Primary, _globalIpc->gvcpControllerIP(Jgv::Channel::Type::Primary));
            return true;
        }
    }
    return false;
}

} // namespace Jiguiviou
