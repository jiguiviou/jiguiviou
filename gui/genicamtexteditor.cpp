/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamtexteditor.h"

#include <QHostAddress>
#include <QLayout>
#include <QLineEdit>
#include <QRegExpValidator>
#include <genicam2/genicaminterface.h>

namespace Jiguiviou {

TextEditor::TextEditor(std::string_view name, QWidget* parent)
    : Editor { name, parent }
    , _lineEdit { new QLineEdit }
{
    layout()->addWidget(_lineEdit);
}

void TextEditor::setData(Jgv::Genicam2::IInode* inode)
{
    if (auto interface { inode->StringInterface() }; interface != nullptr) {
        _lineEdit->setText(interface->value().data());
        _lineEdit->setMaxLength(interface->maxLength());
    }
}

auto TextEditor::value() -> QVariant
{
    return _lineEdit->text();
}

} // namespace Jiguiviou
