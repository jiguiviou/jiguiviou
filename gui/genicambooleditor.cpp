/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicambooleditor.h"

#include <QCheckBox>
#include <QLayout>
#include <QVariant>
#include <genicam2/genicaminterface.h>

namespace Jiguiviou {

BoolEditor::BoolEditor(std::string_view name, QWidget* parent)
    : Editor { name, parent }
    , _checkbox { new QCheckBox { "Active" } }
{
    layout()->addWidget(_checkbox);
}

void BoolEditor::setData(Jgv::Genicam2::IInode* inode)
{
    if (auto interface { inode->BooleanInterface() }; interface != nullptr) {
        _checkbox->setChecked(interface->value());
    }
}

auto BoolEditor::value() -> QVariant
{
    return _checkbox->isChecked();
}

} // namespace Jiguiviou
