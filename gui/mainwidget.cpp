/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "mainwidget.h"

#include "camerasession.h"
#include "cameravalidationpage.h"
#include "discovererwrapper.h"
#include "discoverypage.h"
#include "forceippage.h"
#include "gvsppage.h"
#include "networkselectionpage.h"

#include <QAction>
#include <QDialog>
#include <QDialogButtonBox>
#include <QDockWidget>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QToolBar>
#include <QVBoxLayout>
#include <QWizard>
#include <globalipc/globalipc.h>

namespace Jiguiviou {

class WidgetDialog : public QDialog {
public:
    WidgetDialog(QWidget* widget, const QString& windowTitle, QWidget* parent)
        : QDialog(parent)
    {
        setWindowTitle(windowTitle);

        auto buttonBox { new QDialogButtonBox(QDialogButtonBox::Ok) };
        connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);

        auto layout { new QVBoxLayout };
        layout->addWidget(widget);
        layout->addWidget(buttonBox);

        setLayout(layout);
    }
};

MainWidget::MainWidget(QWidget* parent)
    : QMainWindow(parent)
{
    setWindowIcon(QIcon("://ressources/icons/jiguiviou.png"));

    auto leftDock { new QDockWidget("Genicam") };
    leftDock->setAllowedAreas(Qt::LeftDockWidgetArea);
    leftDock->setVisible(false);
    addDockWidget(Qt::LeftDockWidgetArea, leftDock);

    auto label { new QLabel };
    label->setAlignment(Qt::AlignCenter);
    label->setPixmap(QPixmap("://ressources/icons/jiguiviou.png"));

    setCentralWidget(label);

    auto toggleLeftDockAction { leftDock->toggleViewAction() };
    toggleLeftDockAction->setIcon(QIcon("://ressources/icons/configure-device.png"));
    toggleLeftDockAction->setEnabled(false);

    auto startSessionAction { new QAction(QIcon("://ressources/icons/wizard-add.png"), "Créer une session GiGE Vision", this) };

    auto stopSessionAction { new QAction(QIcon("://ressources/icons/remove-camera.png"), "Fermer la session courante", this) };
    stopSessionAction->setEnabled(false);

    auto start { new QAction(QIcon("://ressources/icons/run.png"), "Démarrer la diffusion", parent) };
    start->setEnabled(false);

    auto stop { new QAction(QIcon("://ressources/icons/stop.png"), "Stopper la diffusion", parent) };
    stop->setEnabled(false);

    auto bootstrapAction { new QAction("Éditer le bootstrap", this) };

    auto mainMenu { new QMenu("Jiguiviou") };
    mainMenu->addAction(startSessionAction);
    mainMenu->addAction(stopSessionAction);
    menuBar()->addMenu(mainMenu);

    auto sessionMenu { new QMenu("Session") };
    sessionMenu->setEnabled(false);
    sessionMenu->addAction(start);
    sessionMenu->addAction(stop);
    sessionMenu->addAction(bootstrapAction);
    menuBar()->addMenu(sessionMenu);

    auto toolbar { addToolBar("Jiguiviou") };
    toolbar->addAction(startSessionAction);
    toolbar->addAction(stopSessionAction);
    toolbar->addAction(toggleLeftDockAction);
    toolbar->addSeparator();
    toolbar->addAction(start);
    toolbar->addAction(stop);
    toolbar->setIconSize(toolbar->iconSize() * 2);

    connect(startSessionAction, &QAction::triggered, [this, startSessionAction, stopSessionAction, sessionMenu, leftDock, toggleLeftDockAction, start, stop]() {
        auto discoverer { std::make_shared<DiscovererWrapper>() };
        QWizard wizard;
        wizard.setWindowTitle("Configurer pour une caméra GiGE Vision");
        auto globalIpc { std::make_shared<Jgv::GlobalIPC::Object>() };
        wizard.addPage(new NetworkSelectionPage(globalIpc)); // NOLINT(cppcoreguidelines-owning-memory)
        wizard.addPage(new DiscoveryPage(discoverer, globalIpc)); // NOLINT(cppcoreguidelines-owning-memory)
        wizard.addPage(new ForceIPPage(discoverer, globalIpc)); // NOLINT(cppcoreguidelines-owning-memory)
        auto cameraValidationPage { new CameraValidationPage(globalIpc) };
        wizard.addPage(cameraValidationPage);
        wizard.addPage(new GvspPage(globalIpc)); // NOLINT(cppcoreguidelines-owning-memory)
        if (wizard.exec() == QDialog::Accepted) {
            auto control { cameraValidationPage->wantControl() };
            _session = std::make_unique<CameraSession>(std::move(globalIpc));
            if (control) {
                if (auto [central, dock] { _session->startControllerSession() }; central != nullptr && dock != nullptr) {
                    setCentralWidget(central);
                    leftDock->setWidget(dock);
                    leftDock->setVisible(true);
                    toggleLeftDockAction->setEnabled(true);
                }
            } else if (auto widget { _session->startMonitorSession() }; widget != nullptr) {
                setCentralWidget(widget);
            }
            startSessionAction->setEnabled(false);
            stopSessionAction->setEnabled(true);
            sessionMenu->setEnabled(true);
            start->setEnabled(true);
            stop->setEnabled(true);
        }
    });

    connect(stopSessionAction, &QAction::triggered, [this, startSessionAction, stopSessionAction, sessionMenu, toggleLeftDockAction, leftDock, start, stop]() {
        auto label { new QLabel };
        label->setAlignment(Qt::AlignCenter);
        label->setPixmap(QPixmap("://ressources/icons/jiguiviou.png"));
        setCentralWidget(label);
        _session.reset();
        startSessionAction->setEnabled(true);
        stopSessionAction->setEnabled(false);
        sessionMenu->setEnabled(false);
        toggleLeftDockAction->setEnabled(false);
        leftDock->setVisible(false);
        start->setEnabled(false);
        stop->setEnabled(false);
    });

    connect(bootstrapAction, &QAction::triggered, [this]() {
        if (_session) {
            auto dialog { new WidgetDialog { _session->bootstrapWidget(), "Bootstrap Editor", this } };
            dialog->setMinimumSize(dialog->sizeHint() * 2);
            dialog->setModal(true);
            dialog->show();
        }
    });

    connect(start, &QAction::triggered, [this]() {
        if (_session) {
            _session->startStream();
        }
    });

    connect(stop, &QAction::triggered, [this]() {
        if (_session) {
            _session->stopStream();
        }
    });
}

MainWidget::~MainWidget() = default;

} // namespace Jiguiviou
