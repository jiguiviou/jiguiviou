/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamintegereditor.h"

#include <QLayout>
#include <QLineEdit>
#include <QSpinBox>
#include <genicam2/genicaminterface.h>

#include <iostream>

namespace Jiguiviou {

class Int64SpinBox : public QAbstractSpinBox {
    int64_t _value { 0 };
    int64_t _min { 0 };
    int64_t _max { 100 };

public:
    Int64SpinBox()
    {
        lineEdit()->setInputMethodHints(Qt::ImhDigitsOnly);
        connect(lineEdit(), &QLineEdit::textEdited, [this](const auto& text) {
            auto pos { 0 };
            auto t { text };
            if (validate(t, pos) == QValidator::Acceptable) {
                _value = t.toLongLong();
            } else {
                lineEdit()->setText(QString::number(_value));
            }
        });
    }
    void setMin(int64_t min) noexcept { _min = min; }
    void setMax(int64_t max) noexcept { _max = max; }
    void setValue(int64_t value) noexcept
    {
        _value = value;
        lineEdit()->setText(QString::number(_value));
    }
    auto value() const noexcept -> int64_t { return _value; }
    void stepBy(int steps) override
    {
        auto newValue { _value + steps };
        if (newValue < _min) {
            newValue = _min;
        } else if (newValue > _max) {
            newValue = _max;
        }
        _value = newValue;
        lineEdit()->setText(QString::number(_value));
    }

private:
    auto validate(QString& input, int& pos) const -> QValidator::State override
    {
        Q_UNUSED(pos)
        bool ok;
        auto val { input.toLongLong(&ok) };
        if (!ok) {
            return QValidator::Invalid;
        }

        if (val < _min) {
            return QValidator::Intermediate;
        }
        if (val > _max) {
            return QValidator::Intermediate;
        }

        return QValidator::Acceptable;
    }

    auto stepEnabled() const -> QAbstractSpinBox::StepEnabled override
    {
        if (_value <= _min) {
            return StepUpEnabled;
        }
        if (_value >= _max) {
            return StepDownEnabled;
        }
        return StepUpEnabled | StepDownEnabled;
    }
};

IntegerEditor::IntegerEditor(std::string_view name, QWidget* parent)
    : Editor { name, parent }
    , _spinbox { new Int64SpinBox }
{
    layout()->addWidget(_spinbox);
}

void IntegerEditor::setData(Jgv::Genicam2::IInode* inode)
{
    if (auto interface { inode->IntegerInterface() }; interface != nullptr) {
        _spinbox->setMin(interface->min());
        _spinbox->setMax(interface->max());
        _spinbox->setValue(interface->value());
    }
}

auto IntegerEditor::value() -> QVariant
{
    return static_cast<qint64>(_spinbox->value());
}

} // namespace Jiguiviou
