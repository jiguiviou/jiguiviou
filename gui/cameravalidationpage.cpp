/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "cameravalidationpage.h"

#include "lineseparator.h"

#include <QFileDialog>
#include <QFormLayout>
#include <QHostAddress>
#include <QPushButton>
#include <QRadioButton>
#include <QTextEdit>
#include <globalipc/globalipc.h>
#include <gvcpdevices/bootstrapregisters.h>
//#include <gvcpdevices/discoveryhelper.h>
#include <gvcpdevices/gvcpclient.h>

namespace {

constexpr std::string_view SUCCESS_COLOR { "green" };
constexpr std::string_view FAILED_COLOR { "red" };

} // namespace

namespace Jiguiviou {

CameraValidationPage::CameraValidationPage(std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc, QWidget* parent)
    : QWizardPage { parent }
    , _globalIpc { std::move(globalIpc) }
{
    setTitle("Mode de connexion");
    setSubTitle("On peut se connecter en tant que contrôleur (primary application) ou en simple moniteur (secondary application)");

    auto primaryApp { new QRadioButton("Mode contrôleur") };
    primaryApp->setObjectName("primaryApp");
    primaryApp->setEnabled(false);

    auto secondaryApp { new QRadioButton("Mode moniteur") };
    secondaryApp->setObjectName("secondaryApp");
    secondaryApp->setEnabled(false);

    auto output { new QTextEdit };

    auto saveXML { new QPushButton("Sauver") };

    auto mainLayout { new QFormLayout };
    mainLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    mainLayout->addRow("Mode de connexion", primaryApp);
    mainLayout->addWidget(secondaryApp);
    mainLayout->addRow(new LineSeparator); // NOLINT(cppcoreguidelines-owning-memory)
    mainLayout->addRow("Status de la connexion", output);
    mainLayout->addRow("Sauver le fichier XML", saveXML);
    setLayout(mainLayout);

    connect(saveXML, &QPushButton::clicked, [this]() {
        if (_gvcp) {
            auto name { QFileDialog::getExistingDirectory(nullptr, "Dossier de sauvegarde", QDir::homePath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks) };
            if (!name.isEmpty()) {
                const auto filename { _gvcp->xmlFilename() };
                const auto file { _gvcp->xmlFile() };
                QDir dir { name };
                QFile xml { dir.absoluteFilePath(filename.data()) };
                xml.open(QIODevice::WriteOnly | QIODevice::Text);
                xml.write(file.data());
                xml.close();
            }
        }
    });

    connect(secondaryApp, &QRadioButton::toggled, [this, secondaryApp]() {
        setFinalPage(secondaryApp->isChecked());
    });
}

CameraValidationPage::~CameraValidationPage() = default;

auto CameraValidationPage::wantControl() const -> bool
{
    if (const auto primaryApp { findChild<QRadioButton*>("primaryApp") }; primaryApp != nullptr) {
        return primaryApp->isChecked();
    }
    return false;
}

void CameraValidationPage::initializePage()
{
    _gvcp = std::make_unique<Jgv::Gvcp::Client>(Jgv::Channel::Type::Primary, _globalIpc);

    auto connected { _gvcp->monitorDevice() };
    auto output { findChild<QTextEdit*>() };
    const QHostAddress controllerIP { _globalIpc->gvcpControllerIP(Jgv::Channel::Type::Primary) };
    const QHostAddress transmitterIP { _globalIpc->gvcpTransmitterIP(Jgv::Channel::Type::Primary) };
    output->insertHtml(QString("Adresse contrôleur : <b>%0</b><br>").arg(controllerIP.toString()));
    output->insertHtml(QString("Adresse transmetteur : <b>%0</b><br>").arg(transmitterIP.toString()));

    if (connected) {
        output->insertHtml(QString("Status de la connexion :  <font color=%0><b>%1</b></font><br>").arg(SUCCESS_COLOR.data()).arg("Connected"));
        const auto ccp { _gvcp->readRegister(Jgv::Gvcp::Bootstrap::Address::ControlChannelPrivilege) };
        const auto accessMode { Jgv::Gvcp::Bootstrap::CCPPrivilege(ccp) };
        switch (accessMode) {
        case Jgv::Gvcp::Bootstrap::CCPPrivilege::OpenAcces:
            if (const auto primaryApp { findChild<QRadioButton*>("primaryApp") }; primaryApp != nullptr) {
                primaryApp->setEnabled(true);
                primaryApp->setChecked(true);
            }
            if (const auto secondaryApp { findChild<QRadioButton*>("secondaryApp") }; secondaryApp != nullptr) {
                secondaryApp->setEnabled(true);
            }
            output->insertHtml(QString("Privilège du transmetteur : <font color=%0><b>%1</b></font><br>").arg(SUCCESS_COLOR.data()).arg("OpenAccess"));
            break;
        case Jgv::Gvcp::Bootstrap::CCPPrivilege::ControlAcces:
            if (const auto primaryApp { findChild<QRadioButton*>("primaryApp") }; primaryApp != nullptr) {
                primaryApp->setEnabled(false);
            }
            if (const auto secondaryApp { findChild<QRadioButton*>("secondaryApp") }; secondaryApp != nullptr) {
                secondaryApp->setChecked(true);
            }
            output->insertHtml(QString("Privilège du transmetteur : <font color=%0><b>%1</b></font><br>").arg(SUCCESS_COLOR.data()).arg("ControlAccess"));
            break;

        case Jgv::Gvcp::Bootstrap::CCPPrivilege::ControlAccesWithSwitchoverEnabled:
            if (const auto primaryApp { findChild<QRadioButton*>("primaryApp") }; primaryApp != nullptr) {
                primaryApp->setEnabled(false);
            }
            if (const auto secondaryApp { findChild<QRadioButton*>("secondaryApp") }; secondaryApp != nullptr) {
                secondaryApp->setChecked(true);
            }
            output->insertHtml(QString("Privilège du transmetteur : <font color=%0><b>%1</b></font><br>").arg(SUCCESS_COLOR.data()).arg("ControlAccesWithSwitchoverEnabled"));
            break;
        case Jgv::Gvcp::Bootstrap::CCPPrivilege::ExclusiveAcces:
            if (const auto primaryApp { findChild<QRadioButton*>("primaryApp") }; primaryApp != nullptr) {
                primaryApp->setEnabled(false);
            }
            if (const auto secondaryApp { findChild<QRadioButton*>("secondaryApp") }; secondaryApp != nullptr) {
                secondaryApp->setEnabled(false);
            }
            output->insertHtml(QString("Privilège du transmetteur : <font color=%0><b>%1</b></font><br>").arg(SUCCESS_COLOR.data()).arg("ExclusiveAcces"));
            break;
        case Jgv::Gvcp::Bootstrap::CCPPrivilege::AccesDenied:
            if (const auto primaryApp { findChild<QRadioButton*>("primaryApp") }; primaryApp != nullptr) {
                primaryApp->setEnabled(false);
            }
            if (const auto secondaryApp { findChild<QRadioButton*>("secondaryApp") }; secondaryApp != nullptr) {
                secondaryApp->setEnabled(false);
            }
            output->insertHtml(QString("Privilège du transmetteur : <font color=%0><b>%1</b></font><br>").arg(SUCCESS_COLOR.data()).arg("AccesDenied"));
            break;
        }

        output->insertHtml(QString("Fichier XML : <font color=%0><b>%1</b></font><br>").arg("black").arg(_gvcp->xmlFilename().data()));

    } else {
        output->insertHtml(QString("Status de la connexion :  <font color=%0><b>%1</b></font><br>").arg(FAILED_COLOR.data()).arg("Failed to connect"));
    }
}

auto CameraValidationPage::validatePage() -> bool
{
    if (_gvcp) {
        const auto scsp0 { _gvcp->readRegister(Jgv::Gvcp::Bootstrap::Address::StreamChannelSourcePort) };
        _globalIpc->setGvspTransmitterPort(Jgv::Channel::Type::Primary, Jgv::Gvcp::Bootstrap::SCSPx::sourcePort(scsp0));

        const auto ccp { _gvcp->readRegister(Jgv::Gvcp::Bootstrap::Address::ControlChannelPrivilege) };
        const auto accessMode { Jgv::Gvcp::Bootstrap::CCPPrivilege(ccp) };
        switch (accessMode) {
        case Jgv::Gvcp::Bootstrap::CCPPrivilege::OpenAcces:
            return true;
        case Jgv::Gvcp::Bootstrap::CCPPrivilege::ControlAcces:
        case Jgv::Gvcp::Bootstrap::CCPPrivilege::ControlAccesWithSwitchoverEnabled:
            if (const auto secondaryApp { findChild<QRadioButton*>("secondaryApp") }; secondaryApp != nullptr) {
                return secondaryApp->isChecked();
            }
            break;
        case Jgv::Gvcp::Bootstrap::CCPPrivilege::ExclusiveAcces:
        case Jgv::Gvcp::Bootstrap::CCPPrivilege::AccesDenied:
            return false;
        }
    }
    return false;
}

void CameraValidationPage::cleanupPage()
{
    if (_gvcp) {
        _gvcp->releaseDevice();
        _gvcp.reset();
    }
}

} // namespace Jiguiviou
