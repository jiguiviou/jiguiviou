/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef CAMERASESSION_P_H
#define CAMERASESSION_P_H

#ifdef QTRECEIVER
#include <epoxy/gl.h>

#include <QOpenGLContext>
#endif

#include <QTimer>
#include <functional>
#include <memory>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jgv::Genicam2 {
class Interface;
} // namespace Jgv::Genicam2

namespace Jgv::Gvsp {
class Receiver;
} // namespace Jgv::Gvsp

namespace Jgv::Gvcp {
class Client;
} // namespace Jgv::Gvcp

namespace Jiguiviou {

class CameraSessionPrivate {
public:
    explicit CameraSessionPrivate(std::shared_ptr<Jgv::GlobalIPC::Object>&& ipc);
    const std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc;
    std::shared_ptr<Jgv::Genicam2::Interface> genicam;
    std::shared_ptr<Jgv::Gvsp::Receiver> gvsp;
    std::shared_ptr<std::function<std::shared_ptr<Jgv::Gvcp::Client>(void)>> gvcpGetterFunction;
    QTimer hearthbeatTimer;
#ifdef QTRECEIVER
    QOpenGLContext context;
#endif
};

} // namespace Jiguiviou

#endif // CAMERASESSION_P_H
