/***************************************************************************
 *   Copyright (C) 2014-2017 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef FORCEIPPAGE_H
#define FORCEIPPAGE_H

#include <QWizardPage>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jiguiviou {

class DiscovererWrapper;

class ForceIPPage : public QWizardPage {
    const std::shared_ptr<DiscovererWrapper> _discoverer;
    const std::shared_ptr<Jgv::GlobalIPC::Object> _globalIpc;
    uint32_t _ip { 0 };
    uint32_t _netmask { 0 };
    uint64_t _mac { 0 };

public:
    explicit ForceIPPage(std::shared_ptr<DiscovererWrapper> discoverer, std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc, QWidget* parent = nullptr);
    ~ForceIPPage();
    ForceIPPage(const ForceIPPage&) = delete;
    ForceIPPage(ForceIPPage&&) = delete;
    auto operator=(const ForceIPPage&) -> ForceIPPage& = delete;
    auto operator=(ForceIPPage &&) -> ForceIPPage& = delete;

private:
    void initializePage() override;
    bool validatePage() override;
};

} // namespace Jiguiviou

#endif // DISCOVERYPAGE_H
