/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPPAGE_H
#define GVSPPAGE_H

#include <QWizardPage>

namespace Jgv::GlobalIPC {
class Object;
} // namespace Jgv::GlobalIPC

namespace Jiguiviou {

class GvspPage : public QWizardPage {
    const std::shared_ptr<Jgv::GlobalIPC::Object> _globalIpc;

public:
    explicit GvspPage(std::shared_ptr<Jgv::GlobalIPC::Object> globalIpc, QWidget* parent = nullptr);
    ~GvspPage();
    GvspPage(const GvspPage&) = delete;
    GvspPage(GvspPage&&) = delete;
    auto operator=(const GvspPage&) -> GvspPage& = delete;
    auto operator=(GvspPage &&) -> GvspPage& = delete;

private:
    bool validatePage() override;
};

} // namespace Jiguiviou

#endif // GVSPPAGE_H
