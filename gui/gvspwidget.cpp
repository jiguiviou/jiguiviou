/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "gvspwidget.h"

#include "lineseparator.h"

#include <QFormLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QVBoxLayout>
#include <gvspdevices/gvsp.h>

namespace {

constexpr double KO { 1024. };
constexpr double MO { KO * KO };
constexpr double GO { MO * KO };

auto pixelToString(uint32_t pixel) -> QString
{
    const auto color { pixel & GVSP_PIX_COLOR_MASK };
    QString type;

    switch (pixel) {

    // Mono buffer format defines
    case GVSP_PIX_MONO8:
    case GVSP_PIX_MONO8_SIGNED:
    case GVSP_PIX_MONO10:
    case GVSP_PIX_MONO10_PACKED:
    case GVSP_PIX_MONO12:
    case GVSP_PIX_MONO12_PACKED:
    case GVSP_PIX_MONO14:
    case GVSP_PIX_MONO16:
        type = "Monochrome";
        break;
        // Bayer buffer format defines
    case GVSP_PIX_BAYGR8:
    case GVSP_PIX_BAYRG8:
    case GVSP_PIX_BAYGB8:
    case GVSP_PIX_BAYBG8:
    case GVSP_PIX_BAYGR10:
    case GVSP_PIX_BAYRG10:
    case GVSP_PIX_BAYGB10:
    case GVSP_PIX_BAYBG10:
    case GVSP_PIX_BAYGR12:
    case GVSP_PIX_BAYRG12:
    case GVSP_PIX_BAYGB12:
    case GVSP_PIX_BAYBG12:
    case GVSP_PIX_BAYGR10_PACKED:
    case GVSP_PIX_BAYRG10_PACKED:
    case GVSP_PIX_BAYGB10_PACKED:
    case GVSP_PIX_BAYBG10_PACKED:
    case GVSP_PIX_BAYGR12_PACKED:
    case GVSP_PIX_BAYRG12_PACKED:
    case GVSP_PIX_BAYGB12_PACKED:
    case GVSP_PIX_BAYBG12_PACKED:
    case GVSP_PIX_BAYGR16:
    case GVSP_PIX_BAYRG16:
    case GVSP_PIX_BAYGB16:
    case GVSP_PIX_BAYBG16:
        type = "Bayer";
        break;
        // RGB Packed buffer format defines
    case GVSP_PIX_RGB8_PACKED:
    case GVSP_PIX_BGR8_PACKED:
    case GVSP_PIX_RGBA8_PACKED:
    case GVSP_PIX_BGRA8_PACKED:
    case GVSP_PIX_RGB10_PACKED:
    case GVSP_PIX_BGR10_PACKED:
    case GVSP_PIX_RGB12_PACKED:
    case GVSP_PIX_BGR12_PACKED:
    case GVSP_PIX_RGB16_PACKED:
    case GVSP_PIX_RGB10V1_PACKED:
    case GVSP_PIX_RGB10V2_PACKED:
    case GVSP_PIX_RGB12V1_PACKED:
    case GVSP_PIX_RGB565_PACKED:
    case GVSP_PIX_BGR565_PACKED:
        type = "RGB Packed";
        break;
        // YUV Packed buffer format defines
    case GVSP_PIX_YUV411_PACKED:
    case GVSP_PIX_YUV422_PACKED:
    case GVSP_PIX_YUV422_YUYV_PACKED:
    case GVSP_PIX_YUV444_PACKED:
        type = "YUV Packed";
        break;
        // RGB Planar buffer format defines
    case GVSP_PIX_RGB8_PLANAR:
    case GVSP_PIX_RGB10_PLANAR:
    case GVSP_PIX_RGB12_PLANAR:
    case GVSP_PIX_RGB16_PLANAR:
        type = "RGB Planar";
        break;
    default:
        type = "Unknow";
    }

    auto colorToString = [](uint32_t color) {
        if (color == GVSP_PIX_MONO) {
            return "Mono";
        }
        if (color == GVSP_PIX_COLOR) {
            return "Color";
        }
        if (color == GVSP_PIX_CUSTOM) {
            return "Custom";
        }
        return "Unknow";
    };

    return QString("%0 %1 bits %2")
        .arg(colorToString(color))
        .arg(GVSP_PIX_PIXEL_SIZE(pixel))
        .arg(type);
}
} // namespace

namespace Jiguiviou {

void EventsWrapper::imageFormat(uint32_t width, uint32_t height, uint32_t format)
{
    emit imageFormatSignal(width, height, format);
}

void EventsWrapper::allocate(uint32_t number, std::size_t size)
{
    emit allocateSignal(number, size);
}

GvspWidget::GvspWidget(QWidget* parent)
    : QWidget(parent)
    , _events { std::make_shared<EventsWrapper>() }
{
    auto title { new QLabel };
    title->setAlignment(Qt::AlignCenter);
    title->setText("<b>GVSP</b>");

    auto pixelFormat { new QLabel };
    pixelFormat->setText(pixelToString(0));

    auto imageWidth { new QLabel };
    imageWidth->setNum(0);

    auto imageHeight { new QLabel };
    imageHeight->setNum(0);

    auto allocateId { new QLabel };
    allocateId->setNum(0);

    auto allocateSize { new QLabel };
    allocateSize->setNum(0);

    auto allocateCumulled { new QLabel };
    allocateCumulled->setNum(0);

    auto layout { new QFormLayout };
    layout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    layout->setAlignment(Qt::AlignCenter);
    layout->addRow("<b>Format du pixel</b>", pixelFormat);
    layout->addRow("<b>Largeur image</b>", imageWidth);
    layout->addRow("<b>Hauteur image</b>", imageHeight);
    layout->addRow(new LineSeparator); // NOLINT(cppcoreguidelines-owning-memory)
    layout->addRow("<b>Index image allouée</b>", allocateId);
    layout->addRow("<b>Taille image allouée</b>", allocateSize);
    layout->addRow("<b>Cumul images allouées</b>", allocateCumulled);

    auto hLayout { new QHBoxLayout };
    hLayout->addStretch();
    hLayout->addLayout(layout);
    hLayout->addStretch();

    auto mainlayout { new QVBoxLayout };
    mainlayout->addSpacing(title->sizeHint().height());
    mainlayout->addWidget(title);
    mainlayout->addWidget(new LineSeparator); // NOLINT(cppcoreguidelines-owning-memory)
    mainlayout->addLayout(hLayout);
    mainlayout->addStretch();

    setLayout(mainlayout);

    connect(
        _events.get(), &EventsWrapper::imageFormatSignal, this, [imageWidth, imageHeight, pixelFormat](uint32_t width, uint32_t height, uint32_t format) {
            pixelFormat->setText(pixelToString(format));
            imageWidth->setNum(static_cast<int>(width));
            imageHeight->setNum(static_cast<int>(height));
        },
        Qt::QueuedConnection);

    connect(
        _events.get(), &EventsWrapper::allocateSignal, this, [this, allocateId, allocateSize, allocateCumulled](uint32_t number, std::size_t size) {
            allocateId->setNum(static_cast<int>(number));
            allocateSize->setText(QString("%0 %1").arg(size).arg(" octets"));
            _allocatedCummuled += size;
            if (_allocatedCummuled < MO) {
                allocateCumulled->setText(QString("%0 %1").arg(_allocatedCummuled / KO).arg(" ko"));
            } else if (_allocatedCummuled < GO) {
                allocateCumulled->setText(QString("%0 %1").arg(_allocatedCummuled / (MO), 0, 'f', 1).arg(" Mo"));
            } else {
                allocateCumulled->setText(QString("%0 %1").arg(_allocatedCummuled / (GO), 0, 'f', 2).arg(" Go"));
            }
        },
        Qt::QueuedConnection);
}

auto GvspWidget::events() -> std::weak_ptr<Jgv::Gvsp::AllocatorEvents>
{
    return _events;
}

} // namespace Jiguiviou
