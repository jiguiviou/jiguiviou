/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamtreeview.h"

#include "genicammodel.h"

#include <QComboBox>
#include <QSortFilterProxyModel>
#include <QTextEdit>
#include <QTreeView>
#include <genicam2/genicaminterface.h>

namespace {
constexpr std::string_view BEGINNER = "Beginner";
constexpr std::string_view EXPERT = "Expert";
constexpr std::string_view GURU = "Guru";
constexpr std::string_view INVISIBLE = "Invisible";

inline void updateProxyModel(const QString& value, QSortFilterProxyModel* proxyModel)
{
    if (value.startsWith(BEGINNER.data())) {
        proxyModel->setFilterRegExp(BEGINNER.data());
    } else if (value.startsWith(EXPERT.data())) {
        proxyModel->setFilterRegExp(QString("%0|%1").arg(BEGINNER.data()).arg(EXPERT.data()));
    } else if (value.startsWith(GURU.data())) {
        proxyModel->setFilterRegExp(
            QString("%0|%1|%2").arg(BEGINNER.data()).arg(EXPERT.data()).arg(GURU.data()));
    } else {
        proxyModel->setFilterRegExp("");
    }
    proxyModel->invalidate();
}

inline void updateInfos(const Jgv::Genicam2::IInode* inode, QTextEdit* infos)
{
    infos->clear();

    // pas de description sur les noeuds category
    if (inode->CategoryInterface() != nullptr) {
        return;
    }

    infos->append(QString("<b>%1</b>").arg(inode->displayName().data()));
    auto description { inode->description() };
    if (description.empty()) {
        description = "Pas de description";
    }
    infos->append(description.data());
    infos->append("");
    infos->append(QString("FEATURE NAME: <b> %0</b>").arg(inode->featureName().data()));
    infos->append(QString("TYPE: <b>%0</b>").arg(inode->type().data()));

    if (const auto interface { inode->IntegerInterface() }; interface != nullptr) {
        bool showInc { false };
        if (const auto min { interface->min() }; min != std::numeric_limits<int64_t>::lowest()) {
            infos->append(QString("MIN: <b>%0</b>").arg(min));
            showInc = true;
        }
        if (const auto max { interface->max() }; max != std::numeric_limits<int64_t>::max()) {
            infos->append(QString("MAX: <b>%0</b>").arg(max));
            showInc = true;
        }
        if (showInc) {
            infos->append(QString("INC: <b>%0</b>").arg(interface->inc()));
        }
    }

    else if (const auto interface { inode->FloatInterface() }; interface != nullptr) {
        bool showInc { false };
        if (const auto min { interface->min() }; min != std::numeric_limits<double>::lowest()) {
            infos->append(QString("MIN: <b>%0</b>").arg(min, 0, 'f'));
            showInc = true;
        }
        if (const auto max { interface->max() }; max != std::numeric_limits<double>::max()) {
            infos->append(QString("MAX: <b>%0</b>").arg(max, 0, 'f'));
            showInc = true;
        }
        if (showInc) {
            if (const auto inc { interface->inc() }; inc != 0.) {
                infos->append(QString("INC: <b>%0</b>").arg(inc, 0, 'f'));
            }
        }
    }
}

} // Anonymous namespace

namespace Jiguiviou {

GenICamTreeView::GenICamTreeView(std::shared_ptr<Jgv::Genicam2::Interface> genicam, bool visibilitySelector, QWidget* parent)
    : QSplitter(Qt::Vertical, parent)
    , _treeView(new QTreeView)
{

    auto model { new GenicamModel(std::move(genicam), this) };

    _treeView->setUniformRowHeights(true);
    _treeView->rootIsDecorated();
    _treeView->setItemDelegate(&_delegate);

    auto infos { new QTextEdit };
    infos->setReadOnly(true);

    if (visibilitySelector) {
        auto visibility { new QComboBox };
        visibility->addItems({ BEGINNER.data(), EXPERT.data(), GURU.data(), INVISIBLE.data() });
        visibility->setCurrentIndex(0);

        addWidget(visibility);

        // on utilise un proxy pour filtrer selon la visibilité
        auto proxyModel { new QSortFilterProxyModel(this) };
        proxyModel->setSourceModel(model);
        // on filtre sur la colonne visibility
        proxyModel->setFilterKeyColumn(2);
        proxyModel->setFilterRegExp(BEGINNER.data());
        _treeView->setModel(proxyModel);

        connect(visibility, static_cast<void (QComboBox::*)(const QString&)>(&QComboBox::currentIndexChanged), [proxyModel](const QString& value) {
            updateProxyModel(value, proxyModel);
        });
    } else {
        _treeView->setModel(model);
    }

    _treeView->setColumnHidden(2, true);

    addWidget(_treeView);
    addWidget(infos);

    if (auto index { indexOf(_treeView) }; index >= 0) {
        setStretchFactor(index, 4);
    }

    connect(_treeView, &QTreeView::clicked, [infos](const QModelIndex& index) {
        if (const auto data { index.data(Qt::EditRole) }; data.canConvert<QModelIndex>()) {
            if (const auto inode { static_cast<Jgv::Genicam2::IInode*>(data.toModelIndex().internalPointer()) }; inode != nullptr) {
                updateInfos(inode, infos);
            }
        }
    });
}

} // namespace Jiguiviou
