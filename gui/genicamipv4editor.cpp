/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "genicamipv4editor.h"

#include <QHostAddress>
#include <QLayout>
#include <QLineEdit>
#include <QRegExpValidator>
#include <genicam2/genicaminterface.h>

namespace Jiguiviou {

Ipv4Editor::Ipv4Editor(std::string_view name, QWidget* parent)
    : Editor { name, parent }
    , _lineEdit { new QLineEdit }
{
    layout()->addWidget(_lineEdit);
    const QString ipRange { "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])" };
    const QRegExp ipRegex("^" + ipRange + "\\." + ipRange + "\\." + ipRange + "\\." + ipRange + "$");
    _lineEdit->setValidator(new QRegExpValidator(ipRegex, this)); // NOLINT(cppcoreguidelines-owning-memory)
}

void Ipv4Editor::setData(Jgv::Genicam2::IInode* inode)
{
    if (auto interface { inode->IntegerInterface() }; interface != nullptr) {
        const QHostAddress address { static_cast<uint32_t>(interface->value()) };
        _lineEdit->setText(address.toString());
    }
}

auto Ipv4Editor::value() -> QVariant
{
    const QHostAddress address { _lineEdit->text() };
    return address.toIPv4Address();
}

} // namespace Jiguiviou
