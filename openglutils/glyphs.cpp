/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "glyphs.h"

#include <fontconfig/fontconfig.h>

#include <fontconfig/fcfreetype.h>

#include <freetype/ftglyph.h>

#include <iostream>

namespace {
std::string fontPath(const std::string& family)
{
    auto config { FcInitLoadConfigAndFonts() };
    auto pattern { FcPatternCreate() };
    if (pattern == nullptr) {
        std::cerr << "Glyphs: fontconfig failed to create pattern." << std::endl;
        return std::string();
    }

    std::basic_string<FcChar8> s { family.cbegin(), family.cend() };
    FcPatternAddString(pattern, FC_FAMILY, s.data());

    if (FcConfigSubstitute(config, pattern, FcMatchPattern) == FcFalse) {
        std::cerr << "Glyphs: fontconfig failed to substitute monospace pattern." << std::endl;
        FcPatternDestroy(pattern);
        return std::string();
    }
    FcDefaultSubstitute(pattern);

    FcResult result;
    auto font { FcFontMatch(config, pattern, &result) };
    FcPatternDestroy(pattern);

    FcChar8* file { nullptr };
    result = FcPatternGetString(font, FC_FILE, 0, &file);
    std::basic_string<FcChar8> path { file };
    //std::string p { tmp.begin(), tmp.end() };

    //std::string path { tmp.begin(), tmp.end() };
    FcPatternDestroy(font);

    return { path.begin(), path.end() };
}

} // namespace

namespace Jgv::OpenGLUtils {

Glyph::~Glyph()
{
    FT_Done_Face(_face);
    FT_Done_FreeType(_library);
}

void Glyph::initialize(unsigned pixelSize, const std::string& family)
{
    if (FT_Init_FreeType(&_library) != 0) {
        std::cerr << "Glyphs failed to initialize freetype." << std::endl;
    }

    auto path { fontPath(family) };
    if (FT_New_Face(_library, path.c_str(), 0, &_face) != 0) {
        std::cerr << "Glyphs failed to load font: " << path << std::endl;
    }
    FT_Set_Pixel_Sizes(_face, 0, pixelSize);
}

bool Glyph::isMonospace() const
{
    return FT_IS_FIXED_WIDTH(_face);
}

void Glyph::setChar(char16_t charac)
{
    if (FT_Load_Char(_face, charac, FT_LOAD_RENDER) != 0) {
        std::cerr << "Glyphs failed to load glyph." << std::endl;
    }
}

float Glyph::advance() const
{
    return _face->glyph->advance.x;
}

uint Glyph::width() const
{
    return _face->glyph->bitmap.width;
}

uint Glyph::height() const
{
    return _face->glyph->bitmap.rows;
}

float Glyph::bearingX() const
{
    return _face->glyph->bitmap_left;
}

float Glyph::bearingY() const
{
    return _face->glyph->bitmap_top;
}

float Glyph::ascender() const
{
    return _face->size->metrics.ascender;
}

float Glyph::descender() const
{
    return _face->size->metrics.descender;
}

float Glyph::fontAdvance() const
{
    return _face->size->metrics.max_advance;
}

float Glyph::interline() const
{
    return _face->size->metrics.height;
}

const uint8_t* Glyph::bitmap() const
{
    return _face->glyph->bitmap.buffer;
}

} // namespace Jgv
