/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GVSPQTOPENGLPROGRAM_H
#define GVSPQTOPENGLPROGRAM_H

#include <epoxy/gl.h>

#include <array>
#include <iostream>
#include <string>
#include <tuple>

namespace Jgv::OpenGLUtils {

constexpr GLsizei QUAD_ELEMENTS_COUNT { 6 };

struct MatOrtho {
    std::array<float, 4> _a;
    std::array<float, 4> _b;
    std::array<float, 4> _c;
    std::array<float, 4> _d;
    MatOrtho(float width, float height, bool flip = false)
        : _a { 2.F / width, 0.F, 0.F, 0.F }
        , _b { 0.F, flip ? -2.F / height : 2.F / height, 0.F, 0.F }
        , _c { 0.F, 0.F, -0.02F, 0.F }
        , _d { -1.F, flip ? 1.F : -1.F, -1.F, 1.F }
    {
    }
    [[nodiscard]] const float* p() const noexcept { return _a.data(); }
};

// Left, Bottom, Right, Top
using Rectangle = std::tuple<float, float, float, float>;

struct Point {
    float x;
    float y;
};

struct Vertice {
    float x;
    float y;
    float z;
};

struct Quad {
    explicit Quad(const Rectangle& rectangle, float depth);
    explicit Quad(float xstart, float ystart, float xstop, float ystop, float depth);
    Vertice bottomLeft;
    Vertice bottomRight;
    Vertice topRight;
    Vertice topLeft;
};

struct QuadTextureCoordinates {
    QuadTextureCoordinates() = default;
    explicit QuadTextureCoordinates(float scale)
    {
        const auto size { 1.F / scale };
        leftBottom.x = 0.5F - size / 2.F;
        leftBottom.y = 0.5F + size / 2.F;
        rightBottom.x = 0.5F + size / 2.F;
        rightBottom.y = leftBottom.y;
        rightTop.x = rightBottom.x;
        rightTop.y = 0.5F - size / 2.F;
        leftTop.x = leftBottom.x;
        leftTop.y = rightTop.y;
    }
    Point leftBottom { 0.F, 1.F };
    Point rightBottom { 1.F, 1.F };
    Point rightTop { 1.F, 0.F };
    Point leftTop { 0.F, 0.F };
};

template <typename T>
struct QuadElements {
    T a { 0 };
    T b { 1 };
    T c { 2 };
    T d { 0 };
    T e { 2 };
    T f { 3 };

    void addOffset(T value)
    {
        a += value;
        b += value;
        c += value;
        d += value;
        e += value;
        f += value;
    }
};

template <typename T>
inline auto offsetPtr(uint offset) -> const void*
{
    return reinterpret_cast<const void*>(offset * sizeof(T));
}

constexpr auto FENCE_STATE_TO_STRING(GLenum state)
{
    if (state == GL_ALREADY_SIGNALED) {
        return "AlreadySignaled";
    }
    if (state == GL_TIMEOUT_EXPIRED) {
        return "TimeoutExpired";
    }
    if (state == GL_CONDITION_SATISFIED) {
        return "ConditionSatisfied";
    }
    if (state == GL_WAIT_FAILED) {
        return "WaitFailed";
    }
    return "UnknowState";
}

static void checkGLerror(const char* file, int line)
{
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR) {
        std::string error;
        switch (err) {
        case GL_INVALID_OPERATION:
            error = "INVALID_OPERATION";
            break;
        case GL_INVALID_ENUM:
            error = "INVALID_ENUM";
            break;
        case GL_INVALID_VALUE:
            error = "INVALID_VALUE";
            break;
        case GL_OUT_OF_MEMORY:
            error = "OUT_OF_MEMORY";
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            error = "INVALID_FRAMEBUFFER_OPERATION";
            break;
        default:
            error = "UNKNOW";
        }

        std::cerr << "GL_" << error << " " << file << " " << line << std::endl;
    }
}
#define glCheckError() checkGLerror(__FILE__, __LINE__)

class Program {
public:
    static GLuint create(const std::string& vertexSource, const std::string& fragmentSource);

}; // class Program

} // namespace Jgv

#endif // GVSPQTOPENGLPROGRAM_H
