/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "openglprogram.h"

#include <vector>

namespace Jgv::OpenGLUtils {

Quad::Quad(const Rectangle& rectangle, float depth)
    : bottomLeft({ std::get<0>(rectangle), std::get<1>(rectangle), depth })
    , bottomRight({ std::get<2>(rectangle), std::get<1>(rectangle), depth })
    , topRight({ std::get<2>(rectangle), std::get<3>(rectangle), depth })
    , topLeft({ std::get<0>(rectangle), std::get<3>(rectangle), depth })
{
}

Quad::Quad(float xstart, float ystart, float xstop, float ystop, float depth)
    : bottomLeft({ xstart, ystart, depth })
    , bottomRight({ xstop, ystart, depth })
    , topRight({ xstop, ystop, depth })
    , topLeft({ xstart, ystop, depth })
{
}

GLuint Program::create(const std::string& vertexSource, const std::string& fragmentSource)
{
    auto shader = [](GLenum shaderType, const std::string& sourceString) {
        auto emptyShader { glCreateShader(shaderType) };
        auto source { static_cast<const GLchar*>(sourceString.c_str()) };
        glShaderSource(emptyShader, 1, &source, nullptr);
        glCompileShader(emptyShader);

        GLint isCompiled { 0 };
        glGetShaderiv(emptyShader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE) {
            GLint maxLength { 0 };
            glGetShaderiv(emptyShader, GL_INFO_LOG_LENGTH, &maxLength);
            std::vector<GLchar> infoLog(static_cast<size_t>(maxLength));
            glGetShaderInfoLog(emptyShader, maxLength, &maxLength, &infoLog[0]);
            glDeleteShader(emptyShader);
            emptyShader = 0;
        }
        return emptyShader;
    };

    auto program { glCreateProgram() };
    auto vertexShader { shader(GL_VERTEX_SHADER, vertexSource) };
    auto fragmentShader { shader(GL_FRAGMENT_SHADER, fragmentSource) };
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    GLint isLinked { 0 };
    glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
    if (isLinked == GL_FALSE) {
        GLint maxLength { 0 };
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);
        std::vector<GLchar> infoLog(static_cast<size_t>(maxLength));
        glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);
        glDeleteProgram(program);
        program = 0;
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }

    glDetachShader(program, vertexShader);
    glDetachShader(program, fragmentShader);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return program;
}

} // namespace Jgv
