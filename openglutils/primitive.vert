R"(
#version 420 core

layout(location = 0) in vec3 position;

uniform mat4 transform;
uniform vec2 scroll;

void main()
{
    gl_Position = transform * vec4(position + vec3(scroll, 0.), 1.);
}
)"
