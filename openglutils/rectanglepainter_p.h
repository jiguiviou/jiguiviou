/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef RECTANGLEPAINTER_P_H
#define RECTANGLEPAINTER_P_H

#include <epoxy/gl.h>
#include <unordered_map>
#include <vector>

namespace Jgv::OpenGLUtils {

using Color = std::tuple<float, float, float, float>;
using Rectangle = std::tuple<float, float, float, float>;
using ClippingRectangle = std::tuple<GLint, GLint, GLsizei, GLsizei>;

struct RectanglePainterObject;

struct RectangleGLObject {
    GLuint vbo { 0U };
    GLuint vao { 0U };
    std::size_t scrollVectorIndex { 0U };
    std::size_t clipping { 0U };
    Rectangle rectangle { 0.F, 0.F, 0.F, 0.F };
    Color color { 0.F, 0.F, 0.F, 0.F };
    float depth;
};

class RectanglePainterPrivate {
public:
    GLuint program { 0U };
    GLuint ebo { 0U };
    GLint transformId { -1 };
    GLint scrollId { -1 };
    GLint colorId { -1 };

    std::unordered_map<std::size_t, RectangleGLObject> rectangleObjects;
    std::unordered_map<std::size_t, ClippingRectangle> clippingObjects;
    std::unordered_map<std::size_t, std::array<GLfloat, 2>> scrollVectors;
    std::unordered_map<std::size_t, RectangleGLObject> objectsToDestroy;

    void initialize();
    void updateVao(RectangleGLObject* object);
    void render(const std::vector<std::vector<RectanglePainterObject>>& proxy);
    void destroy();

    auto createUUID() noexcept -> uint64_t
    {
        return ++uuid;
    }

private:
    uint64_t uuid { 1 };
};

} // namespace Jgv::OpenGLUtils

#endif // RECTANGLEPAINTER_P_H
