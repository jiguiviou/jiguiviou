R"(
#version 420 core
layout(binding = 0) uniform sampler2D tex;
layout(location = 0) out vec4 outColor;
uniform uint pixelSize;
in vec2 vTexCoord;

void main()
{
    outColor = ( pixelSize == 1 ) ? vec4(texture(tex, vTexCoord).rrr, 1.) : texture(tex, vTexCoord);
}
)"

