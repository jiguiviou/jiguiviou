﻿/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef TEXTUREPAINTER_P_H
#define TEXTUREPAINTER_P_H

#include "moodycamel/readerwriterqueue.h"
#include "openglprogram.h"

#include <epoxy/gl.h>
#include <queue>
#include <string>
#include <unordered_map>

namespace Jgv::OpenGLUtils {

struct TextureBuffer {
    uint width { 0U };
    uint height { 0U };
    uint pixelsize { 0U };
    std::vector<uint8_t> buffer;
};

struct TextureGLObject {
    std::size_t texture { 0U };
    std::size_t clipping { 0U };
    std::size_t scrollVector { 0U };
    GLuint vbo { 0U };
    GLuint cbo { 0U };
    GLuint vao { 0U };
    GLuint externalTexture { 0U };
    float centerScalefactor { 1.F };
};

struct Texture {
    std::string filename;
    GLuint id { 0U };
    uint pixelSize { 0U };
    uint width { 0U };
    uint height { 0U };
};

struct TexturePainterObject;

using ClippingRectangle = std::tuple<int, int, int, int>;

struct ExternalTextureTuple {
    uint32_t index;
    GLuint texture;
};

struct TexturePainterPrivate {
    GLuint program { 0U };
    GLuint ebo { 0U };
    GLint projectionId { -1 };
    GLint scrollId { -1 };
    GLint pixelSizeId { -1 };

    std::unordered_map<uint32_t, TextureGLObject> textureObjects;
    std::unordered_map<std::size_t, ClippingRectangle> clippingObjects;
    std::unordered_map<std::size_t, std::tuple<GLfloat, GLfloat>> scrollVectors;
    std::unordered_map<std::size_t, Texture> textures;

    std::queue<std::tuple<std::size_t, Rectangle, float>> rectanglesBuffer;

    moodycamel::ReaderWriterQueue<ExternalTextureTuple> externalTextureBuffer;

    std::unordered_map<uint32_t, TextureGLObject> textureObjectsToDestroy;
    std::unordered_map<std::size_t, Texture> texturesToDestroy;

    void initialize();
    void updateVao(TextureGLObject* object, Rectangle rectangle, float depth);
    void render(const std::vector<std::vector<TexturePainterObject>>& proxy);
    void destroy();

    auto createUUID() noexcept -> uint32_t
    {
        return ++uuid;
    }

private:
    uint32_t uuid { 1 };
};

} // namespace Jgv

#endif // TEXTUREPAINTER_P_H
