/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef TEXTPAINTER_P_H
#define TEXTPAINTER_P_H

#include "glyphs.h"
#include "moodycamel/readerwriterqueue.h"

#include <epoxy/glx.h>

#include <chrono>
#include <codecvt>
#include <locale>
#include <string>
#include <unordered_map>
#include <vector>

namespace Jgv::OpenGLUtils {

using Bbox = std::tuple<float, float, float, float>;
using Color = std::tuple<float, float, float, float>;

struct Character {
    GLuint texture { 0U };
    float width { 0.F };
    float height { 0.F };
    float bearingX { 0.F };
    float bearingY { 0.F };
    float advance { 0.F };
};

struct TextObject {
    std::u16string text { u"" }; // le texte au format freetype
    std::size_t clippingRectangleObject { 0U }; // l'index des clipsObject
    std::size_t scrollVectorObject { 0U }; // l'index des scrollVectors
    std::size_t maxSize { 0 }; // la taille de réserve
    Color color { 1.F, 1.F, 1.F, 1.F }; // la couleur
    Bbox boundingBox { 0.F, 0.F, 0.F, 0.F }; // la boite au format left, bottom, right, top

    GLuint vao { 0U };
    GLuint vbo { 0U };
    std::tuple<GLfloat, GLfloat, GLfloat> position { 0.F, 0.F, 0.F };
    float scale { 1.F };
    bool cursor { false };
};

// left, bottom, width, height
using ClipRectangle = std::tuple<int, int, int, int>;
using Clock = std::chrono::steady_clock;

struct TextPainterObject;

struct TextPainterPrivate {
    Glyph glyph;
    std::unordered_map<char16_t, Character> characters;

    GLuint program { 0U };
    GLuint cbo { 0U };
    GLuint ebo { 0U };
    GLint transformId { -1 };
    GLint scrollId { -1 };
    GLint colorId { -1 };

    Clock::time_point lastTime;
    std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> utf16conf;

    std::unordered_map<std::size_t, TextObject> textObjects;
    std::unordered_map<std::size_t, ClipRectangle> clipObjects;
    std::unordered_map<std::size_t, std::tuple<GLfloat, GLfloat>> scrollObject;

    moodycamel::ReaderWriterQueue<std::tuple<std::size_t, std::string>> asyncTextsBuffer;
    moodycamel::ReaderWriterQueue<std::tuple<std::size_t, Color>> asyncColorBuffer;

    std::unordered_map<std::size_t, TextObject> textObjectsToDestroy;

    bool flip { false };

    void initialize(unsigned fontPixelSize, const std::string& fontFamily, bool invertCoordinates);
    void validateTexture(char16_t c);
    void updateVao(TextObject* object) noexcept;
    void updateBoundingBox(TextObject* object);
    void updateText(TextObject* object);

    void render(const std::vector<std::vector<TextPainterObject>>& proxy);
    void destroy();

    auto createUUID() noexcept -> uint64_t
    {
        return ++uuid;
    }

private:
    uint64_t uuid { 1 };
};

} // namespace Jgv::OpenGLUtils

#endif // TEXTPAINTER_P_H
