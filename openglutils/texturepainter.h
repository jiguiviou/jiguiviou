/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef TEXTUREPAINTER_H
#define TEXTUREPAINTER_H

#include <epoxy/gl.h>
#include <memory>
#include <vector>

namespace Jgv::OpenGLUtils {

struct TexturePainterObject {
    uint32_t index;
};

struct TextureObject {
    std::size_t index;
};

struct TextureClipObject {
    std::size_t index;
};

struct TextureScrollObject {
    std::size_t index;
};

// Left, Bottom, Right, Top
using Rectangle = std::tuple<float, float, float, float>;
using TexturePainterObjectsVector = std::vector<TexturePainterObject>;
using TexturePainterObjectsProxy = std::vector<TexturePainterObjectsVector>;
using ClippingRectangle = std::tuple<int, int, int, int>;
using TextureSize = std::tuple<int, int>;

struct TexturePainterPrivate;
class TexturePainter {
public:
    TexturePainter();
    ~TexturePainter();
    TexturePainter(const TexturePainter&) = delete;
    TexturePainter(TexturePainter&&) = delete;
    auto operator=(const TexturePainter&) -> TexturePainter& = delete;
    auto operator=(TexturePainter &&) -> TexturePainter& = delete;

    auto createObject() -> TexturePainterObject;
    void destroyObject(TexturePainterObject object);

    auto createTextureObject(const std::string& filename) -> TextureObject;
    void destroyTextureObject(TextureObject texture);

    auto createClipObject() -> TextureClipObject;
    void destroyClipObject(TextureClipObject object);

    auto createScrollObject() noexcept -> TextureScrollObject;
    void destroyScrollObject(TextureScrollObject vector);

    void setTexture(TexturePainterObject object, TextureObject texture);
    void setExternalTexture(TexturePainterObject object, GLuint id);
    void updateExternalTexture(TexturePainterObject object, GLuint id);
    void setRectangle(TexturePainterObject object, const Rectangle& rectangle, float depth = 0.F) noexcept;
    void setCenterScaleFactor(TexturePainterObject object, float factor);

    auto textureSize(TextureObject texture) -> TextureSize;

    void setClipObject(TexturePainterObject object, TextureClipObject clippingObject) noexcept;
    void setScrollObject(TexturePainterObject object, TextureScrollObject scrollvector) noexcept;

    void setClippingRectangle(TextureClipObject clippingObject, const ClippingRectangle& rectangle) noexcept;
    void setScrollVector(TextureScrollObject scrollVectorObject, float x, float y);

    void initializeGL();
    void resizeViewGL(float width, float height);
    void renderGL(const TexturePainterObjectsProxy& proxy);
    void renderGL(const TexturePainterObjectsVector& objects);
    void renderGL(TexturePainterObject object);
    void destroyGL();

private:
    const std::unique_ptr<TexturePainterPrivate> _impl;
};

} // namespace Jgv::OpenGLUtils

#endif // TEXTUREPAINTER_H
