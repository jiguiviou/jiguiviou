/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "textpainter.h"
#include "textpainter_p.h"

#include "openglprogram.h"

namespace Jgv::OpenGLUtils {

constexpr std::size_t STRING_MAX_SIZE { 128 };
constexpr char16_t CURSOR { u'_' };
constexpr auto FT_TO_PIXEL_FACTOR { 1.F / 64.F };
constexpr auto BLINK_PERIODE { 500 };

void TextPainterPrivate::initialize(unsigned fontPixelSize, const std::string& fontFamily, bool invertCoordinates)
{
    flip = invertCoordinates;
    glyph.initialize(fontPixelSize, fontFamily);

    const std::string vSource =
#include "shader.vert"
        ;
    const std::string fSource =
#include "shader.frag"
        ;
    program = Program::create(vSource, fSource);

    std::array<QuadTextureCoordinates, STRING_MAX_SIZE> texture;
    glGenBuffers(1, &cbo);
    glBindBuffer(GL_ARRAY_BUFFER, cbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(texture), texture.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    std::array<QuadElements<GLushort>, STRING_MAX_SIZE> elements;
    glGenBuffers(1, &ebo);
    GLushort offset { 0 };
    for (auto& quad : elements) {
        quad.addOffset(offset);
        offset += 4U;
    }
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glUseProgram(program);
    transformId = glGetUniformLocation(program, "transform");
    scrollId = glGetUniformLocation(program, "scroll");
    colorId = glGetUniformLocation(program, "color");
    glUseProgram(0);
}

void TextPainterPrivate::validateTexture(char16_t c)
{
    auto& val { characters[c] };
    if (val.texture == 0) {
        glyph.setChar(c);

        GLuint textureID { 0U };
        // si le glyph doit être dessinné (pas pour l'espace par exemple)
        if (glyph.width() > 0 && glyph.height() > 0) {
            glGenTextures(1, &textureID);
            glBindTexture(GL_TEXTURE_2D, textureID);
            glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8, glyph.width(), glyph.height());

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            glTexSubImage2D(GL_TEXTURE_2D,
                0,
                0,
                0,
                glyph.width(),
                glyph.height(),
                GL_RED,
                GL_UNSIGNED_BYTE,
                glyph.bitmap());
            glBindTexture(GL_TEXTURE_2D, 0);
        }

        val.texture = textureID;
        val.width = glyph.width();
        val.height = glyph.height();
        val.bearingX = glyph.bearingX();
        val.bearingY = glyph.bearingY();
        val.advance = glyph.advance();
    }
}

void TextPainterPrivate::updateVao(TextObject* object) noexcept
{

    // si les buffer objects n'exite pas, on les crées
    if (object->vao == 0U) {
        GLuint vbo { 0U };
        GLuint vao { 0U };

        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, object->maxSize * sizeof(Quad), nullptr, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glGenVertexArrays(1, &vao);

        glBindVertexArray(vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), offsetPtr<GLfloat>(0));
        glBindBuffer(GL_ARRAY_BUFFER, cbo);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), offsetPtr<GLfloat>(0));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glBindVertexArray(0);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        object->vao = vao;
        object->vbo = vbo;
    }

    // on calcule les vertices pour la position et le texte
    std::vector<Quad> quads;
    quads.reserve(object->text.size() + 1);
    const auto scale { object->scale };
    auto [x, y, z] { object->position };
    for (const auto& c : object->text) {
        const auto& val { characters[c] };
        const auto xstart { x + (val.bearingX * scale) };
        const auto ystart { y - (val.height - val.bearingY) * scale };
        quads.emplace_back(xstart,
            ystart,
            xstart + val.width * scale,
            ystart + val.height * scale,
            z);
        x += val.advance * FT_TO_PIXEL_FACTOR * scale;
    }

    if (object->cursor) {
        const auto& val { characters[CURSOR] };
        const auto xstart { x + val.bearingX * scale };
        const auto ystart { y - (val.height - val.bearingY) * scale };
        quads.emplace_back(
            xstart, ystart, xstart + val.width, ystart + val.height, z);
    }

    glBindBuffer(GL_ARRAY_BUFFER, object->vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, quads.size() * sizeof(Quad), quads.data());
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void TextPainterPrivate::updateBoundingBox(TextObject* objectText)
{
    const auto scale { objectText->scale };
    auto& [left, bottom, right, top] { objectText->boundingBox };

    for (const auto c : objectText->text) {
        glyph.setChar(c);
        // augmente la largeur
        right += glyph.advance() * scale;
        // les coordonnées sont relatives à la ligne de base
        bottom = std::min(bottom, glyph.descender() * scale);
        top = std::max(top, glyph.ascender() * scale);
    }

    left *= FT_TO_PIXEL_FACTOR;
    bottom *= FT_TO_PIXEL_FACTOR;
    right *= FT_TO_PIXEL_FACTOR;
    top *= FT_TO_PIXEL_FACTOR;
}

void TextPainterPrivate::updateText(TextObject* object)
{
    if (object->cursor) {
        validateTexture(CURSOR);
    }

    for (auto c : object->text) {
        validateTexture(c);
    }

    updateVao(object);
}

void TextPainterPrivate::render(const std::vector<std::vector<TextPainterObject>>& proxy)
{
    // changement text asynchrone
    std::tuple<std::size_t, std::string> buf;
    if (asyncTextsBuffer.try_dequeue(buf)) {
        auto& [index, text] { buf };
        auto& object { textObjects[index] };
        // si la taille max n'a pas été limitée, la première affectation
        // devient la taille max
        if (object.maxSize == 0) {
            object.maxSize = std::min(text.size(), STRING_MAX_SIZE);
        }
        // on convertit le texte au format freetype, en cropant si necessaire
        object.text = utf16conf.from_bytes(text.size() > object.maxSize ? text.substr(0, object.maxSize) : text);
        // on met à jour la bbox
        updateBoundingBox(&object);

        if (object.cursor) {
            validateTexture(CURSOR);
        }
        for (auto c : object.text) {
            validateTexture(c);
        }

        updateVao(&object);
    }

    std::tuple<std::size_t, Color> colorBuf;
    if (asyncColorBuffer.try_dequeue(colorBuf)) {
        const auto& [index, color] { colorBuf };
        auto& object { textObjects[index] };
        object.color = color;
    }

    glUseProgram(program);
    // le blending est systèmatique pour le rendu des polices
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glBlendEquation(GL_FUNC_ADD);

    std::size_t scissorId { 0 };
    std::size_t scroll { 0 };
    Color currentColor { 0.F, 0.F, 0.F, 0.F };

    for (const auto& vector : proxy) {
        for (auto index : vector) {
            const auto& text { textObjects[index.index] };

            if (text.clippingRectangleObject > 0) {
                glEnable(GL_SCISSOR_TEST);
                if (text.clippingRectangleObject != scissorId) {
                    scissorId = text.clippingRectangleObject;
                    auto [left, bottom, width, height] { clipObjects[scissorId] };
                    glScissor(left, bottom, width, height);
                }
            }

            if (text.scrollVectorObject > 0 && text.scrollVectorObject != scroll) {
                scroll = text.scrollVectorObject;
                auto [x, y] { scrollObject[scroll] };
                glUniform2f(scrollId, x, y);
            }

            if (const auto old { std::exchange(currentColor, text.color) }; old != currentColor) {
                const auto [r, g, b, a] { currentColor };
                glUniform4f(colorId, r, g, b, a);
            }

            if (text.vao == 0U || text.vbo == 0U) {
                continue;
            }

            glBindVertexArray(text.vao);
            uint offset { 0 };
            for (auto c : text.text) {
                if (characters[c].texture > 0) {
                    glBindTexture(GL_TEXTURE_2D, characters[c].texture);
                    glDrawElements(GL_TRIANGLES, QUAD_ELEMENTS_COUNT, GL_UNSIGNED_SHORT, offsetPtr<GLushort>(offset));
                }
                offset += QUAD_ELEMENTS_COUNT;
            }

            if (text.cursor) {
                const auto now { Clock::now() };
                const auto interval {
                    std::chrono::duration_cast<std::chrono::milliseconds>(now - lastTime)
                };
                if (interval.count() > BLINK_PERIODE) {
                    if (interval.count() > 2 * BLINK_PERIODE) {
                        lastTime = now;
                    }
                    glBindTexture(GL_TEXTURE_2D, characters[CURSOR].texture);
                    glDrawElements(GL_TRIANGLES, QUAD_ELEMENTS_COUNT, GL_UNSIGNED_SHORT, offsetPtr<GLushort>(offset));
                }
            }

            glBindTexture(GL_TEXTURE_2D, 0);
            glBindVertexArray(0);

            glDisable(GL_SCISSOR_TEST);
        }
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
    glDisable(GL_BLEND);
    glUseProgram(0);

    if (scroll != 0) {
        glUniform2f(scrollId, 0.F, 0.F);
    }

    if (!textObjectsToDestroy.empty()) {
        for (const auto& [key, value] : textObjectsToDestroy) {
            glDeleteVertexArrays(1, &value.vao);
            glDeleteBuffers(1, &value.vbo);
        }
        textObjectsToDestroy.clear();
    }
}

void TextPainterPrivate::destroy()
{
    for (const auto& [key, value] : characters) {
        glDeleteTextures(1, &value.texture);
    }

    for (const auto& [key, value] : textObjects) {
        glDeleteVertexArrays(1, &value.vao);
        glDeleteBuffers(1, &value.vbo);
    }

    glDeleteBuffers(1, &ebo);
    glDeleteBuffers(1, &cbo);
    glDeleteProgram(program);
}

TextPainter::TextPainter()
    : _impl(std::make_unique<TextPainterPrivate>())
{
}

TextPainter::~TextPainter() = default;

auto TextPainter::interline() const -> float
{
    return _impl->glyph.interline() * FT_TO_PIXEL_FACTOR;
}

auto TextPainter::createObject() -> TextPainterObject
{
    const auto uuid { _impl->createUUID() };
    _impl->textObjects.emplace(std::piecewise_construct, std::forward_as_tuple(uuid), std::forward_as_tuple());
    return { uuid };
}

void TextPainter::destroyObject(TextPainterObject object)
{
    auto node { (_impl->textObjects.extract(object.index)) };
    _impl->textObjectsToDestroy.insert(std::move(node));
}

auto TextPainter::createClipObject() -> TextClipObject
{
    const auto uuid { _impl->createUUID() };
    _impl->clipObjects.emplace(uuid, ClipRectangle { 0, 0, 0, 0 });
    return { uuid };
}

void TextPainter::destroyClipObject(TextClipObject object)
{
    _impl->clipObjects.erase(object.index);
}

auto TextPainter::createScrollObject() -> TextScrollObject
{
    const auto uuid { _impl->createUUID() };
    _impl->scrollObject.emplace(uuid, std::make_tuple(0.F, 0.F));
    return { uuid };
}

void TextPainter::destroyScrollObject(TextScrollObject object)
{
    _impl->scrollObject.erase(object.index);
}

void TextPainter::setScaleFactor(TextPainterObject object, float factor)
{
    _impl->textObjects[object.index].scale = factor;
}

void TextPainter::updateClipObjectRectangle(TextClipObject clipObject, const ClipRectangle& clip) noexcept
{
    _impl->clipObjects[clipObject.index] = clip;
}

void TextPainter::updateScrollObjectVector(TextScrollObject scrollObject, const Vector& vector) noexcept
{
    _impl->scrollObject[scrollObject.index] = vector;
}

void TextPainter::reserve(TextPainterObject object, std::size_t size) noexcept
{
    auto it { _impl->textObjects.find(object.index) };
    if (it != _impl->textObjects.cend()) {
        if (it->second.vao > 0) {
            std::clog << "TextPainter can't reserve, vbo allready allocated."
                      << std::endl;
        } else if (size > STRING_MAX_SIZE) {
            std::clog << "TextPainter can't reserve " << size << " reduce to "
                      << STRING_MAX_SIZE << std::endl;
            it->second.maxSize = STRING_MAX_SIZE;
        } else {
            it->second.maxSize = size;
        }
    }
}

void TextPainter::setText(TextPainterObject object, const std::string& text) noexcept
{
    auto& textObject { _impl->textObjects[object.index] };
    if (textObject.maxSize == 0) {
        textObject.maxSize = std::min(text.size(), STRING_MAX_SIZE);
    }
    // si la taille du texte est supérieur à la réserve, on crop le texte
    textObject.text = _impl->utf16conf.from_bytes(
        (text.size() > textObject.maxSize)
            ? text.substr(0, textObject.maxSize)
            : text);
    _impl->updateBoundingBox(&textObject);
    _impl->updateText(&textObject);
}

void TextPainter::setText(TextPainterObject object, std::string_view text) noexcept
{
    auto& textObject { _impl->textObjects[object.index] };
    if (textObject.maxSize == 0) {
        textObject.maxSize = std::min(text.size(), STRING_MAX_SIZE);
    }
    // si la taille du texte est supérieur à la réserve, on crop le texte
    textObject.text = _impl->utf16conf.from_bytes(
        (text.size() > textObject.maxSize)
            ? std::string { text.substr(0, textObject.maxSize) }
            : std::string { text });
    _impl->updateBoundingBox(&textObject);
    _impl->updateText(&textObject);
}

void TextPainter::updateText(TextPainterObject object, const std::string& text) noexcept
{
    _impl->asyncTextsBuffer.enqueue({ object.index, text });
}

void TextPainter::updateText(TextPainterObject object, std::string_view text) noexcept
{
    _impl->asyncTextsBuffer.enqueue({ object.index, std::string { text } });
}

auto TextPainter::boundingBox(TextPainterObject object) const -> Bbox
{
    return _impl->textObjects[object.index].boundingBox;
}

void TextPainter::setColor(TextPainterObject object, const Color& color)
{
    _impl->textObjects[object.index].color = color;
}

void TextPainter::updateColor(TextPainterObject object, Color color)
{
    _impl->asyncColorBuffer.enqueue({ object.index, color });
}

void TextPainter::setClippingRectangle(TextPainterObject object, TextClipObject clippingObject) noexcept
{
    auto it { _impl->textObjects.find(object.index) };
    if (it != _impl->textObjects.cend()) {
        it->second.clippingRectangleObject = clippingObject.index;
    }
}

void TextPainter::setScrollVector(TextPainterObject object, TextScrollObject scrollObject)
{
    auto it { _impl->textObjects.find(object.index) };
    if (it != _impl->textObjects.cend()) {
        it->second.scrollVectorObject = scrollObject.index;
    }
}

void TextPainter::enableCursor(TextPainterObject object, bool enable)
{
    auto it { _impl->textObjects.find(object.index) };
    if (it != _impl->textObjects.cend()) {
        it->second.cursor = enable;
    }
}

void TextPainter::setPosition(TextPainterObject object, const Position& position)
{
    auto& textObject { _impl->textObjects[object.index] };
    textObject.position = position;
    _impl->updateBoundingBox(&textObject);
    _impl->updateVao(&textObject);
}

auto TextPainter::position(TextPainterObject object) const noexcept -> Position
{
    const auto it { _impl->textObjects.find(object.index) };
    return it == _impl->textObjects.cend() ? Position {} : it->second.position;
}

void TextPainter::initializeGL(unsigned fontPixelSize, const std::string& fontFamily, bool invertCoordinates)
{
    _impl->initialize(fontPixelSize, fontFamily, invertCoordinates);
}

void TextPainter::resizeViewGL(float width, float height)
{
    glUseProgram(_impl->program);
    glUniformMatrix4fv(_impl->transformId, 1, GL_FALSE, MatOrtho { width, height, _impl->flip }.p());
    glUseProgram(0);
}

void TextPainter::renderGL(const TextPainterObjectProxy& proxy)
{
    _impl->render(proxy);
}

void TextPainter::renderGL(const TextPainterObjectVector& objects)
{
    _impl->render({ objects });
}

void TextPainter::renderGL(TextPainterObject object)
{
    _impl->render({ { object } });
}

void TextPainter::destroyGL()
{
    _impl->destroy();
}

} // namespace Jgv::OpenGLUtils
