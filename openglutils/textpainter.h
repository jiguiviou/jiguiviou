﻿/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef TEXTPAINTER_H
#define TEXTPAINTER_H

#include <memory>
#include <string>
#include <string_view>
#include <vector>

namespace Jgv::OpenGLUtils {

struct TextPainterObject {
    std::size_t index;
};

struct TextClipObject {
    std::size_t index;
};

struct TextScrollObject {
    std::size_t index;
};

using Position = std::tuple<float, float, float>;
using Vector = std::tuple<float, float>;
using ClipRectangle = std::tuple<int, int, int, int>; // left, bottom, width, height
using Bbox = std::tuple<float, float, float, float>;
using Color = std::tuple<float, float, float, float>;
using TextPainterObjectVector = std::vector<TextPainterObject>;
using TextPainterObjectProxy = std::vector<TextPainterObjectVector>;

struct TextPainterPrivate;
class TextPainter {
public:
    TextPainter();
    ~TextPainter();
    TextPainter(const TextPainter&) = delete;
    TextPainter(TextPainter&&) = delete;
    auto operator=(const TextPainter&) -> TextPainter& = delete;
    auto operator=(TextPainter &&) -> TextPainter& = delete;

    [[nodiscard]] auto interline() const -> float;

    auto createObject() -> TextPainterObject;
    void destroyObject(TextPainterObject object);

    auto createClipObject() -> TextClipObject;
    void destroyClipObject(TextClipObject object);

    auto createScrollObject() -> TextScrollObject;
    void destroyScrollObject(TextScrollObject object);

    void setScaleFactor(TextPainterObject object, float factor);
    void setClippingRectangle(TextPainterObject object, TextClipObject clippingObject) noexcept;
    void setScrollVector(TextPainterObject object, TextScrollObject scrollObject);

    void updateClipObjectRectangle(TextClipObject clipObject, const ClipRectangle& clip) noexcept;
    void updateScrollObjectVector(TextScrollObject scrollObject, const Vector& vector) noexcept;

    void reserve(TextPainterObject object, std::size_t size) noexcept;
    void setText(TextPainterObject object, const std::string& text) noexcept;
    void setText(TextPainterObject object, std::string_view text) noexcept;
    void updateText(TextPainterObject object, const std::string& text) noexcept;
    void updateText(TextPainterObject object, std::string_view text) noexcept;

    void setColor(TextPainterObject object, const Color& color);
    void updateColor(TextPainterObject object, Color color);

    void enableCursor(TextPainterObject object, bool enable);

    void setPosition(TextPainterObject object, const Position& position);
    [[nodiscard]] auto position(TextPainterObject object) const noexcept -> Position;
    [[nodiscard]] auto boundingBox(TextPainterObject object) const -> Bbox;

    void initializeGL(unsigned fontPixelSize, const std::string& fontFamily = "monospace", bool invertCoordinates = false);
    void resizeViewGL(float width, float height);
    void renderGL(const TextPainterObjectProxy& proxy);
    void renderGL(const TextPainterObjectVector& objects);
    void renderGL(TextPainterObject object);
    void destroyGL();

private:
    const std::unique_ptr<TextPainterPrivate> _impl;
};

} // namespace Jgv::OpenGLUtils

#endif // TEXTPAINTER_H
