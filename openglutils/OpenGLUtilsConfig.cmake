get_filename_component(SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(${SELF_DIR}/OpenGLUtils.cmake)
get_filename_component(openglutils_INCLUDE_DIRS "${SELF_DIR}/../../include/openglutils" ABSOLUTE)

message(STATUS "OpenGLUtilsConfig: include : ${openglutils_INCLUDE_DIRS}")
