/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "rectanglepainter.h"
#include "rectanglepainter_p.h"

#include "openglprogram.h"

#include <algorithm>
#include <tuple>
#include <utility>

namespace Jgv::OpenGLUtils {

void RectanglePainterPrivate::initialize()
{
    const std::string vSource =
#include "primitive.vert"
        ;
    const std::string fSource =
#include "primitive.frag"
        ;
    program = Program::create(vSource, fSource);

    QuadElements<GLubyte> elements;
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), &elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glUseProgram(program);
    transformId = glGetUniformLocation(program, "transform");
    scrollId = glGetUniformLocation(program, "scroll");
    colorId = glGetUniformLocation(program, "color");

    scrollVectors[0] = { 0.F, 0.F };
    glUniform2fv(scrollId, 1, scrollVectors[0].data());

    glUseProgram(0);
}

void RectanglePainterPrivate::updateVao(RectangleGLObject* object)
{
    if (object->vao == 0) {
        GLuint vbo { 0 };
        GLuint vao { 0 };

        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(Quad), nullptr, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), offsetPtr<GLfloat>(0));
        glEnableVertexAttribArray(0);
        glBindVertexArray(0);

        object->vao = vao;
        object->vbo = vbo;

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    const auto vertices { Quad { object->rectangle, object->depth } };
    glBindBuffer(GL_ARRAY_BUFFER, object->vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Quad), &vertices);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void RectanglePainterPrivate::render(const std::vector<std::vector<RectanglePainterObject>>& proxy)
{
    glUseProgram(program);

    std::size_t scrollIndex { 0U };
    std::size_t clipppingIndex { 0U };
    bool blending { false };

    bool scissorTest { false };

    auto color { std::make_tuple(0.F, 0.F, 0.F, 1.F) };

    for (const auto& vector : proxy) {
        for (auto index : vector) {
            const auto& object { rectangleObjects[index.index] };
            if (object.vao == 0) {
                continue;
            }

            // met à jour le vecteur de scrolling
            if (const auto old { std::exchange(scrollIndex, object.scrollVectorIndex) }; old != scrollIndex) {
                glUniform2fv(scrollId, 1, scrollVectors[scrollIndex].data());
            }

            // change le status du test scissor
            if (const auto old { std::exchange(scissorTest, object.clipping > 0U) }; scissorTest != old) {
                if (scissorTest) {
                    glEnable(GL_SCISSOR_TEST);
                } else {
                    glDisable(GL_SCISSOR_TEST);
                }
            }

            // si le test scissor est actif, on met à jour les datas
            if (scissorTest) {
                if (const auto old { std::exchange(clipppingIndex, object.clipping) }; clipppingIndex != old) {
                    const auto [left, bottom, width, height] { clippingObjects[clipppingIndex] };
                    glScissor(left, bottom, width, height);
                }
            }

            // on mets à jour la couleur
            if (const auto old { std::exchange(color, object.color) }; old != color) {
                const auto [r, g, b, a] { color };
                glUniform4f(colorId, r, g, b, a);
                // active le blending que si on a de l'alpha
                if (blending != std::exchange(blending, a < 1.F)) {
                    if (blending) {
                        glEnable(GL_BLEND);
                        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                        glBlendEquation(GL_FUNC_ADD);
                    } else {
                        glDisable(GL_BLEND);
                    }
                }
            }

            glBindVertexArray(object.vao);
            glDrawElements(GL_TRIANGLES, QUAD_ELEMENTS_COUNT, GL_UNSIGNED_BYTE, offsetPtr<GLubyte>(0));
        }
    }

    // reset le vecteur de scroll
    glUniform2f(scrollId, 0.F, 0.F);
    // reset la couleur
    glUniform4f(colorId, 0.F, 0.F, 0.F, 0.F);

    glBindVertexArray(0);
    glDisable(GL_SCISSOR_TEST);
    glDisable(GL_BLEND);
    glUseProgram(0);

    if (!objectsToDestroy.empty()) {
        for (const auto& [key, value] : objectsToDestroy) {
            glDeleteVertexArrays(1, &value.vao);
            glDeleteBuffers(1, &value.vbo);
        }
        objectsToDestroy.clear();
    }
}

void RectanglePainterPrivate::destroy()
{
    for (const auto& [key, value] : rectangleObjects) {
        glDeleteVertexArrays(1, &value.vao);
        glDeleteBuffers(1, &value.vbo);
    }
    glDeleteBuffers(1, &ebo);
    glDeleteProgram(program);
}

RectanglePainter::RectanglePainter()
    : _impl(std::make_unique<RectanglePainterPrivate>())
{
}

RectanglePainter::~RectanglePainter() = default;

auto RectanglePainter::createObject() -> RectanglePainterObject
{
    const auto uuid { _impl->createUUID() };
    _impl->rectangleObjects.emplace(uuid, RectangleGLObject());
    return { uuid };
}

void RectanglePainter::destroyObject(RectanglePainterObject object)
{
    auto node { (_impl->rectangleObjects.extract(object.index)) };
    _impl->objectsToDestroy.insert(std::move(node));
}

auto RectanglePainter::createClipObject() -> RectangleClippingObject
{
    const auto uuid { _impl->createUUID() };
    _impl->clippingObjects.emplace(uuid, ClippingRectangle {});
    return { uuid };
}

void RectanglePainter::destroyClipObject(RectangleClippingObject object)
{
    _impl->clippingObjects.erase(object.index);
}

auto RectanglePainter::createScrollObject() -> RectangleScrollObject
{
    const auto uuid { _impl->createUUID() };
    _impl->scrollVectors[uuid] = {};
    return { uuid };
}

void RectanglePainter::destroyScrollObject(RectangleScrollObject object)
{
    _impl->scrollVectors.erase(object.index);
}

void RectanglePainter::setColor(RectanglePainterObject object, Color color) noexcept
{
    _impl->rectangleObjects[object.index].color.swap(color);
}

void RectanglePainter::setRectangle(RectanglePainterObject object, const Rectangle& rectangle, float depth) noexcept
{
    auto& rectangleObject { _impl->rectangleObjects[object.index] };
    rectangleObject.rectangle = rectangle;
    rectangleObject.depth = depth;
    _impl->updateVao(&rectangleObject);
}

auto RectanglePainter::rectangle(RectanglePainterObject object) const -> Rectangle
{
    return _impl->rectangleObjects[object.index].rectangle;
}

void RectanglePainter::setClipObject(RectanglePainterObject object, RectangleClippingObject clippingObject) noexcept
{
    _impl->rectangleObjects[object.index].clipping = clippingObject.index;
}

void RectanglePainter::setScrollObject(RectanglePainterObject object, RectangleScrollObject scrollObject) noexcept
{
    _impl->rectangleObjects[object.index].scrollVectorIndex = scrollObject.index;
}

void RectanglePainter::updateClippingRectangle(RectangleClippingObject clippingObject, const ClippingRectangle& rectangle) noexcept
{
    _impl->clippingObjects[clippingObject.index] = rectangle;
}

void RectanglePainter::updateScrollVector(RectangleScrollObject scrollObject, float x, float y)
{
    _impl->scrollVectors[scrollObject.index] = { x, y };
}

void RectanglePainter::initializeGL()
{
    _impl->initialize();
}

void RectanglePainter::resizeViewGL(float width, float height)
{
    glUseProgram(_impl->program);
    glUniformMatrix4fv(_impl->transformId, 1, GL_FALSE, MatOrtho { width, height }.p());
    glUseProgram(0);
}

void RectanglePainter::renderGL(const RectanglePainterObjectsProxy& proxy)
{
    _impl->render(proxy);
}

void RectanglePainter::renderGL(const RectanglePainterObjectsVector& objects)
{
    _impl->render({ objects });
}

void RectanglePainter::renderGL(RectanglePainterObject object)
{
    _impl->render({ { object } });
}

void RectanglePainter::destroyGL()
{
    _impl->destroy();
}

} // namespace Jgv::OpenGLUtils
