/***************************************************************************
 *   Copyright (C) 2020 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef RECTANGLEPAINTER_H
#define RECTANGLEPAINTER_H

#include <memory>
#include <vector>

namespace Jgv::OpenGLUtils {

struct RectanglePainterObject {
    std::size_t index;
};

struct RectangleClippingObject {
    std::size_t index;
};

struct RectangleScrollObject {
    std::size_t index;
};

struct RectangleColorObject {
    std::size_t index;
};

using Color = std::tuple<float, float, float, float>;
using Rectangle = std::tuple<float, float, float, float>;
using RectanglePainterObjectsVector = std::vector<RectanglePainterObject>;
using RectanglePainterObjectsProxy = std::vector<RectanglePainterObjectsVector>;
using ClippingRectangle = std::tuple<int, int, int, int>;

class RectanglePainterPrivate;
class RectanglePainter {
public:
    RectanglePainter();
    ~RectanglePainter();
    RectanglePainter(const RectanglePainter&) = delete;
    RectanglePainter(RectanglePainter&&) = delete;
    auto operator=(const RectanglePainter&) -> RectanglePainter& = delete;
    auto operator=(RectanglePainter &&) -> RectanglePainter& = delete;

    auto createObject() -> RectanglePainterObject;
    void destroyObject(RectanglePainterObject object);

    auto createClipObject() -> RectangleClippingObject;
    void destroyClipObject(RectangleClippingObject object);

    auto createScrollObject() -> RectangleScrollObject;
    void destroyScrollObject(RectangleScrollObject object);

    void setColor(RectanglePainterObject object, Color color) noexcept;
    void setRectangle(RectanglePainterObject object, const Rectangle& rectangle, float depth) noexcept;
    [[nodiscard]] auto rectangle(RectanglePainterObject object) const -> Rectangle;

    void setClipObject(RectanglePainterObject object, RectangleClippingObject clippingObject) noexcept;
    void setScrollObject(RectanglePainterObject object, RectangleScrollObject scrollObject) noexcept;

    void updateClippingRectangle(RectangleClippingObject clippingObject, const ClippingRectangle& rectangle) noexcept;
    void updateScrollVector(RectangleScrollObject scrollObject, float x, float y);

    void initializeGL();
    void resizeViewGL(float width, float height);
    void renderGL(const RectanglePainterObjectsProxy& proxy);
    void renderGL(const RectanglePainterObjectsVector& objects);
    void renderGL(RectanglePainterObject object);
    void destroyGL();

private:
    const std::unique_ptr<RectanglePainterPrivate> _impl;
};

} // namespace Jgv::OpenGLUtils

#endif // RECTANGLEPAINTER_H
