/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "texturepainter.h"
#include "texturepainter_p.h"

#include "openglprogram.h"

#include <algorithm>
#include <array>
#include <iostream>
#include <png.h>
#include <turbojpeg.h>

#include <thread>

namespace {

const std::size_t SIG_SIZE { 8U };
constexpr std::array<uint8_t, 4> PNG_MGNM { 0x89, 0x50, 0x4E, 0x47 };
constexpr std::array<uint8_t, 2> JPG_MGNM { 0xFF, 0xD8 };

auto loadPng(std::FILE* file) -> Jgv::OpenGLUtils::TextureBuffer;
auto loadJpg(std::FILE* file) -> Jgv::OpenGLUtils::TextureBuffer;

auto loadFile(const std::string& filename) -> Jgv::OpenGLUtils::TextureBuffer
{

    auto closeFile = [](std::FILE* p) {
        if (p != nullptr) {
            std::fclose(p);
        }
    };

    std::unique_ptr<std::FILE, decltype(closeFile)> file { std::fopen(filename.c_str(), "rb"), closeFile };
    if (!file) {
        std::cerr << "TexturePainter: failed to load file " << filename << std::endl;
        std::perror("error: ");
        return {};
    }

    // détermine la taille du fichier
    std::fseek(file.get(), 0, SEEK_END);
    auto filesize { std::ftell(file.get()) };
    std::fseek(file.get(), 0, SEEK_SET);
    if (filesize < 4) {
        std::cerr << "TexturePainter: file to small " << filesize << std::endl;
        return {};
    }

    // on lit le magic number pour déterminer le type de fichier
    std::array<uint8_t, 4> buffer {};
    const auto s { std::fread(buffer.data(), sizeof(char), buffer.size(), file.get()) };
    if (s == buffer.size()) {
        if (buffer == PNG_MGNM) {
            std::fseek(file.get(), 0, SEEK_SET);
            return loadPng(file.get());
        }
        if (buffer[0] == JPG_MGNM[0] && buffer[1] == JPG_MGNM[1]) {
            std::fseek(file.get(), 0, SEEK_SET);
            return loadJpg(file.get());
        }
    }

    return {};
}

auto loadPng(std::FILE* file) -> Jgv::OpenGLUtils::TextureBuffer
{
    auto deleteReadPng = [](png_structp p) {
        if (p != nullptr) {
            png_destroy_read_struct(&p, nullptr, nullptr);
        }
    };

    std::array<u_char, SIG_SIZE> sig {};
    const auto read { std::fread(sig.data(), sizeof(u_char), sig.size(), file) };
    if (read != SIG_SIZE || !png_check_sig(sig.data(), sig.size())) {
        std::cerr << "TexturePainter: file is not png." << std::endl;
        return {};
    }

    std::unique_ptr<png_struct, decltype(deleteReadPng)> png { png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr), deleteReadPng };
    if (!png) {
        std::cerr << "TexturePainter: png_create_read_struct error." << std::endl;
        return {};
    }

    auto info { png_create_info_struct(png.get()) };
    if (info == nullptr) {
        std::cerr << "TexturePainter: png_create_info_struct error." << std::endl;
        return {};
    }

    if (setjmp(png_jmpbuf(png.get()))) {
        std::cerr << "TexturePainter: setjmp error." << std::endl;
        return {};
    }

    png_init_io(png.get(), file);
    png_set_sig_bytes(png.get(), sig.size());
    png_read_info(png.get(), info);

    Jgv::OpenGLUtils::TextureBuffer textureBuffer;
    textureBuffer.width = png_get_image_width(png.get(), info);
    textureBuffer.height = png_get_image_height(png.get(), info);
    textureBuffer.pixelsize = 4;

    png_set_interlace_handling(png.get());

    png_read_update_info(png.get(), info);

    if (setjmp(png_jmpbuf(png.get()))) {
        std::cerr << "TexturePainter: error during read image." << std::endl;
        return {};
    }

    auto row { png_get_rowbytes(png.get(), info) };
    textureBuffer.buffer.resize(textureBuffer.height * row, 0);
    std::vector<png_bytep> rowPointers;
    rowPointers.reserve(textureBuffer.height);
    for (uint y = 0; y < textureBuffer.height; ++y) {
        rowPointers.push_back(&textureBuffer.buffer[y * row]);
    }

    png_read_image(png.get(), rowPointers.data());
    png_read_end(png.get(), info);

    return textureBuffer;
}

auto loadJpg(std::FILE* file) -> Jgv::OpenGLUtils::TextureBuffer
{

    // détermine la taille du fichier
    std::fseek(file, 0, SEEK_END);
    auto filesize = static_cast<int>(std::ftell(file));
    std::fseek(file, 0, SEEK_SET);

    auto freeBuffer = [](unsigned char* buffer) {
        if (buffer != nullptr) {
            tjFree(buffer);
        }
    };

    std::unique_ptr<unsigned char, decltype(freeBuffer)> buffer { tjAlloc(filesize), freeBuffer };
    // transfert vers le buffer
    std::fread(buffer.get(), filesize, 1, file);

    auto destroyHandle = [](tjhandle handle) {
        if (handle != nullptr) {
            tjDestroy(handle);
        }
    };

    std::unique_ptr<void, decltype(destroyHandle)> tjInstance { tjInitDecompress(), destroyHandle };

    int width { 0 };
    int height { 0 };
    int inSubsamp { 0 };
    int inColorspace { 0 };
    if (tjDecompressHeader3(tjInstance.get(), buffer.get(), static_cast<unsigned>(filesize), &width, &height, &inSubsamp, &inColorspace) < 0) {
        std::perror("TexturePainter::loadJpg failed to decompress Header");
        return {};
    }

    if (inColorspace != TJCS_GRAY) {
        return {};
    }

    Jgv::OpenGLUtils::TextureBuffer textureBuffer;
    textureBuffer.buffer.resize(static_cast<std::size_t>(width * height /* * tjPixelSize[inColorspace]*/));
    textureBuffer.width = static_cast<uint>(width);
    textureBuffer.height = static_cast<uint>(height);
    textureBuffer.pixelsize = 1;

    if (tjDecompress2(tjInstance.get(), buffer.get(), filesize, textureBuffer.buffer.data(), width, 0, height, TJPF_GRAY, TJFLAG_NOREALLOC) < 0) {
        std::cout << "decompress failed" << std::endl;
        return {};
    }

    return textureBuffer;
}

auto textureFromImage(const Jgv::OpenGLUtils::TextureBuffer& image) -> GLuint
{
    if (image.buffer.empty()) {
        std::cerr << "TexturePainterPrivate::render image buffer empty." << std::endl;
        return 0;
    }
    GLuint texture { 0 };
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    if (image.pixelsize == 1) {
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_R8, image.width, image.height);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image.width, image.height, GL_RED, GL_UNSIGNED_BYTE, image.buffer.data());
    } else {
        glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, image.width, image.height);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, image.width, image.height, GL_RGBA, GL_UNSIGNED_BYTE, image.buffer.data());
    }

    return texture;
}

void updateScaleFactor(Jgv::OpenGLUtils::TextureGLObject* object)
{
    Jgv::OpenGLUtils::QuadTextureCoordinates textureCoord { object->centerScalefactor };
    glBindBuffer(GL_ARRAY_BUFFER, object->cbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof textureCoord, &textureCoord, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

} // namespace

namespace Jgv::OpenGLUtils {

void TexturePainterPrivate::initialize()
{
    const std::string vSource =
#include "default.vert"
        ;
    const std::string fSource =
#include "default.frag"
        ;
    program = Program::create(vSource, fSource);

    QuadElements<GLubyte> elements;
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(QuadElements<GLubyte>), &elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    glUseProgram(program);
    projectionId = glGetUniformLocation(program, "transform");
    scrollId = glGetUniformLocation(program, "scroll");
    pixelSizeId = glGetUniformLocation(program, "pixelSize");
    glUseProgram(0);
}

void TexturePainterPrivate::updateVao(TextureGLObject* object, Rectangle rectangle, float depth)
{

    if (object->vao == 0U) {
        GLuint vbo { 0U };
        GLuint cbo { 0U };
        GLuint vao { 0U };

        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(Quad), nullptr, GL_STATIC_DRAW);

        QuadTextureCoordinates textureCoord { object->centerScalefactor };
        glGenBuffers(1, &cbo);
        glBindBuffer(GL_ARRAY_BUFFER, cbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(QuadTextureCoordinates), &textureCoord, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), offsetPtr<GLfloat>(0));
        glBindBuffer(GL_ARRAY_BUFFER, cbo);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), offsetPtr<GLfloat>(0));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glBindVertexArray(0);

        object->vao = vao;
        object->cbo = cbo;
        object->vbo = vbo;

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    const auto vertices { Quad { rectangle, depth } };
    glBindBuffer(GL_ARRAY_BUFFER, object->vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Quad), &vertices);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void TexturePainterPrivate::render(const std::vector<std::vector<TexturePainterObject>>& proxy)
{
    // traite les mises à jour faites depuis un thread différent
    ExternalTextureTuple externalTexture {};
    if (externalTextureBuffer.try_dequeue(externalTexture)) {
        auto& object { textureObjects[externalTexture.index] };
        object.externalTexture = externalTexture.texture;
    }

    glUseProgram(program);

    GLuint pixelSize { 4 };
    glUniform1ui(pixelSizeId, pixelSize);

    auto scroll { std::make_tuple(0.F, 0.F) };
    glUniform2f(scrollId, 0.F, 0.F);

    bool blending { false };

    bool scissorTest { false };
    auto scissor { std::make_tuple(0, 0, 0, 0) };

    for (const auto& vector : proxy) {
        for (auto index : vector) {
            const auto& object { textureObjects[index.index] };

            // met à jour le vecteur de scrolling
            auto oldScroll { std::exchange(scroll, scrollVectors[object.scrollVector]) };
            if (scroll != oldScroll) {
                auto [x, y] { scroll };
                glUniform2f(scrollId, x, y);
            }

            if (object.externalTexture > 0U) {
                // fixe le shader selon RGBA ou Mono
                auto oldPixelSize { std::exchange(pixelSize, 4) };
                if (oldPixelSize != pixelSize) {
                    glUniform1ui(pixelSizeId, pixelSize);
                }
                glBindTexture(GL_TEXTURE_2D, object.externalTexture);

            } else if (object.texture > 0U) {
                const auto& texture { textures[object.texture] };
                auto oldPixelSize { std::exchange(pixelSize, texture.pixelSize) };
                if (oldPixelSize != pixelSize) {
                    glUniform1ui(pixelSizeId, pixelSize);
                }

                glBindTexture(GL_TEXTURE_2D, texture.id);

            } else {
                continue;
            }

            // si aucune taille n'a été spécifiée, le vao est nul
            if (object.vao == 0U || object.vbo == 0U) {
                continue;
            }

            // on active le blending pour les textures RGBA
            auto oldBlending { std::exchange(blending, pixelSize == 4) };
            if (blending != oldBlending) {
                if (blending) {
                    glEnable(GL_BLEND);
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                    glBlendEquation(GL_FUNC_ADD);
                } else {
                    glDisable(GL_BLEND);
                }
            }

            // active ou désactive le scissor
            auto oldScissorTest { std::exchange(scissorTest, object.clipping > 0U) };
            if (scissorTest != oldScissorTest) {
                if (scissorTest) {
                    glEnable(GL_SCISSOR_TEST);
                } else {
                    glDisable(GL_SCISSOR_TEST);
                }
            }

            // si le test scissor est actif, on met à jour les datas
            if (scissorTest) {
                auto oldScissor { std::exchange(scissor, clippingObjects[object.clipping]) };
                if (scissor != oldScissor) {
                    auto [left, bottom, width, height] { scissor };
                    glScissor(left, bottom, width, height);
                }
            }

            glBindVertexArray(object.vao);
            glDrawElements(GL_TRIANGLES, QUAD_ELEMENTS_COUNT, GL_UNSIGNED_BYTE, offsetPtr<GLubyte>(0));
        }
    }

    glBindVertexArray(0);
    glDisable(GL_SCISSOR_TEST);
    glDisable(GL_BLEND);
    glUseProgram(0);

    if (!textureObjectsToDestroy.empty()) {
        for (const auto& [key, value] : textureObjectsToDestroy) {
            glDeleteVertexArrays(1, &value.vao);
            glDeleteBuffers(1, &value.vbo);
            glDeleteBuffers(1, &value.cbo);
        }
        textureObjectsToDestroy.clear();
    }

    if (!texturesToDestroy.empty()) {
        for (const auto& [key, value] : texturesToDestroy) {
            glDeleteTextures(1, &value.id);
        }
        texturesToDestroy.clear();
    }
}

void TexturePainterPrivate::destroy()
{
    for (const auto& [key, value] : textureObjects) {
        glDeleteVertexArrays(1, &value.vao);
        glDeleteBuffers(1, &value.vbo);
        glDeleteBuffers(1, &value.cbo);
    }

    for (const auto& [key, value] : textures) {
        glDeleteTextures(1, &value.id);
    }

    glDeleteBuffers(1, &ebo);
    glDeleteProgram(program);
}

TexturePainter::TexturePainter()
    : _impl(std::make_unique<TexturePainterPrivate>())
{
}

TexturePainter::~TexturePainter() = default;

auto TexturePainter::createObject() -> TexturePainterObject
{
    const auto uuid { _impl->createUUID() };
    _impl->textureObjects.emplace(uuid, TextureGLObject {});
    return { uuid };
}

void TexturePainter::destroyObject(TexturePainterObject object)
{
    auto node { (_impl->textureObjects.extract(object.index)) };
    _impl->textureObjectsToDestroy.insert(std::move(node));
}

auto TexturePainter::createTextureObject(const std::string& filename) -> TextureObject
{
    // on cherche si la texture n'existe pas déjà
    auto it = std::find_if(_impl->textures.cbegin(), _impl->textures.cend(), [&filename](const auto& value) {
        return value.second.filename == filename;
    });
    if (it == _impl->textures.cend()) {
        // la texture n'existe pas, on crée l'entrée
        const auto uuid { _impl->createUUID() };
        const auto buf { loadFile(filename) };
        _impl->textures.emplace(uuid, Texture { filename, textureFromImage(buf), buf.pixelsize, buf.width, buf.height });

        return { uuid };
    }

    return { it->first };
}

void TexturePainter::destroyTextureObject(TextureObject texture)
{
    // les textures sont partagées, on s'assure qu'aucun objet ne l'utilise
    for (const auto& [key, value] : _impl->textureObjects) {
        if (value.texture == texture.index) {
            return;
        }
    }

    auto node { (_impl->textures.extract(texture.index)) };
    _impl->texturesToDestroy.insert(std::move(node));
}

auto TexturePainter::createClipObject() -> TextureClipObject
{
    const auto uuid { _impl->createUUID() };
    _impl->clippingObjects.emplace(uuid, ClippingRectangle {});
    return { uuid };
}

void TexturePainter::destroyClipObject(TextureClipObject object)
{
    _impl->clippingObjects.erase(object.index);
}

auto TexturePainter::createScrollObject() noexcept -> TextureScrollObject
{
    const auto uuid { _impl->createUUID() };
    _impl->scrollVectors.emplace(uuid, std::make_tuple(0.F, 0.F));
    return { uuid };
}

void TexturePainter::destroyScrollObject(TextureScrollObject vector)
{
    _impl->scrollVectors.erase(vector.index);
}

void TexturePainter::setTexture(TexturePainterObject object, TextureObject texture)
{
    _impl->textureObjects[object.index].texture = texture.index;
}

void TexturePainter::setExternalTexture(TexturePainterObject object, GLuint id)
{
    _impl->textureObjects[object.index].externalTexture = id;
}

void TexturePainter::updateExternalTexture(TexturePainterObject object, GLuint id)
{
    _impl->externalTextureBuffer.enqueue({ object.index, id });
}

void TexturePainter::setRectangle(TexturePainterObject object, const Rectangle& rectangle, float depth) noexcept
{
    auto& texturePainterObject { _impl->textureObjects[object.index] };
    _impl->updateVao(&texturePainterObject, rectangle, depth);
}

void TexturePainter::setCenterScaleFactor(TexturePainterObject object, float factor)
{
    auto& texturePainterObject { _impl->textureObjects[object.index] };
    texturePainterObject.centerScalefactor = factor;
    updateScaleFactor(&texturePainterObject);
}

auto TexturePainter::textureSize(TextureObject texture) -> TextureSize
{
    const auto& object { _impl->textures[texture.index] };
    return std::make_tuple(object.width, object.height);
}

void TexturePainter::setClipObject(TexturePainterObject object, TextureClipObject clippingObject) noexcept
{
    _impl->textureObjects[object.index].clipping = clippingObject.index;
}

void TexturePainter::setScrollObject(TexturePainterObject object, TextureScrollObject scrollvector) noexcept
{
    _impl->textureObjects[object.index].scrollVector = scrollvector.index;
}

void TexturePainter::setClippingRectangle(TextureClipObject clippingObject, const ClippingRectangle& rectangle) noexcept
{
    _impl->clippingObjects[clippingObject.index] = rectangle;
}

void TexturePainter::setScrollVector(TextureScrollObject scrollVectorObject, float x, float y)
{
    _impl->scrollVectors[scrollVectorObject.index] = std::make_tuple(x, y);
}

void TexturePainter::initializeGL()
{
    _impl->initialize();
}

void TexturePainter::resizeViewGL(float width, float height)
{
    //    auto projection = glm::ortho ( 0.F, width, 0.F, height, 0.F, 100.F );
    //    std::cout << "_____________________________" << std::endl;
    //    std::cout << width << " " << height << std::endl;
    //    std::cout << projection[0][0] << " " << projection[0][1] << " " << projection[0][2] << " " << projection[0][3] << std::endl;
    //    std::cout << projection[1][0] << " " << projection[1][1] << " " << projection[1][2] << " " << projection[1][3] << std::endl;
    //    std::cout << projection[2][0] << " " << projection[2][1] << " " << projection[2][2] << " " << projection[2][3] << std::endl;
    //    std::cout << projection[3][0] << " " << projection[3][1] << " " << projection[3][2] << " " << projection[3][3] << std::endl;

    //    auto view { glm::mat4 { 1.F } };
    //view =
    //    view = glm::translate ( view, glm::vec3 ( -1200/2.F, -height/2.F, 0.F ) );
    //    view = glm::rotate ( view, glm::radians( 60.F ), glm::vec3( 0.F,0.5F, 0.F ) );
    //    view = glm::translate ( view, glm::vec3 ( 1200/2.F, height/2.F, 0.F ) );
    //    view = glm::translate ( view, glm::vec3 (0.F, 0.F, -1.F ) );

    glUseProgram(_impl->program);
    glUniformMatrix4fv(_impl->projectionId, 1, GL_FALSE, MatOrtho { width, height }.p());
    glUseProgram(0);
}

void TexturePainter::renderGL(const TexturePainterObjectsProxy& proxy)
{
    _impl->render(proxy);
}

void TexturePainter::renderGL(const TexturePainterObjectsVector& objects)
{
    _impl->render({ objects });
}

void TexturePainter::renderGL(TexturePainterObject object)
{
    _impl->render({ { object } });
}

void TexturePainter::destroyGL()
{
    _impl->destroy();
}

} // namespace Jgv
