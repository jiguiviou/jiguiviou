/***************************************************************************
 *   Copyright (C) 2019 by Cyril BALETAUD                                  *
 *   cyril.baletaud@gmail.com                                              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef GLYPHS_H
#define GLYPHS_H

#include <ft2build.h>
#include FT_FREETYPE_H

#include <memory>
#include <vector>

namespace Jgv::OpenGLUtils {

class GlyphPrivate;
class Glyph {
    FT_Library _library { nullptr };
    FT_Face _face { nullptr };

public:
    Glyph() = default;
    ~Glyph();

    Glyph(const Glyph&) = delete;
    Glyph(Glyph&&) = delete;
    Glyph& operator=(const Glyph&) = delete;
    Glyph& operator=(Glyph&&) = delete;

    void initialize(unsigned pixelSize, const std::string& family);
    [[nodiscard]] bool isMonospace() const;

    void setChar(char16_t charac);

    [[nodiscard]] float advance() const;
    [[nodiscard]] uint width() const;
    [[nodiscard]] uint height() const;
    [[nodiscard]] float bearingX() const;
    [[nodiscard]] float bearingY() const;

    [[nodiscard]] float ascender() const;
    [[nodiscard]] float descender() const;
    [[nodiscard]] float fontAdvance() const;
    [[nodiscard]] float interline() const;
    [[nodiscard]] const uint8_t* bitmap() const;
};

} // namespace Jgv::OpenGLUtils

#endif // GLYPHS_H
