R"(
#version 420 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoord;

uniform mat4 transform;
uniform vec2 scroll;
out vec2 vTexCoord;

void main()
{
    vTexCoord = texCoord;
    gl_Position = transform * vec4(position + vec3(scroll, 0.), 1.);
}
)"
